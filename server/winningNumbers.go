package server

import (
	"net/http"
	"net/url"
	"olg-services/config"
	"olg-services/lotteries/winningNumbers"
	"olg-services/publicis/utils/log"
	//	"strconv"
	"strings"
	"time"

	"github.com/golang/snappy"
	"github.com/julienschmidt/httprouter"
)

func WinningNumbers(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	//	startTime := time.Now()
	//	if config.GlobalConfig.Environment.IsDummyMode {
	//		defer log.TimeTrack(startTime, "server -> router: WinningNumbers")
	//	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	var jsonifiedFeed []byte

	// try to first get the winning numbers feed from cache
	cachedFeed, found := Cache.Get("winningNumbers")
	if found && cachedFeed != nil {

		jsonifiedFeed = cachedFeed.([]byte)

		if config.GlobalConfig.Environment.IsDummyMode {
			jsonifiedFeed = []byte(strings.Replace(string(jsonifiedFeed), "{", "{\"Cached\":\"Y\",\n", 1))
		}

	} else {

		correct := true

		feed := winningNumbers.NewWinningNumbers("Winning Numbers Regular Feed", false, false)

		qs, qsErr := url.ParseQuery(r.URL.Query().Encode())

		if qsErr != nil {
			log.Error("Winning Numbers Parse Query String Error", qsErr)
		}

		mockFeed := qs.Get("mockFeed")
		timeOffset := qs.Get("timeOffset")

		err := feed.LoadFeed(mockFeed, timeOffset)
		if err != nil {
			correct = false
		}

		// because this is a legacy OLG format, we need to embed the feed into the common JSON wrapper,
		// to make sure we adhere to the internal JSON response format
		newFormatFeedWrapper := winningNumbers.EmbedIntoNewFeedStruct(feed)

		jsonifiedFeed, err = ToJson(newFormatFeedWrapper)

		// put the numbers into the cache only if the feed was correctly ingested
		if correct {
			Cache.Add("winningNumbers", jsonifiedFeed, 2000*time.Millisecond)
		}

	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	//	if config.GlobalConfig.Environment.IsDummyMode {
	//		elapsed := time.Since(startTime)

	//		// convert the elapsed time to miliseconds
	//		milisecs := elapsed.Nanoseconds() / 1e6
	//		jsonifiedFeed = []byte(strings.Replace(string(jsonifiedFeed), "{", "{\"DebugTimeElapsedInternally\":\""+strconv.FormatInt(milisecs, 10)+" ms\",\n", 1))
	//	}

	// if snappy compression is enabled, then compress the feed
	if parseErr := r.ParseForm(); parseErr != nil {
		log.Error("WinningNumbers - request.ParseForm()", parseErr)
	} else {

		// if the compress request parameter is set to "y" then apply snappy compression to the feed
		// this should not be done on payloads smaller than 50 Kilobytes
		snappyCompressionEnabled := (r.Form.Get("compress") == "y" || r.Form.Get("compress") == "Y")
		if snappyCompressionEnabled {
			jsonifiedFeed = snappy.Encode(nil, jsonifiedFeed)
		}
	}

	_, err := w.Write(jsonifiedFeed)
	if err != nil {
		log.Error("server.WinningNumbers route - w.Write", err)
		return
	}

}
