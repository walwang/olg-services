package server

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/proline/base"
	"olg-services/proline/eventsResults"
	"olg-services/proline/sports"
	"olg-services/publicis/utils/log"
	"time"
)

func ProlineEvents(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ProlineEventsResults(w, r, params, false)
}

func ProlineResults(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ProlineEventsResults(w, r, params, true)
}

func ProlineEventsResults(w http.ResponseWriter, r *http.Request, params httprouter.Params, isResults bool) {

	logFlag := "Proline Events"

	// the cache flag is to avoid overwriting proline events cache with proline results cache
	// it will act as prefix to cache keys (e.g. "Events_HKY" or "Results_SCR")
	cacheFlag := "Events_"

	if isResults {
		logFlag = "Proline Results"
		cacheFlag = "Results_"
	}

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: "+logFlag)
	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	sportType, ok := sports.SportTypes[params.ByName("sportCode")]

	// in dummy mode, allow the "timeout" value as a special case, to simulate server timeouts
	if config.GlobalConfig.Environment.IsDummyMode && params.ByName("sportCode") == "timeout" {
		// change the sportType code to "timeout" so the appropriate fake feed is initialized
		sportType.Code = "timeout"

	} else {

		// verify the validity of the sport type param
		if ok == false {

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			err := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, "The specified sportType parameter is not a recognized value")
			if err != nil {
				log.Error("server."+logFlag+" route - WriteJSONError", err)
			}
			return
		}

	}

	var cacheKey string = cacheFlag + sportType.Code

	feed, err := events.NewProlineEventsResultsFeedWithCache(ProlineEventsCache, sportType, sportType.Name+" feed", false, false, nil, isResults)
	if err != nil {
		log.Error("server."+logFlag+" route - NewProlineEvents", err)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, err.Error())
		if jsonErr != nil {
			log.Error("server."+logFlag+" route - WriteJSONError", jsonErr)
		}

		return
	}

	// load the request parameters into the feed
	reqErr := base.LoadRequestParams(feed, r)
	if reqErr != nil {

		log.Error("server."+logFlag+" route - feed.LoadRequestParams", reqErr)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
		if wjErr != nil {
			log.Error("server."+logFlag+" route - WriteJSONError", wjErr)
		}
		return

	}

	var loadFeedUncached bool = false

	// try to identify whether the ProlineEventsCache holds the cache for the past year
	// for this sport type
	cachedFeed, found := ProlineEventsCache.Get(cacheKey)
	if found == false {

		loadFeedUncached = true

		// activate the go routine to load the entire date range into cache
		go loadProlineEventsIntoCache(sportType, isResults)

	} else {

		log.Debug("server." + logFlag + " route: found caching for sport type: " + sportType.Code + " - trying to find if it implements IProlineEventsResults")

		// feed found in cache, retrieve only the records in the date range
		cachedProlineEventsFeed, ok := cachedFeed.(interfaces.IProlineEventsResults)
		if ok {
			log.Debug("server." + logFlag + " route: found caching object for sport type: " + sportType.Code + " - that implements IProlineEventsResults")

			curatedFeed, err := events.GetCachedFeedWithDateRange(cachedProlineEventsFeed, sportType, feed.GetRequestParam("startDate"), feed.GetRequestParam("endDate"))

			if err != nil {

				log.Error("server."+logFlag+" route - GetCachedFeedWithDateRange", err)

				// in case of errors make sure the regular feed will be loaded via OLG call
				loadFeedUncached = true

			} else {

				// if no interface conversion errors, assign the feed to nil for garbage collection's sake
				// then point it to the curated feed that only contains the values within the date range
				feed = nil
				feed = curatedFeed

				// we need to attach the request parameters again for the new feed
				feed.SetRequestParams(r)

				// re-sort the cached feed accordingly if the request params contain a valid sortBy
				feed.SortBy(feed.GetRequestParam("sortBy"))

			}

		} else {

			log.Error("server."+logFlag+" route", log.NewErrorLocal("Interface type assertion error", "The object in cache does not implement IProlineEventsResults"))
			loadFeedUncached = false
		}

	}

	if loadFeedUncached {

		// proceed with the uncached version
		if isResults {
			feed.SetRequestParam("results", "true")
		}

		err = feed.LoadFeed()
		if err != nil {
			log.Error("server."+logFlag+" route - feed.LoadFeed()", err)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			wjErr := WriteJSONError(w, feed.GetStatusCode(), feed.GetErrorMessage())
			if wjErr != nil {
				log.Error("server."+logFlag+" route - WriteJSONError (feed.LoadFeed uncached)", wjErr)
			}
			return
		}

		// check for the sortBy flag before loading the feed
		if sortBy := feed.GetRequestParam("sortBy"); sortBy != "" {
			feed.SortBy(feed.GetRequestParam("sortBy"))
		}

	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	elapsed := time.Since(startTime)

	snappyCompressionEnabled := (feed.GetRequestParam("compress") == "y" || feed.GetRequestParam("compress") == "Y")

	err = JSONPrint(w, feed, snappyCompressionEnabled, &elapsed)
	if err != nil {
		log.Error("server."+logFlag+" route - JSONPrint", err)
		return
	}
}

func loadProlineEventsIntoCache(sportType sports.SportType, isResults bool) {

	logFlag := "Proline Events"

	// the cache flag is to avoid overwriting proline events cache with proline results cache
	// it will act as prefix to cache keys (e.g. "Events_HKY" or "Results_SCR")
	cacheFlag := "Events_"

	if isResults {
		logFlag = "Proline Results"
		cacheFlag = "Results_"
	}

	var statusKey string = cacheFlag + sportType.Code + "_CACHE_STATUS"
	var cacheKey string = cacheFlag + sportType.Code

	cachedFeedStatus, found := ProlineEventsCache.Get(statusKey)
	if found {
		if cachedFeedStatus.(string) == "LOADING" {
			// another go routine already in the process of loading
			log.Debug("loadProlineEventsIntoCache: Started caching for sport type: " + sportType.Code +
				" \n------------ Exiting (another cache loading routing already running)")
			return
		}
	} else {
		log.Debug("loadProlineEventsIntoCache: Started caching for sport type: " + sportType.Code +
			" \n------------ Continuing (No other caching process for this sport)")
	}

	// We are going to set the "LOADING" flag inside the statuc cache key,
	// with auto-expiry in 60 seconds. This means, that if anything happens during the cache
	// loading process, the LOADING flag will only gets stuck as true for a max of 60 seconds
	ProlineEventsCache.Set(statusKey, "LOADING", 60*time.Second)

	// make sure we remove the status cache
	defer ProlineEventsCache.Delete(statusKey)

	feedToBeCached, err := events.NewProlineEventsResultsFeed(sportType, sportType.Name+" feed", false, false, isResults)

	if err != nil {
		log.Error("loadProlineEventsIntoCache go routine called from server."+logFlag+" route - NewProlineEvents", err)
		return
	}

	// notify the feed that it's in cache storing mode
	feedToBeCached.SetIsCached(true)

	// specify the sport type
	feedToBeCached.SetRequestParam("type", "ProLine")
	feedToBeCached.SetRequestParam("sportId", sportType.OlgSportId)

	if isResults {
		feedToBeCached.SetRequestParam("results", "true")
	}

	// calculate the start date parameter to today plus 30 days
	// calculate the end date to today minus 30 days
	nowDate := time.Now()
	calculatedEndDate := nowDate.AddDate(0, 0, 30)
	calculatedStartDate := nowDate.AddDate(0, 0, -30)
	feedToBeCached.SetRequestParam("startDate", calculatedStartDate.Format(events.EVENT_DATE_PARSE_FORMAT)+"T00:00:00")
	feedToBeCached.SetRequestParam("endDate", calculatedEndDate.Format(events.EVENT_DATE_PARSE_FORMAT)+"T23:59:59")

	// proceed with loading the feed
	err = feedToBeCached.LoadFeed()
	if err != nil {
		log.Error("loadProlineEventsIntoCache go routine called from server."+logFlag+" route - feedToBeCached.LoadFeed()", err)
		return
	}

	// store the feed into cache
	ProlineEventsCache.Set(cacheKey, feedToBeCached, 0)

	log.Debug("loadProlineEventsIntoCache: Finished caching " + logFlag + " for sport type: " + sportType.Code)

}
