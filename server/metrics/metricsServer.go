package metrics

import (
	"net"
	"net/http"
	"net/rpc"
	"utils/sysinfo"
	"olg-services/publicis/utils/log"
)

type MainStatus struct {
	Server         sysinfo.ServerStatus
	CurrentProcess sysinfo.ProcessStatus
}

// for now, the Args struct is empty
type Args struct {
}

// Initializes the RPC metrics service
func InitMetricsService(rpcServerAddress string) error {

	localServerStatus := new(MainStatus)
	err := rpc.Register(localServerStatus)
	if err != nil {
		return log.NewError("Server Metrics - InitMetricsService() rpc.Register error", err)
	}

	rpc.HandleHTTP()

	l, err := net.Listen("tcp", rpcServerAddress)
	if err != nil {
		return log.NewError("Server Metrics - InitMetricsService() net.Listen error", err)
	}

	go http.Serve(l, nil)

	return nil

}

// Gather the local metrics
func (m *MainStatus) GetLocalMetrics(args Args, status *MainStatus) error {

	// gather system stats
	serverStatus, err := sysinfo.GetLocalServerStatus()
	if err != nil {
		log.Error("metrics.GetLocalMetrics - sysinfo.GetLocalServerStatus()", err)
		return err
	}

	// gather local process stats
	currentProcessStatus, err := sysinfo.GetCurrentProcessStatus()
	if err != nil {
		log.Error("webconsole.MainStatus route - sysinfo.GetCurrentProcessStatus()", err)
		return err
	}

	status = &MainStatus{
		Server:         serverStatus,
		CurrentProcess: currentProcessStatus,
	}

	return nil

}
