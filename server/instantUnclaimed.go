package server

import (
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/lotteries/instantUnclaimed"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
)

func InstantUnclaimedPrizes(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: InstantUnclaimedPrizes")
	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	var jsonifiedFeed []byte

	// try to first get the unclaimed instant prizes feed from cache
	cachedFeed, found := UnclaimedInstantPrizesCache.Get("instantUnclaimedPrizes")
	if found && cachedFeed != nil {

		jsonifiedFeed = cachedFeed.([]byte)

		if config.GlobalConfig.Environment.IsDummyMode {
			jsonifiedFeed = []byte(strings.Replace(string(jsonifiedFeed), "{", "{\"Cached\":\"Y\",\n", 1))
		}

	} else {

		feed := instantUnclaimed.NewInstantUnclaimedNumbers("Instant Unclaimed Prizes Regular Feed", false, false)

		// load the request parameters into the feed
		reqErr := base.LoadRequestParams(feed, r)
		if reqErr != nil {

			log.Error("server.instantUnclaimed route - feed.LoadRequestParams", reqErr)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
			if wjErr != nil {
				log.Error("server.instantUnclaimed route - WriteJSONError", wjErr)
			}
			return

		}

		err := feed.LoadFeed()
		if err != nil {

			log.Error("server.InstantUnclaimedPrizes route - LoadFeed()", err)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_FEED_XML_DECODE_ERROR, "Load Feed Error: "+err.Error())
			if jsonErr != nil {
				log.Error("server.InstantUnclaimedPrizes route - WriteJSONError", jsonErr)
			}
			return

		}

		jsonifiedFeed, err = ToJson(feed)
		if err != nil {

			log.Error("server.InstantUnclaimedPrizes route - ToJson()", err)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_SYSTEM_ERROR, "ToJson(): "+err.Error())
			if jsonErr != nil {
				log.Error("server.InstantUnclaimedPrizes route - WriteJSONError", jsonErr)
			}
			return

		}

		// no errors, add the feed to the cache
		UnclaimedInstantPrizesCache.Add("instantUnclaimedPrizes", jsonifiedFeed, 30*time.Minute)

	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	if config.GlobalConfig.Environment.IsDummyMode {
		elapsed := time.Since(startTime)
		milisecs := elapsed.Nanoseconds() / 1e6
		jsonifiedFeed = []byte(strings.Replace(string(jsonifiedFeed), "{", "{\"DebugTimeElapsedInternally\":\""+strconv.FormatInt(milisecs, 10)+" ms\",\n", 1))
	}

	_, err := w.Write(jsonifiedFeed)
	if err != nil {
		log.Error("server.InstantUnclaimedPrizes route - w.Write", err)
		return
	}

}
