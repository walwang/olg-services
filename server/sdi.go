package server

import (
	"fmt"
	"net/http"
	"olg-services/constants"
	"olg-services/proline/sdi"

	"github.com/julienschmidt/httprouter"
	//"olg-services/publicis/utils/log"
	//"strconv"
	"strings"
)

//my main function - call ( http://localhost:81/service/peter/atom/fff)
func SdiDataFeedEvent(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	//sdi atom feed url
	//var url string = "http://xml.sportsdirectinc.com/Atom?newerThan=2015-11-12T14:31:41.1384-3:00&apiKey=741E333E-7546-45A1-8A03-1FA91BFFF499"
	var url = sdi.GetSdiAtomUrlFromConfig()
	//fmt.Println("url = " + url)

	var xmlAtomFeed sdi.XMLAtomFeed = sdi.GetSdiAtomXml(url)

	//now we've got our full sports object from SDI
	//time to put it into cache:
	sdi.AddToCache(xmlAtomFeed)

	//read and convert Olg-Sdi data maping (sports/leages/teams) to our structure
	var xmlOlgSidMap sdi.XMLOlgSidMap
	sdi.ReadMappingFromXmlConfig(&xmlOlgSidMap)

	//olg-sdi sports mapping (from config) => cache
	//var sports map[string]string = ReadMapFromConfig("SPORT_OLG_SDI_MAP_")
	var sports map[string]string = sdi.ReadMapFromConfig2(xmlOlgSidMap.Sports.Maps)
	sdi.AddToCacheMapping(sdi.SDI_CACHE_KEY_SPORTS_MAP, sports)

	//olg-sdi leages mapping (from config) => cache
	//var leages map[string]string = ReadMapFromConfig("LEAGE_OLG_SDI_MAP_")
	var leages map[string]string = sdi.ReadMapFromConfig2(xmlOlgSidMap.Leages.Maps)
	sdi.AddToCacheMapping(sdi.SDI_CACHE_KEY_LEAGES_MAP, leages)

	//olg-sdi teams mapping (from config) => cache
	//var teams map[string]string = ReadMapFromConfig("TEAM_OLG_SDI_MAP_")
	var teams map[string]string = sdi.ReadMapFromConfig2(xmlOlgSidMap.Teams.Maps)
	sdi.AddToCacheMapping(sdi.SDI_CACHE_KEY_TEAMS_MAP, teams)

	//we keep all SDI info in this dome object
	var domeSports []sdi.SDISport = sdi.GetSdiSportEventsDocument(w, xmlAtomFeed)

	//put SDI object into cache
	sdi.AddToCacheSdiData(domeSports)

	var cnt = 0
	for _, value := range domeSports {
		cnt = cnt + len(value.Leages)
		sdi.DebugFormatToSDILogfile("leages in : %s %d \n", value.Name, len(value.Leages))
	}
	sdi.DebugFormatToSDILogfile("leages TOTAL  %d \n", cnt)
	//print leages
	for i3, value3 := range domeSports {
		fmt.Printf("i3:\t %d %s \n", i3+1, value3.Name)
		for i4, value4 := range value3.Leages {
			sdi.DebugFormatToSDILogfile("i4:\t %d  %s \n", i4+1, value4.Name)
		}
	}
	//add: olg-sdi event time difference tolerance threshold (in minutes)
	sdi.AddToCacheEventThreshold("sdi.EventTimeThreshold")
}

//localhost:81/service/peter/find/CFB@CFB@TEXAS ST.@LOUISIANA MONROE@2015-11-19 21:30:00.0
func FindSdiSportEvent(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var eventOutput sdi.XMLFindSdiEventOutput
	//var timeBefore int64 = sdi.GetTimeString()
	//fmt.Printf("TIME BEFORE: \t %s \n", getTimeString())
	//	fmt.Printf("params httprouter.Params length \t %d \n", len(params))
	//	fmt.Printf("params httprouter.Params \t %s \n", params[0].Value)
	var input = params[0].Value
	var keyValue []string = strings.Split(input, "@")
	//	var sport = "CFB"                       //string(params[0].Value)
	//	var leage = "CFB"                       // string(params[1].Value)
	//	var homeTeam = "TEXAS ST."              // string(params[2].Value)
	//	var awayTeam = "LOUISIANA MONROE"       // string(params[3].Value)
	//	var eventDate = "2015-11-19 21:30:00.0" // string(params[4].Value)
	var sport = keyValue[0]
	var leage = keyValue[1]
	var homeTeam = keyValue[2]
	var awayTeam = keyValue[3]
	var eventDate = keyValue[4]
	//get SDI object from cache
	var found bool = false
	var domeSports []sdi.SDISport
	domeSports, found = sdi.GetFromCacheSdiData()
	if found == false {
		//sdi.DebugFormatToSDILogfile("SDI object was not found in cache\n")
		eventOutput.Code = constants.FEED_READ_DATA_FROM_CACHE_ERROR_CODE
		eventOutput.Status = constants.FEED_READ_DATA_FROM_CACHE_ERROR
		//json output
		JSONPrint(w, eventOutput, false, nil)
		return
	}
	var sdiSport string
	var sdiLeage string
	var sdiSportId string
	var sdiLeageId string
	var sdiSeasonId string
	var event sdi.XMLCompetition
	sdiSport, sdiSportId, sdiLeage, sdiLeageId, sdiSeasonId, event = sdi.FindSdiEvent(sport, leage, homeTeam, awayTeam, eventDate, domeSports)
	//fmt.Printf("\tevent home team, away team, date %s, %s, %s \n", event.HomeTeamContent, event.AwayTeamContent, event.StartDate)
	//var timeAfter int64 = sdi.GetTimeString()
	//var timeDiff float64 = float64(timeAfter - timeBefore)
	//log.Info("TIME ELAPSED (milliseconds): \t" + strconv.FormatFloat(timeDiff, 'f', -1, 64))
	//sdi.DebugFormatToSDILogfile("TIME ELAPSED (milliseconds) : \t %d \n", timeAfter-timeBefore)
	//by default found=false
	eventOutput.Data.Found = false
	eventOutput.Code = constants.FEED_STATUS_CODE_OK
	eventOutput.Status = "success"
	if len(event.Id) > 0 {
		eventOutput.Data.Found = true
		eventOutput.Data.Sport = sdiSport
		eventOutput.Data.Leage = sdiLeage
		eventOutput.Data.HomeTeam = event.HomeTeamContent.Team.Name
		eventOutput.Data.AwayTeam = event.AwayTeamContent.Team.Name
		eventOutput.Data.Id = event.Id
		eventOutput.Data.DataSportId = sdiSportId
		eventOutput.Data.DataCompetitionId = sdiLeageId
		eventOutput.Data.DataEditionId = sdiSeasonId
		eventOutput.Data.StartDate = event.StartDate
	}
	//json output
	JSONPrint(w, eventOutput, false, nil)
}
