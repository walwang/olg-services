package server

// import (
// 	"net/http"
// 	"net/rpc"
// 	"olg-services/config"
// 	"olg-services/publicis/utils/log"
// 	"olg-services/utils/sysinfo"

// 	"github.com/julienschmidt/httprouter"
// )

// type MainStatus struct {
// 	Server         sysinfo.ServerStatus
// 	CurrentProcess sysinfo.ProcessStatus
// }

// // for now, the Args struct is empty
// type Args struct {
// }

// type ServerMetrics struct {
// 	ServerInfo   config.AppServer
// 	ServerStatus MainStatus
// 	MetricsError string
// }

// type MetricsSummary struct {
// 	Servers []ServerMetrics
// }

// // The GatherMetrics handler checks the configuration for a list of servers, and
// // attempts to call each server via Go RPC, and obtain the system metrics, to detect any issues.
// // The webconsole should be activated for the GatherMetrics functionality to be available.
// // It's recommended to activate this on the staging / authoring server and it can thus
// // ping the production servers.
// func GatherMetrics(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

// 	metricsSummary := &MetricsSummary{}

// 	// if the servers.conf has been created and properly completed, the
// 	// list of production servers should reside in the config.GlobalConfig.AppServers slice
// 	if len(config.GlobalConfig.AppServers) > 0 {

// 	}

// 	snappyCompressionEnabled := false
// 	err := JSONPrint(w, metricsSummary, snappyCompressionEnabled, nil)
// 	if err != nil {
// 		log.Error("server.NumbersEverWon route - JSONPrint", err)
// 		return
// 	}

// }

// // The serverAddress should be in ip:port format (e.g. "192.168.1.1:1234")
// func callServerMetricsViaRpc(serverAddress string) error {

// 	// establish connection with the rpc server
// 	client, err := rpc.DialHTTP("tcp", serverAddress)
// 	if err != nil {
// 		return log.NewError("callServerMetricsViaRpc - error dialing: "+serverAddress, err)
// 	}

// 	// Perform synchronous call
// 	args := Args{}
// 	var reply MainStatus
// 	err = client.Call("MainStatus.GetLocalMetrics", args, &reply)
// 	if err != nil {
// 		return log.NewError("MainStatus.GetLocalMetrics rpc call error:", err)
// 	}

// 	return nil
// }
