package server

import (
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/proline/base"
	"olg-services/proline/pointspread"
	"olg-services/proline/sports"
	"olg-services/publicis/utils/log"
	"time"

	"github.com/julienschmidt/httprouter"
)

func ProlinePointSpreadEvents(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ProlinePointSpread(w, r, params, false)
}
func ProlinePointSpreadResults(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ProlinePointSpread(w, r, params, true)
}

func ProlinePointSpread(w http.ResponseWriter, r *http.Request, params httprouter.Params, isResults bool) {

	logFlag := "Proline Pointspread Events"
	cacheFlag := "Pointspread_Events_"

	if isResults {
		logFlag = "Proline Results"
		cacheFlag = "Pointspread_Results_"
	}

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: "+logFlag)
	}

	httpStatusCode := 200
	sportType, ok := sports.SportTypes[params.ByName("sportCode")]

	// in dummy mode, allow the "timeout" value as a special case, to simulate server timeouts
	if config.GlobalConfig.Environment.IsDummyMode && params.ByName("sportCode") == "timeout" {
		// change the sportType code to "timeout" so the appropriate fake feed is initialized
		sportType.Code = "timeout"
	} else {
		// verify the validity of the sport type param
		if ok == false {
			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			err := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, "The specified sportType parameter is not a recognized value")
			if err != nil {
				log.Error("server."+logFlag+" route - WriteJSONError", err)
			}
			return
		}
	}

	var cacheKey string = cacheFlag + sportType.Code

	feed, err := pointspread.NewProlinePointSpreadEventsResultsFeedWithCache(ProlinePointSpreadCache, sportType, sportType.Name+" feed", false, false, nil, isResults)

	if err != nil {
		log.Error("server."+logFlag+" route - NewProlinePointSpreadEvents", err)

		WriteJSONStatus(w, httpStatusCode)

		jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, err.Error())
		if jsonErr != nil {
			log.Error("server."+logFlag+" route - WriteJSONError", jsonErr)
		}

		return
	}

	// load the request parameters into the feed
	reqErr := base.LoadRequestParams(feed, r)
	if reqErr != nil {

		log.Error("server."+logFlag+" route - feed.LoadRequestParams", reqErr)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
		if wjErr != nil {
			log.Error("server."+logFlag+" route - WriteJSONError", wjErr)
		}
		return

	}

	var loadFeedUncached bool = false

	// try to identify whether the ProlinePointSpreadCache holds the cache for the past year
	// for this sport type
	cachedFeed, found := ProlinePointSpreadCache.Get(cacheKey)
	if found == false {

		loadFeedUncached = true

		// activate the go routine to load the entire date range into cache
		go loadProlinePointSpreadIntoCache(sportType, isResults)

	} else {

		log.Debug("server." + logFlag + " route: found caching for sport type: " + sportType.Code + " - trying to find if it implements IProlineEventsResults")

		// feed found in cache, retrieve only the records in the date range
		cachedProlinePointSpreadFeed, ok := cachedFeed.(interfaces.IProlinePointSpread)
		if ok {
			log.Debug("server." + logFlag + " route: found caching object for sport type: " + sportType.Code + " - that implements IProlineEventsResults")

			curatedFeed, err := pointspread.GetCachedFeedWithDateRange(cachedProlinePointSpreadFeed, sportType, feed.GetRequestParam("startDate"), feed.GetRequestParam("endDate"))

			if err != nil {

				log.Error("server."+logFlag+" route - GetCachedFeedWithDateRange", err)

				// in case of errors make sure the regular feed will be loaded via OLG call
				loadFeedUncached = true

			} else {

				// if no interface conversion errors, assign the feed to nil for garbage collection's sake
				// then point it to the curated feed that only contains the values within the date range
				feed = nil
				feed = curatedFeed

				// we need to attach the request parameters again for the new feed
				feed.SetRequestParams(r)

				// re-sort the cached feed accordingly if the request params contain a valid sortBy
				feed.SortBy(feed.GetRequestParam("sortBy"))

			}

		} else {

			log.Error("server."+logFlag+" route", log.NewErrorLocal("Interface type assertion error", "The object in cache does not implement IProlineEventsResults"))
			loadFeedUncached = false
		}

	}

	if loadFeedUncached {

		// proceed with the uncached version
		if isResults {
			feed.SetRequestParam("results", "true")
		}

		err = feed.LoadFeed()
		if err != nil {
			log.Error("server."+logFlag+" route - feed.LoadFeed()", err)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			wjErr := WriteJSONError(w, feed.GetStatusCode(), feed.GetErrorMessage())
			if wjErr != nil {
				log.Error("server."+logFlag+" route - WriteJSONError (feed.LoadFeed uncached)", wjErr)
			}
			return
		}

		// check for the sortBy flag before loading the feed
		if sortBy := feed.GetRequestParam("sortBy"); sortBy != "" {
			feed.SortBy(feed.GetRequestParam("sortBy"))
		}

	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	elapsed := time.Since(startTime)

	snappyCompressionEnabled := (feed.GetRequestParam("compress") == "y" || feed.GetRequestParam("compress") == "Y")

	err = JSONPrint(w, feed, snappyCompressionEnabled, &elapsed)
	if err != nil {
		log.Error("server."+logFlag+" route - JSONPrint", err)
		return
	}

}

func loadProlinePointSpreadIntoCache(sportType sports.SportType, isResults bool) {

	logFlag := "Proline Pointspread Events"
	cacheFlag := "Pointspread_Events_"

	if isResults {
		logFlag = "Proline Results"
		cacheFlag = "Pointspread_Results_"
	}

	log.Debug("loadProlinePointSpreadIntoCache: Started caching for sport type: " + sportType.Code)

	var statusKey string = cacheFlag + sportType.Code + "_CACHE_STATUS"
	var cacheKey string = cacheFlag + sportType.Code

	cachedFeedStatus, found := ProlinePointSpreadCache.Get(statusKey)
	if found {
		if cachedFeedStatus.(string) == "LOADING" {
			// another go routine already in the process of loading
			return
		}
	}

	// We are going to set the "LOADING" flag inside the statuc cache key,
	// with auto-expiry in 60 seconds. This means, that if anything happens during the cache
	// loading process, the LOADING flag will only gets stuck as true for a max of 60 seconds
	ProlinePointSpreadCache.Set(statusKey, "LOADING", 60*time.Second)

	// make sure we remove the status cache
	defer ProlinePointSpreadCache.Delete(statusKey)

	feedToBeCached, err := pointspread.NewProlinePointSpreadEventsResultsFeed(sportType, sportType.Name+" feed", false, false, isResults)

	if err != nil {
		log.Error("loadProlinePointSpreadIntoCache go routine called from server."+logFlag+" route - NewProlinePointSpread", err)
		return
	}

	// notify the feed that it's in cache storing mode
	feedToBeCached.SetIsCached(true)

	// specify the sport type
	feedToBeCached.SetRequestParam("type", "PointSpread")
	feedToBeCached.SetRequestParam("sportId", sportType.OlgSportId)

	if isResults {
		feedToBeCached.SetRequestParam("results", "true")
	}

	// calculate the start date parameter to today plus 30 days
	// calculate the end date to today minus 30 days
	nowDate := time.Now()
	calculatedEndDate := nowDate.AddDate(0, 0, 30)
	calculatedStartDate := nowDate.AddDate(0, 0, -30)
	feedToBeCached.SetRequestParam("startDate", calculatedStartDate.Format(pointspread.EVENT_DATE_PARSE_FORMAT)+"T00:00:00")
	feedToBeCached.SetRequestParam("endDate", calculatedEndDate.Format(pointspread.EVENT_DATE_PARSE_FORMAT)+"T23:59:59")

	// proceed with loading the feed
	err = feedToBeCached.LoadFeed()
	if err != nil {
		log.Error("loadProlinePointSpreadIntoCache go routine called from server."+logFlag+" route - feedToBeCached.LoadFeed()", err)
		return
	}

	// store the feed into cache
	ProlinePointSpreadCache.Set(cacheKey, feedToBeCached, 0)

	log.Debug("loadProlinePointSpreadIntoCache: Finished caching " + logFlag + " for sport type: " + sportType.Code)

}
