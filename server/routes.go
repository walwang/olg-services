package server

import (
	"bytes"
	"encoding/json"
	"expvar"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"

	"github.com/golang/snappy"
	"github.com/julienschmidt/httprouter"
)

const (
	contentType    = "Content-Type"
	acceptLanguage = "Accept-Language"
)

var addressOfMainServicesListener = ""

func ExpvarHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Fprintf(w, "{\n")
	first := true
	expvar.Do(func(kv expvar.KeyValue) {
		if !first {
			fmt.Fprintf(w, ",\n")
		}
		first = false
		fmt.Fprintf(w, "%q: %s", kv.Key, kv.Value)
	})
	fmt.Fprintf(w, "\n}\n")
}

// Initializes the routes and starts the server, based on the specified address.
// The format of the address is flexible, it can contain only the port preceded by
// colon (e.g. :8080 means listen to every available IP on port 8080), or
// explicitly listen to a hostname or ip address (e.g. localhost:8080)
func Start(address string, webConsoleEnabled bool) {

	router := httprouter.New()

	// initialize the caching objects
	initCaching()

	// initialize the routes
	initRoutes(router, webConsoleEnabled)

	go func() {

		// a package variable used by the GetBaseURL() function
		addressOfMainServicesListener = address

		// if the format is just ":8080" (or another port), add the "localhost" in front
		if strings.HasPrefix(addressOfMainServicesListener, ":") {
			addressOfMainServicesListener = "localhost" + addressOfMainServicesListener
		}

		err := http.ListenAndServe(address, router)
		if err != nil {
			log.FatalErr("server.Start() executing ListenAndServe", err)
		}

	}()

}

// Allows the web profiler on the specified address
func StartWebProfiler(address string) {

	// start the pprof server
	go func() {
		http.ListenAndServe(address, nil)
	}()

}

func initRoutes(router *httprouter.Router, webConsoleEnabled bool) {

	// Lotteries services

	// have your numbers ever won
	router.GET("/service/lotteries/numbersEverWon/:gameType", NumbersEverWon)

	// past winning numbers
	router.GET("/service/lotteries/pastWinningNumbers/:gameType", PastWinningNumbers)

	// winning frequency
	// router.GET("/service/lotteries/frequency/:gameType", WinningFrequency)

	// winning numbers
	router.GET("/service/lotteries/winningNumbers", WinningNumbers)

	// instant unclaimed prizes
	router.GET("/service/lotteries/instantUnclaimed", InstantUnclaimedPrizes)

	// win trackers
	router.GET("/service/lotteries/winTrackers/:gameType", WinTrackers)

	// Proline services

	// proline events
	//	router.GET("/service/proline/events/:sportCode", ProlineEvents)

	// proline results
	//	router.GET("/service/proline/results/:sportCode", ProlineResults)

	// proline pointspread
	//	router.GET("/service/proline/pointspread/events/:sportCode", ProlinePointSpreadEvents)
	//	router.GET("/service/proline/pointspread/results/:sportCode", ProlinePointSpreadResults)

	// proline propicks
	//	router.GET("/service/proline/propicks/events/:sportCode", ProlineProPicksEvents)
	//	router.GET("/service/proline/propicks/results/:sportCode", ProlineProPicksResults)

	// docs with demo links
	router.GET("/service/docs/docs.css", DocsCss)
	router.GET("/service/docs", ServicesDocs)
	router.GET("/service/system/docs", SystemOverviewDocs)
	router.GET("/service/lotteries/docs", LotteriesDocs)
	router.GET("/service/proline/docs", ProlineDocs)

	// Peter Test Routes
	//	router.GET("/service/proline/sdi/find/:competition", FindSdiSportEvent)
	//	router.GET("/service/proline/sdi/feed/:none", SdiDataFeedEvent)
	//	router.POST("/service/proline/email/sendemail/:email", SendEmailMyBetsToFriends)
	//	router.GET("/service/proline/email/startjs/:none", StartJSClient)

	router.GET("/debug/vars", ExpvarHandler)

	// if webConsoleEnabled {

	// 	// metrics console
	// 	router.GET("/service/webconsole/metrics", GatherMetrics)
	// }

}

func GetBaseURL() string {

	if config.GlobalConfig.Environment.DocsUrlBase != "" {
		return config.GlobalConfig.Environment.DocsUrlBase
	}

	return addressOfMainServicesListener

}

// JSON response with application/json; charset=UTF-8 Content type.
// If compressToSnappy is true, snappy compression will be applied.
// If debugDuration is not nil, insert it at the top of the JSON document as a DebugTimeElapsed field
func JSONPrint(w http.ResponseWriter, v interface{}, compressToSnappy bool, debugDuration *time.Duration) error {

	if v == nil {
		log.Error("server.JSONPrint", log.NewErrorLocal("nil parameter", "nil interface passed"))
		return nil
	}

	unencodedJson := &bytes.Buffer{}
	if err := json.NewEncoder(unencodedJson).Encode(v); err != nil {
		return err
	}

	if compressToSnappy {
		w.Write(snappy.Encode(nil, unencodedJson.Bytes()))
		return nil
	}

	if config.GlobalConfig.Environment.IsDummyMode {

		buf := &bytes.Buffer{}

		// in dummy mode indent the JSON output
		err := json.Indent(buf, unencodedJson.Bytes(), "", "\t")
		if err != nil {
			return err
		}

		// if the debugDuration is not nil, append a tiny debug time elapsed info
		if debugDuration != nil {
			milisecs := debugDuration.Nanoseconds() / 1e6
			debugBuf := []byte(strings.Replace(string(buf.Bytes()), "{", "{\"DebugTimeElapsedInternally\":\""+strconv.FormatInt(milisecs, 10)+" ms\",", 1))

			w.Write(debugBuf)
			return nil
		}

		w.Write(buf.Bytes())
		return nil
	}

	w.Write(unencodedJson.Bytes())
	return nil

}

func ToJson(v interface{}) ([]byte, error) {

	if v == nil {
		log.Error("server.ToJson", log.NewErrorLocal("nil parameter", "nil interface passed"))
		return nil, nil
	}

	unencodedJson := &bytes.Buffer{}
	if err := json.NewEncoder(unencodedJson).Encode(v); err != nil {
		return nil, err
	}

	if config.GlobalConfig.Environment.IsDummyMode {

		buf := &bytes.Buffer{}

		// in dummy mode indent the JSON output
		err := json.Indent(buf, unencodedJson.Bytes(), "", "\t")
		if err != nil {
			return nil, err
		}

		return buf.Bytes(), nil
	}

	return unencodedJson.Bytes(), nil

}

func WriteJSONStatus(w http.ResponseWriter, status int) {

	w.Header().Set(contentType, "application/json; charset=UTF-8")
	w.WriteHeader(status)
}

//JSON response with application/json; charset=UTF-8 Content type
func WriteJSONError(w http.ResponseWriter, statusCode int, errorMessage string) error {

	escapedErrorMessage := strings.Replace(errorMessage, "\"", "\\\"", -1)

	unescapedJson := &bytes.Buffer{}
	_, err := unescapedJson.WriteString("{\"code\":\"" + strconv.Itoa(statusCode) + "\",\"status\":\"" + constants.FEED_STATUS_MESSAGE_ERROR + "\",\"message\":\"" + escapedErrorMessage + "\"}")
	if err != nil {
		return err
	}

	unencodedJson := &bytes.Buffer{}
	json.HTMLEscape(unencodedJson, unescapedJson.Bytes())

	if config.GlobalConfig.Environment.IsDummyMode {

		buf := &bytes.Buffer{}

		// in dummy mode indent the JSON output
		err := json.Indent(buf, unencodedJson.Bytes(), "", "\t")
		if err != nil {
			return err
		}
		w.Write(buf.Bytes())
		return nil
	}

	w.Write(unencodedJson.Bytes())
	return nil
}
