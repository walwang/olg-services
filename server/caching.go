package server

import (
	"time"

	cache "github.com/pmylund/go-cache"
)

// Cache store for the winning numbers quasi-real-time feed;
// this has to be refreshed fairly fast, every 5 seconds or so
var Cache *cache.Cache

// Cache store for the past winning numbers feed;
// this can be stored much longer, setting it to 5 minutes initially
var PastWinningNumbersCache *cache.Cache

// Cache store for the winning frequency feed;
// this can be stored much longer, setting it to 5 minutes initially
var WinningFrequencyCache *cache.Cache

// Cache store for the unclaimed instant prizes feed;
// this can be refreshed more seldom, once every 30 minutes
var UnclaimedInstantPrizesCache *cache.Cache

// Cache store for the win tracker feeds;
// this can be refreshed more seldom, once every 30 minutes
var WinTrackersCache *cache.Cache

// Cache store for the Proline Events feed;
// this cannot be stored for long, setting it to 5 seconds initially
var ProlineEventsCache *cache.Cache

var ProlineResultsCache *cache.Cache

var ProlinePointSpreadCache *cache.Cache

var ProlineProPicksCache *cache.Cache

func initCaching() {

	// initialize the winning numbers cache with a 30 seconds expiry time per item, and a
	// mop-up every 1 minute
	Cache = cache.New(30*time.Second, 1*time.Minute)

	PastWinningNumbersCache = cache.New(15*time.Minute, 5*time.Minute)

	// initialize the winning frequency cache with a 5 minutes expiry time per item, and a
	// mop-up every 1 minute
	WinningFrequencyCache = cache.New(5*time.Minute, 3*time.Minute)

	// initialize the unclaimed instant prizes cache with a 30 minutes expiry time per item,
	// and a mop-up every 5 minute	s
	UnclaimedInstantPrizesCache = cache.New(30*time.Minute, 5*time.Minute)

	// initialize the win trackers cache with a 30 minutes expiry time per item,
	// and a mop-up every 5 minute	s
	WinTrackersCache = cache.New(10*time.Minute, 5*time.Minute)

	// initialize the Proline Events cache with a 5 seconds expiry time per item, and a
	// mop-up every 1 second
	//	ProlineEventsCache = cache.New(5000*time.Millisecond, 1000*time.Millisecond)

	//	ProlineResultsCache = cache.New(5000*time.Millisecond, 1000*time.Millisecond)

	//	ProlinePointSpreadCache = cache.New(5000*time.Millisecond, 1000*time.Millisecond)

	//	ProlineProPicksCache = cache.New(5000*time.Millisecond, 1000*time.Millisecond)
}
