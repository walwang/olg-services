package server

import (
	"net/http"

	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/lotteries/games"
	"olg-services/lotteries/pastWinningNumbers"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"

	"github.com/julienschmidt/httprouter"
)

func PastWinningNumbers(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: PastWinningNumbers")
	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	gameType, ok := games.GameTypes[params.ByName("gameType")]

	// in dummy mode, allow the "timeout" value as a special case, to simulate server timeouts
	if config.GlobalConfig.Environment.IsDummyMode && params.ByName("gameType") == "timeout" {
		// change the gameType code to "timeout" so the appropriate fake feed is initialized
		gameType.Code = "timeout"

	} else {

		// verify the validity of the game type param
		if ok == false {

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			err := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, "The specified gameType parameter is not a recognized value")
			if err != nil {
				log.Error("server.PastWinningNumbers route - WriteJSONError", err)
			}
			return
		}

	}

	feed, err := pastWinningNumbers.NewPastWinningNumbersWithCache(PastWinningNumbersCache, gameType, gameType.Name+" feed", false, false, nil)
	if err != nil {
		log.Error("server.PastWinningNumbers route - NewPastWinningNumbers", err)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, err.Error())
		if jsonErr != nil {
			log.Error("server.PastWinningNumbers route - WriteJSONError", jsonErr)
		}

		return
	}

	// load the request parameters into the feed
	reqErr := base.LoadRequestParams(feed, r)
	if reqErr != nil {

		log.Error("server.PastWinningNumbers route - feed.LoadRequestParams", reqErr)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
		if wjErr != nil {
			log.Error("server.PastWinningNumbers route - WriteJSONError", wjErr)
		}
		return

	}

	var loadFeedUncached bool = false

	// try to identify whether the PastWinningNumbersCache holds the cache for the past year
	// for this game type
	cachedFeed, found := PastWinningNumbersCache.Get(gameType.Code)
	if found == false {

		loadFeedUncached = true

		// activate the go routine to load the entire date range into cache
		go loadFullDateRangeIntoCache(gameType)

	} else {

		log.Debug("server.PastWinningNumbers route: found caching for game type: " + gameType.Code + " - trying to find if it implements IPastWinningNumbers")

		// feed found in cache, retrieve only the records in the date range
		pastWinningNumbersFeed, ok := cachedFeed.(interfaces.IPastWinningNumbers)
		if ok {
			log.Debug("server.PastWinningNumbers route: found caching object for game type: " + gameType.Code + " - that implements IPastWinningNumbers")

			curatedFeed, err := pastWinningNumbers.GetCachedFeedWithDateRange(pastWinningNumbersFeed, gameType,
				feed.GetRequestParam("startDate"), feed.GetRequestParam("endDate"))

			if err != nil {

				log.Error("server.PastWinningNumbers route - GetCachedFeedWithDateRange", err)

				// in case of errors make sure the regular feed will be loaded via OLG call
				loadFeedUncached = true
			} else {

				// save the "OnlyLastDraw" flag for later
				onlyLastDraw := feed.GetOnlyLastDraw()

				// if no errors, assign the feed to nil for garbage collection's sake
				// then point it to the curated feed that only contains the values within the date range
				feed = nil
				feed = curatedFeed

				// we need to attach the request parameters again for the new feed
				feed.SetRequestParams(r)

				// make sure that the OnlyLastDraw flag is carried over
				feed.SetOnlyLastDraw(onlyLastDraw)
			}

		}

	}

	if loadFeedUncached {

		// if feed does not have start and end date set, we have to treat it as special case
		if feed.GetOnlyLastDraw() {

			// this is the version with start date and end date empty
			// calculate the end date to today, and start date to be 2 months prior to today's date
			var calculatedEndDate time.Time
			calculatedEndDate = time.Now()

			// because OLG testing environment is delayed, we need to shift the interval x months back
			if config.GlobalConfig.Environment.IsProd == false {
				calculatedEndDate = time.Now().AddDate(0, -6, 0) // x months back from now
			}

			calculatedStartDate := calculatedEndDate.AddDate(0, 0, -8) // x days back from then

			//			fmt.Println("now", time.Now())
			//			fmt.Println("start-date", calculatedStartDate)
			//			fmt.Println("end-date", calculatedEndDate)

			PWNFeedSlice, err := loadFeedByDateRange(gameType, calculatedStartDate, calculatedEndDate)

			if err != nil {
				log.Error("server.PastWinningNumbers route - feed.LoadFeed() - uncached no startdate and enddate", err)

				// set the JSON header with the status code
				WriteJSONStatus(w, httpStatusCode)

				wjErr := WriteJSONError(w, feed.GetStatusCode(), feed.GetErrorMessage())
				if wjErr != nil {
					log.Error("server.PastWinningNumbers route - WriteJSONError (feed.LoadFeed uncached no startdate and enddate)", wjErr)
				}
				return
			}

			log.Debug("server.PastWinningNumbers route: the four month no start or end date feed: " + gameType.Code + " - trying to find if it implements IPastWinningNumbers")

			// feed found in cache, retrieve only the records in the date range
			PastWinningFeedSlice, ok := PWNFeedSlice.(interfaces.IPastWinningNumbers)
			if ok {

				// if no errors, assign the feed to nil for garbage collection's sake
				// then point it to the curated feed that only contains the values within the date range
				feed = nil
				feed = PastWinningFeedSlice

				// we need to attach the request parameters again for the new feed
				feed.SetRequestParams(r)

				// make sure that the OnlyLastDraw flag is carried over
				feed.SetOnlyLastDraw(true)
			}

		} else {

			// this is the version with start date and end date not empty
			// proceed with the uncached version
			err = feed.LoadFeed()
			if err != nil {
				log.Error("server.PastWinningNumbers route - feed.LoadFeed()", err)

				// set the JSON header with the status code
				WriteJSONStatus(w, httpStatusCode)

				wjErr := WriteJSONError(w, feed.GetStatusCode(), feed.GetErrorMessage())
				if wjErr != nil {
					log.Error("server.PastWinningNumbers route - WriteJSONError (feed.LoadFeed uncached)", wjErr)
				}
				return
			}
		}
	}

	// everything should be fine, we only need to check for the "OnlyLastDraw" flag, that should
	// be activated if startDate and lastDate are both empty
	if feed.GetOnlyLastDraw() == true {

		// eliminate all draws except the one at index 0
		feed.TrimDrawsStartingWithIndex(2)
	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	elapsed := time.Since(startTime)

	snappyCompressionEnabled := (feed.GetRequestParam("compress") == "y" || feed.GetRequestParam("compress") == "Y")

	err = JSONPrint(w, feed, snappyCompressionEnabled, &elapsed)
	if err != nil {
		log.Error("server.PastWinningNumbers route - JSONPrint", err)
		return
	}
}

func loadFullDateRangeIntoCache(gameType games.GameType) {

	log.Debug("loadFullDateRangeIntoCache: Started caching for game type: " + gameType.Code)

	var statusKey string = gameType.Code + "_CACHE_STATUS"

	cachedFeedStatus, found := PastWinningNumbersCache.Get(statusKey)
	if found {
		if cachedFeedStatus.(string) == "LOADING" {
			// another go routine already in the process of loading
			return
		}
	}

	// We are going to set the "LOADING" flag inside the statuc cache key,
	// with auto-expiry in 60 seconds. This means, that if anything happens during the cache
	// loading process, the LOADING flag will only gets stuck as true for a max of 60 seconds
	PastWinningNumbersCache.Set(statusKey, "LOADING", 60*time.Second)

	// make sure we remove the status cache
	defer PastWinningNumbersCache.Delete(statusKey)

	// calculate the end date to today, and start date to be 2 months prior to today's date
	calculatedEndDate := time.Now()
	calculatedStartDate := calculatedEndDate.AddDate(0, -2, 0)
	masterFeedToBeCached, err := loadFeedByDateRange(gameType, calculatedStartDate, calculatedEndDate)
	if err != nil {
		log.Error("loadFullDateRangeIntoCache - loadFeedByDateRange", err)
		return
	}

	masterPastWinningNumbersFeed, ok := masterFeedToBeCached.(interfaces.IPastWinningNumbers)
	if ok {

		//	 the olg feed currently allows retrieval of data from a range of only two months
		//	 we need to split the past 12 months in 6 chunks of 2 months each
		//	 we already retrieved the two months immediately prior to current date in the
		//	 lines above. So now we need 5 more chunks, in a for loop

		for i := 1; i <= 5; i++ {

			calculatedEndDate = calculatedStartDate.AddDate(0, 0, -1)
			calculatedStartDate = calculatedEndDate.AddDate(0, -2, 0)
			subsequentFeed, err := loadFeedByDateRange(gameType, calculatedStartDate, calculatedEndDate)
			if err != nil {
				log.Error("loadFullDateRangeIntoCache - subsequent loadFeedByDateRange", err)
				return
			} else {

				pastWinningNumbersFeed, ok := subsequentFeed.(interfaces.IPastWinningNumbers)
				if ok {

					log.Debug("loadFullDateRangeIntoCache: about to merge feed from iteration: " + strconv.Itoa(i) + " to the initial feed")
					masterPastWinningNumbersFeed.AppendDraws(pastWinningNumbersFeed)
				}

			}

		}

	}

	// store the feed into cache

	PastWinningNumbersCache.Set(gameType.Code, masterFeedToBeCached, 0)

	log.Debug("loadFullDateRangeIntoCache: Finished caching for game type: " + gameType.Code)

}

func loadFeedByDateRange(gameType games.GameType, calculatedStartDate, calculatedEndDate time.Time) (interfaces.NewXmlFeed, error) {

	feedToBeCached, err := pastWinningNumbers.NewPastWinningNumbers(gameType, gameType.Name+" feed", false, false)
	if err != nil {
		log.Error("loadFullDateRangeIntoCache go routine called from server.PastWinningNumbers route - NewPastWinningNumbers", err)
		return nil, err
	}

	// notify the feed that it's in cache storing mode
	feedToBeCached.SetIsCached(true)

	var testEnv, _ = config.GetEntryValueDirectlyFromConfigFile("testEnvironment")

	feedToBeCached.SetRequestParam("game", gameType.Code)
	feedToBeCached.SetRequestParam("startDate", calculatedStartDate.Format(pastWinningNumbers.DRAW_DATE_PARSE_FORMAT))
	feedToBeCached.SetRequestParam("endDate", calculatedEndDate.Format(pastWinningNumbers.DRAW_DATE_PARSE_FORMAT))
	feedToBeCached.SetRequestParam("env", testEnv)
	feedToBeCached.SetRequestParam("feedType", "pastWinningNumbers")

	// proceed with loading the feed
	err = feedToBeCached.LoadFeed()
	if err != nil {
		log.Error("loadFullDateRangeIntoCache go routine called from server.PastWinningNumbers route - feedToBeCached.LoadFeed()", err)
		return nil, err
	}

	return feedToBeCached, nil

}
