package server

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"olg-services/publicis/utils/log"
	"strings"
)

// Reads the file in the docs folder and returns it's content
func readDocsFile(fileName string) string {

	fileContent, err := ioutil.ReadFile("./docs/" + fileName)
	if err != nil {
		log.Error("readDocsFile trying to read the file: ./docs/"+fileName, err)
		return ""
	}

	return string(fileContent)

}

func DocsCss(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	docsContent := readDocsFile("docs.css")
	if docsContent == "" {
		docsContent = "/* css file not found */"
	}

	_, err := fmt.Fprint(w, docsContent)
	if err != nil {
		log.Error("server.ServicesDocs route - fmt.Fprint", err)
		return
	}
}

func ServicesDocs(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	docsContent := strings.Replace(readDocsFile("services-docs.html"), "URL-BASE-PLACEHOLDER", GetBaseURL(), -1)
	if docsContent == "" {
		docsContent = strings.Replace(DOCS_NOT_FOUND_WARNING, "URL-DOC-FILE-PLACEHOLDER", "services-docs.html", -1)
	}

	_, err := fmt.Fprint(w, docsContent)
	if err != nil {
		log.Error("server.ServicesDocs route - fmt.Fprint", err)
		return
	}
}

/* **************************************** */
/* BEGIN: System Overview					*/
/* **************************************** */

func SystemOverviewDocs(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	docsContent := strings.Replace(readDocsFile("system-overview-docs.html"), "URL-BASE-PLACEHOLDER", GetBaseURL(), -1)
	if docsContent == "" {
		docsContent = strings.Replace(DOCS_NOT_FOUND_WARNING, "URL-DOC-FILE-PLACEHOLDER", "system-overview-docs.html", -1)
	}

	_, err := fmt.Fprint(w, docsContent)
	if err != nil {
		log.Error("server.SystemOverviewDocs route - fmt.Fprint", err)
		return
	}
}

/* **************************************** */
/* BEGIN: Lotteries API	Docs				*/
/* **************************************** */

func LotteriesDocs(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	docsContent := strings.Replace(readDocsFile("lotteries-docs.html"), "URL-BASE-PLACEHOLDER", GetBaseURL(), -1)
	if docsContent == "" {
		docsContent = strings.Replace(DOCS_NOT_FOUND_WARNING, "URL-DOC-FILE-PLACEHOLDER", "lotteries-docs.html", -1)
	}
	_, err := fmt.Fprint(w, docsContent)
	if err != nil {
		log.Error("server.LotteriesDocs route - fmt.Fprint", err)
		return
	}
}

/* **************************************** */
/* BEGIN: Proline API	Docs				*/
/* **************************************** */

func ProlineDocs(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	docsContent := strings.Replace(readDocsFile("proline-docs.html"), "URL-BASE-PLACEHOLDER", GetBaseURL(), -1)
	if docsContent == "" {
		docsContent = strings.Replace(DOCS_NOT_FOUND_WARNING, "URL-DOC-FILE-PLACEHOLDER", "proline-docs.html", -1)
	}

	_, err := fmt.Fprint(w, docsContent)
	if err != nil {
		log.Error("server.ProlineDocs route - fmt.Fprint", err)
		return
	}
}

const DOCS_NOT_FOUND_WARNING = `<html>
	<head>
		<title>Documentation Not Found</title>
	</head>
	<body>
		<h1>Error - Documentation file <strong>URL-DOC-FILE-PLACEHOLDER</strong> not found</h1>
		
		<p>The router tried to find the appropriate file in the docs subfolder but could not find it.</p>

		<h2>Administrators: </h2>
		<p>Please locate the application executable path, create <strong>docs</strong> subfolder, and place 
		the appropriate <strong>URL-DOC-FILE-PLACEHOLDER</strong> doc file in that subfolder.</p>

	</body>
</html>
`
