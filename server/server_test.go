package server

import (
	"net/http"
	"testing"
)

var GameTypes = map[string]string{
	"lotto649":       "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"lottomax":       "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"nhllotto":       "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"pokerlotto":     "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"wheeloffortune": "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"pick4":          "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"pick3":          "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"pick2":          "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"dailykeno":      "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"livingthelife":  "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"megadice":       "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"lottario":       "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
	"ontario49":      "?num1=1&num2=2&num3=3&num4=4&num5=5&num6=6&num7=7",
}

// Lotteries - Numbers Ever Won
func TestLotteriesNumEverWon(t *testing.T) {

	games := 0
	status := make(chan bool, 1)

	for game, params := range GameTypes {

		go func(gameType string) {

			url := "http://localhost:8080/service/lotteries/numbersEverWon/"
			endpoint := url + game + params

			resp, err := http.Get(endpoint)
			if err != nil {
				t.Error("http error", err)
			}

			defer resp.Body.Close()

			//	"net/http/httputil"

			//			dump, err := httputil.DumpResponse(resp, false)
			//			if err != nil {
			//				t.Error("http dump error", err)
			//			}

			//			fmt.Println(endpoint, string(dump))
			games++

			if games == len(GameTypes) {
				status <- true
			}
		}(game)
	}

	<-status
}

// Lotteries - Past Winning Numbers
func TestLotteriesPastWinning(t *testing.T) {

	games := 0
	status := make(chan bool, 1)

	for game, params := range GameTypes {

		go func(gameType string) {

			url := "http://localhost:8080/service/lotteries/pastWinningNumbers/"
			endpoint := url + game + params

			resp, err := http.Get(endpoint)
			if err != nil {
				t.Error("http error", err)
			}

			defer resp.Body.Close()

			//			dump, err := httputil.DumpResponse(resp, false)
			//			if err != nil {
			//				t.Error("http dump error", err)
			//			}

			games++

			if games == len(GameTypes) {
				status <- true
			}
		}(game)
	}

	<-status
}

// winning frequency
//"/service/lotteries/frequency/:gameType

// winning numbers
//"/service/lotteries/winningNumbers"

// instant unclaimed prizes
//"/service/lotteries/instantUnclaimed"

// win trackers
//"/service/lotteries/winTrackers/:gameType"

// proline events
//"/service/proline/events/:sportCode"

// proline results
//	"/service/proline/results/:sportCode"

// proline pointspread
//"/service/proline/pointspread/events/:sportCode"
//"/service/proline/pointspread/results/:sportCode"

// proline propicks
//"/service/proline/propicks/events/:sportCode"
//"/service/proline/propicks/results/:sportCode"
