package server

import (
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/lotteries/frequency"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"time"

	"github.com/julienschmidt/httprouter"
)

func WinningFrequency(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: WinningFrequency")
	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	gameType, ok := games.GameTypes[params.ByName("gameType")]

	// in dummy mode, allow the "timeout" value as a special case, to simulate server timeouts
	if config.GlobalConfig.Environment.IsDummyMode && params.ByName("gameType") == "timeout" {
		// change the gameType code to "timeout" so the appropriate fake feed is initialized
		gameType.Code = "timeout"

	} else {

		// verify the validity of the game type param
		if ok == false {

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			err := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, "The specified gameType parameter is not a recognized value")
			if err != nil {
				log.Error("server.WinningFrequency route - WriteJSONError", err)
			}
			return
		}

	}

	feed, err := frequency.NewWinningFrequencyWithCache(WinningFrequencyCache, gameType, gameType.Name+" feed", false, false, nil)
	if err != nil {
		log.Error("server.WinningFrequency route - NewWinningFrequency", err)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, err.Error())
		if jsonErr != nil {
			log.Error("server.WinningFrequency route - WriteJSONError", jsonErr)
		}

		return
	}

	// load the request parameters into the feed
	reqErr := base.LoadRequestParams(feed, r)
	if reqErr != nil {

		log.Error("server.WinningFrequency route - feed.LoadRequestParams", reqErr)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
		if wjErr != nil {
			log.Error("server.WinningFrequency route - WriteJSONError", wjErr)
		}
		return

	}

	var loadFeedUncached bool = false

	// try to identify whether the WinningFrequencyCache holds the cache for the past year
	// for this game type
	cachedFeed, found := WinningFrequencyCache.Get(gameType.Code)
	if found == false {

		loadFeedUncached = true

		// activate the go routine to load the entire date range into cache
		go loadWinningFrequencyIntoCache(gameType)

	} else {

		log.Debug("server.WinningFrequency route: found caching for game type: " + gameType.Code + " - trying to find if it implements IWinningFrequency")

		// feed found in cache, retrieve only the records in the date range
		winningFrequencyFeed, ok := cachedFeed.(interfaces.IWinningFrequency)
		if ok {
			log.Debug("server.WinningFrequency route: found caching object for game type: " + gameType.Code + " - that implements IWinningFrequency")

			// if no interface conversion errors, assign the feed to nil for garbage collection's sake
			// then point it to the curated feed that only contains the values within the date range
			feed = nil
			feed = winningFrequencyFeed

			// we need to attach the request parameters again for the new feed
			feed.SetRequestParams(r)

			// re-sort the cached feed accordingly if the request params contain a valid sortBy
			feed.SortBy(feed.GetRequestParam("sortBy"))

		} else {

			log.Error("server.WinningFrequency route", log.NewErrorLocal("Interface type assertion error", "The object in cache does not implement IWinningFrequency"))
			loadFeedUncached = false
		}

	}

	if loadFeedUncached {

		// proceed with the uncached version
		err = feed.LoadFeed()
		if err != nil {
			log.Error("server.WinningFrequency route - feed.LoadFeed()", err)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			wjErr := WriteJSONError(w, feed.GetStatusCode(), feed.GetErrorMessage())
			if wjErr != nil {
				log.Error("server.WinningFrequency route - WriteJSONError (feed.LoadFeed uncached)", wjErr)
			}
			return
		}

	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	elapsed := time.Since(startTime)

	snappyCompressionEnabled := (feed.GetRequestParam("compress") == "y" || feed.GetRequestParam("compress") == "Y")

	err = JSONPrint(w, feed, snappyCompressionEnabled, &elapsed)
	if err != nil {
		log.Error("server.WinningFrequency route - JSONPrint", err)
		return
	}
}

func loadWinningFrequencyIntoCache(gameType games.GameType) {

	log.Debug("loadWinningFrequencyIntoCache: Started caching for game type: " + gameType.Code)

	var statusKey string = gameType.Code + "_CACHE_STATUS"

	cachedFeedStatus, found := WinningFrequencyCache.Get(statusKey)
	if found {
		if cachedFeedStatus.(string) == "LOADING" {
			// another go routine already in the process of loading
			return
		}
	}

	// We are going to set the "LOADING" flag inside the statuc cache key,
	// with auto-expiry in 60 seconds. This means, that if anything happens during the cache
	// loading process, the LOADING flag will only gets stuck as true for a max of 60 seconds
	WinningFrequencyCache.Set(statusKey, "LOADING", 60*time.Second)

	// make sure we remove the status cache
	defer WinningFrequencyCache.Delete(statusKey)

	feedToBeCached, err := frequency.NewWinningFrequency(gameType, gameType.Name+" feed", false, false)
	if err != nil {
		log.Error("loadWinningFrequencyIntoCache go routine called from server.WinningFrequency route - NewWinningFrequency", err)
		return
	}

	// notify the feed that it's in cache storing mode
	feedToBeCached.SetIsCached(true)

	feedToBeCached.SetRequestParam("game", gameType.Code)

	// Test Mode Parameters
	if config.GlobalConfig.Environment.IsDummyMode {
		feedToBeCached.SetRequestParam("env", "t1")
		feedToBeCached.SetRequestParam("feedType", "winningFrequency")
	}

	// proceed with loading the feed
	err = feedToBeCached.LoadFeed()
	if err != nil {
		log.Error("loadWinningFrequencyIntoCache go routine called from server.WinningFrequency route - feedToBeCached.LoadFeed()", err)
		return
	}

	// store the feed into cache
	WinningFrequencyCache.Set(gameType.Code, feedToBeCached, 0)

	log.Debug("loadWinningFrequencyIntoCache: Finished caching for game type: " + gameType.Code)

}
