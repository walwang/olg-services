package server

import (
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/lotteries/games"
	"olg-services/lotteries/winTrackers"
	"olg-services/publicis/utils/log"
	"time"

	"github.com/julienschmidt/httprouter"
)

func WinTrackers(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: WinTrackers")
	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	gameType, ok := games.GameTypes[params.ByName("gameType")]

	// in dummy mode, allow the "timeout" value as a special case, to simulate server timeouts
	if config.GlobalConfig.Environment.IsDummyMode && params.ByName("gameType") == "timeout" {
		// change the gameType code to "timeout" so the appropriate fake feed is initialized
		gameType.Code = "timeout"

	} else {

		// verify the validity of the game type param
		if ok == false {

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			err := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, "The specified gameType parameter is not a recognized value")
			if err != nil {
				log.Error("server.WinTrackers route - WriteJSONError", err)
			}
			return
		}

	}

	feed, err := winTrackers.NewWinTrackerWithCache(WinTrackersCache, gameType, gameType.Name+" feed", false, false, nil)
	if err != nil {
		log.Error("server.WinTrackers route - NewWinTrackers", err)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, err.Error())
		if jsonErr != nil {
			log.Error("server.WinTrackers route - WriteJSONError", jsonErr)
		}

		return
	}

	// load the request parameters into the feed
	reqErr := base.LoadRequestParams(feed, r)
	if reqErr != nil {

		log.Error("server.WinTrackers route - feed.LoadRequestParams", reqErr)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
		if wjErr != nil {
			log.Error("server.WinTrackers route - WriteJSONError", wjErr)
		}
		return

	}

	var loadFeedUncached bool = false

	// try to identify whether the WinTrackersCache holds the cache for the past year
	// for this game type
	cachedFeed, found := WinTrackersCache.Get(gameType.Code)
	if found == false {

		loadFeedUncached = true

		// activate the go routine to load the entire date range into cache
		go loadWinTrackersIntoCache(gameType)

	} else {

		log.Debug("server.WinTrackers route: found caching for game type: " + gameType.Code + " - trying to find if it implements IWinTrackers")

		// feed found in cache, retrieve only the records in the date range
		WinTrackersFeed, ok := cachedFeed.(interfaces.IWinTracker)
		if ok {
			log.Debug("server.WinTrackers route: found caching object for game type: " + gameType.Code + " - that implements IWinTrackers")

			// if no interface conversion errors, assign the feed to nil for garbage collection's sake
			// then point it to the curated feed that only contains the values within the date range
			feed = nil
			feed = WinTrackersFeed

			// we need to attach the request parameters again for the new feed
			feed.SetRequestParams(r)

			// re-sort the cached feed accordingly if the request params contain a valid sortBy
			feed.SortBy(feed.GetRequestParam("sortBy"))

		} else {

			log.Error("server.WinTrackers route", log.NewErrorLocal("Interface type assertion error", "The object in cache does not implement IWinTrackers"))
			loadFeedUncached = false
		}

	}

	if loadFeedUncached {

		// proceed with the uncached version
		err = feed.LoadFeed()
		if err != nil {
			log.Error("server.WinTrackers route - feed.LoadFeed()", err)

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			wjErr := WriteJSONError(w, feed.GetStatusCode(), feed.GetErrorMessage())
			if wjErr != nil {
				log.Error("server.WinTrackers route - WriteJSONError (feed.LoadFeed uncached)", wjErr)
			}
			return
		}

	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	elapsed := time.Since(startTime)

	snappyCompressionEnabled := (feed.GetRequestParam("compress") == "y" || feed.GetRequestParam("compress") == "Y")

	err = JSONPrint(w, feed, snappyCompressionEnabled, &elapsed)
	if err != nil {
		log.Error("server.WinTrackers route - JSONPrint", err)
		return
	}
}

func loadWinTrackersIntoCache(gameType games.GameType) {

	log.Debug("loadWinTrackersIntoCache: Started caching for game type: " + gameType.Code)

	var statusKey string = gameType.Code + "_CACHE_STATUS"

	cachedFeedStatus, found := WinTrackersCache.Get(statusKey)
	if found {
		if cachedFeedStatus.(string) == "LOADING" {
			// another go routine already in the process of loading
			return
		}
	}

	// We are going to set the "LOADING" flag inside the statuc cache key,
	// with auto-expiry in 60 seconds. This means, that if anything happens during the cache
	// loading process, the LOADING flag will only gets stuck as true for a max of 60 seconds
	WinTrackersCache.Set(statusKey, "LOADING", 60*time.Second)

	// make sure we remove the status cache
	defer WinTrackersCache.Delete(statusKey)

	feedToBeCached, err := winTrackers.NewWinTracker(gameType, gameType.Name+" feed", false, false)
	if err != nil {
		log.Error("loadWinTrackersIntoCache go routine called from server.WinTrackers route - NewWinTrackers", err)
		return
	}

	// notify the feed that it's in cache storing mode
	feedToBeCached.SetIsCached(true)

	feedToBeCached.SetRequestParam("game", gameType.Code)

	// proceed with loading the feed
	err = feedToBeCached.LoadFeed()
	if err != nil {
		log.Error("loadWinTrackersIntoCache go routine called from server.WinTrackers route - feedToBeCached.LoadFeed()", err)
		return
	}

	// store the feed into cache
	WinTrackersCache.Set(gameType.Code, feedToBeCached, 0)

	log.Debug("loadWinTrackersIntoCache: Finished caching for game type: " + gameType.Code)

}
