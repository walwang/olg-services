package server

import (
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/lotteries/games"
	"olg-services/lotteries/numbersEverWon"
	"olg-services/publicis/utils/log"
	"time"

	"github.com/julienschmidt/httprouter"
)

func NumbersEverWon(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	startTime := time.Now()
	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(startTime, "server -> router: NumbersEverWon")
	}

	// initial status code set to 200 OK
	httpStatusCode := 200

	gameType, ok := games.GameTypes[params.ByName("gameType")]

	// in dummy mode, allow the "timeout" value as a special case, to simulate server timeouts
	if config.GlobalConfig.Environment.IsDummyMode && params.ByName("gameType") == "timeout" {
		// change the gameType code to "timeout" so the appropriate fake feed is initialized
		gameType.Code = "timeout"

	} else {

		// verify the validity of the game type param
		if ok == false {

			// set the JSON header with the status code
			WriteJSONStatus(w, httpStatusCode)

			err := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, "The specified gameType parameter is not a recognized value")
			if err != nil {
				log.Error("server.NumbersEverWon route - WriteJSONError", err)
			}
			return
		}

	}

	feed, err := numbersEverWon.NewNumbersEverWon(gameType, gameType.Name+" feed", false, false)
	if err != nil {
		log.Error("server.NumbersEverWon route - NewNumbersEverWonNumbers", err)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		jsonErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, err.Error())
		if jsonErr != nil {
			log.Error("server.NumbersEverWon route - WriteJSONError", jsonErr)
		}

		return
	}

	// load the request parameters into the feed
	reqErr := base.LoadRequestParams(feed, r)
	if reqErr != nil {

		log.Error("server.NumbersEverWon route - feed.LoadRequestParams", reqErr)

		// set the JSON header with the status code
		WriteJSONStatus(w, httpStatusCode)

		wjErr := WriteJSONError(w, constants.FEED_STATUS_CODE_INVALID_PARAMETER, reqErr.Error())
		if wjErr != nil {
			log.Error("server.NumbersEverWon route - WriteJSONError", wjErr)
		}
		return

	}

	err = feed.LoadFeed()
	if err != nil {
		log.Error("server.NumbersEverWon route - feed.LoadFeed()", err)
	}

	// set the JSON header with the status code
	WriteJSONStatus(w, httpStatusCode)

	elapsed := time.Since(startTime)
	snappyCompressionEnabled := false
	err = JSONPrint(w, feed, snappyCompressionEnabled, &elapsed)
	if err != nil {
		log.Error("server.NumbersEverWon route - JSONPrint", err)
		return
	}
}
