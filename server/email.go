package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"olg-services/constants"
	"olg-services/proline/email"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/gomail.v2"
)

//email to list of friends
func SendEmailMyBetsToFriends(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	var eventOutput email.XMLSendEmailOutput

	var formData = r.FormValue("data")
	var rEmail email.EmailBets
	for key, _ := range r.Form {
		err := json.Unmarshal([]byte(key), &rEmail)
		if err != nil {
			email.DebugFormatToEmailLogfile("EmailMyBetsToFriends: r.FormValue(data) \t  TIME:\t  %s\n", formData, time.Now())
			log.Error(constants.EMAIL_UNMARSHALING_INPUT_TO_EMAIL_OBJECT_ERROR, err)
			eventOutput.Code = constants.EMAIL_UNMARSHALING_INPUT_TO_EMAIL_OBJECT_ERROR_CODE
			eventOutput.Status = constants.EMAIL_UNMARSHALING_INPUT_TO_EMAIL_OBJECT_ERROR
			JSONPrint(w, eventOutput, false, nil)
			return
		}
	}
	//read email ticket template from cache
	ticket, found := email.GetOlgParamFromCache(email.OLG_CACHE_KEY_TICKET_TEMPLATE)
	if found == false {
		eventOutput.Code = constants.OLG_READ_DATA_FROM_CACHE_ERROR_CODE
		eventOutput.Status = constants.OLG_READ_DATA_FROM_CACHE_ERROR
		JSONPrint(w, eventOutput, false, nil)
		return
	}
	//read email bet template from cache
	bet, found := email.GetOlgParamFromCache(email.OLG_CACHE_KEY_BET_TEMPLATE)
	if found == false {
		eventOutput.Code = constants.OLG_READ_DATA_FROM_CACHE_ERROR_CODE
		eventOutput.Status = constants.OLG_READ_DATA_FROM_CACHE_ERROR
		JSONPrint(w, eventOutput, false, nil)
		return
	}
	//email recipients
	var recipients []string = rEmail.Request.Data.Recipients
	//build email content by cards/bets info
	var vals []string
	var vhtml string = string(ticket)
	var betsTotal = ""
	for _, value := range rEmail.Request.Data.Cards {
		vhtml = strings.Replace(vhtml, "{{num}}", value.Num, len(value.Num))
		vhtml = strings.Replace(vhtml, "{{date}}", value.Date, len(value.Date))
		vhtml = strings.Replace(vhtml, "{{datetime}}", value.CloseDate, len(value.CloseDate))
		vhtml = strings.Replace(vhtml, "{{period}}", value.PlayPeriod, len(value.PlayPeriod))
		vhtml = strings.Replace(vhtml, "{{wager}}", value.Wager, len(value.Wager))
		for _, value2 := range value.Bets {
			var bet2 string = string(bet)
			bet2 = strings.Replace(bet2, "{{num}}", value2.Num, len(value2.Num))
			bet2 = strings.Replace(bet2, "{{hometeam}}", value2.HomeTeam, len(value2.HomeTeam))
			bet2 = strings.Replace(bet2, "{{awayteam}}", value2.AwayTeam, len(value2.AwayTeam))
			bet2 = strings.Replace(bet2, "{{datetime}}", value2.DateTime, len(value2.DateTime))
			bet2 = strings.Replace(bet2, "{{sport}}", value2.Sport, len(value2.Sport))
			bet2 = strings.Replace(bet2, "{{winner}}", value2.Winner, len(value2.Winner))
			vals = []string{betsTotal, bet2}
			betsTotal = strings.Join(vals, "")
		}
	}
	//insert bets into card
	vhtml = strings.Replace(vhtml, "{{bets}}", betsTotal, len(betsTotal))
	//send email to all recipents
	res := SendEmailToMultipleRecipients(recipients, vhtml)
	if res == true {
		eventOutput.Code = constants.EMAIL_STATUS_CODE_OK
		eventOutput.Status = constants.EMAIL_STATUS_MESSAGE_OK
	} else {
		eventOutput.Code = constants.OLG_EMAIL_SEND_GENERIC_ERROR_CODE
		eventOutput.Status = constants.OLG_EMAIL_SEND_GENERIC_ERROR
	}
	JSONPrint(w, eventOutput, false, nil)
	return
}

//start js client to make ajax call with json param to EmailMyBetsToFriends
func StartJSClient(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	//var input = params[0].Value
	//fmt.Printf("params httprouter.Params \t %s \n", input)
	var page = `
		<html>
           <head>
             <script type="text/javascript"
               src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js">
             </script>
             <style> 
               div {
                 font-family: "Times New Roman", Georgia, Serif;
                 font-size: 2em;
                 width: 13.3em;
                 padding: 8px 8px; 
                 border: 2px solid #2B1B17;
                 border-radius: 10px;
                 color: #2B1B17;
                 text-shadow: 1px 1px #E5E4E2;
                 background: #FFFFFF;
               }
             </style>
           </head>
           <body>
           <h2>SEND EMAIL TO YOUR FRIENDS!</h2>
           <div id="output"></div>
           <script type="text/javascript">
	
             $(document).ready(function () {
               //$("#output").append("Waiting for system time..");
               //setInterval("delayedPost()", 100000);
             });
			var data1  = 
			{
				"request": 
				{
				  "data": 
					{
						"recipients" :  ["peter.yanitsky@gmail.com", "peter.yanitsky@nurun.com"],
						"cards" : [
							{
					 			"num": "41",
					 			"name": "Hockey Card",
					 			"date": "WED DEC 9",
					 			"datetime": "WED DEC 9 7:00PM",
					 			"period": "Dec 09 - Dec 09",
					 			"wager": "$250.00",
					 			"bets" : 
								[
					 				{"num": "No.1","sport":"HKY","hometeam":"NEW YORK-I","awayteam":"PHILADELPHIA","datetime":"THU DEC 10","winner":"CLB WIN (H)"},
					 				{"num": "No.2","sport":"HKY","hometeam":"DETROIT","awayteam":"WASHINGTON","datetime":"THU DEC 10","winner":"CLB WIN (H)"},
					 				{"num": "No.3","sport":"HKY","hometeam":"LOS ANGELES","awayteam":"COLUMBUS","datetime":"THU DEC 10","winner":"CLB WIN (H)"},
					 				{"num": "No.4","sport":"HKY","hometeam":"TORONTO","awayteam":"COLUMBUS","datetime":"THU DEC 10","winner":"CLB WIN (H)"},
					 				{"num": "No.5","sport":"CFB","hometeam":"UTAH","awayteam":"BRIGHAM YOUNG","datetime":"THU DEC 12","winner":"CLB WIN (H)"}
								]
				 			}
						]
					}
				}
			}
			function delayedPost() 
			{
			    $.post(
					"http://localhost:81/service/proline/email/sendemail/FFFF", 
					JSON.stringify(data1), 
					function(data, status) {
						$("#output").empty();
			            //$("#output").html(status);
			            $("#output").html(data);
			    	});
			}	
           </script>
			<div>
				<input type="button" onclick="delayedPost()">Send email</input>
			</div>
           </body>
        </html>`
	fmt.Fprint(w, page)
}

//send email to one recipient
func SendEmail(recipient string, vhtml string) {
	m := gomail.NewMessage()
	//read email-from from cache
	from, found := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_FROM)
	if found == false {
		return
	}
	m.SetHeader("From", from)
	m.SetHeader("To", recipient)
	//read email-subject from cache
	subject, found := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_SUBJECT)
	if found == false {
		return
	}
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", vhtml)
	//read email-server from cache
	server, found := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_SERVER)
	if found == false {
		return
	}
	//read email-port from cache
	port, found := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_PORT)
	if found == false {
		return
	}
	//read email-user from cache
	user, found := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_USER)
	if found == false {
		return
	}
	//read email-password from cache
	password, found := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_PASSWORD)
	if found == false {
		return
	}
	var iport, err = strconv.Atoi(port)
	if err != nil {
		log.NewError("SendEmail: error by converting port number to integer", err)
		return
	}
	d := gomail.NewPlainDialer(server, iport, user, password)
	//send the email
	if err := d.DialAndSend(m); err != nil {
		log.NewError("SendEmail: error by sending email", err)
		return
	}
}

//send email to multiple recipients
func SendEmailToMultipleRecipients(recipients []string, vhtml string) bool {
	//	var timeBefore int64 = GetTimeString()
	m := gomail.NewMessage()
	//read email-from from cache
	from, result := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_FROM)
	if result == false {
		return result
	}
	m.SetHeader("From", from)
	m.SetHeader("To", recipients...)
	//read email-subject from cache
	subject, result := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_SUBJECT)
	if result == false {
		return result
	}
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", vhtml)
	//read email-server from cache
	server, result := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_SERVER)
	if result == false {
		return result
	}
	//read email-port from cache
	port, result := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_PORT)
	if result == false {
		return result
	}
	//read email-user from cache
	user, result := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_USER)
	if result == false {
		return result
	}
	//read email-password from cache
	password, result := email.GetOlgParamFromCache(email.OLG_CACHE_EMAIL_PASSWORD)
	if result == false {
		return result
	}
	var iport, err = strconv.Atoi(port)
	if err != nil {
		log.Error("SendEmailToMultipleRecipients: error by converting port number to integer", err)
		return false
	}
	//	var timeAfter int64 = GetTimeString()
	//	email.DebugFormatToEmailLogfile("TIME ELAPSED 1 : \t %d \n", timeAfter-timeBefore)
	//asynchronous sending email to the list of recipients
	var wg sync.WaitGroup
	wg.Add(2)
	result = true
	go func() {
		defer wg.Done()
		d := gomail.NewPlainDialer(server, iport, user, password)
		if d == nil {
			log.Error("SendEmailToMultipleRecipients: error by NewPlainDialer", nil)
			result = false
			return
		}
		//send the email
		if err := d.DialAndSend(m); err != nil {
			log.Error("SendEmailToMultipleRecipients: error by sending email", err)
			result = false
			return
		}
	}()
	//	var timeAfter2 int64 = GetTimeString()
	//	email.DebugFormatToEmailLogfile("TIME ELAPSED 2 : \t %d \n", timeAfter2-timeBefore)
	return result
}

//get time in milliseconds
func GetTimeString() int64 {
	now := time.Now()
	// correct way to convert time to millisecond - with UnixNano()
	unixNano := now.UnixNano()
	umillisec := unixNano / 1000000
	//fmt.Printf("\n umillisec =  %d ", umillisec)
	return umillisec
}
