package base

import (
	"olg-services/cronJob"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"time"
)

type BaseLotteriesStruct struct {
	cronJob.BaseCronJobStruct

	// Field used for game feeds. It should be instantiated with the game code
	GameType games.GameType `json:"-"`

	FeedOptions FeedOptions `json:"-"`

	LastOkDate        time.Time `json:"-"`
	LastDateProcessed time.Time `json:"-"`
	LastErrorMessage  string    `json:"-"`
}

// Overriding the default LogError for lotteries jobs. If the status is faulty already,
// do not send the emails as it is assumed that the first one had already been sent.
func (cronJobStruct *BaseLotteriesStruct) LogRecurringError(errorMessage string, err error) {

	log.Error(errorMessage, err)

	if cronJobStruct.GetJobStatus() == cronJob.JOB_STATUS_FAULTY {
		return
	}

	cronJobStruct.SetJobStatus(cronJob.JOB_STATUS_FAULTY)
	cronJobStruct.LogEmailOrSmsError(errorMessage, err)

}

func (cronJobStruct *BaseLotteriesStruct) LogRecoveryNoticeAndSetJobStatusToRunning(recoveryMessage string) {

	cronJobStruct.SetJobStatus(cronJob.JOB_STATUS_RUNNING)
	cronJobStruct.LogEmailOrSmsInfo(recoveryMessage)

}

// implement the CronJobStatus interface
func (feed *BaseLotteriesStruct) GetLastCorrectlyProcessedDate() string {

	return feed.LastOkDate.Format(time.RFC3339)
}

func (feed *BaseLotteriesStruct) SetLastCorrectlyProcessedDate(t time.Time) {

	feed.LastOkDate = t
}

func (feed *BaseLotteriesStruct) GetLastDateProcessed() string {
	return feed.LastDateProcessed.Format(time.RFC3339)
}

func (feed *BaseLotteriesStruct) SetLastDateProcessed(t time.Time) {
	feed.LastDateProcessed = t
}

func (feed *BaseLotteriesStruct) GetLastErrorMessage() string {
	return feed.LastErrorMessage
}

func (feed *BaseLotteriesStruct) SetLastErrorMessage(errMessage string) {
	feed.LastErrorMessage = errMessage
}

func (feed *BaseLotteriesStruct) GetGameCode() string {
	return feed.GameType.Code
}
