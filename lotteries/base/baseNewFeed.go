package base

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"net/url"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/publicis/utils/charset"
	_ "olg-services/publicis/utils/charset/data"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"
)

// this type maps the new format of feeds produced by OLG for the redesign project
type BaseNewFeed struct {
	BaseLotteriesStruct

	Response xml.Name `xml:"response" json:"-"`

	StatusCode    string `xml:"statusCode" json:"code"`
	StatusMessage string `json:"status"`
	ErrorMessage  string `json:"message,omitempty"`

	// this should be set to true when the master (whole) feed is loaded into cache,
	// in order to trigger date sorting, trimming and other feed loading functionality
	IsCachedVersion bool `json:"-"`

	InputParams *url.Values `json:"-"`

	AdditionalParams *url.Values `json:"-"`
}

// Returns the current status code returned from the feed. If the code is not
// a valid integer, return FEED_STATUS_CODE_INVALID_STATUS_CODE_RECEIVED
func (f *BaseNewFeed) GetStatusCode() int {

	statusCode, err := strconv.Atoi(f.StatusCode)
	if err != nil {
		log.Error("BaseNewFeed.GetStatusCode() - strconv.Atoi", err)
		return constants.FEED_STATUS_CODE_INVALID_STATUS_CODE_RECEIVED
	}

	return statusCode

}

func (f *BaseNewFeed) SetIsCached(isCached bool) {

	f.IsCachedVersion = isCached

}

func (f *BaseNewFeed) SetStatusCode(statusCode int) {

	f.StatusCode = strconv.Itoa(statusCode)

}

func (f *BaseNewFeed) GetStatusMessage() string {

	return f.StatusMessage
}

func (f *BaseNewFeed) SetStatusMessage(statusMessage string) {

	f.StatusMessage = statusMessage

}

func (f *BaseNewFeed) GetErrorMessage() string {

	return f.ErrorMessage
}

func (f *BaseNewFeed) SetErrorMessage(errorMessage string) {

	f.ErrorMessage = errorMessage

}

func (f *BaseNewFeed) GetFeedUrl() string {

	return "base_url_please_replace"
}

// Part of the interfaces.NewXmlFeed interface
// Returns true by default, descendant structs must implement properly
func (feed *BaseNewFeed) IsValid() bool {
	return true
}

// Part of the interfaces.NewXmlFeed interface
// Returns nil by default, descendant structs must implement properly
func (feed *BaseNewFeed) LoadFeed() error {
	return nil
}

// Base implementation of ValidateRequestParams method
// that just returns constants.FEED_STATUS_CODE_OK and constants.FEED_STATUS_MESSAGE_OK.
// Custom feed structures will need to override this method.
func (feed *BaseNewFeed) ValidateRequestParams() (int, string) {

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

func (feed *BaseNewFeed) SetRequestParams(request *http.Request) {

	feed.InputParams = &request.Form

}

// Allows custom parameters to be passed to the feed
func (feed *BaseNewFeed) SetRequestParam(key string, value string) {

	if feed.AdditionalParams == nil {
		feed.AdditionalParams = &url.Values{}
	}

	feed.AdditionalParams.Set(key, value)

}

// Search the request parameters first, then the additional parameters
// (the ones introduced programatically during the feed processing).
// If nothing is found, an empty string is returned.
func (feed *BaseNewFeed) GetRequestParam(key string) string {

	var value string = ""

	if feed.InputParams != nil {

		value = feed.InputParams.Get(key)

		if value == "" && feed.AdditionalParams != nil {
			value = feed.AdditionalParams.Get(key)
		}

	}

	return value
}

// Allows parameters to be passed to the feed via HTTP GET or POST.
// Each feed struct can override this as needed
func LoadRequestParams(feedToValidate interfaces.NewXmlFeed, request *http.Request) error {

	if request == nil {
		return nil
	}

	if err := request.ParseForm(); err != nil {
		return log.NewError("LoadRequestParams - req.ParseForm()", err)
	}

	feedToValidate.SetRequestParams(request)

	// if there are parameters passed in the form, attempt to call the associated feed
	// ValidateRequestParams method.
	if &request.Form != nil {

		code, message := feedToValidate.ValidateRequestParams()
		if code != constants.FEED_STATUS_CODE_OK {
			return log.NewErrorLocal("Invalid parameter", message)
		}

	}

	return nil
}

// Loads the xml feed from the specified url and loads it in the specified feedModule
// The additional feedOptions param specifies the http or https client transport and net options
func LoadXMLWithFeedOptions(feedOptions FeedOptions, feedModule interfaces.NewXmlFeed, url string) error {

	var feedClientTimeoutSeconds int = config.GlobalConfig.Environment.DefaultFeedTimeout
	var timeout time.Duration

	// attempt a default of 15 seconds to timeout from calling feeds
	// or use the timeout inside the feed options if greater than zero
	if feedOptions.ClientTimeout > 0 {
		timeout = time.Duration(time.Duration(feedOptions.ClientTimeout) * time.Second)
	} else {
		timeout = time.Duration(time.Duration(feedClientTimeoutSeconds) * time.Second)
	}

	var client http.Client

	// if http is used, a custom transport is needed
	if feedOptions.IsHttps {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: feedOptions.InsecureSkipVerify},
		}

		client = http.Client{
			Timeout:   timeout,
			Transport: tr,
		}

	} else {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = http.Client{
			Timeout:   timeout,
			Transport: tr,
		}

	}

	return loadXML(client, feedModule, url)
}

// Loads the xml feed from the specified url and loads it in the specified feedModule
func LoadXML(feedModule interfaces.NewXmlFeed, url string) error {

	var feedClientTimeoutSeconds int = config.GlobalConfig.Environment.DefaultFeedTimeout

	// the timeout is in the settings.conf in the defaultFeedTimeout key
	timeout := time.Duration(time.Duration(feedClientTimeoutSeconds) * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	return loadXML(client, feedModule, url)

}

// Loads the xml feed from the specified url and loads it in the specified feedModule
func loadXML(client http.Client, feedModule interfaces.NewXmlFeed, url string) error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> baseNewFeed: LoadXML for url: "+url)
	}

	timeoutCheck := time.Now()
	resp, err := client.Get(url)

	if err != nil {

		feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_HTTP_ERROR)

		elapsedTimeoutCheck := time.Since(timeoutCheck)
		if elapsedTimeoutCheck.Seconds() >= client.Timeout.Seconds() {
			feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_HTTP_CLIENT_TIMEOUT_ERROR)
		}

		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
		feedModule.SetErrorMessage(err.Error())

		return log.NewError("LoadXML Error at http.Get", err)
	}

	defer resp.Body.Close()

	// indicate we should use ioutilReadAll and not use a streamed
	// approach when instantiating the xml decoder
	// the tests indicate that for small payload the ioutil.ReadAll is more than
	// twice as fast as passing the body to the xml decoder as a Reader interface
	httpBodyReadAllApproach := true

	var decoder *xml.Decoder

	if httpBodyReadAllApproach {

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {

			feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_HTTP_ERROR)
			feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
			feedModule.SetErrorMessage(err.Error())

			return log.NewError("LoadXML Error at ioutil.ReadAll", err)
		}

		reader := bytes.NewReader(body)
		decoder = xml.NewDecoder(reader)

	} else {
		decoder = xml.NewDecoder(resp.Body)
	}

	decoder.CharsetReader = charset.NewReader
	err = decoder.Decode(feedModule)
	if err != nil {

		feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_XML_DECODE_ERROR)
		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
		feedModule.SetErrorMessage(err.Error())

		return log.NewError("LoadXML Error at decoder.Decode", err)
	}

	// because the feeds do not have a schema it's very possible that
	// after decoding, a different, invalid xml or html was ingested
	// without any errors, and the data structure is simply empty
	// we need to check if we are good
	if feedModule.IsValid() == false {
		feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_XML_DECODE_ERROR)
		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
		feedModule.SetErrorMessage("Invalid xml or html was ingested - please check the feed url")
		return log.NewErrorLocal("LoadXML Error post decoder.Decode", "Invalid xml or html was ingested from "+url)
	}

	// normally after the feed has been decoded, the status code arriving should be 0 (OK)
	if feedModule.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
		feedModule.SetErrorMessage("")
	}

	return nil

}

// Constructs an Http GET query string, including the initial question mark character.
// Expects a variadic list of keyValuePair strings in the "k=v" format
func (f *BaseNewFeed) GetQueryParams(keyValuePairs ...string) string {

	var entireString string = "?"

	for i := range keyValuePairs {
		entireString = entireString + keyValuePairs[i] + "&"
	}

	if len(entireString) > 1 {
		// get rid of the final &
		entireString = strings.TrimRight(entireString, "&")
	}

	return entireString

}

// Constructs an k=v string out of the key and value, and returns it e.g. param1=abc
func (f *BaseNewFeed) KVPair(key, value string) string {
	return key + "=" + value
}

// Constructs an k=v string out of the key and value, and returns it e.g. param1=abc
// If value is empty, returns an overall empty string.
func (f *BaseNewFeed) KVPairOmitEmpty(key, value string) string {

	if value == "" {
		return ""
	}
	return key + "=" + value
}
