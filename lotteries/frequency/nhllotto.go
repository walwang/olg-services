package frequency

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"sort"
	"strings"
)

/* Types */

// Main structure of the feed for Lotto 69 number frequency
type NHLLottoWinningFrequency struct {

	// embed the Frequency base struct
	FrequencyBase

	NHLLottoFrequency NHLLottoFrequency `xml:"winningFrequency" json:"data"`
}

// This corresponds to the unique "winningFrequency" xml element in the Lotto MAX feed
type NHLLottoFrequency struct {
	XMLName         xml.Name  `xml:"winningFrequency" json:"-"`
	NHLLottoElement *NHLLotto `xml:"nhllotto" json:"nhllotto,omitempty"`
}

// This corresponds to the unique "NHLLotto" xml element in the Lotto MAX feed
type NHLLotto struct {
	XMLName xml.Name `xml:"nhllotto" json:"-"`

	// The collection of team numbers
	Items []Item `xml:"item" json:"items"`

	// The collection of team names
	Teams []Team `xml:"team" json:"teams"`
}

// This corresponds to one of the many "team" xml elements in the NHL Lotto number frequency feeds
type Team struct {
	XMLName xml.Name `xml:"team" json:"-"`

	// The name (such as team name) for certain games (e.g. NHL Lotto)
	Name string `xml:"name" json:"name,omitempty"`

	// The frequency for the given number
	Frequency int `xml:"frequency" json:"frequency"`
}

/* BEGIN: Team Sorting Section */

// ByTeamName implements sort.Interface for []Team based on
// the Name field.
type ByTeamName []Team

func (a ByTeamName) Len() int           { return len(a) }
func (a ByTeamName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByTeamName) Less(i, j int) bool { return a[i].Name < a[j].Name }

// ByTeamFrequency implements sort.Interface for []Team based on
// the Frequency field.
type ByTeamFrequency []Team

func (a ByTeamFrequency) Len() int           { return len(a) }
func (a ByTeamFrequency) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByTeamFrequency) Less(i, j int) bool { return a[i].Frequency < a[j].Frequency }

func SortTeam(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

// Sort the items based on the sortByValue parameter. That one can be "freq" or "name"
func SortTeams(teams []Team, sortByValue string) {

	if sortByValue == "" {
		return
	}

	if strings.ToLower(sortByValue) == "freq" {
		SortTeam(ByTeamFrequency(teams))
	} else if strings.ToLower(sortByValue) == "name" {
		SortTeam(ByTeamName(teams))
	}

}

/* END: Item Sorting Section */

/* Methods */

func (feed *NHLLottoWinningFrequency) GetFeedUrl() string {

	// until OLG provides the unified feeds, hard code this here for dev mode
	if config.GlobalConfig.Environment.IsDummyMode {
		//return return config.GlobalConfig.LotteryFeedURLs.WinningFrequency
		return "https://mat.olg.ca/MAT/XMLRetriever"
	}

	return config.GlobalConfig.LotteryFeedURLs.WinningFrequency
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *NHLLottoWinningFrequency) IsValid() bool {
	return feed.NHLLottoFrequency.NHLLottoElement != nil
}

// Loads the past winning numbers NHLLotto feed from OLG
func (feed *NHLLottoWinningFrequency) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> NHLLottoWinningFrequency: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := NHLLottoWinningFrequency{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareFrequencyFeed(&feed.BaseNewFeed, constants.GAME_CODE_NHLLOTTO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.NHLLottoFrequency = newFeedData.NHLLottoFrequency
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws

	// if the request contains a sortBy parameter then sort according to the value
	SortItems(feed.NHLLottoFrequency.NHLLottoElement.Items, feed.GetRequestParam("sortBy"))
	SortTeams(feed.NHLLottoFrequency.NHLLottoElement.Teams, feed.GetRequestParam("sortBy"))

	return nil

}

// Sorts the values by name, if name exists for this game type.
// Implements the IWinningFrequency interface.
func (feed *NHLLottoWinningFrequency) SortBy(sortByValue string) {

	// if the request contains a sortBy parameter then sort according to the value
	SortItems(feed.NHLLottoFrequency.NHLLottoElement.Items, feed.GetRequestParam("sortBy"))
	SortTeams(feed.NHLLottoFrequency.NHLLottoElement.Teams, feed.GetRequestParam("sortBy"))
}
