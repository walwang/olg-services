package frequency

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

// Main structure of the feed for Lottario number frequency
type LottarioWinningFrequency struct {

	// embed the Frequency base struct
	FrequencyBase

	LottarioFrequency LottarioFrequency `xml:"winningFrequency" json:"data"`
}

// This corresponds to the unique "winningFrequency" xml element in the Lottario feed
type LottarioFrequency struct {
	XMLName         xml.Name  `xml:"winningFrequency" json:"-"`
	LottarioElement *Lottario `xml:"lottario" json:"lottario,omitempty"`
}

// This corresponds to the unique "Lottario" xml element in the Lottario feed
type Lottario struct {
	XMLName xml.Name `xml:"lottario" json:"-"`
	Items   []Item   `xml:"item" json:"items"`
}

/* Methods */

func (feed *LottarioWinningFrequency) GetFeedUrl() string {

	// until OLG provides the unified feeds, hard code this here for dev mode
	if config.GlobalConfig.Environment.IsDummyMode {
		//return return config.GlobalConfig.LotteryFeedURLs.WinningFrequency
		return "https://mat.olg.ca/MAT/XMLRetriever"
	}

	return config.GlobalConfig.LotteryFeedURLs.WinningFrequency
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LottarioWinningFrequency) IsValid() bool {
	return feed.LottarioFrequency.LottarioElement != nil
}

// Loads the past winning numbers Lottario feed from OLG
func (feed *LottarioWinningFrequency) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LottarioWinningFrequency: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := LottarioWinningFrequency{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareFrequencyFeed(&feed.BaseNewFeed, constants.GAME_CODE_LOTTARIO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LottarioFrequency = newFeedData.LottarioFrequency
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws

	// if the request contains a sortBy parameter then sort according to the value
	SortItems(feed.LottarioFrequency.LottarioElement.Items, feed.GetRequestParam("sortBy"))

	return nil

}

// Sorts the values by name, if name exists for this game type.
// Implements the IWinningFrequency interface.
func (feed *LottarioWinningFrequency) SortBy(sortByValue string) {

	// if the request contains a sortBy parameter then sort according to the value
	SortItems(feed.LottarioFrequency.LottarioElement.Items, feed.GetRequestParam("sortBy"))
}
