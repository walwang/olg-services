package frequency

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"sort"
	"strings"
	"time"

	cache "github.com/pmylund/go-cache"
)

const DRAW_DATE_PARSE_FORMAT = "2006-01-02"

/* BEGIN: FrequencyBase */

// Base parent structure for "Winning Number Frequency" feed.
// It helps maintain a single validation point, as this feed doesn't have much
// parameter variability.
// It also helps with cacheability routines, by implementing interfaces.IFeedCache
type FrequencyBase struct {

	// embed the base new feeds
	base.BaseNewFeed

	CacheStore *cache.Cache `json:"-"`
}

// Common validation base method for "Winning Number Frequency" feed.
// Generally, there should be no need for any validation for this feed.
func (feed *FrequencyBase) ValidateRequestParams() (int, string) {

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Implements interfaces.ICacheable. Sets the pointer to cache store created for this particular type of feed
func (feed *FrequencyBase) SetCacheStore(store interface{}) {

	if store != nil {

		cacheStore, found := store.(*cache.Cache)
		if found {
			feed.CacheStore = cacheStore
		}
	}

}

// Implements interfaces.ICacheable. Returns the pointer to cache store created for this particular type of feed
func (feed *FrequencyBase) GetCacheStore() interface{} {

	return feed.CacheStore

}

/* END: FrequencyBase */

// This corresponds to one of the many "item" xml elements in the number frequency feeds
type Item struct {
	XMLName xml.Name `xml:"item" json:"-"`

	// The name (such as team name) for certain games (e.g. NHL Lotto)
	Name string `xml:"name" json:"name,omitempty"`

	// The actual number drawn based on game type
	Number int `xml:"number" json:"number"`

	// The the card code, for games such as Poker Lotto
	Card string `xml:"card" json:"card,omitempty"`

	// The frequency for the given number
	Frequency int `xml:"frequency" json:"frequency"`

	// The bonus frequency for the given number, if applicable to the game type
	BonusFrequency int `xml:"bonusFrequency" json:"bonusFrequency,omitempty"`

	// The bonus draw frequency for the given number, if applicable to the game type
	BonusDrawFrequency int `xml:"bonusDrawFrequency" json:"bonusDrawFrequency,omitempty"`

	// Pick two, three and four game type related fields
	FrequencyFirst  string `xml:"frequencyFirst" json:"frequencyFirst,omitempty"`
	FrequencySecond string `xml:"frequencySecond" json:"frequencySecond,omitempty"`
	FrequencyThird  string `xml:"frequencyThird" json:"frequencyThird,omitempty"`
	FrequencyFourth string `xml:"frequencyFourth" json:"frequencyFourth,omitempty"`
}

/* BEGIN: Item Sorting Section */

// ByItemName implements sort.Interface for []Item based on
// the Name field.
type ByItemName []Item

func (a ByItemName) Len() int           { return len(a) }
func (a ByItemName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByItemName) Less(i, j int) bool { return a[i].Name < a[j].Name }

// ByItemNumber implements sort.Interface for []Item based on
// the Number field.
type ByItemNumber []Item

func (a ByItemNumber) Len() int           { return len(a) }
func (a ByItemNumber) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByItemNumber) Less(i, j int) bool { return a[i].Number < a[j].Number }

// ByItemFrequency implements sort.Interface for []Item based on
// the Frequency field.
type ByItemFrequency []Item

func (a ByItemFrequency) Len() int           { return len(a) }
func (a ByItemFrequency) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByItemFrequency) Less(i, j int) bool { return a[i].Frequency < a[j].Frequency }

// ByItemCard implements sort.Interface for []Item based on
// the Card field (for games such as Poker Lotto).
type ByItemCard []Item

func (a ByItemCard) Len() int           { return len(a) }
func (a ByItemCard) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByItemCard) Less(i, j int) bool { return a[i].Card < a[j].Card }

func SortItem(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

// Sort the items based on the sortByValue parameter. That one can be "freq", "name", or "num".
func SortItems(items []Item, sortByValue string) {

	if sortByValue == "" {
		return
	}

	if strings.ToLower(sortByValue) == "freq" {
		SortItem(ByItemFrequency(items))
	} else if strings.ToLower(sortByValue) == "name" {
		SortItem(ByItemName(items))
	} else if strings.ToLower(sortByValue) == "num" {
		SortItem(ByItemNumber(items))
	} else if strings.ToLower(sortByValue) == "card" {
		SortItem(ByItemCard(items))
	}

}

/* END: Item Sorting Section */

/* ******************************************* */
/* BEGIN: Frequency Common Functions */
/* ******************************************* */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
// PastWinningNumbers feeds have a common set of input parameters, the
// start and end date
func PrepareFrequencyFeed(feed *base.BaseNewFeed, gameCode string) (string, base.FeedOptions) {

	var requestParams string = ""
	var testEnv, _ = config.GetEntryValueDirectlyFromConfigFile("testEnvironment")

	// prepare the request parameters
	if feed.InputParams != nil {
		requestParams = feed.GetQueryParams(
			feed.KVPair("game", gameCode),
		)
	}

	var feedOptions base.FeedOptions

	// adjust the request params and specify the env and feed type
	if feed.InputParams != nil {
		requestParams = feed.GetQueryParams(
			feed.KVPair("env", testEnv),
			feed.KVPair("feedType", "winningFrequency"),
			feed.KVPair("game", gameCode),
		)
	}

	feedOptions = base.FeedOptions{
		IsHttps:            false,
		InsecureSkipVerify: true,
	}

	// END: Test Mode Parameters

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* ******************************************* */
/* END: Frequency Common Functions   */
/* ******************************************* */
