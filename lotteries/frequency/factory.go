package frequency

import (
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"strings"

	cache "github.com/pmylund/go-cache"
)

func NewWinningFrequency(gameType games.GameType, jobName string, sendErrorEmails, sendErrorSms bool) (interfaces.NewXmlFeed, error) {

	return NewWinningFrequencyWithCache(nil, gameType, jobName, sendErrorEmails, sendErrorSms, nil)

}

func NewWinningFrequencyWithCache(cacheStore *cache.Cache, gameType games.GameType, jobName string,
	sendErrorEmails, sendErrorSms bool,
	feedToCloneFrom interfaces.IWinningFrequency) (interfaces.IWinningFrequency, error) {

	var feed interfaces.IWinningFrequency

	log.Debug("instantiating new WinningFrequency feed for game type: " + gameType.Code)

	if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTO649 {

		structFeed := &Lotto649WinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*Lotto649WinningFrequency)
			if ok {
				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.Lotto649Frequency.Lotto649Element = &Lotto649{}
				structFeed.Lotto649Frequency.Lotto649Element.Items = make([]Item, len(cloneSource.Lotto649Frequency.Lotto649Element.Items))
				copy(structFeed.Lotto649Frequency.Lotto649Element.Items, cloneSource.Lotto649Frequency.Lotto649Element.Items)
			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTOMAX {

		structFeed := &LottoMaxWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*LottoMaxWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LottoMaxFrequency.LottoMaxElement = &LottoMax{}
				structFeed.LottoMaxFrequency.LottoMaxElement.Items = make([]Item, len(cloneSource.LottoMaxFrequency.LottoMaxElement.Items))
				copy(structFeed.LottoMaxFrequency.LottoMaxElement.Items, cloneSource.LottoMaxFrequency.LottoMaxElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_NHLLOTTO {

		structFeed := &NHLLottoWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*NHLLottoWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.NHLLottoFrequency.NHLLottoElement = &NHLLotto{}
				structFeed.NHLLottoFrequency.NHLLottoElement.Items = make([]Item, len(cloneSource.NHLLottoFrequency.NHLLottoElement.Items))
				copy(structFeed.NHLLottoFrequency.NHLLottoElement.Items, cloneSource.NHLLottoFrequency.NHLLottoElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_POKERLOTTO {

		structFeed := &PokerLottoWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PokerLottoWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PokerLottoFrequency.PokerLottoElement = &PokerLotto{}
				structFeed.PokerLottoFrequency.PokerLottoElement.Items = make([]Item, len(cloneSource.PokerLottoFrequency.PokerLottoElement.Items))
				copy(structFeed.PokerLottoFrequency.PokerLottoElement.Items, cloneSource.PokerLottoFrequency.PokerLottoElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_FOUR {

		structFeed := &PickFourWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PickFourWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PickFourFrequency.PickFourElement = &PickFour{}
				structFeed.PickFourFrequency.PickFourElement.Items = make([]Item, len(cloneSource.PickFourFrequency.PickFourElement.Items))
				copy(structFeed.PickFourFrequency.PickFourElement.Items, cloneSource.PickFourFrequency.PickFourElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_THREE {

		structFeed := &PickThreeWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PickThreeWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PickThreeFrequency.PickThreeElement = &PickThree{}
				structFeed.PickThreeFrequency.PickThreeElement.Items = make([]Item, len(cloneSource.PickThreeFrequency.PickThreeElement.Items))
				copy(structFeed.PickThreeFrequency.PickThreeElement.Items, cloneSource.PickThreeFrequency.PickThreeElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_TWO {

		structFeed := &PickTwoWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PickTwoWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PickTwoFrequency.PickTwoElement = &PickTwo{}
				structFeed.PickTwoFrequency.PickTwoElement.Items = make([]Item, len(cloneSource.PickTwoFrequency.PickTwoElement.Items))
				copy(structFeed.PickTwoFrequency.PickTwoElement.Items, cloneSource.PickTwoFrequency.PickTwoElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_DAILY_KENO {

		structFeed := &DailyKenoWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*DailyKenoWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.DailyKenoFrequency.DailyKenoElement = &DailyKeno{}
				structFeed.DailyKenoFrequency.DailyKenoElement.Items = make([]Item, len(cloneSource.DailyKenoFrequency.DailyKenoElement.Items))
				copy(structFeed.DailyKenoFrequency.DailyKenoElement.Items, cloneSource.DailyKenoFrequency.DailyKenoElement.Items)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_MEGADICE {

		structFeed := &MegaDiceWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*MegaDiceWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.MegaDiceFrequency.MegaDiceElement = &MegaDice{}
				structFeed.MegaDiceFrequency.MegaDiceElement.Items = make([]Item, len(cloneSource.MegaDiceFrequency.MegaDiceElement.Items))
				copy(structFeed.MegaDiceFrequency.MegaDiceElement.Items, cloneSource.MegaDiceFrequency.MegaDiceElement.Items)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTARIO {

		structFeed := &LottarioWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*LottarioWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LottarioFrequency.LottarioElement = &Lottario{}
				structFeed.LottarioFrequency.LottarioElement.Items = make([]Item, len(cloneSource.LottarioFrequency.LottarioElement.Items))
				copy(structFeed.LottarioFrequency.LottarioElement.Items, cloneSource.LottarioFrequency.LottarioElement.Items)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_ONTARIO49 {

		structFeed := &Ontario49WinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*Ontario49WinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.Ontario49Frequency.Ontario49Element = &Ontario49{}
				structFeed.Ontario49Frequency.Ontario49Element.Items = make([]Item, len(cloneSource.Ontario49Frequency.Ontario49Element.Items))
				copy(structFeed.Ontario49Frequency.Ontario49Element.Items, cloneSource.Ontario49Frequency.Ontario49Element.Items)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_DAILYGRAND {

		structFeed := &DailyGrandWinningFrequency{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*DailyGrandWinningFrequency)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.DailyGrandFrequency.DailyGrandElement = &DailyGrand{}
				structFeed.DailyGrandFrequency.DailyGrandElement.Items = make([]Item, len(cloneSource.DailyGrandFrequency.DailyGrandElement.Items))
				copy(structFeed.DailyGrandFrequency.DailyGrandElement.Items, cloneSource.DailyGrandFrequency.DailyGrandElement.Items)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == "timeout" {
		// the timeout feed just waits 35 seconds
		feed = &TimeoutWinningFrequency{}
	} else {
		return nil, log.NewErrorLocal("WinningFrequency.NewWinningFrequency", "the specified gameType.Code ("+gameType.Code+") does not correspond to a valid game type")
	}

	// if the conversion to ICacheable interface succeeds, set the cache store
	cacheableStruct, ok := feed.(interfaces.ICacheable)
	if ok {
		cacheableStruct.SetCacheStore(cacheStore)
	}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	}

	return feed, nil

}
