package winTrackers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

// The TimeoutWinningFrequency struct is used to simulate a server timeout situation
type TimeoutWinTracker struct {

	// embed the base new feeds
	base.BaseNewFeed

	TimeoutHtml TimeoutHtml `xml:"html" json:"data"`
}

// This corresponds to the timeout "html" element in the timeout feed
type TimeoutHtml struct {
	XMLName xml.Name `xml:"html" json:"-"`
	Body    string   `xml:"body" json:"body,omitempty"`
}

/* Methods */

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *TimeoutWinTracker) IsValid() bool {
	return feed.TimeoutHtml.Body != ""
}

func (feed *TimeoutWinTracker) GetFeedUrl() string {

	// Hit the timeout server with a 35 seconds setting
	return "http://www.publicistechlabs.ca/timeout/seconds/35"
}

// Loads the past winning numbers Timeout feed from OLG
func (feed *TimeoutWinTracker) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> TimeoutWinTracker: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := TimeoutWinTracker{}

	err := base.LoadXML(&newFeedData, feed.GetFeedUrl())
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.TimeoutHtml = newFeedData.TimeoutHtml
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	return nil
}

// Sorts the values by name, if name exists for this game type.
// Implements the IWinTracker interface.
func (feed *TimeoutWinTracker) SortBy(sortByValue string) {

	// noop for timeout
}
