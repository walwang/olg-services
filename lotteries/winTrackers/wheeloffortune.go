package winTrackers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

// Main structure of the feed for Wheel Of Fortune win tracker feed
type WheelOfFortuneWinTracker struct {
	XMLName xml.Name `xml:"Wheel" json:"-"`

	// embed the WinTrackerBase base struct
	WinTrackerBase

	// Last updated date
	LastUpdatedDate string `xml:"LastUpdatedDate" json:"lastUpdatedDate"`

	// Top winning trackers
	TopWinners []Winner `xml:"TopPrizeWinners>Winner" json:"topPrizeWinners,omitempty"`
}

/* Methods */

func (feed *WheelOfFortuneWinTracker) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.WinTrackerWheelOfFortune
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *WheelOfFortuneWinTracker) IsValid() bool {
	return feed.TopWinners != nil
}

// Loads the past winning numbers WheelOfFortune feed from OLG
func (feed *WheelOfFortuneWinTracker) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> WheelOfFortuneWinTracker: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := WheelOfFortuneWinTracker{}

	// Because the win tracker feeds from OLG are old format, and do not come with a status code element,
	// we need to initialize the status code to OK then decode the feed
	// The system will override the OK with any error in case it happens.
	// In essence, we must not allow StatusCode to me empty string as it needs to be an integer.
	newFeedData.SetStatusCode(constants.FEED_STATUS_CODE_OK)

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareWinTrackerFeed(&feed.BaseNewFeed, constants.GAME_CODE_WHEEL_OF_FORTUNE)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.TopWinners = newFeedData.TopWinners
	feed.LastUpdatedDate = newFeedData.LastUpdatedDate
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws

	// if the request contains a sortBy parameter then sort according to the value
	SortWinners(feed.TopWinners, feed.GetRequestParam("sortBy"))

	return nil

}

// Sorts the values by name, if name exists for this game type.
// Implements the IWinTracker interface.
func (feed *WheelOfFortuneWinTracker) SortBy(sortByValue string) {

	// if the request contains a sortBy parameter then sort according to the value
	SortWinners(feed.TopWinners, feed.GetRequestParam("sortBy"))
}
