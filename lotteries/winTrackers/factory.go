package winTrackers

import (
	cache "github.com/pmylund/go-cache"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"strings"
)

func NewWinTracker(gameType games.GameType, jobName string, sendErrorEmails, sendErrorSms bool) (interfaces.NewXmlFeed, error) {

	return NewWinTrackerWithCache(nil, gameType, jobName, sendErrorEmails, sendErrorSms, nil)

}

func NewWinTrackerWithCache(cacheStore *cache.Cache, gameType games.GameType, jobName string,
	sendErrorEmails, sendErrorSms bool,
	feedToCloneFrom interfaces.IWinTracker) (interfaces.IWinTracker, error) {

	var feed interfaces.IWinTracker

	log.Debug("instantiating new WinTracker feed for game type: " + gameType.Code)

	if strings.ToLower(gameType.Code) == constants.GAME_CODE_NHLLOTTO {

		structFeed := &NHLLottoWinTracker{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*NHLLottoWinTracker)
			if ok {

				// perform manual cloning due to pointer references
				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LastUpdatedDate = cloneSource.LastUpdatedDate
				structFeed.TopWinners = make([]Winner, len(cloneSource.TopWinners))
				copy(structFeed.TopWinners, cloneSource.TopWinners)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_POKERLOTTO {

		structFeed := &PokerLottoWinTracker{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PokerLottoWinTracker)
			if ok {

				// perform manual cloning due to pointer references
				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LastUpdatedDate = cloneSource.LastUpdatedDate
				structFeed.TopWinners = make([]Winner, len(cloneSource.TopWinners))
				copy(structFeed.TopWinners, cloneSource.TopWinners)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_MEGADICE {

		structFeed := &MegaDiceWinTracker{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*MegaDiceWinTracker)
			if ok {

				// perform manual cloning due to pointer references
				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LastUpdatedDate = cloneSource.LastUpdatedDate
				structFeed.TopWinners = make([]Winner, len(cloneSource.TopWinners))
				copy(structFeed.TopWinners, cloneSource.TopWinners)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_WHEEL_OF_FORTUNE {

		structFeed := &WheelOfFortuneWinTracker{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*WheelOfFortuneWinTracker)
			if ok {

				// perform manual cloning due to pointer references
				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LastUpdatedDate = cloneSource.LastUpdatedDate
				structFeed.TopWinners = make([]Winner, len(cloneSource.TopWinners))
				copy(structFeed.TopWinners, cloneSource.TopWinners)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == "timeout" {
		// the timeout feed just waits 35 seconds
		feed = &TimeoutWinTracker{}
	} else {
		return nil, log.NewErrorLocal("WinTracker.NewWinTracker", "the specified gameType.Code ("+gameType.Code+") does not correspond to a valid game type")
	}

	// if the conversion to ICacheable interface succeeds, set the cache store
	cacheableStruct, ok := feed.(interfaces.ICacheable)
	if ok {
		cacheableStruct.SetCacheStore(cacheStore)
	}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	}

	return feed, nil

}
