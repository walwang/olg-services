package winTrackers

import (
	"encoding/xml"
	cache "github.com/pmylund/go-cache"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"sort"
	"strings"
	"time"
)

const DRAW_DATE_PARSE_FORMAT = "2006-01-02"

/* BEGIN: WinTrackerBase */

// Base parent structure for "Win Trackers" feed.
// It helps maintain a single validation point, as this feed doesn't have much
// parameter variability.
// It also helps with cacheability routines, by implementing interfaces.IFeedCache
type WinTrackerBase struct {

	// embed the base new feeds
	base.BaseNewFeed

	CacheStore *cache.Cache `json:"-"`
}

// Common validation base method for any of the "Win Trackers" feeds.
// Generally, there should be no need for any validation for this type of feed.
func (feed *WinTrackerBase) ValidateRequestParams() (int, string) {

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Implements interfaces.ICacheable. Sets the pointer to cache store created for this particular type of feed
func (feed *WinTrackerBase) SetCacheStore(store interface{}) {

	if store != nil {

		cacheStore, found := store.(*cache.Cache)
		if found {
			feed.CacheStore = cacheStore
		}
	}

}

// Implements interfaces.ICacheable. Returns the pointer to cache store created for this particular type of feed
func (feed *WinTrackerBase) GetCacheStore() interface{} {

	return feed.CacheStore

}

/* END: WinTrackerBase */

// This corresponds to the unique "TopPrizeWinners" xml element in the NHL Lotto feed
type TopPrizeWinners struct {
	XMLName xml.Name `xml:"TopPrizeWinners" json:"-"`
	Winners []Winner `xml:"Winner" json:"winners"`
}

// This corresponds to one of the many "Winner" xml elements in the win tracker feeds
type Winner struct {
	XMLName xml.Name `xml:"Winner" json:"-"`

	// The OLG tracker id
	PrzTRKRid string `xml:"PrzTRKRid" json:"przTRKRid,omitempty"`

	// The winning date
	Date string `xml:"Date" json:"date"`

	// The prize amount
	PrizeAmount string `xml:"PrizeAmount" json:"prizeAmount"`

	// The store name where the ticket was bought
	StoreName string `xml:"StoreName" json:"storeName,omitempty"`

	// The English language address
	AddressEN string `xml:"AddressEN" json:"addressEN,omitempty"`
	// The French language address
	AddressFR string `xml:"AddressFR" json:"addressFR,omitempty"`

	// The city where the ticket was bought
	City string `xml:"City" json:"city,omitempty"`

	// The English language prize description
	PrizeDescriptionEN string `xml:"PrizeDescriptionEN" json:"prizeDescriptionEN,omitempty"`
	// The French language prize description
	PrizeDescriptionFR string `xml:"PrizeDescriptionFR" json:"prizeDescriptionFR,omitempty"`
}

/* BEGIN: Winning Ticket Sorting Section */

// ByWinnerDate implements sort.Interface for []Winner based on
// the Date field.
type ByWinnerDate []Winner

func (a ByWinnerDate) Len() int           { return len(a) }
func (a ByWinnerDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByWinnerDate) Less(i, j int) bool { return a[i].Date < a[j].Date }

func SortWinner(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

// Sort the items based on the sortByValue parameter. That one can be "date".
func SortWinners(winners []Winner, sortByValue string) {

	if sortByValue == "" {
		return
	}

	if strings.ToLower(sortByValue) == "date" {
		SortWinner(ByWinnerDate(winners))
	}

}

/* END: Winning Ticket Sorting Section */

/* ******************************************* */
/* BEGIN: Win Tracker Common Functions */
/* ******************************************* */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
func PrepareWinTrackerFeed(feed *base.BaseNewFeed, gameCode string) (string, base.FeedOptions) {

	var requestParams string = ""

	var feedOptions base.FeedOptions

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* ******************************************* */
/* END: Win Tracker Common Functions   */
/* ******************************************* */
