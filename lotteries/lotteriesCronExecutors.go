package lotteries

import (
	"olg-services/constants"

	"olg-services/lotteries/games"
	"olg-services/lotteries/pastWinningNumbers"
	"olg-services/lotteries/winningNumbers"
)

// instantiate singleton cron job-friendly structures for various lotteries feeds

var CurrentWinningNumbers = winningNumbers.NewWinningNumbers("Winning Numbers Feed Job", true, true)

var Lotto649PastWinningNumbers *pastWinningNumbers.Lotto649PastWinningNumbers

// Initializes the singleton structures so their methods can be call from inside
// cron modules
func InitCronTestStructures() error {

	// Past winning numbers - Lotto 649
	ILotto649PastWinningNumbers, err := pastWinningNumbers.NewPastWinningNumbers(games.GameTypes[constants.GAME_CODE_LOTTO649], "Lotto 649 Past Winning Numbers Feed Job", true, true)
	if err != nil {
		return err
	} else {
		Lotto649PastWinningNumbers = ILotto649PastWinningNumbers.(*pastWinningNumbers.Lotto649PastWinningNumbers)
	}

	return nil

}
