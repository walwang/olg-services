package games

import (
	"time"
)

type GameType struct {
	OlgId int
	Name  string
	Code  string

	// this is the code used in the legacy winning numbers feed
	WinningNumbersCode string

	// the days of the week when the draw occurs (0 is Sunday, 1 is Monday and so forth)
	DrawDays []time.Weekday

	// DrawTime is an array of two int elements, where the first element is the hour (e.g. 22)
	// and the second element is the minutes (e.g. 30). For example, to express 22:30,
	// you'd have the following value: [2]int {22, 30}
	DrawTime [2]int

	HasAfternoonDraw  bool
	AfternoonDrawTime [2]int

	//hasJackpot is used to determine the calculating status
	HasJackpot bool
	Category   string
}

// Returns true if the specified weekDay is amongst the draw days of the game type.
// Sunday is 0, Monday is 1, and so forth.
func (g *GameType) IsDrawWeekday(weekDay time.Weekday) bool {

	if g == nil {
		return false
	}

	for i := range g.DrawDays {
		if weekDay == g.DrawDays[i] {
			return true
		}
	}

	return false
}

func (g *GameType) IsWithinDrawMinutesRange(currentTime time.Time, minutesRange int) bool {

	if g == nil {
		return false
	}

	if g.DrawTime[0] == -1 {
		return false
	}

	drawDate := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), g.DrawTime[0], g.DrawTime[1], 0, 0, currentTime.Location())

	if int(currentTime.Sub(drawDate).Minutes()) < minutesRange {
		return true
	}

	// if game has afternoon draw, check it as well
	if g.HasAfternoonDraw {

		afternoonTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), g.AfternoonDrawTime[0], g.AfternoonDrawTime[1], 0, 0, currentTime.Location())

		if int(currentTime.Sub(afternoonTime).Minutes()) < minutesRange {
			return true
		}
	}

	return false

}

// Each and every day
var EveryDrawDayPattern = []time.Weekday{0, 1, 2, 3, 4, 5, 6}

// Wednesdays and Saturdays
var Lotto649DrawDaysPattern = []time.Weekday{3, 6}
var Ontario49DrawDaysPattern = []time.Weekday{3, 6}

// Fridays only
var FridayDrawDaysPattern = []time.Weekday{5}

// Saturdays only
var SaturdayDrawDaysPattern = []time.Weekday{6}

// A typical 22:30 draw time array
var TenThirtyDrawTime = [2]int{22, 30}

// A typical 14:00 draw time array
var TwoPmDrawTime = [2]int{14, 00}

// Unavailable draw time
var UnavailableDrawTime = [2]int{-1, -1}

const LOTTO_GAME = "LOTTO_GAME"
const WATCH_AND_WIN = "WATCH_AND_WIN"
const DAILY = "DAILY"

var GameTypes map[string]GameType = map[string]GameType{
	"lotto649": GameType{OlgId: 1, Name: "Lotto 6/49", Code: "lotto649", HasJackpot: true, Category: LOTTO_GAME,
		WinningNumbersCode: "LOTTO 6/49", DrawDays: Lotto649DrawDaysPattern, DrawTime: TenThirtyDrawTime},

	"lottomax": GameType{OlgId: 2, Name: "LottoMAX", Code: "lottomax", HasJackpot: true, Category: LOTTO_GAME,
		WinningNumbersCode: "LOTTO MAX", DrawDays: FridayDrawDaysPattern, DrawTime: TenThirtyDrawTime},

	"nhllotto": GameType{OlgId: 3, Name: "NHL Lotto", Code: "nhllotto", HasJackpot: false, Category: WATCH_AND_WIN,
		WinningNumbersCode: "NHL LOTTO", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime},

	"pokerlotto": GameType{OlgId: 4, Name: "Poker Lotto", Code: "pokerlotto", HasJackpot: true, Category: WATCH_AND_WIN,
		WinningNumbersCode: "POKER LOTTO", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime},

	"wheeloffortune": GameType{OlgId: 5, Name: "Wheel of Fortune", Code: "wheeloffortune", HasJackpot: false, Category: WATCH_AND_WIN,
		WinningNumbersCode: "WHEEL FORTUNE LOTTO", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime},

	"pick4": GameType{OlgId: 6, Name: "Pick4", Code: "pick4", HasJackpot: false, Category: DAILY,
		WinningNumbersCode: "PICK 4", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime,
		HasAfternoonDraw: true, AfternoonDrawTime: TwoPmDrawTime},

	"pick3": GameType{OlgId: 7, Name: "Pick3", Code: "pick3", HasJackpot: false, Category: DAILY,
		WinningNumbersCode: "PICK 3", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime,
		HasAfternoonDraw: true, AfternoonDrawTime: TwoPmDrawTime},

	"pick2": GameType{OlgId: 8, Name: "Pick2", Code: "pick2", HasJackpot: false, Category: DAILY,
		WinningNumbersCode: "PICK 2", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime,
		HasAfternoonDraw: true, AfternoonDrawTime: TwoPmDrawTime},

	"dailykeno": GameType{OlgId: 9, Name: "Daily Keno", Code: "dailykeno", HasJackpot: false, Category: DAILY,
		WinningNumbersCode: "DAILY KENO", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime,
		HasAfternoonDraw: true, AfternoonDrawTime: TwoPmDrawTime},

	"livingthelife": GameType{OlgId: 10, Name: "Living the Life", Code: "livingthelife", HasJackpot: false,
		WinningNumbersCode: "LIVING THE LIFE LOTTERY", DrawTime: UnavailableDrawTime},

	"megadice": GameType{OlgId: 11, Name: "MegaDice", Code: "megadice", HasJackpot: false, Category: WATCH_AND_WIN,
		WinningNumbersCode: "MEGADICE LOTTO", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime},

	"lottario": GameType{OlgId: 12, Name: "Lottario", Code: "lottario", HasJackpot: true, Category: LOTTO_GAME,
		WinningNumbersCode: "LOTTARIO", DrawDays: SaturdayDrawDaysPattern, DrawTime: TenThirtyDrawTime},

	"ontario49": GameType{OlgId: 13, Name: "Ontario 49", Code: "ontario49", HasJackpot: true, Category: LOTTO_GAME,
		WinningNumbersCode: "ONTARIO 49", DrawDays: Ontario49DrawDaysPattern, DrawTime: TenThirtyDrawTime},

	"dailygrand": GameType{OlgId: 14, Name: "Daily Grand", Code: "dailygrand", HasJackpot: true, Category: LOTTO_GAME,
		WinningNumbersCode: "DAILY GRAND", DrawDays: EveryDrawDayPattern, DrawTime: TenThirtyDrawTime},
}

// The "name" element in winning numbers legacy feed is different from the new feeds.
// This is stored int he WinningNumbersCode field. This function finds the GameType structure with
// this code inside the global GameTypes map. If the second return value is false, it means it was not found.
func FindGameTypeByWinningNumberCode(winningNumbersCode string) (GameType, bool) {

	for i := range GameTypes {

		if GameTypes[i].WinningNumbersCode == winningNumbersCode {
			return GameTypes[i], true
		}
	}

	return GameType{}, false

}
