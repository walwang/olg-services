package instantUnclaimed

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"

	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type InstantUnclaimedPrizes struct {

	// embed the InstantUnclaimedBase base struct
	InstantUnclaimedBase

	UnclaimedPrizes *UnclaimedPrizesContainer `xml:"instantUnclaimedPrizes" json:"data"`
}

// This corresponds to the unique "instantUnclaimedPrizes" xml element in the feed
type UnclaimedPrizesContainer struct {
	XMLName xml.Name         `xml:"instantUnclaimedPrizes" json:"-"`
	Prizes  []UnclaimedPrize `xml:"instantUnclaimedPrize" json:"prizes,omitempty"`
}

// This corresponds to the unique "instantUnclaimedPrize" xml element in the instantUnclaimedPrizes xml container
type UnclaimedPrize struct {
	XMLName    xml.Name `xml:"instantUnclaimedPrize" json:"-"`
	GameCost   string   `xml:"gameCost" json:"cost"`
	GameName   string   `xml:"gameName" json:"name"`
	GameNumber string   `xml:"gameNumber" json:"number"`
	PrizeEN    string   `xml:"prizeEN" json:"prizeEN"`
	PrizeFR    string   `xml:"prizeFR" json:"prizeFR"`
	Total      int      `xml:"totalPrizes" json:"total"`
	Unclaimed  int      `xml:"unclaimedPrizes" json:"unclaimed"`
}

/* Methods */

func (feed *InstantUnclaimedPrizes) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.InstantUnclaimedPrizes
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *InstantUnclaimedPrizes) IsValid() bool {
	return feed.UnclaimedPrizes != nil
}

// Loads the instant unclaimed prizes feed from OLG
func (feed *InstantUnclaimedPrizes) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> InstantUnclaimedPrizes: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := InstantUnclaimedPrizes{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareInstantUnclaimedFeed(&feed.BaseNewFeed)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	log.Debug("instant unclaimed url -->" + feed.GetFeedUrl() + requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.UnclaimedPrizes = newFeedData.UnclaimedPrizes
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	/*
		for i := range feed.UnclaimedPrizes.Prizes {

			// todo
		}
	*/

	return nil

}
