package instantUnclaimed

import (
	"olg-services/interfaces"
)

// Factory function for the "Unclaimed Instant Prizes" feeds
// based on the GameType structure in lotteries/games package
func NewInstantUnclaimedNumbers(jobName string, sendErrorEmails, sendErrorSms bool) interfaces.NewXmlFeed {

	var feed interfaces.NewXmlFeed = &InstantUnclaimedPrizes{}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	}

	return feed

}
