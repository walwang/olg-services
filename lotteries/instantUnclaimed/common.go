package instantUnclaimed

import (
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"time"

	cache "github.com/pmylund/go-cache"
)

const DRAW_DATE_PARSE_FORMAT = "2006-01-02"

/* BEGIN: InstantUnclaimedBase */

// Base parent structure for "Instant Unclaimed" feeds.
// It helps maintain a single validation point, as this feed doesn't have much
// parameter variability.
// It also helps with cacheability routines, by implmenenting interfaces.IFeedCache
type InstantUnclaimedBase struct {

	// embed the base new feeds
	base.BaseNewFeed

	CacheStore *cache.Cache `json:"-"`
}

// Common validation base method for Instant Unclaimed feed.
// Normally, there should be no parameters passed to this feed, so just return ok code and message
func (feed *InstantUnclaimedBase) ValidateRequestParams() (int, string) {

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Implements interfaces.ICacheable. Sets the pointer to cache store created for this particular type of feed
func (feed *InstantUnclaimedBase) SetCacheStore(store interface{}) {

	if store != nil {

		cacheStore, found := store.(*cache.Cache)
		if found {
			feed.CacheStore = cacheStore
		}
	}

}

// Implements interfaces.ICacheable. Returns the pointer to cache store created for this particular type of feed
func (feed *InstantUnclaimedBase) GetCacheStore() interface{} {

	return feed.CacheStore

}

/* END: InstantUnclaimedBase */

/* ************************************************ */
/* BEGIN: Instant Unclaimed Prizes Common Functions */
/* ************************************************ */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
// Instant Unclaimed feeds do not currently have a set of parameters
func PrepareInstantUnclaimedFeed(feed *base.BaseNewFeed) (string, base.FeedOptions) {

	var requestParams string = ""

	var feedOptions base.FeedOptions

	// adjust the request params and specify the env and feed type
	if feed.InputParams != nil {

		//read test environment name from config
		var testEnv, _ = config.GetEntryValueDirectlyFromConfigFile("testEnvironment")

		requestParams = feed.GetQueryParams(
			//PY 2016-01-12
			feed.KVPair("env", testEnv),
			//feed.KVPair("env", "t1"),
			feed.KVPair("feedType", "instantUnclaimed"),
		)
	}

	feedOptions = base.FeedOptions{
		IsHttps:            false,
		InsecureSkipVerify: true,
	}

	// END: Test Mode Parameters

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

/* Validation functions */

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* *********************************************** */
/* END: Instant Unclaimed Prizes Common Functions  */
/* *********************************************** */
