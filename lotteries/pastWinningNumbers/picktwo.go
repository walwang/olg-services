package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type PickTwoPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	PickTwoWinnings PickTwoWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the NHL Lotto feed
type PickTwoWinnings struct {
	XMLName        xml.Name `xml:"winnings" json:"-"`
	PickTwoElement *PickTwo `xml:"pick2" json:"pick2,omitempty"`
}

// This corresponds to the unique "lotto649" xml element in the NHL Lotto feed
type PickTwo struct {
	XMLName xml.Name `xml:"pick2" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

// Part of the interfaces.NewXmlFeed interface
func (feed *PickTwoPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *PickTwoPastWinningNumbers) IsValid() bool {
	return feed.PickTwoWinnings.PickTwoElement != nil
}

// Loads the past winning numbers NHL Lotto feed from OLG
func (feed *PickTwoPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> PickTwoPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := PickTwoPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_PICK_TWO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.PickTwoWinnings = newFeedData.PickTwoWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PickTwoWinnings.PickTwoElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.PickTwoWinnings.PickTwoElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.PickTwoWinnings.PickTwoElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.PickTwoWinnings.PickTwoElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *PickTwoPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.PickTwoWinnings.PickTwoElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PickTwoWinnings.PickTwoElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.PickTwoWinnings.PickTwoElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.PickTwoWinnings.PickTwoElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.PickTwoWinnings.PickTwoElement.Draws[i])
		}
	}

	feed.PickTwoWinnings.PickTwoElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *PickTwoPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.PickTwoWinnings.PickTwoElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.PickTwoWinnings.PickTwoElement.Draws[:index])

	feed.PickTwoWinnings.PickTwoElement.Draws = nil

	feed.PickTwoWinnings.PickTwoElement.Draws = append(feed.PickTwoWinnings.PickTwoElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *PickTwoPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*PickTwoPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found PickTwoPastWinningNumbers")
		feed.PickTwoWinnings.PickTwoElement.Draws = append(feed.PickTwoWinnings.PickTwoElement.Draws, additionalFeed.PickTwoWinnings.PickTwoElement.Draws...)
	}

}
