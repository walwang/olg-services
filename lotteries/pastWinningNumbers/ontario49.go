package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type Ontario49PastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	Ontario49Winnings Ontario49Winnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Ontario 49 feed
type Ontario49Winnings struct {
	XMLName          xml.Name   `xml:"winnings" json:"-"`
	Ontario49Element *Ontario49 `xml:"ontario49" json:"ontario49,omitempty"`
}

// This corresponds to the unique "ontario49" xml element in the Ontario 49 feed
type Ontario49 struct {
	XMLName xml.Name `xml:"ontario49" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *Ontario49PastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *Ontario49PastWinningNumbers) IsValid() bool {
	return feed.Ontario49Winnings.Ontario49Element != nil
}

// Loads the past winning numbers LottoMAX feed from OLG
func (feed *Ontario49PastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> Ontario49PastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := Ontario49PastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_ONTARIO49)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.Ontario49Winnings = newFeedData.Ontario49Winnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Ontario49Winnings.Ontario49Element.Draws {

		// split the comma-separated regular numbers in the draws
		feed.Ontario49Winnings.Ontario49Element.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.Ontario49Winnings.Ontario49Element.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.Ontario49Winnings.Ontario49Element.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *Ontario49PastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.Ontario49Winnings.Ontario49Element.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Ontario49Winnings.Ontario49Element.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.Ontario49Winnings.Ontario49Element.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.Ontario49Winnings.Ontario49Element.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.Ontario49Winnings.Ontario49Element.Draws[i])
		}
	}

	feed.Ontario49Winnings.Ontario49Element.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *Ontario49PastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.Ontario49Winnings.Ontario49Element.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.Ontario49Winnings.Ontario49Element.Draws[:index])

	feed.Ontario49Winnings.Ontario49Element.Draws = nil

	feed.Ontario49Winnings.Ontario49Element.Draws = append(feed.Ontario49Winnings.Ontario49Element.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *Ontario49PastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*Ontario49PastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found Ontario49PastWinningNumbers")
		feed.Ontario49Winnings.Ontario49Element.Draws = append(feed.Ontario49Winnings.Ontario49Element.Draws, additionalFeed.Ontario49Winnings.Ontario49Element.Draws...)
	}

}
