package pastWinningNumbers

import (
	"encoding/xml"

	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"strings"
	"time"

	cache "github.com/pmylund/go-cache"
)

const DRAW_DATE_PARSE_FORMAT = "2006-01-02"

/* BEGIN: PastWinningNumbersBase */

// Base parent structure for "Past Winning Number" feeds.
// It helps maintain a single validation point, as this feed doesn't have much
// parameter variability.
// It also helps with cacheability routines, by implmenenting interfaces.IFeedCache
type PastWinningNumbersBase struct {

	// embed the base new feeds
	base.BaseNewFeed

	// If true, indicates that only the last draw should be extracted.
	// By default, it is false
	OnlyLastDraw bool

	CacheStore *cache.Cache `json:"-"`
}

// Common validation base method for Past Winning Numbers feeds.
// Generally, the past winning number feeds all require just the
// start date and end date parameters to be valid. For special cases, this
// method will need to be overridden in the particular struct of that feed.
func (feed *PastWinningNumbersBase) ValidateRequestParams() (int, string) {

	var validationErr string = ""

	// Validate the start and end dates only if at least one of them is not empty.
	// Otherwise, assume only the last draw is to be extracted
	if feed.InputParams.Get("startDate") == "" && feed.InputParams.Get("endDate") == "" {

		feed.OnlyLastDraw = true

		return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
	}

	if validationErr = ValidateNonEmptyDateParameter("startDate", feed.InputParams.Get("startDate")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyDateParameter("endDate", feed.InputParams.Get("endDate")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Implements the namesake method in the IPastWinningNumbers interface.
// In general this returns true if startDate and lastDate parameters are empty.
// You can look at this method as a flag that tells the flow what to do.
func (feed *PastWinningNumbersBase) GetOnlyLastDraw() bool {
	return feed.OnlyLastDraw
}

// Implements the namesake method in the IPastWinningNumbers interface.
// In general this should be set to true if startDate and lastDate parameters are empty.
// You can look at this method as a way to set a flag.
func (feed *PastWinningNumbersBase) SetOnlyLastDraw(onlyLastDraw bool) {
	feed.OnlyLastDraw = onlyLastDraw
}

// Implements interfaces.ICacheable. Sets the pointer to cache store created for this particular type of feed
func (feed *PastWinningNumbersBase) SetCacheStore(store interface{}) {

	if store != nil {

		cacheStore, found := store.(*cache.Cache)
		if found {
			feed.CacheStore = cacheStore
		}
	}

}

// Implements interfaces.ICacheable. Returns the pointer to cache store created for this particular type of feed
func (feed *PastWinningNumbersBase) GetCacheStore() interface{} {

	return feed.CacheStore

}

// Clones the supplied feed and if the start and end date are not empty strings, chops the draws to include only
// the ones in the supplied interval. If the start and the end date are empty, returns a clone of the feed.
func GetCachedFeedWithDateRange(pastWinningFeed interfaces.IPastWinningNumbers, gameType games.GameType, startDate string, endDate string) (interfaces.IPastWinningNumbers, error) {

	// we need to make sure we clone the feed struct, because otherwise the cached struct itself
	// will be chopped up when doing the TrimDateRange operation
	pastWinningFeedClone, err := NewPastWinningNumbersWithCache(nil, gameType, gameType.Code, false, false, pastWinningFeed)
	if err != nil {
		return nil, err
	}

	// return the clone directly without trimming if start and end date are empty
	if startDate == "" && endDate == "" {

		return pastWinningFeedClone, nil
	}

	expectedDateTimeFormat := "2006-01-02"
	startDateParsed, err := time.Parse(expectedDateTimeFormat, startDate)
	if err != nil {
		return nil, err
	}
	endDateParsed, err := time.Parse(expectedDateTimeFormat, endDate)
	if err != nil {
		return nil, err
	}

	pastWinningFeedClone.TrimDateRange(startDateParsed, endDateParsed)

	return pastWinningFeedClone, nil

}

/* END: PastWinningNumbersBase */

// This corresponds to one of the many "draw" xml elements in the past winning numbers feeds
type Draw struct {
	XMLName xml.Name `xml:"draw" json:"-"`

	DrawDate time.Time `json:"-"`

	// Date of draw: e.g. "2015-03-14"
	Date string `xml:"date" json:"date"`

	// Shortened day code: e.g. "Sat"
	Day string `xml:"day" json:"day"`

	// Time of day : e.g. "EVENING"
	Time string `xml:"time" json:"time,omitempty"`

	// The main details of the draw
	MainDetails Main `xml:"main" json:"main"`

	// The guaranteed millions details of the draw
	GuaranteedMillions *GuaranteedMillions `xml:"guaranteedMillions" json:"guaranteedMillions,omitempty"`

	// MaxMillions for LottoMax
	MaxMillions *MaxMillions `xml:"maxmillions" json:"maxmillions,omitempty"`

	// SpecialEvent for Lotto649
	SpecialEvents []SpecialEvent `xml:"specialEvent" json:"specialEvents,omitempty"`

	Prizes Prizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`

	// The encore detailes of the draw
	Encore *Encore `xml:"encore" json:"encore,omitempty"`
}

// This corresponds to the unique "main" xml element inside the "draw" element
type Main struct {

	// Comma-separated numbers drawn for this particular draw
	// e.g. 09,19,38,40,45,47
	Regular string `xml:"regular" json:"-"`

	// for team-based games, like NHL Lotto
	Team string `xml:"team" json:"team,omitempty"`

	// this gets calculated after xml import
	RegularNumbers []string `xml:"-" json:"regular,omitempty"`

	// for Wheel of Fortune, the results are split between English and French

	CategoryEn       string   `xml:"categoryEN" json:"categoryEN,omitempty"`
	CategoryFr       string   `xml:"categoryFR" json:"categoryFR,omitempty"`
	RegularEn        string   `xml:"regularEN" json:"-"`
	RegularFr        string   `xml:"regularFR" json:"-"`
	RegularNumbersEn []string `xml:"-" json:"regularEN,omitempty"`
	RegularNumbersFr []string `xml:"-" json:"regularFR,omitempty"`

	// The bonus number for this particular draw
	Bonus string `xml:"bonus" json:"bonus,omitempty"`

	// The bonus number for this particular draw
	EarlyBird        string   `xml:"earlyBird" json:"-"`
	EarlyBirdNumbers []string `xml:"-" json:"earlyBird,omitempty"`

	// corresponds to prizeShares array
	Prizes Prizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`

	// corresponds to instantPrizeShares array
	InstantPrizes Prizes `xml:"instantPrizeShares>prize" json:"instantPrizeShares,omitempty"`
}

type SpecialEvent struct {
	DescriptionEn  string   `xml:"descriptionEn" json:"descriptionEn"`
	DescriptionFr  string   `xml:"descriptionFr" json:"descriptionFr"`
	Numbers        string   `xml:"numbers" json:"-"`
	RegularNumbers []string `xml:"-" json:"numbers"`
	Prizes         Prizes   `xml:"prizeShares>prize" json:"prizeShares,omitempty"`
}

// This corresponds to the unique "guaranteedMillions" xml element inside the "draw" element
type GuaranteedMillions struct {

	// Guaranteed millions numbers drawn this time
	Numbers        string   `xml:"numbers" json:"-"`
	RegularNumbers []string `xml:"-" json:"numbers"`
	// embed the Prizes for better JSON output
	Prizes Prizes `xml:"prizeShares>prize" json:"prizeShares"`
}

// This corresponds to the unique "MaxMillions" xml element inside the "draw" element
type MaxMillions struct {
	Numbers        []string   `xml:"numbers" json:"-"`
	RegularNumbers [][]string `xml:"-" json:"numbers"`
}

// This corresponds to the unique "encore" xml element inside the "draw" element
type Encore struct {

	// Encore number drawn this time
	Number string `xml:"number" json:"number"`

	// embed the Prizes for better JSON output
	Prizes Prizes `xml:"prizeShares>prize" json:"prizeShares"`
}

type Prizes []Prize

type Prize struct {
	//XMLName xml.Name `xml:"prize" json:"-"`
	Match          string `xml:"match" json:"match,omitempty"`
	WinningTickets string `xml:"winningTickets" json:"winningTickets"`
	Amount         string `xml:"amount" json:"amount"`
}

// Splits comma-separated series into a slice of numbers
func (m *Main) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("Main.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Regular, ",") {
		m.RegularNumbers = strings.Split(m.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Regular)
}

func (m *SpecialEvent) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("SpecialEvent.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Numbers, ",") {
		m.RegularNumbers = strings.Split(m.Numbers, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Numbers)
}

func (m *GuaranteedMillions) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("GuaranteedMillions.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Numbers, ",") {
		m.RegularNumbers = strings.Split(m.Numbers, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Numbers)
}

func (m *MaxMillions) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("MaxMillions.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	for i := range m.Numbers {
		if strings.Contains(m.Numbers[i], ",") {
			m.RegularNumbers = append(m.RegularNumbers, strings.Split(m.Numbers[i], ","))
		}
	}
}

// Splits comma-separated series into a slice of numbers
// This is a "Lottario" dedicated operation
func (m *Main) SplitEarlyBirdNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("Main.SplitEarlyBirdNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.EarlyBird, ",") {
		m.EarlyBirdNumbers = strings.Split(m.EarlyBird, ",")
		return
	}

	// if comma not present, append the sole element
	m.EarlyBirdNumbers = append(m.EarlyBirdNumbers, m.EarlyBird)
}

// Splits comma-separated series into a slice of string words
// This is a "Wheel Of Fortune" dedicated operation
func (m *Main) SplitWheelOfFortuneNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("Main.SplitWheelOfFortuneNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.RegularEn, ",") {
		m.RegularNumbersEn = strings.Split(m.RegularEn, ",")
	} else {
		// if comma not present, append the sole element
		m.RegularNumbersEn = append(m.RegularNumbersEn, m.RegularEn)
	}

	if strings.Contains(m.RegularFr, ",") {
		m.RegularNumbersFr = strings.Split(m.RegularFr, ",")
	} else {
		// if comma not present, append the sole element
		m.RegularNumbersFr = append(m.RegularNumbersFr, m.RegularFr)
	}

}

/* ******************************************* */
/* BEGIN: Past Winning Number Common Functions */
/* ******************************************* */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
// PastWinningNumbers feeds have a common set of input parameters, the
// start and end date
func PreparePastWinningNumbersFeed(feed *base.BaseNewFeed, gameCode string) (string, base.FeedOptions) {

	var requestParams string = ""

	// prepare the request parameters
	//	if feed.InputParams != nil {
	//		requestParams = feed.GetQueryParams(
	//			feed.KVPair("game", gameCode),
	//			feed.KVPair("startDate", feed.InputParams.Get("startDate")),
	//			feed.KVPair("endDate", feed.InputParams.Get("endDate")),
	//		)
	//	}

	var feedOptions base.FeedOptions

	// adjust the request params and specify the env and feed type
	if feed.InputParams != nil {

		//read test environment name from config
		var testEnv, _ = config.GetEntryValueDirectlyFromConfigFile("testEnvironment")

		startDate := feed.InputParams.Get("startDate")
		endDate := feed.InputParams.Get("endDate")

		parsedStart, err := time.Parse("2006-01-02", startDate)
		parsedEnd, err := time.Parse("2006-01-02", endDate)

		if err != nil {
			log.Error("Past winning numbers, error parsing start and end date", err)
			// if there is an error parsing the query strings, just use
			// their original values
			startDate = feed.InputParams.Get("startDate")
			endDate = feed.InputParams.Get("endDate")
		} else {

			// if tamarin sends in a date range like,
			// start = 2017-02-05 and end = 2017-02-07,
			// we want to return startDate and endDate as 2017-02-06
			// if we want to return a single day, OLG's API expects
			// start date and enddate to be the same date
			if parsedEnd.Sub(parsedStart).String() == "48h0m0s" {
				startDate = parsedEnd.AddDate(0, 0, -1).Format("2006-01-02")
				endDate = startDate
			}
		}

		requestParams = feed.GetQueryParams(
			feed.KVPair("env", testEnv),
			feed.KVPair("feedType", "pastWinningNumbers"),
			feed.KVPair("game", gameCode),
			feed.KVPair("startDate", startDate),
			feed.KVPair("endDate", endDate),
		)

	}

	feedOptions = base.FeedOptions{
		IsHttps:            false,
		InsecureSkipVerify: true,
	}

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

/* Validation functions */

// Insures the date param is not empty adheres to a yyyy-mm-dd format.
// Returns empty if all good, or an appropriate error message string otherwise.
func ValidateNonEmptyDateParameter(paramName string, paramValue string) string {

	if paramValue == "" {
		return paramName + " is empty"
	}

	expectedDateTimeFormat := "2006-01-02"
	if _, err := time.Parse(expectedDateTimeFormat, paramValue); err != nil {
		return paramName + " not a valid yyyy-mm-dd date parameter"
	}

	return ""

}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* ******************************************* */
/* END: Past Winning Number Common Functions   */
/* ******************************************* */
