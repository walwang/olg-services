package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"
)

/* Types */

type LivingTheLifePastWinningNumbers struct {

	// embed the base new feeds
	base.BaseNewFeed

	// If true, indicates that only the last draw should be extracted.
	// By default, it is false
	OnlyLastDraw bool

	LivingTheLifeWinnings LivingTheLifeWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Living The Life feed
type LivingTheLifeWinnings struct {
	XMLName              xml.Name       `xml:"winnings" json:"-"`
	LivingTheLifeElement *LivingTheLife `xml:"livingthelife" json:"livingthelife,omitempty"`
}

// This corresponds to the unique "livingthelife" xml element in the Living The Life feed
type LivingTheLife struct {
	XMLName xml.Name         `xml:"livingthelife" json:"-"`
	Draws   []LivingLifeDraw `xml:"draw" json:"draws"`
}

// This corresponds to one of the many "draw" xml elements in the livingthelife past winning numbers feeds
// This feed does not have a "main" element embedded inside
type LivingLifeDraw struct {
	XMLName xml.Name `xml:"draw" json:"-"`

	DrawDate time.Time `json:"-"`

	// Date of draw: e.g. "2015-03-14"
	Date string `xml:"date" json:"date"`

	// Shortened day code: e.g. "Sat"
	Day string `xml:"day" json:"day"`

	// Draw type: e.g. "REG" (regular) or "EB" (early bird)
	DrawType string `xml:"drawType" json:"drawType,omitempty"`

	EventEn string `xml:"eventEN" json:"eventEN,omitempty"`
	EventFr string `xml:"eventFR" json:"eventFR,omitempty"`

	// corresponds to the category elements
	Categories []LivingTheLifeCategory `xml:"category" json:"categories,omitempty"`

	// corresponds to prizeShares array specifically for livingthelife
	Prizes Prizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`
}

type LivingTheLifeCategory struct {
	XMLName xml.Name `xml:"category" json:"-"`

	Prize string `xml:"prize" json:"prize,omitempty"`

	// Comma-separated numbers drawn for this particular draw
	// e.g. 09,19,38,40,45,47
	Regular string `xml:"regular" json:"-"`

	// this gets calculated after xml import
	RegularNumbers []string `xml:"-" json:"regular,omitempty"`
}

/* Data transformation methods */

// Splits comma-separated series into a slice of numbers
func (m *LivingTheLifeCategory) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("LivingTheLifeCategory.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Regular, ",") {
		m.RegularNumbers = strings.Split(m.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Regular)
}

/* Interface Methods */

func (feed *LivingTheLifePastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Implements the namesake method in the IPastWinningNumbers interface.
// In general this returns true if startDate and lastDate parameters are empty.
// You can look at this method as a flag that tells the flow what to do.
func (feed *LivingTheLifePastWinningNumbers) GetOnlyLastDraw() bool {
	return feed.OnlyLastDraw
}

// Implements the namesake method in the IPastWinningNumbers interface.
// In general this should be set to true if startDate and lastDate parameters are empty.
// You can look at this method as a way to set a flag.
func (feed *LivingTheLifePastWinningNumbers) SetOnlyLastDraw(onlyLastDraw bool) {
	feed.OnlyLastDraw = onlyLastDraw
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LivingTheLifePastWinningNumbers) IsValid() bool {
	return feed.LivingTheLifeWinnings.LivingTheLifeElement != nil
}

// Loads the past winning numbers LottoMAX feed from OLG
func (feed *LivingTheLifePastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LivingTheLifePastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := LivingTheLifePastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_LIVING_THE_LIFE)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LivingTheLifeWinnings = newFeedData.LivingTheLifeWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws {

		for cats := range feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i].Categories {
			// split the comma-separated regular numbers in the categories
			feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i].Categories[cats].SplitRegularNumbers()
		}

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}

	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *LivingTheLifePastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []LivingLifeDraw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[i])
		}
	}

	feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *LivingTheLifePastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]LivingLifeDraw, index)
	copy(newSlice, feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws[:index])

	feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws = nil

	feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws = append(feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *LivingTheLifePastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*LivingTheLifePastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found LivingTheLifePastWinningNumbers")
		feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws = append(feed.LivingTheLifeWinnings.LivingTheLifeElement.Draws, additionalFeed.LivingTheLifeWinnings.LivingTheLifeElement.Draws...)
	}

}
