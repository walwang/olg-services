package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type NHLLottoPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	NHLLottoWinnings NHLLottoWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the NHL Lotto feed
type NHLLottoWinnings struct {
	XMLName         xml.Name  `xml:"winnings" json:"-"`
	NHLLottoElement *NHLLotto `xml:"nhllotto" json:"nhllotto,omitempty"`
}

// This corresponds to the unique "lotto649" xml element in the NHL Lotto feed
type NHLLotto struct {
	XMLName xml.Name `xml:"nhllotto" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *NHLLottoPastWinningNumbers) IsValid() bool {
	return feed.NHLLottoWinnings.NHLLottoElement != nil
}

// Part of the interfaces.NewXmlFeed interface
func (feed *NHLLottoPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Loads the past winning numbers NHL Lotto feed from OLG
func (feed *NHLLottoPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> NHLLottoPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := NHLLottoPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_NHLLOTTO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.NHLLottoWinnings = newFeedData.NHLLottoWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.NHLLottoWinnings.NHLLottoElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.NHLLottoWinnings.NHLLottoElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.NHLLottoWinnings.NHLLottoElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.NHLLottoWinnings.NHLLottoElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *NHLLottoPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.NHLLottoWinnings.NHLLottoElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.NHLLottoWinnings.NHLLottoElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.NHLLottoWinnings.NHLLottoElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.NHLLottoWinnings.NHLLottoElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.NHLLottoWinnings.NHLLottoElement.Draws[i])
		}
	}

	feed.NHLLottoWinnings.NHLLottoElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *NHLLottoPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.NHLLottoWinnings.NHLLottoElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.NHLLottoWinnings.NHLLottoElement.Draws[:index])

	feed.NHLLottoWinnings.NHLLottoElement.Draws = nil

	feed.NHLLottoWinnings.NHLLottoElement.Draws = append(feed.NHLLottoWinnings.NHLLottoElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *NHLLottoPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*NHLLottoPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found NHLLottoPastWinningNumbers")
		feed.NHLLottoWinnings.NHLLottoElement.Draws = append(feed.NHLLottoWinnings.NHLLottoElement.Draws, additionalFeed.NHLLottoWinnings.NHLLottoElement.Draws...)
	}

}
