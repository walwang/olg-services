package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type PickThreePastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	PickThreeWinnings PickThreeWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the NHL Lotto feed
type PickThreeWinnings struct {
	XMLName          xml.Name   `xml:"winnings" json:"-"`
	PickThreeElement *PickThree `xml:"pick3" json:"pick3,omitempty"`
}

// This corresponds to the unique "lotto649" xml element in the NHL Lotto feed
type PickThree struct {
	XMLName xml.Name `xml:"pick3" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

// Part of the interfaces.NewXmlFeed interface
func (feed *PickThreePastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *PickThreePastWinningNumbers) IsValid() bool {
	return feed.PickThreeWinnings.PickThreeElement != nil
}

// Loads the past winning numbers NHL Lotto feed from OLG
func (feed *PickThreePastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> PickThreePastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := PickThreePastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_PICK_THREE)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.PickThreeWinnings = newFeedData.PickThreeWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PickThreeWinnings.PickThreeElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.PickThreeWinnings.PickThreeElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.PickThreeWinnings.PickThreeElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.PickThreeWinnings.PickThreeElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *PickThreePastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.PickThreeWinnings.PickThreeElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PickThreeWinnings.PickThreeElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.PickThreeWinnings.PickThreeElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.PickThreeWinnings.PickThreeElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.PickThreeWinnings.PickThreeElement.Draws[i])
		}
	}

	feed.PickThreeWinnings.PickThreeElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *PickThreePastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.PickThreeWinnings.PickThreeElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.PickThreeWinnings.PickThreeElement.Draws[:index])

	feed.PickThreeWinnings.PickThreeElement.Draws = nil

	feed.PickThreeWinnings.PickThreeElement.Draws = append(feed.PickThreeWinnings.PickThreeElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *PickThreePastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*PickThreePastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found PickThreePastWinningNumbers")
		feed.PickThreeWinnings.PickThreeElement.Draws = append(feed.PickThreeWinnings.PickThreeElement.Draws, additionalFeed.PickThreeWinnings.PickThreeElement.Draws...)
	}

}
