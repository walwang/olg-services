package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"
)

/* Types */

type DailyKenoPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	DailyKenoWinnings DailyKenoWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the dailykeno feed
type DailyKenoWinnings struct {
	XMLName          xml.Name   `xml:"winnings" json:"-"`
	DailyKenoElement *DailyKeno `xml:"dailykeno" json:"dailykeno,omitempty"`
}

// This corresponds to the unique "dailykeno" xml element in the Lotto MAX feed
type DailyKeno struct {
	XMLName xml.Name   `xml:"dailykeno" json:"-"`
	Draws   []KenoDraw `xml:"draw" json:"draws"`
}

// This corresponds to one of the many "draw" xml elements in the dailykeno past winning numbers feeds
type KenoDraw struct {
	XMLName xml.Name `xml:"draw" json:"-"`

	DrawDate time.Time `json:"-"`

	// Date of draw: e.g. "2015-03-14"
	Date string `xml:"date" json:"date"`

	// Shortened day code: e.g. "Sat"
	Day string `xml:"day" json:"day"`

	// Time of day : e.g. "EVENING"
	Time string `xml:"time" json:"time,omitempty"`

	// The main details of the draw specifically for daily keno
	MainDetails KenoMain `xml:"main" json:"main"`

	// The encore detailes of the draw
	Encore *Encore `xml:"encore" json:"encore,omitempty"`
}

// This corresponds to the unique dailykeno "main" xml element inside the "draw" element
type KenoMain struct {

	// Comma-separated numbers drawn for this particular draw
	// e.g. 09,19,38,40,45,47
	Regular string `xml:"regular" json:"-"`

	// for team-based games, like NHL Lotto
	Team string `xml:"team" json:"team,omitempty"`

	// this gets calculated after xml import
	RegularNumbers []string `xml:"-" json:"regular,omitempty"`

	// for Wheel of Fortune, the results are split between English and French

	CategoryEn       string   `xml:"categoryEN" json:"categoryEN,omitempty"`
	CategoryFr       string   `xml:"categoryFR" json:"categoryFR,omitempty"`
	RegularEn        string   `xml:"regularEN" json:"-"`
	RegularFr        string   `xml:"regularFR" json:"-"`
	RegularNumbersEn []string `xml:"-" json:"regularEN,omitempty"`
	RegularNumbersFr []string `xml:"-" json:"regularFR,omitempty"`

	// The bonus number for this particular draw
	Bonus string `xml:"bonus" json:"bonus,omitempty"`

	// corresponds to prizeShares array specifically for dailykeno
	Prizes KenoPrizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`
}

type KenoPrizes []KenoPrize

type KenoPrize struct {
	XMLName xml.Name `xml:"prize" json:"-"`

	Match string `xml:"match" json:"match,omitempty"`

	// unlike other games, the daily keno can have multiple winningTickets elements
	// inside the prize element
	WinningTickets []KenoWinningTicket `xml:"winningTickets" json:"winningTickets"`

	Amount string `xml:"amount" json:"amount"`
}

type KenoWinningTicket struct {
	XMLName xml.Name `xml:"winningTickets" json:"-"`
	Bet     string   `xml:"bet,attr" json:"bet,omitempty"`
	Count   string   `xml:",chardata" json:"count,omitempty"`
}

/* Data transformation methods */

// Splits comma-separated series into a slice of numbers
func (m *KenoMain) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("Main.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Regular, ",") {
		m.RegularNumbers = strings.Split(m.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Regular)
}

/* Interface Methods */

func (feed *DailyKenoPastWinningNumbers) GetFeedUrl() string {

	//	// until OLG provides the unified feeds, hard code this here for dev mode
	//	if config.GlobalConfig.Environment.IsDummyMode {

	//		//sometimes we need to fake the feed, to test if the notifier is working for example
	//		fake, err := config.GetEntryValueDirectlyFromConfigFile("fake.feed.pastwinningnumbers.dailykeno.error")
	//		if err != nil {
	//			log.Error("GetFeedUrl() error in dummy mode when running config.GetEntryValueDirectlyFromConfigFile: ", err)
	//		} else {

	//			if fake != "" {

	//				log.Debug("------------------------------------------------------------------\n " +
	//					"WARNING - WE ARE USING A FAKE Daily Keno FEED INSTEAD OF THE REGULAR ONE.\n THE FAKE IS: " +
	//					fake + "\n------------------------------------------------------------------------")

	//				return fake
	//			}
	//		}

	//	}

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *DailyKenoPastWinningNumbers) IsValid() bool {
	return feed.DailyKenoWinnings.DailyKenoElement != nil
}

// Loads the past winning numbers LottoMAX feed from OLG
func (feed *DailyKenoPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> DailyKenoPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := DailyKenoPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_DAILY_KENO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.DailyKenoWinnings = newFeedData.DailyKenoWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.DailyKenoWinnings.DailyKenoElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.DailyKenoWinnings.DailyKenoElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.DailyKenoWinnings.DailyKenoElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.DailyKenoWinnings.DailyKenoElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *DailyKenoPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []KenoDraw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.DailyKenoWinnings.DailyKenoElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.DailyKenoWinnings.DailyKenoElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.DailyKenoWinnings.DailyKenoElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.DailyKenoWinnings.DailyKenoElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.DailyKenoWinnings.DailyKenoElement.Draws[i])
		}
	}

	feed.DailyKenoWinnings.DailyKenoElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *DailyKenoPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.DailyKenoWinnings.DailyKenoElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]KenoDraw, index)
	copy(newSlice, feed.DailyKenoWinnings.DailyKenoElement.Draws[:index])

	feed.DailyKenoWinnings.DailyKenoElement.Draws = nil

	feed.DailyKenoWinnings.DailyKenoElement.Draws = append(feed.DailyKenoWinnings.DailyKenoElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *DailyKenoPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*DailyKenoPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found DailyKenoPastWinningNumbers")
		feed.DailyKenoWinnings.DailyKenoElement.Draws = append(feed.DailyKenoWinnings.DailyKenoElement.Draws, additionalFeed.DailyKenoWinnings.DailyKenoElement.Draws...)
	}

}
