package pastWinningNumbers

import (
	"encoding/xml"
	"strings"

	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type DailyGrandPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	DailyGrandWinnings DailyGrandWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Lotto 649 feed
type DailyGrandWinnings struct {
	XMLName           xml.Name    `xml:"winnings" json:"-"`
	DailyGrandElement *DailyGrand `xml:"dailygrand" json:"dailygrand,omitempty"`
}

// This corresponds to the unique "dailygrand" xml element in the Lotto 649 feed
type DailyGrand struct {
	XMLName xml.Name         `xml:"dailygrand" json:"-"`
	Draws   []DailyGrandDraw `xml:"draw" json:"draws"`
}

type DailyGrandDraw struct {
	XMLName xml.Name `xml:"draw" json:"-"`

	DrawDate time.Time `json:"-"`

	// Date of draw: e.g. "2015-03-14"
	Date string `xml:"date" json:"date"`

	// Shortened day code: e.g. "Sat"
	Day string `xml:"day" json:"day"`

	// Time of day : e.g. "EVENING"
	Time string `xml:"time" json:"time,omitempty"`

	// The main details of the draw
	MainDetails DailyGrandMain `xml:"main" json:"main"`

	SpecialEvents []DailyGrandSpecialEvent `xml:"specialEvent" json:"specialEvents,omitempty"`

	Prizes DailyGrandPrizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`

	// The encore detailes of the draw
	Encore *DailyGrandEncore `xml:"encore" json:"encore,omitempty"`
}

// This corresponds to the unique "main" xml element inside the "draw" element
type DailyGrandMain struct {

	// Comma-separated numbers drawn for this particular draw
	// e.g. 09,19,38,40,45,47
	Regular string `xml:"regular" json:"-"`

	// for team-based games, like NHL Lotto
	Team string `xml:"team" json:"team,omitempty"`

	// this gets calculated after xml import
	RegularNumbers []string `xml:"-" json:"regular,omitempty"`

	// for Wheel of Fortune, the results are split between English and French

	CategoryEn       string   `xml:"categoryEN" json:"categoryEN,omitempty"`
	CategoryFr       string   `xml:"categoryFR" json:"categoryFR,omitempty"`
	RegularEn        string   `xml:"regularEN" json:"-"`
	RegularFr        string   `xml:"regularFR" json:"-"`
	RegularNumbersEn []string `xml:"-" json:"regularEN,omitempty"`
	RegularNumbersFr []string `xml:"-" json:"regularFR,omitempty"`

	// The bonus number for this particular draw
	Bonus string `xml:"bonus" json:"bonus,omitempty"`

	// The bonus number for this particular draw
	EarlyBird        string   `xml:"earlyBird" json:"-"`
	EarlyBirdNumbers []string `xml:"-" json:"earlyBird,omitempty"`

	// corresponds to prizeShares array
	Prizes DailyGrandPrizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`

	// corresponds to instantPrizeShares array
	InstantPrizes DailyGrandPrizes `xml:"instantPrizeShares>prize" json:"instantPrizeShares,omitempty"`
}

type DailyGrandSpecialEvent struct {
	SpecialEventNumSet   string                          `xml:"numSet,attr" json:"numSet,omitempty"`
	SpecialEventNumDrawn string                          `xml:"numDrawn,attr" json:"numDrawn,omitempty"`
	TempDescription      []string                        `xml:"description" json:"-"`
	DescriptionEn        string                          `xml:"-" json:"descriptionEn"`
	DescriptionFr        string                          `xml:"-" json:"descriptionFr"`
	Numbers              []DailyGrandSpecialEventNumbers `xml:"number" json:"numbers"`
	//	Bonus                string                          `xml:"number>bonus" json:"bonus"`
	Amount string `xml:"amount" json:"amount"`
	//	RegularNumbers       []string                        `xml:"-" json:"numbers"`
	Prizes DailyGrandPrizes `xml:"prizeShares>prize" json:"prizeShares,omitempty"`
}

type DailyGrandSpecialEventNumbers struct {
	Regular        string   `xml:"regular" json:"-"`
	RegularNumbers []string `xml:"-" json:"regular"`
	Bonus          string   `xml:"bonus" json:"bonus"`
}

// This corresponds to the unique "encore" xml element inside the "draw" element
type DailyGrandEncore struct {

	// Encore number drawn this time
	Number string `xml:"number" json:"number"`

	// embed the Prizes for better JSON output
	Prizes Prizes `xml:"prizeShares>prize" json:"prizeShares"`
}

type DailyGrandPrizes []DailyGrandPrize

type DailyGrandPrize struct {
	//XMLName xml.Name `xml:"prize" json:"-"`
	Match          string `xml:"match" json:"match,omitempty"`
	WinningTickets string `xml:"winningTickets" json:"winningTickets"`

	TempAmount []string `xml:"amount" json:"-"`
	Amount     string   `xml:"-" json:"amount"`
	AmountFr   string   `xml:"-" json:"amountFr,omitempty"`
}

/* Methods */
// Splits comma-separated series into a slice of numbers
func (m *DailyGrandMain) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("Main.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Regular, ",") {
		m.RegularNumbers = strings.Split(m.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Regular)
}

func (p *DailyGrandPrize) NormalizePrizeAmount() {

	if p == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("p.NormalizePrizeAmount: returning to avoid panic", "the instance p is nil - this should not happen"))
		return
	}

	if len(p.TempAmount) == 2 {
		p.Amount = p.TempAmount[0]
		p.AmountFr = p.TempAmount[1]
	}
}

func (p *DailyGrandSpecialEvent) NormalizeDescription() {

	if p == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("p.NormalizeDescription: returning to avoid panic", "the instance p is nil - this should not happen"))
		return
	}

	if len(p.TempDescription) == 2 {
		p.DescriptionEn = p.TempDescription[0]
		p.DescriptionFr = p.TempDescription[1]
	}
}

func (m *DailyGrandSpecialEventNumbers) SplitRegularNumbers() {

	if m == nil {
		log.Error("lotteries/pastWinningNumbers package:", log.NewErrorLocal("SpecialEvent.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(m.Regular, ",") {
		m.RegularNumbers = strings.Split(m.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	m.RegularNumbers = append(m.RegularNumbers, m.Regular)
}

func (feed *DailyGrandPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *DailyGrandPastWinningNumbers) IsValid() bool {
	return feed.DailyGrandWinnings.DailyGrandElement != nil
}

// Loads the past winning numbers DailyGrand feed from OLG
func (feed *DailyGrandPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> DailyGrandPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := DailyGrandPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_DAILYGRAND)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)

	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.DailyGrandWinnings = newFeedData.DailyGrandWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.DailyGrandWinnings.DailyGrandElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.DailyGrandWinnings.DailyGrandElement.Draws[i].MainDetails.SplitRegularNumbers()

		// Normalize Amount field
		for j := range feed.DailyGrandWinnings.DailyGrandElement.Draws[i].MainDetails.Prizes {
			feed.DailyGrandWinnings.DailyGrandElement.Draws[i].MainDetails.Prizes[j].NormalizePrizeAmount()
		}

		if feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents != nil {
			for q := range feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents {

				//				feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].SplitRegularNumbers()
				feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].NormalizeDescription()

				if feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].Prizes != nil {
					for r := range feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].Prizes {
						feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].Prizes[r].NormalizePrizeAmount()
					}
				}

				if feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].Numbers != nil {
					for v := range feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].Numbers {
						feed.DailyGrandWinnings.DailyGrandElement.Draws[i].SpecialEvents[q].Numbers[v].SplitRegularNumbers()
					}
				}
			}
		}

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.DailyGrandWinnings.DailyGrandElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.DailyGrandWinnings.DailyGrandElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *DailyGrandPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []DailyGrandDraw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.DailyGrandWinnings.DailyGrandElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.DailyGrandWinnings.DailyGrandElement.Draws {

		if InTimeSpan(startDate, endDate, feed.DailyGrandWinnings.DailyGrandElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.DailyGrandWinnings.DailyGrandElement.Draws[i])
		}
	}

	feed.DailyGrandWinnings.DailyGrandElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *DailyGrandPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.DailyGrandWinnings.DailyGrandElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]DailyGrandDraw, index)
	rowsCopied := copy(newSlice, feed.DailyGrandWinnings.DailyGrandElement.Draws[:index])

	log.Debug("TrimDrawsStartingWithIndex - draws records copied: " + strconv.Itoa(rowsCopied))

	feed.DailyGrandWinnings.DailyGrandElement.Draws = nil

	feed.DailyGrandWinnings.DailyGrandElement.Draws = append(feed.DailyGrandWinnings.DailyGrandElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *DailyGrandPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*DailyGrandPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found DailyGrandPastWinningNumbers")
		feed.DailyGrandWinnings.DailyGrandElement.Draws = append(feed.DailyGrandWinnings.DailyGrandElement.Draws, additionalFeed.DailyGrandWinnings.DailyGrandElement.Draws...)
	}

}
