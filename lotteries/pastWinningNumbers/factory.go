package pastWinningNumbers

import (
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"strings"

	cache "github.com/pmylund/go-cache"
)

func NewPastWinningNumbers(gameType games.GameType, jobName string, sendErrorEmails, sendErrorSms bool) (interfaces.NewXmlFeed, error) {

	return NewPastWinningNumbersWithCache(nil, gameType, jobName, sendErrorEmails, sendErrorSms, nil)

}

func NewPastWinningNumbersWithCache(cacheStore *cache.Cache, gameType games.GameType, jobName string,
	sendErrorEmails, sendErrorSms bool,
	feedToCloneFrom interfaces.IPastWinningNumbers) (interfaces.IPastWinningNumbers, error) {

	var feed interfaces.IPastWinningNumbers

	if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTO649 {

		structFeed := &Lotto649PastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*Lotto649PastWinningNumbers)
			if ok {
				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.Lotto64Winnings.Lotto64Element = &Lotto64{}
				structFeed.Lotto64Winnings.Lotto64Element.Draws = make([]Draw, len(cloneSource.Lotto64Winnings.Lotto64Element.Draws))
				copy(structFeed.Lotto64Winnings.Lotto64Element.Draws, cloneSource.Lotto64Winnings.Lotto64Element.Draws)
			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTOMAX {

		structFeed := &LottoMaxPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*LottoMaxPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LottoMaxWinnings.LottoMaxElement = &LottoMax{}
				structFeed.LottoMaxWinnings.LottoMaxElement.Draws = make([]Draw, len(cloneSource.LottoMaxWinnings.LottoMaxElement.Draws))
				copy(structFeed.LottoMaxWinnings.LottoMaxElement.Draws, cloneSource.LottoMaxWinnings.LottoMaxElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_DAILYGRAND {

		structFeed := &DailyGrandPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*DailyGrandPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.DailyGrandWinnings.DailyGrandElement = &DailyGrand{}
				structFeed.DailyGrandWinnings.DailyGrandElement.Draws = make([]DailyGrandDraw, len(cloneSource.DailyGrandWinnings.DailyGrandElement.Draws))
				copy(structFeed.DailyGrandWinnings.DailyGrandElement.Draws, cloneSource.DailyGrandWinnings.DailyGrandElement.Draws)
			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_NHLLOTTO {

		structFeed := &NHLLottoPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*NHLLottoPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.NHLLottoWinnings.NHLLottoElement = &NHLLotto{}
				structFeed.NHLLottoWinnings.NHLLottoElement.Draws = make([]Draw, len(cloneSource.NHLLottoWinnings.NHLLottoElement.Draws))
				copy(structFeed.NHLLottoWinnings.NHLLottoElement.Draws, cloneSource.NHLLottoWinnings.NHLLottoElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_POKERLOTTO {

		structFeed := &PokerLottoPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PokerLottoPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PokerLottoWinnings.PokerLottoElement = &PokerLotto{}
				structFeed.PokerLottoWinnings.PokerLottoElement.Draws = make([]Draw, len(cloneSource.PokerLottoWinnings.PokerLottoElement.Draws))
				copy(structFeed.PokerLottoWinnings.PokerLottoElement.Draws, cloneSource.PokerLottoWinnings.PokerLottoElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_WHEEL_OF_FORTUNE {

		structFeed := &WheelOfFortunePastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*WheelOfFortunePastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.WheelOfFortuneWinnings.WheelOfFortuneElement = &WheelOfFortune{}
				structFeed.WheelOfFortuneWinnings.WheelOfFortuneElement.Draws = make([]Draw, len(cloneSource.WheelOfFortuneWinnings.WheelOfFortuneElement.Draws))
				copy(structFeed.WheelOfFortuneWinnings.WheelOfFortuneElement.Draws, cloneSource.WheelOfFortuneWinnings.WheelOfFortuneElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_FOUR {

		structFeed := &PickFourPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PickFourPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PickFourWinnings.PickFourElement = &PickFour{}
				structFeed.PickFourWinnings.PickFourElement.Draws = make([]Draw, len(cloneSource.PickFourWinnings.PickFourElement.Draws))
				copy(structFeed.PickFourWinnings.PickFourElement.Draws, cloneSource.PickFourWinnings.PickFourElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_THREE {

		structFeed := &PickThreePastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PickThreePastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PickThreeWinnings.PickThreeElement = &PickThree{}
				structFeed.PickThreeWinnings.PickThreeElement.Draws = make([]Draw, len(cloneSource.PickThreeWinnings.PickThreeElement.Draws))
				copy(structFeed.PickThreeWinnings.PickThreeElement.Draws, cloneSource.PickThreeWinnings.PickThreeElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_TWO {

		structFeed := &PickTwoPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*PickTwoPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.PickTwoWinnings.PickTwoElement = &PickTwo{}
				structFeed.PickTwoWinnings.PickTwoElement.Draws = make([]Draw, len(cloneSource.PickTwoWinnings.PickTwoElement.Draws))
				copy(structFeed.PickTwoWinnings.PickTwoElement.Draws, cloneSource.PickTwoWinnings.PickTwoElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_DAILY_KENO {

		structFeed := &DailyKenoPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*DailyKenoPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.DailyKenoWinnings.DailyKenoElement = &DailyKeno{}
				structFeed.DailyKenoWinnings.DailyKenoElement.Draws = make([]KenoDraw, len(cloneSource.DailyKenoWinnings.DailyKenoElement.Draws))
				copy(structFeed.DailyKenoWinnings.DailyKenoElement.Draws, cloneSource.DailyKenoWinnings.DailyKenoElement.Draws)

			}

		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LIVING_THE_LIFE {

		structFeed := &LivingTheLifePastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*LivingTheLifePastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LivingTheLifeWinnings.LivingTheLifeElement = &LivingTheLife{}
				structFeed.LivingTheLifeWinnings.LivingTheLifeElement.Draws = make([]LivingLifeDraw, len(cloneSource.LivingTheLifeWinnings.LivingTheLifeElement.Draws))
				copy(structFeed.LivingTheLifeWinnings.LivingTheLifeElement.Draws, cloneSource.LivingTheLifeWinnings.LivingTheLifeElement.Draws)

			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_MEGADICE {

		structFeed := &MegaDicePastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*MegaDicePastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.MegaDiceWinnings.MegaDiceElement = &MegaDice{}
				structFeed.MegaDiceWinnings.MegaDiceElement.Draws = make([]Draw, len(cloneSource.MegaDiceWinnings.MegaDiceElement.Draws))
				copy(structFeed.MegaDiceWinnings.MegaDiceElement.Draws, cloneSource.MegaDiceWinnings.MegaDiceElement.Draws)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTARIO {

		structFeed := &LottarioPastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*LottarioPastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.LottarioWinnings.LottarioElement = &Lottario{}
				structFeed.LottarioWinnings.LottarioElement.Draws = make([]Draw, len(cloneSource.LottarioWinnings.LottarioElement.Draws))
				copy(structFeed.LottarioWinnings.LottarioElement.Draws, cloneSource.LottarioWinnings.LottarioElement.Draws)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_ONTARIO49 {

		structFeed := &Ontario49PastWinningNumbers{}
		structFeed.GameType = gameType

		if feedToCloneFrom != nil {

			cloneSource, ok := feedToCloneFrom.(*Ontario49PastWinningNumbers)
			if ok {

				structFeed.BaseNewFeed = cloneSource.BaseNewFeed
				structFeed.Ontario49Winnings.Ontario49Element = &Ontario49{}
				structFeed.Ontario49Winnings.Ontario49Element.Draws = make([]Draw, len(cloneSource.Ontario49Winnings.Ontario49Element.Draws))
				copy(structFeed.Ontario49Winnings.Ontario49Element.Draws, cloneSource.Ontario49Winnings.Ontario49Element.Draws)
			}
		}

		feed = structFeed

	} else if strings.ToLower(gameType.Code) == "timeout" {
		// the timeout feed just waits 35 seconds
		feed = &TimeoutPastWinningNumbers{}
	} else {
		return nil, log.NewErrorLocal("pastWinningNumbers.NewPastWinningNumbers", "the specified gameType.Code ("+gameType.Code+") does not correspond to a valid game type")
	}

	// if the conversion to ICacheable interface succeeds, set the cache store
	cacheableStruct, ok := feed.(interfaces.ICacheable)
	if ok {
		cacheableStruct.SetCacheStore(cacheStore)
	}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	} else {
		log.Debug("We have a problem in NewPastWinningNumbersWithCache factory function where a feed does not meet the interfaces.CronJob implementation criteria")
	}

	return feed, nil

}
