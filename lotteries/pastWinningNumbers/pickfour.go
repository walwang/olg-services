package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type PickFourPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	PickFourWinnings PickFourWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the NHL Lotto feed
type PickFourWinnings struct {
	XMLName         xml.Name  `xml:"winnings" json:"-"`
	PickFourElement *PickFour `xml:"pick4" json:"pick4,omitempty"`
}

// This corresponds to the unique "lotto649" xml element in the NHL Lotto feed
type PickFour struct {
	XMLName xml.Name `xml:"pick4" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

// Part of the interfaces.NewXmlFeed interface
func (feed *PickFourPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *PickFourPastWinningNumbers) IsValid() bool {
	return feed.PickFourWinnings.PickFourElement != nil
}

// Loads the past winning numbers NHL Lotto feed from OLG
func (feed *PickFourPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> PickFourPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := PickFourPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_PICK_FOUR)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.PickFourWinnings = newFeedData.PickFourWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PickFourWinnings.PickFourElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.PickFourWinnings.PickFourElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.PickFourWinnings.PickFourElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.PickFourWinnings.PickFourElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *PickFourPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.PickFourWinnings.PickFourElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PickFourWinnings.PickFourElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.PickFourWinnings.PickFourElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.PickFourWinnings.PickFourElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.PickFourWinnings.PickFourElement.Draws[i])
		}
	}

	feed.PickFourWinnings.PickFourElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *PickFourPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.PickFourWinnings.PickFourElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.PickFourWinnings.PickFourElement.Draws[:index])

	feed.PickFourWinnings.PickFourElement.Draws = nil

	feed.PickFourWinnings.PickFourElement.Draws = append(feed.PickFourWinnings.PickFourElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *PickFourPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*PickFourPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found PickFourPastWinningNumbers")
		feed.PickFourWinnings.PickFourElement.Draws = append(feed.PickFourWinnings.PickFourElement.Draws, additionalFeed.PickFourWinnings.PickFourElement.Draws...)
	}

}
