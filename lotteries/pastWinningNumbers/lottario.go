package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type LottarioPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	LottarioWinnings LottarioWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Lottario feed
type LottarioWinnings struct {
	XMLName         xml.Name  `xml:"winnings" json:"-"`
	LottarioElement *Lottario `xml:"lottario" json:"lottario,omitempty"`
}

// This corresponds to the unique "lottario" xml element in the Lotto MAX feed
type Lottario struct {
	XMLName xml.Name `xml:"lottario" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *LottarioPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LottarioPastWinningNumbers) IsValid() bool {
	return feed.LottarioWinnings.LottarioElement != nil
}

// Loads the past winning numbers Lottario feed from OLG
func (feed *LottarioPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LottarioPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := LottarioPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_LOTTARIO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LottarioWinnings = newFeedData.LottarioWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.LottarioWinnings.LottarioElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.LottarioWinnings.LottarioElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for lottario, split the early bird numbers too
		feed.LottarioWinnings.LottarioElement.Draws[i].MainDetails.SplitEarlyBirdNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.LottarioWinnings.LottarioElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.LottarioWinnings.LottarioElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *LottarioPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.LottarioWinnings.LottarioElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.LottarioWinnings.LottarioElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.LottarioWinnings.LottarioElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.LottarioWinnings.LottarioElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.LottarioWinnings.LottarioElement.Draws[i])
		}
	}

	feed.LottarioWinnings.LottarioElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *LottarioPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.LottarioWinnings.LottarioElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.LottarioWinnings.LottarioElement.Draws[:index])

	feed.LottarioWinnings.LottarioElement.Draws = nil

	feed.LottarioWinnings.LottarioElement.Draws = append(feed.LottarioWinnings.LottarioElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *LottarioPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*LottarioPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found LottarioPastWinningNumbers")
		feed.LottarioWinnings.LottarioElement.Draws = append(feed.LottarioWinnings.LottarioElement.Draws, additionalFeed.LottarioWinnings.LottarioElement.Draws...)
	}

}
