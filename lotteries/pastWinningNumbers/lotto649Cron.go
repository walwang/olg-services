package pastWinningNumbers

import (
	"olg-services/config"
	"olg-services/constants"
	"olg-services/publicis/utils/log"
	"time"
)

// Implements the olg-services/cronJob/CronJob interface
func (feed *Lotto649PastWinningNumbers) ExecuteCronJob() {

	// if in dummy mode, calculate the time
	var testStartTime time.Time
	if config.GlobalConfig.Environment.IsDummyMode {
		testStartTime = time.Now()
	}

	// check if the previous job is still running
	if feed.IsJobStillRunning() {
		feed.LogDebug("Previous winning number feed ingestion still operating, still waiting. ")
		return
	}

	// Indicate the start of the operation
	feed.SetJobRunningFlag(true)

	feed.SetLastDateProcessed(time.Now())

	// The defer statement will insure that we indicate the end of the operation
	// in any exit circumstances. Without the defer statement we would need to call this
	// any time (in case we exit this routine early due to an error)
	defer feed.SetJobRunningFlag(false)

	// make sure we also recover from any panic
	defer func() {
		if r := recover(); r != nil {
			log.ErrorWithInterface("FATAL ERRROR: Recovered from panic", r)
		}
	}()

	err := feed.LoadFeed()
	if err != nil {

		// being inside a cron job, this is a recurring error
		// we need to treat it with the special LogRecurringError method
		// that method set the job status to faulty
		feed.LogRecurringError(feed.GetGameCode()+" XML Feed Error: ", err)

		// set the error message
		feed.SetLastErrorMessage(err.Error())

		return
	}

	// dummy mode tweaks
	if config.GlobalConfig.Environment.IsDummyMode {

		for _, e := range feed.Lotto64Winnings.Lotto64Element.Draws {

			if e.Date == "" {
				e.Date = "---- Date empty, ALERT"
			}

			log.Debug("Date: " + e.Date)

		}

	}

	// we appear to have arrived at the end of the job interation unscathed
	// if the status is faulty, it means it was from the previous iteration
	// set it to running again and notify the stakeholders via email or sms that is was repaired
	if feed.GetJobStatus() == constants.CRON_JOB_STATUS_FAULTY {
		feed.LogRecoveryNoticeAndSetJobStatusToRunning("This is an automated notification: The " + feed.GetJobName() + " Feed job functionality has been restored. The feed is now working properly.")
	}

	// set the timestamp for correctly processed to now
	feed.SetLastCorrectlyProcessedDate(time.Now())

	// calculate and output time elapsed
	if config.GlobalConfig.Environment.IsDummyMode {
		elapsed := time.Since(testStartTime)
		log.DebugWithInterface("%s took %s", feed.GetJobName(), elapsed)
	}

	return

}
