package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type LottoMaxPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	LottoMaxWinnings LottoMaxWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Lotto MAX feed
type LottoMaxWinnings struct {
	XMLName         xml.Name  `xml:"winnings" json:"-"`
	LottoMaxElement *LottoMax `xml:"lottomax" json:"lottomax,omitempty"`
}

// This corresponds to the unique "lottomax" xml element in the Lotto MAX feed
type LottoMax struct {
	XMLName xml.Name `xml:"lottomax" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *LottoMaxPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LottoMaxPastWinningNumbers) IsValid() bool {
	return feed.LottoMaxWinnings.LottoMaxElement != nil
}

// Loads the past winning numbers LottoMAX feed from OLG
func (feed *LottoMaxPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LottoMaxPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the current feed
	newFeedData := LottoMaxPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_LOTTOMAX)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	log.Debug("feed.GetFeedUrl Lottomax --> " + feed.GetFeedUrl() + requestParams)

	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LottoMaxWinnings = newFeedData.LottoMaxWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.LottoMaxWinnings.LottoMaxElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.LottoMaxWinnings.LottoMaxElement.Draws[i].MainDetails.SplitRegularNumbers()

		//split maxmillions numbers property
		if feed.LottoMaxWinnings.LottoMaxElement.Draws[i].MaxMillions != nil {
			feed.LottoMaxWinnings.LottoMaxElement.Draws[i].MaxMillions.SplitRegularNumbers()
		}

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.LottoMaxWinnings.LottoMaxElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.LottoMaxWinnings.LottoMaxElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *LottoMaxPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.LottoMaxWinnings.LottoMaxElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.LottoMaxWinnings.LottoMaxElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.LottoMaxWinnings.LottoMaxElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.LottoMaxWinnings.LottoMaxElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.LottoMaxWinnings.LottoMaxElement.Draws[i])
		}
	}

	feed.LottoMaxWinnings.LottoMaxElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *LottoMaxPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.LottoMaxWinnings.LottoMaxElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.LottoMaxWinnings.LottoMaxElement.Draws[:index])

	feed.LottoMaxWinnings.LottoMaxElement.Draws = nil

	feed.LottoMaxWinnings.LottoMaxElement.Draws = append(feed.LottoMaxWinnings.LottoMaxElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *LottoMaxPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalLottoMaxFeed, ok := feedToAppend.(*LottoMaxPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found LottoMaxPastWinningNumbers")
		feed.LottoMaxWinnings.LottoMaxElement.Draws = append(feed.LottoMaxWinnings.LottoMaxElement.Draws, additionalLottoMaxFeed.LottoMaxWinnings.LottoMaxElement.Draws...)
	}

}
