package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type PokerLottoPastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	PokerLottoWinnings PokerLottoWinnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Lotto MAX feed
type PokerLottoWinnings struct {
	XMLName           xml.Name    `xml:"winnings" json:"-"`
	PokerLottoElement *PokerLotto `xml:"pokerlotto" json:"pokerlotto,omitempty"`
}

// This corresponds to the unique "pokerlotto" xml element in the Lotto MAX feed
type PokerLotto struct {
	XMLName xml.Name `xml:"pokerlotto" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *PokerLottoPastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *PokerLottoPastWinningNumbers) IsValid() bool {
	return feed.PokerLottoWinnings.PokerLottoElement != nil
}

// Loads the past winning numbers LottoMAX feed from OLG
func (feed *PokerLottoPastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> PokerLottoPastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := PokerLottoPastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_POKERLOTTO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.PokerLottoWinnings = newFeedData.PokerLottoWinnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PokerLottoWinnings.PokerLottoElement.Draws {

		// split the comma-separated regular numbers in the draws
		feed.PokerLottoWinnings.PokerLottoElement.Draws[i].MainDetails.SplitRegularNumbers()

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.PokerLottoWinnings.PokerLottoElement.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.PokerLottoWinnings.PokerLottoElement.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *PokerLottoPastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.PokerLottoWinnings.PokerLottoElement.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.PokerLottoWinnings.PokerLottoElement.Draws {

		log.Debug("TrimDateRange - iterating over draw date: " + feed.PokerLottoWinnings.PokerLottoElement.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.PokerLottoWinnings.PokerLottoElement.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.PokerLottoWinnings.PokerLottoElement.Draws[i])
		}
	}

	feed.PokerLottoWinnings.PokerLottoElement.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *PokerLottoPastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.PokerLottoWinnings.PokerLottoElement.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	copy(newSlice, feed.PokerLottoWinnings.PokerLottoElement.Draws[:index])

	feed.PokerLottoWinnings.PokerLottoElement.Draws = nil

	feed.PokerLottoWinnings.PokerLottoElement.Draws = append(feed.PokerLottoWinnings.PokerLottoElement.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *PokerLottoPastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*PokerLottoPastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found PokerLottoPastWinningNumbers")
		feed.PokerLottoWinnings.PokerLottoElement.Draws = append(feed.PokerLottoWinnings.PokerLottoElement.Draws, additionalFeed.PokerLottoWinnings.PokerLottoElement.Draws...)
	}

}
