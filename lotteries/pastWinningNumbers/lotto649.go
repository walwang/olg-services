package pastWinningNumbers

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

/* Types */

type Lotto649PastWinningNumbers struct {

	// embed the Past Winning Numbers base struct
	PastWinningNumbersBase

	Lotto64Winnings Lotto649Winnings `xml:"winnings" json:"data"`
}

// This corresponds to the unique "winnings" xml element in the Lotto 649 feed
type Lotto649Winnings struct {
	XMLName        xml.Name `xml:"winnings" json:"-"`
	Lotto64Element *Lotto64 `xml:"lotto649" json:"lotto649,omitempty"`
}

// This corresponds to the unique "lotto649" xml element in the Lotto 649 feed
type Lotto64 struct {
	XMLName xml.Name `xml:"lotto649" json:"-"`
	Draws   []Draw   `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *Lotto649PastWinningNumbers) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.PastWinningNumbers
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *Lotto649PastWinningNumbers) IsValid() bool {
	return feed.Lotto64Winnings.Lotto64Element != nil
}

// Loads the past winning numbers Lotto649 feed from OLG
func (feed *Lotto649PastWinningNumbers) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> Lotto649PastWinningNumbers: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := Lotto649PastWinningNumbers{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PreparePastWinningNumbersFeed(&feed.BaseNewFeed, constants.GAME_CODE_LOTTO649)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.Lotto64Winnings = newFeedData.Lotto64Winnings
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Lotto64Winnings.Lotto64Element.Draws {

		// split the comma-separated regular numbers in the draws
		feed.Lotto64Winnings.Lotto64Element.Draws[i].MainDetails.SplitRegularNumbers()

		// split the comma-separated special events
		if feed.Lotto64Winnings.Lotto64Element.Draws[i].SpecialEvents != nil {
			for j := range feed.Lotto64Winnings.Lotto64Element.Draws[i].SpecialEvents {
				feed.Lotto64Winnings.Lotto64Element.Draws[i].SpecialEvents[j].SplitRegularNumbers()
			}
		}

		if feed.Lotto64Winnings.Lotto64Element.Draws[i].GuaranteedMillions != nil {
			// split the comma-separated guaranteed million numbers
			feed.Lotto64Winnings.Lotto64Element.Draws[i].GuaranteedMillions.SplitRegularNumbers()
		}

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the draw date and store it in the DrawDate field
		if feed.IsCachedVersion {
			feed.Lotto64Winnings.Lotto64Element.Draws[i].DrawDate, err = time.Parse(DRAW_DATE_PARSE_FORMAT, feed.Lotto64Winnings.Lotto64Element.Draws[i].Date)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage("draw date:" + err.Error())
				return err
			}
		}
	}

	return nil

}

// Part of the interfaces.IPastWinningNumbers interface
// Eliminates any draws that are not inside the specified date range
func (feed *Lotto649PastWinningNumbers) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newDraws []Draw

	log.Debug("TrimDateRange - entered method, found number of draws: " + strconv.Itoa(len(feed.Lotto64Winnings.Lotto64Element.Draws)))

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Lotto64Winnings.Lotto64Element.Draws {

		//log.Debug("TrimDateRange - iterating over draw date: " + feed.Lotto64Winnings.Lotto64Element.Draws[i].Date)
		if InTimeSpan(startDate, endDate, feed.Lotto64Winnings.Lotto64Element.Draws[i].DrawDate) {
			newDraws = append(newDraws, feed.Lotto64Winnings.Lotto64Element.Draws[i])
		}
	}

	feed.Lotto64Winnings.Lotto64Element.Draws = newDraws

}

// Part of the interfaces.IPastWinningNumbers interface
// Cuts down any draws after (and including) the specified zero-based index
func (feed *Lotto649PastWinningNumbers) TrimDrawsStartingWithIndex(index int) {

	initialSliceLength := len(feed.Lotto64Winnings.Lotto64Element.Draws)

	// do not do anything if the index is 0 or larger or equal than the length of the slice
	if index == 0 || index >= initialSliceLength {
		return
	}

	newSlice := make([]Draw, index)
	rowsCopied := copy(newSlice, feed.Lotto64Winnings.Lotto64Element.Draws[:index])

	log.Debug("TrimDrawsStartingWithIndex - draws records copied: " + strconv.Itoa(rowsCopied))

	feed.Lotto64Winnings.Lotto64Element.Draws = nil

	feed.Lotto64Winnings.Lotto64Element.Draws = append(feed.Lotto64Winnings.Lotto64Element.Draws, newSlice...)

}

// Part of the interfaces.IPastWinningNumbers interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *Lotto649PastWinningNumbers) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	log.Debug("AppendDraws - entered method")

	additionalFeed, ok := feedToAppend.(*Lotto649PastWinningNumbers)
	if ok {

		log.Debug("AppendDraws - found Lotto649PastWinningNumbers")
		feed.Lotto64Winnings.Lotto64Element.Draws = append(feed.Lotto64Winnings.Lotto64Element.Draws, additionalFeed.Lotto64Winnings.Lotto64Element.Draws...)
	}

}
