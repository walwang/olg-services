package winningNumbers

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/cronJob"
	"olg-services/lotteries/base"
	"olg-services/lotteries/games"
	"olg-services/publicis/utils/charset"
	_ "olg-services/publicis/utils/charset/data"
	"olg-services/publicis/utils/log"
	"olg-services/publicis/utils/web"
	"strconv"
	"strings"
	"time"
)

const (
	WINNING_NUMBERS_GAME_STATUS_OK          = "OK"
	WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS = "InProgress"
	WINNING_NUMBERS_GAME_STATUS_CALCULATING = "Calculating"
	WINNING_NUMBERS_GAME_STATUS_UNKNOWN     = "Unknown"
)

const LOTTO_GAME = "LOTTO_GAME"
const WATCH_AND_WIN = "WATCH_AND_WIN"
const DAILY = "DAILY"

type WinningNumbers struct {

	// the StatusCode, StatusMessage and ErrorMessage will not be exported to json
	// because the WinningNumbers struct will be embedded into a WinningNumbersWrapper struct
	// which will export them to json
	StatusCode    string `json:"-"`
	StatusMessage string `json:"-"`
	ErrorMessage  string `json:"-"`

	// embed the baseLotteriesStruct for default functionality
	base.BaseLotteriesStruct

	XMLName xml.Name `xml:"WinningNumbers" json:"-"`
	Games   []Game   `xml:"game"`
}

type Game struct {
	XMLName xml.Name `xml:"game" json:"-"`

	Name string `xml:"name" json:"name,omitempty"`

	// The Status field can be "OK", "InProgress" or "Missing"
	Status string `xml:"-" json:"status"`

	DrawDate          string   `xml:"drawDate" json:"drawDate,omitempty"`
	NextDrawDate      string   `xml:"nextdrawDate" json:"nextDrawDate,omitempty"`
	Jackpot           *Jackpot `xml:"jackpot" json:"-"`
	JackpotOutput     string   `xml:"-" json:"jackpot,omitempty"`
	JackDescriptionEn string   `xml:"-" json:"jackpotDescriptionEn,omitempty"`
	JackDescriptionFr string   `xml:"-" json:"jackpotDescriptionFr,omitempty"`

	Regular        string   `xml:"regular" json:"-"`
	RegularNumbers []string `xml:"-" json:"regular,omitempty"`

	RegularMidday        string   `xml:"regular_midday" json:"-"`
	RegularNumbersMidday []string `xml:"-" json:"regularMidday,omitempty"`

	RegularEvening        string   `xml:"regular_evening" json:"-"`
	RegularNumbersEvening []string `xml:"-" json:"regularEvening,omitempty"`

	// Encore values, depending on the game type
	Encore        string `xml:"encore" json:"encore,omitempty"`
	EncoreEvening string `xml:"encore_evening" json:"encoreEvening,omitempty"`
	EncoreMidday  string `xml:"encore_midday" json:"encoreMidday,omitempty"`

	// Lottario specific
	EarlyBird        string   `xml:"earlyBird" json:"-"`
	EarlyBirdNumbers []string `xml:"-" json:"earlyBird,omitempty"`

	// Lotto 649 specific
	Bonus                     string                  `xml:"bonus" json:"bonus,omitempty"`
	GuaranteedMillion         []string                `xml:"guaranteedMillion" json:"guaranteedMillion,omitempty"`
	GuaranteedMillionsJackpot string                  `xml:"guaranteedMillionsJackpot" json:"guaranteedMillionsJackpot,omitempty"`
	SpecialEvent              []SpecialEvent          `xml:"specialEvent" json:"specialEvent,omitempty"`
	UpcomingSpecialEvent      []UpcomingSpecialEvents `xml:"upcomingSpecialEvent" json:"upcomingSpecialEvent,omitempty"`

	// LottoMax specific
	MaxMillions         []string `xml:"maxmillons" json:"maxmillions,omitempty"`
	MaxMillionsJackpots string   `xml:"maxmillionsjackpots" json:"maxmillionsjackpots,omitempty"`

	// NHLLotto specific
	NHLTeam string `xml:"NHLTeam" json:"NHLTeam,omitempty"`
	NHLCity string `xml:"NHLCity" json:"NHLCity,omitempty"`

	// Wheel of Fortune specific
	CategoryEN string `xml:"CategoryEN" json:"categoryEN,omitempty"`
	CategoryFR string `xml:"CategoryFR" json:"categoryFR,omitempty"`
	RegularEN  string `xml:"regularEN" json:"regularEN,omitempty"`
	RegularFR  string `xml:"regularFR" json:"regularFR,omitempty"`

	DebugDate        string
	DebugCurrentTime string
}

type Jackpot struct {
	Chardata    string               `xml:",chardata" json:"-"`
	InnerXml    string               `xml:",innerxml" json:"-"`
	Amount      string               `xml:"amount" json:"amount,omitempty"`
	Description []JackpotDescription `xml:"description" json:"description,omitempty"`
}

type JackpotDescription struct {
	Language    string `xml:"lang,attr" json:"-"`
	Description string `xml:",chardata" json:"-"`
}

type Number struct {
	Regular        string   `xml:"regular" json:"-"`
	RegularNumbers []string `xml:"-" json:"regular,omitempty"`
	Bonus          string   `xml:"bonus" json:"bonus,omitempty"`
}

type SpecialEvent struct {
	SpecialEventWinNums  string   `xml:"specialEventWinNums" json:"specialEventWinNums,omitempty"`
	SpecialEventDescEN   string   `xml:"specialEventDescEN" json:"specialEventDescEN,omitempty"`
	SpecialEventDescFR   string   `xml:"specialEventDescFR" json:"specialEventDescFR,omitempty"`
	SpecialEventNumSet   string   `xml:"numSet,attr" json:"numSet,omitempty"`
	SpecialEventNumDrawn string   `xml:"numDrawn,attr" json:"numDrawn,omitempty"`
	Description          []string `xml:"description" json:"description,omitempty"`
	Amount               string   `xml:"amount" json:"amount,omitempty"`
	Number               []Number `xml:"number" json:"number,omitempty"`
}

type UpcomingSpecialEvents struct {
	SpecialEventJackpot          string   `xml:"specialEventJackpot" json:"specialEventJackpot,omitempty"`
	SpecialEventDescEN           string   `xml:"specialEventDescEN" json:"specialEventDescEN,omitempty"`
	SpecialEventDescFR           string   `xml:"specialEventDescFR" json:"specialEventDescFR,omitempty"`
	UpcomingSpecialEventNumSet   string   `xml:"numSet,attr" json:"numset,omitempty"`
	UpcomingSpecialEventNumDrawn string   `xml:"numDrawn,attr" json:"numDrawn,omitempty"`
	Description                  []string `xml:"description" json:"description,omitempty"`
	Amount                       string   `xml:"amount" json:"amount,omitempty"`
}

// Splits comma-separated series into a slice of numbers
func (g *Game) SplitRegularNumbers() {

	if g == nil {
		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.SplitRegularNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(g.Regular, ",") {
		g.RegularNumbers = strings.Split(g.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	g.RegularNumbers = append(g.RegularNumbers, g.Regular)
}

func (g *Game) SplitRegularNumbersMidday() {

	if g == nil {
		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.SplitRegularNumbersMidday: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(g.RegularMidday, ",") {
		g.RegularNumbersMidday = strings.Split(g.RegularMidday, ",")
		return
	}

	// if comma not present, append the sole element
	g.RegularNumbersMidday = append(g.RegularNumbersMidday, g.RegularMidday)
}

func (g *Game) SplitRegularNumbersEvening() {

	if g == nil {
		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.SplitRegularNumbersEvening: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(g.RegularEvening, ",") {
		g.RegularNumbersEvening = strings.Split(g.RegularEvening, ",")
		return
	}

	// if comma not present, append the sole element
	g.RegularNumbersEvening = append(g.RegularNumbersEvening, g.RegularEvening)
}

// Splits comma-separated early bird series into a slice of numbers.
// Mostly for Lottario
func (g *Game) SplitEarlyBirdNumbers() {

	if g == nil {
		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.SplitEarlyBirdNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return
	}

	if strings.Contains(g.EarlyBird, ",") {
		g.EarlyBirdNumbers = strings.Split(g.EarlyBird, ",")
		return
	}

	// if comma not present, append the sole element
	g.EarlyBirdNumbers = append(g.EarlyBirdNumbers, g.EarlyBird)
}

func (n *Number) SplitRegularNumbers() {

	if n == nil {
		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.SplitRegularNumbers: returning to avoid panic", "the instance n is nil - this should not happen"))
		return
	}

	if strings.Contains(n.Regular, ",") {
		n.RegularNumbers = strings.Split(n.Regular, ",")
		return
	}

	// if comma not present, append the sole element
	n.RegularNumbers = append(n.RegularNumbers, n.Regular)
}

// Global Variables

// Interface Methods

// Implements the olg-services/cronJob/CronJob interface
func (winningNumbersStruct *WinningNumbers) ExecuteCronJob() {

	// check if the previous job is still running
	if winningNumbersStruct.IsJobStillRunning() {
		winningNumbersStruct.LogDebug("Previous winning number feed ingestion still operating, still waiting. ")
		return
	}

	// Indicate the start of the operation
	winningNumbersStruct.SetJobRunningFlag(true)

	winningNumbersStruct.SetLastDateProcessed(time.Now())

	// The defer statement will insure that we indicate the end of the operation
	// in any exit circumstances. Without the defer statement we would need to call this
	// any time (in case we exit this routine early due to an error)
	defer winningNumbersStruct.SetJobRunningFlag(false)

	// make sure we also recover from any panic
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("FATAL ERRROR: Recovered from panic", r)
		}
	}()

	now := time.Now()
	debugDumpFileNameSuffix := now.Format("-2006-01-02-15-04-05-error.txt")
	var debugDump bool = false
	if config.GlobalConfig.Environment.IsDummyMode {
		debugDump = true
	}

	err := winningNumbersStruct.LoadFeedWithDebug(debugDump, debugDumpFileNameSuffix, "", "")
	if err != nil {

		// being inside a cron job, this is a recurring error
		// we need to treat it with the special LogRecurringError method
		// that method set the job status to faulty
		winningNumbersStruct.LogRecurringError("Error grabbing the XML Webservice: ", err)

		// set the error message
		winningNumbersStruct.SetLastErrorMessage(err.Error())

		// for errors, in dummy mode scrape the incumbent olg site winning numbers page
		// from: http://www.olg.ca/lotteries/viewWinningNumbers.do
		// along side the xml feed to see what happened
		if config.GlobalConfig.Environment.IsDummyMode {

			go web.ScrapeToFile("http://www.olg.ca/lotteries/viewWinningNumbers.do", "./winningNumbersPAGE"+debugDumpFileNameSuffix)

		}

		return
	}

	// we appear to have arrived at the end of the job interation unscathed
	// if the status is faulty, it means it was from the previous iteration
	// set it to running again and notify the stakeholders via email or sms that is was repaired
	if winningNumbersStruct.GetJobStatus() == cronJob.JOB_STATUS_FAULTY {
		winningNumbersStruct.LogRecoveryNoticeAndSetJobStatusToRunning("This is an automated notification: The Winning Numbers Feed job functionality has been restored. The feed is now working properly.")
	}

	// set the timestamp for correctly processed to now
	winningNumbersStruct.SetLastCorrectlyProcessedDate(time.Now())

	return

}

// Loads the winning numbers feed from OLG without dumping any debug info in case of error
func (winningNumbersStruct *WinningNumbers) LoadFeed(mockFeedStr string, timeOffset string) error {

	return winningNumbersStruct.LoadFeedWithDebug(false, "", mockFeedStr, timeOffset)

}

func isJackpotEmpty(j *Jackpot) bool {

	if j == nil {
		return true
	}

	r := strings.NewReplacer(" ", "", "\n", "", "\t", "", "\r", "")

	rChardata := r.Replace(j.Chardata)
	rInnerXml := r.Replace(j.InnerXml)

	if rChardata == "" && rInnerXml != "" {
		//we have sub elements
		return false
	} else {
		//only have jackpot
		if rChardata == "" {
			return true
		}

		return false
	}

}

func (g *Game) modifyStatus(drawDate string, nextDrawDate string, currentDate time.Time, dayOfWeek time.Weekday, timeOffsetInt int) error {

	//	var timeOffset, _ = config.GetEntryValueDirectlyFromConfigFile("timeOffset")
	//	timeOffsetInt, err := strconv.Atoi(timeOffset)
	//	if err != nil {
	//		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.modifyStatus: returning to avoid panic", "time offset to int"))
	//		return err
	//	}

	timeZone, err := time.LoadLocation("America/New_York")

	if err != nil {
		log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.modifyStatus: returning to avoid panic", "LoadLocation"))
		return err
	}

	var drawDateXml time.Time
	var drawDate_year int
	var drawDate_month int
	var drawDate_day int

	// currentDate is important to the variable "now" because it factors
	// in fake date and time before this method is called
	now := currentDate.Add(time.Duration(timeOffsetInt) * time.Hour)

	if drawDate != "" {
		// drawDate is an xml node from each game, we want to split
		// the date string so we can use actual date objects
		// dates follow the format YYYY-MM-DD
		drawDate_dateStr := strings.Split(drawDate, "-")

		if len(drawDate_dateStr) < 3 {
			// if we don't have 3 elements from splitting the date
			// something is wrong
			return errors.New("Error splitting drawDate_dateStr")
		}

		// take each part of the date string and convert it to an int
		drawDate_year, err = strconv.Atoi(drawDate_dateStr[0])
		drawDate_month, err = strconv.Atoi(drawDate_dateStr[1])
		drawDate_day, err = strconv.Atoi(drawDate_dateStr[2][:2])

		if err != nil {
			log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.modifyStatus: returning to avoid panic", "Atoi Error"))
			return err
		}

		//use the same year, so it doesnt matter if OLGs fake date is off by a year
		drawDateXml = time.Date(drawDate_year, time.Month(drawDate_month), drawDate_day, 0, 0, 0, 0, timeZone)
	}

	var nextDrawDateXml time.Time

	if nextDrawDate != "" {

		nextDrawDate_dateStr := strings.Split(nextDrawDate, "-")

		if len(nextDrawDate_dateStr) < 3 {
			return errors.New("Error splitting nextDrawDate_dateStr")
		}

		nextDrawDate_year, err := strconv.Atoi(nextDrawDate_dateStr[0])
		nextDrawDate_month, err := strconv.Atoi(nextDrawDate_dateStr[1])
		nextDrawDate_day, err := strconv.Atoi(nextDrawDate_dateStr[2][:2])

		if err != nil {
			log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.modifyStatus: returning to avoid panic", "Atoi Error"))
			return err
		}

		nextDrawDateXml = time.Date(nextDrawDate_year, time.Month(nextDrawDate_month), nextDrawDate_day, 0, 0, 0, 0, timeZone)
	}

	r := strings.NewReplacer("/", "", " ", "")
	result := r.Replace(g.Name)
	key := strings.ToLower(result)

	//exception for WOF and megadice keys
	if key == "megadicelotto" {
		key = "megadice"
	}

	if key == "wheelfortunelotto" {
		key = "wheeloffortune"
	}

	if key == "" {
		fmt.Println("Winning Numbers Error: Key is nil")
		return errors.New("Winning Numbers Error: Key is nil")
	}

	// non-daily games draw time
	var eveningDraw time.Time
	var isDailyGame bool
	var isWatchNWin bool
	var isMiddayDraw bool
	var isEveningDraw bool
	drawDateXml_DayOnly := time.Date(drawDate_year, time.Month(drawDate_month), drawDate_day, 0, 0, 0, 0, timeZone)

	if games.GameTypes[key].Category == DAILY {

		if now.Hour() < 18 {
			//if before 6pm, use midday draw time
			// using the current month and day, set the time to midday draw time
			isMiddayDraw = true
			drawDateXml = time.Date(drawDate_year, time.Month(drawDate_month), drawDate_day, 14, 0, 0, 0, timeZone)
		} else {
			// using the current month and day, set the time to evening draw time
			isEveningDraw = true
			drawDateXml = time.Date(drawDate_year, time.Month(drawDate_month), drawDate_day, 22, 30, 0, 0, timeZone)
		}

		isDailyGame = true

		g.DebugCurrentTime = now.String()

		middayDrawTime := drawDateXml.Add(time.Hour * 24)
		if isMiddayDraw && now.After(middayDrawTime) && now.Sub(middayDrawTime).Hours() < 9 {
			g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
		}
		//https://play.golang.org/p/YrNAN8HCn1
		if isEveningDraw && now.After(drawDateXml) && now.Sub(drawDateXml).Hours() < 9 {
			g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
		}

	} else if games.GameTypes[key].Category == WATCH_AND_WIN {

		drawDateXml = time.Date(drawDate_year, time.Month(drawDate_month), drawDate_day, 22, 30, 0, 0, timeZone)

		isDailyGame = true
		isWatchNWin = true
		dailyDrawTime := drawDateXml.Add(time.Hour * 24)
		g.DebugCurrentTime = now.String()

		if now.After(dailyDrawTime) && now.Sub(dailyDrawTime).Hours() < 9 {
			g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
		}

	} else {

		isDailyGame = false
		if games.GameTypes[key].Category == LOTTO_GAME {

			isSaturday := time.Saturday == now.Weekday()
			isWednesday := time.Wednesday == now.Weekday()
			isFriday := time.Friday == now.Weekday()
			dailygrandDrawDay := time.Monday == now.Weekday() || time.Thursday == now.Weekday()
			g.DebugCurrentTime = now.String()

			eveningDraw = time.Date(now.Year(), now.Month(), now.Day(), 22, 30, 0, 0, timeZone)
			diff := now.Sub(eveningDraw).Hours()

			if key == "lotto649" && (isWednesday || isSaturday) && now.After(eveningDraw) && diff < 9 {
				g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
			}
			if key == "lottario" && isSaturday && now.After(eveningDraw) && diff < 9 {
				g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
			}
			if key == "ontario49" && (isWednesday || isSaturday) && now.After(eveningDraw) && diff < 9 {
				g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
			}
			if key == "lottomax" && (isFriday) && now.After(eveningDraw) && diff < 9 {
				g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
			}
			if key == "dailygrand" && (dailygrandDrawDay) && now.After(eveningDraw) && diff < 9 {
				g.Status = WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS
			}
		}
	}

	// Calculating
	if games.GameTypes[key].HasJackpot == true && games.GameTypes[key].Category != WATCH_AND_WIN {
		eveningDraw = time.Date(now.Year(), now.Month(), now.Day(), 22, 30, 0, 0, timeZone)
		if now.After(eveningDraw) {
			if isJackpotEmpty(g.Jackpot) && g.NextDrawDate == "" {
				g.Status = WINNING_NUMBERS_GAME_STATUS_CALCULATING
			}
		}
	}

	today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, timeZone)

	if isDailyGame && drawDateXml_DayOnly.Equal(today) {

		// Only Daily Games
		if isMiddayDraw && g.RegularMidday != "" {
			g.Status = WINNING_NUMBERS_GAME_STATUS_OK
		}

		if isEveningDraw && g.RegularEvening != "" {
			g.Status = WINNING_NUMBERS_GAME_STATUS_OK
		}

		// Watch N Win
		if isWatchNWin {
			g.Status = WINNING_NUMBERS_GAME_STATUS_OK
		}

	} else {
		// Lotto Games
		if now.After(drawDateXml) && now.Before(nextDrawDateXml) {
			g.Status = WINNING_NUMBERS_GAME_STATUS_OK
		}
	}

	return nil
}

func useMockFeed(mockFeedStr string) (bool, string) {

	if mockFeedStr != "" {
		return true, mockFeedStr
	} else {
		return false, ""
	}
}

func useTimeOffset(timeOffsetStr string) string {

	if timeOffsetStr != "" {
		return timeOffsetStr
	} else {
		return "0"
	}
}

// Loads the winning numbers feed from OLG
func (winningNumbersStruct *WinningNumbers) LoadFeedWithDebug(debugDump bool, debugDumpFileNameSuffix string, mockFeedStr string, timeOffsetStr string) error {

	var err error

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}

	var resp *http.Response
	mockFeed, endpoint := useMockFeed(mockFeedStr)
	timeOffset := useTimeOffset(timeOffsetStr)

	if mockFeed {

		mockableUrl, _ := config.GetEntryValueDirectlyFromConfigFile("mockableUrl")

		req, err := http.NewRequest("GET", mockableUrl+endpoint, nil)

		if err != nil {
			log.Error("Error constructing request for mock feed", err)
		}
		fmt.Println("req", req)
		resp, err = client.Do(req)

		if err != nil {
			log.Error("Error trying to get mock feed", err)
		}

	} else {
		var testEnv, _ = config.GetEntryValueDirectlyFromConfigFile("testEnvironment")
		resp, err = client.Get(config.GlobalConfig.LotteryFeedURLs.WinningNumbers + "?env=" + testEnv)
	}

	if err != nil {
		winningNumbersStruct.StatusCode = strconv.Itoa(constants.FEED_STATUS_CODE_FEED_HTTP_ERROR)
		winningNumbersStruct.StatusMessage = constants.FEED_STATUS_MESSAGE_ERROR
		winningNumbersStruct.ErrorMessage = err.Error()
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {

		if debugDump {
			var dumpBody string = "Error: " + err.Error() + "\n"
			if body != nil {
				dumpBody = dumpBody + string(body)
			}
			go writeDumpToFile([]byte(dumpBody), "./winningNumbersXML-ReadAll-"+debugDumpFileNameSuffix)
		}

		winningNumbersStruct.StatusCode = strconv.Itoa(constants.FEED_STATUS_CODE_FEED_HTTP_ERROR)
		winningNumbersStruct.StatusMessage = constants.FEED_STATUS_MESSAGE_ERROR
		winningNumbersStruct.ErrorMessage = err.Error()
		return err
	}

	newFeedXmlStruct := WinningNumbers{}

	reader := bytes.NewReader(body)
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = charset.NewReader
	err = decoder.Decode(&newFeedXmlStruct)
	if err != nil {

		if debugDump {
			var dumpBody string = "Error: " + err.Error() + "\n"
			if body != nil {
				dumpBody = dumpBody + string(body)
			}
			go writeDumpToFile([]byte(dumpBody), "./winningNumbersXML-decoderDecode-"+debugDumpFileNameSuffix)
		}

		winningNumbersStruct.StatusCode = strconv.Itoa(constants.FEED_STATUS_CODE_FEED_XML_DECODE_ERROR)
		winningNumbersStruct.StatusMessage = constants.FEED_STATUS_MESSAGE_ERROR
		winningNumbersStruct.ErrorMessage = err.Error()
		return err
	}

	// create a temporary map to identify which games were not sent by OLG
	var gamesActuallySentViaFeed map[string]bool = make(map[string]bool)

	// apply various JSON-output-oriented transformation in each of the games
	for i := range newFeedXmlStruct.Games {

		//currentGame := newFeedXmlStruct.Games[i].Name

		// apply the OK status code, initially
		newFeedXmlStruct.Games[i].Status = WINNING_NUMBERS_GAME_STATUS_OK

		// identify this game type as sent
		gamesActuallySentViaFeed[newFeedXmlStruct.Games[i].Name] = true

		// split the comma-separated regular numbers in the draws
		if newFeedXmlStruct.Games[i].Regular != "" {
			newFeedXmlStruct.Games[i].SplitRegularNumbers()
		}

		if newFeedXmlStruct.Games[i].RegularMidday != "" {
			newFeedXmlStruct.Games[i].SplitRegularNumbersMidday()
		}

		if newFeedXmlStruct.Games[i].RegularEvening != "" {
			newFeedXmlStruct.Games[i].SplitRegularNumbersEvening()
		}

		// split the early bird numbers for Lottario and other applicable games types
		if newFeedXmlStruct.Games[i].EarlyBird != "" {
			newFeedXmlStruct.Games[i].SplitEarlyBirdNumbers()
		}

		// Apply daily grand transformations for Jackpot
		r := strings.NewReplacer(" ", "", "\n", "", "\t", "", "\r", "")

		if newFeedXmlStruct.Games[i].Jackpot != nil {

			rChardata := r.Replace(newFeedXmlStruct.Games[i].Jackpot.Chardata)
			rInnerXml := r.Replace(newFeedXmlStruct.Games[i].Jackpot.InnerXml)

			// if chardata is not empty, this is the old format (only a number)
			if rChardata == "" && rInnerXml != "" {

				// we have the jackpot subelement to deal with
				newFeedXmlStruct.Games[i].JackpotOutput = newFeedXmlStruct.Games[i].Jackpot.Amount
				if len(newFeedXmlStruct.Games[i].Jackpot.Description) > 0 {
					newFeedXmlStruct.Games[i].JackDescriptionEn = newFeedXmlStruct.Games[i].Jackpot.Description[0].Description
					newFeedXmlStruct.Games[i].JackDescriptionFr = newFeedXmlStruct.Games[i].Jackpot.Description[1].Description
				}
			} else {

				newFeedXmlStruct.Games[i].JackpotOutput = newFeedXmlStruct.Games[i].Jackpot.Chardata
			}
		}

		// Split the nested numbers elemnt for daily grand special events
		if newFeedXmlStruct.Games[i].SpecialEvent != nil {
			for j := range newFeedXmlStruct.Games[i].SpecialEvent {
				if newFeedXmlStruct.Games[i].SpecialEvent[j].Number != nil {
					for k := range newFeedXmlStruct.Games[i].SpecialEvent[j].Number {
						newFeedXmlStruct.Games[i].SpecialEvent[j].Number[k].SplitRegularNumbers()
					}
				}
			}
		}

		//		var timeOffset, err = config.GetEntryValueDirectlyFromConfigFile("timeOffset")
		//		if err != nil {
		//			log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.loadFeedWithDebug: returning to avoid panic", "time offset"))
		//			return err
		//		}

		timeOffsetInt, err := strconv.Atoi(timeOffset)
		if err != nil {
			log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.loadFeedWithDebug: returning to avoid panic", "time offset to int"))
			return err
		}

		timeZone, err := time.LoadLocation("America/New_York")

		if err != nil {
			log.Error("lotteries/winningNumbers package:", log.NewErrorLocal("Game.modifyStatus: returning to avoid panic", "LoadLocation"))
			return err
		}

		var useFakeDate, _ = config.GetEntryValueDirectlyFromConfigFile("useFakeDate")
		var fakeDate time.Time

		if useFakeDate == "true" {
			var fakeYearStr, _ = config.GetEntryValueDirectlyFromConfigFile("fakeYear")
			var fakeMonthStr, _ = config.GetEntryValueDirectlyFromConfigFile("fakeMonth")
			var fakeDayStr, _ = config.GetEntryValueDirectlyFromConfigFile("fakeDay")
			fakeYear, _ := strconv.Atoi(fakeYearStr)
			fakeMonthInt, _ := strconv.Atoi(fakeMonthStr)
			fakeMonth := time.Month(fakeMonthInt)
			fakeDay, _ := strconv.Atoi(fakeDayStr)
			now := time.Now()
			fakeDate = time.Date(fakeYear, fakeMonth, fakeDay, now.Hour(), now.Minute(), now.Second(), 0, timeZone) //.Add(time.Duration(timeOffsetInt) * time.Hour)
		}

		var currentDate time.Time
		if useFakeDate == "true" {
			currentDate = fakeDate
		} else {
			currentDate = time.Now().Add(time.Duration(timeOffsetInt) * time.Hour)
		}
		dow := time.Weekday(currentDate.Weekday())

		drawDate := newFeedXmlStruct.Games[i].DrawDate
		nextDrawDate := newFeedXmlStruct.Games[i].NextDrawDate

		newFeedXmlStruct.Games[i].DebugDate = currentDate.String()

		modErr := newFeedXmlStruct.Games[i].modifyStatus(drawDate, nextDrawDate, currentDate, dow, timeOffsetInt)

		if modErr != nil {
			fmt.Println("Winning Numbers Error: Could not set", newFeedXmlStruct.Games[i].Name, "status")
		}

	}

	// all seems to be going in order, so replace the old struct with the new one
	winningNumbersStruct.ReplaceData(newFeedXmlStruct)

	// when the draw occurs, some of the games simply dissappear from the OLG feed
	// so we need to at least give the CMS (Tamarin) an indication that the
	// particular game has a draw in progress via the Status field

	for _, gameType := range games.GameTypes {

		_, found := gamesActuallySentViaFeed[gameType.WinningNumbersCode]
		if found == false {

			// at any point if we are missing a game, it is in progress
			fakeGameEntry := Game{Name: gameType.WinningNumbersCode, Status: WINNING_NUMBERS_GAME_STATUS_IN_PROGRESS}
			winningNumbersStruct.Games = append(winningNumbersStruct.Games, fakeGameEntry)

		}
	}

	winningNumbersStruct.StatusCode = strconv.Itoa(constants.FEED_STATUS_CODE_OK)
	winningNumbersStruct.StatusMessage = constants.FEED_STATUS_MESSAGE_OK
	winningNumbersStruct.ErrorMessage = ""

	return nil

}

func (winningNumbersStruct *WinningNumbers) ReplaceData(newWinningNumbersStruct WinningNumbers) {

	winningNumbersStruct.Games = nil
	newGamesSlice := make([]Game, len(newWinningNumbersStruct.Games))
	copy(newGamesSlice, newWinningNumbersStruct.Games)
	winningNumbersStruct.Games = newGamesSlice

}

func NewWinningNumbers(jobName string, sendErrorEmails, sendErrorSms bool) WinningNumbers {

	winningNumbersJob := WinningNumbers{}

	winningNumbersJob.JobName = jobName

	winningNumbersJob.SetSendEmailMessages(sendErrorEmails)
	winningNumbersJob.SetSendSmsMessages(sendErrorSms)

	return winningNumbersJob
}

// this struct is used as a wrapper to simulate a standardized JSON response
type WinningNumbersWrapper struct {
	StatusCode    string `json:"code"`
	StatusMessage string `json:"status"`
	ErrorMessage  string `json:"message,omitempty"`

	Data WinningNumbersData `json:"data"`
}

type WinningNumbersData struct {
	WinningNumbers WinningNumbers
}

func EmbedIntoNewFeedStruct(winningNumbersStruct WinningNumbers) WinningNumbersWrapper {

	newFeedFormatStruct := WinningNumbersWrapper{}
	newFeedFormatStruct.StatusCode = winningNumbersStruct.StatusCode
	newFeedFormatStruct.StatusMessage = winningNumbersStruct.StatusMessage
	newFeedFormatStruct.ErrorMessage = winningNumbersStruct.ErrorMessage
	newFeedFormatStruct.Data.WinningNumbers = winningNumbersStruct
	return newFeedFormatStruct

}

func writeDumpToFile(contents []byte, destinationFile string) {
	writeErr := ioutil.WriteFile(destinationFile, contents, 0644)
	if writeErr != nil {
		log.Error("winningNumbers - writeDumpToFile(): ioutil.WriteFile error: ", writeErr)
	}
}
