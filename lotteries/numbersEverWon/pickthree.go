package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type Pick3NumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	Pick3Results Pick3Results `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the Pick3 feed
type Pick3Results struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	Pick3Element *Pick3 `xml:"pick3" json:"pick3,omitempty"`
}

// This corresponds to the unique "pick3" xml element in the Pick3 feed
type Pick3 struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"pick3" json:"-"`

	Draws []Draw `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *Pick3NumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *Pick3NumbersEverWon) IsValid() bool {
	return feed.Pick3Results.Pick3Element != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *Pick3NumbersEverWon) ValidateRequestParams() (int, string) {

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	// Pick 3 is allowed to contain duplicates
	//	if validationErr = ValidateDuplication(
	//		feed.InputParams.Get("num1"),
	//		feed.InputParams.Get("num2"),
	//		feed.InputParams.Get("num3")); validationErr != "" {
	//		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	//	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" Pick3 feed from OLG
func (feed *Pick3NumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> Pick3NumbersEverWon: LoadFeed")
	}

	// instantiate a new feed structure
	newFeedData := Pick3NumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_PICK_THREE)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.Pick3Results = newFeedData.Pick3Results
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.Pick3Results.Pick3Element.Searched = feed.Pick3Results.Pick3Element.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByPrizeMatch(feed.Pick3Results.Pick3Element.Draws))
	}

	return nil

}
