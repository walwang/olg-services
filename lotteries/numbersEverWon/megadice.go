package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type MegaDiceNumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	MegaDiceResults MegaDiceResults `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the MegaDice feed
type MegaDiceResults struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	MegaDiceElement *MegaDice `xml:"megadice" json:"megadice,omitempty"`
}

// This corresponds to the unique "megadice" xml element in the MegaDice feed
type MegaDice struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"megadice" json:"-"`

	Draws []Draw `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *MegaDiceNumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *MegaDiceNumbersEverWon) IsValid() bool {
	return feed.MegaDiceResults.MegaDiceElement != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *MegaDiceNumbersEverWon) ValidateRequestParams() (int, string) {

	// Megadice requires 7 numbers, each from 0 to 39 inclusive

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 0, 39); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 0, 39); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 0, 39); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num4", feed.InputParams.Get("num4"), 0, 39); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num5", feed.InputParams.Get("num5"), 0, 39); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num6", feed.InputParams.Get("num6"), 0, 39); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	//	if validationErr = ValidateNonEmptyNumber("num7", feed.InputParams.Get("num7"), 0, 39); validationErr != "" {
	//		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	//	}

	// ensure the parameters do not contain duplicates
	if validationErr = ValidateDuplication(
		feed.InputParams.Get("num1"),
		feed.InputParams.Get("num2"),
		feed.InputParams.Get("num3"),
		feed.InputParams.Get("num4"),
		feed.InputParams.Get("num5"),
		feed.InputParams.Get("num6"),
	//feed.InputParams.Get("num7")
	); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" MegaDice feed from OLG
func (feed *MegaDiceNumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> MegaDiceNumbersEverWon: LoadFeed")
	}

	// instantiate a new feed structure
	newFeedData := MegaDiceNumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_MEGADICE)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.MegaDiceResults = newFeedData.MegaDiceResults
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.MegaDiceResults.MegaDiceElement.Searched = feed.MegaDiceResults.MegaDiceElement.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByPrizeMatch(feed.MegaDiceResults.MegaDiceElement.Draws))
	}

	return nil

}
