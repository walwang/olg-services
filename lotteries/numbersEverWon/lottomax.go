package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type LottoMaxNumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	LottoMaxResults LottoMaxResults `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the LottoMax feed
type LottoMaxResults struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	LottoMaxElement *LottoMax `xml:"lottomax" json:"lottomax,omitempty"`
}

// This corresponds to the unique "lottomax" xml element in the LottoMax feed
type LottoMax struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"lottomax" json:"-"`

	Draws []LottoMaxDraw `xml:"draw" json:"draws"`
}

// The LottoMaxDraw embeds the common Draw struct, but adds support for the "MaxMillion" info
type LottoMaxDraw struct {

	// embed the Draw structure
	Draw

	// add the MaxMillion support

	// Numbers drawn with that particular "MaxMillion" occastion
	// and the match flag
	MaxMillion []Number `xml:"maxmillion>number" json:"maxmillion,omitempty"`
}

/* BEGIN: LottoMax Sorting Section */

// ByLottoMaxPrizeMatch implements sort.Interface for []Draw based on
// the PrizeMatch field specifically for LottoMax lottery.
type ByLottoMaxPrizeMatch []LottoMaxDraw

func (a ByLottoMaxPrizeMatch) Len() int           { return len(a) }
func (a ByLottoMaxPrizeMatch) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByLottoMaxPrizeMatch) Less(i, j int) bool { return a[i].PrizeMatch < a[j].PrizeMatch }

/* END: LottoMax Sorting Section */

/* Methods */

func (feed *LottoMaxNumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LottoMaxNumbersEverWon) IsValid() bool {
	return feed.LottoMaxResults.LottoMaxElement != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *LottoMaxNumbersEverWon) ValidateRequestParams() (int, string) {

	// Lotto MAX requires 7 numbers, each from 1 to 49 inclusive

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num4", feed.InputParams.Get("num4"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num5", feed.InputParams.Get("num5"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num6", feed.InputParams.Get("num6"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num7", feed.InputParams.Get("num7"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	// ensure the parameters do not contain duplicates
	if validationErr = ValidateDuplication(
		feed.InputParams.Get("num1"),
		feed.InputParams.Get("num2"),
		feed.InputParams.Get("num3"),
		feed.InputParams.Get("num4"),
		feed.InputParams.Get("num5"),
		feed.InputParams.Get("num6"),
		feed.InputParams.Get("num7")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" LottoMax feed from OLG
func (feed *LottoMaxNumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LottoMaxNumbersEverWon: LoadFeed")
	}

	// instantiate a new feed structure
	newFeedData := LottoMaxNumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_LOTTOMAX)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LottoMaxResults = newFeedData.LottoMaxResults
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.LottoMaxResults.LottoMaxElement.Searched = feed.LottoMaxResults.LottoMaxElement.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByLottoMaxPrizeMatch(feed.LottoMaxResults.LottoMaxElement.Draws))
	}

	return nil

}
