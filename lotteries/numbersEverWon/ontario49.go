package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type Ontario49NumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	Ontario49Results Ontario49Results `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the Ontario49 feed
type Ontario49Results struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	Ontario49Element *Ontario49 `xml:"ontario49" json:"ontario49,omitempty"`
}

// This corresponds to the unique "ontario49" xml element in the Ontario49 feed
type Ontario49 struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"ontario49" json:"-"`

	Draws []Draw `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *Ontario49NumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *Ontario49NumbersEverWon) IsValid() bool {
	return feed.Ontario49Results.Ontario49Element != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *Ontario49NumbersEverWon) ValidateRequestParams() (int, string) {

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num4", feed.InputParams.Get("num4"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num5", feed.InputParams.Get("num5"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num6", feed.InputParams.Get("num6"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	// ensure the parameters do not contain duplicates
	if validationErr = ValidateDuplication(
		feed.InputParams.Get("num1"),
		feed.InputParams.Get("num2"),
		feed.InputParams.Get("num3"),
		feed.InputParams.Get("num4"),
		feed.InputParams.Get("num5"),
		feed.InputParams.Get("num6")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" Ontario49 feed from OLG
func (feed *Ontario49NumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> Ontario49NumbersEverWon: LoadFeed")
	}

	// instantiate a new feed structure
	newFeedData := Ontario49NumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_ONTARIO49)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.Ontario49Results = newFeedData.Ontario49Results
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.Ontario49Results.Ontario49Element.Searched = feed.Ontario49Results.Ontario49Element.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByPrizeMatch(feed.Ontario49Results.Ontario49Element.Draws))
	}

	return nil

}
