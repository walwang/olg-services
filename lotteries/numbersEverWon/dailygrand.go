package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type DailyGrandNumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	DailyGrandResults DailyGrandResults `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the Lotto 649 feed
type DailyGrandResults struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	DailyGrandElement *DailyGrand `xml:"dailygrand" json:"dailygrand,omitempty"`
}

// This corresponds to the unique "dailygrand" xml element in the Lotto 649 feed
type DailyGrand struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums>regular" json:"-"`
	Bonus           string          `xml:"searchNums>bonus" json:"bonus"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"dailygrand" json:"-"`

	Draws []Draw `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *DailyGrandNumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *DailyGrandNumbersEverWon) IsValid() bool {
	return feed.DailyGrandResults.DailyGrandElement != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *DailyGrandNumbersEverWon) ValidateRequestParams() (int, string) {

	// Daily Grand requires 5 numbers, each from 1 to 49 inclusive

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num4", feed.InputParams.Get("num4"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("grandNum", feed.InputParams.Get("num5"), 1, 49); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	// ensure the parameters do not contain duplicates
	if validationErr = ValidateDuplication(
		feed.InputParams.Get("num1"),
		feed.InputParams.Get("num2"),
		feed.InputParams.Get("num3"),
		feed.InputParams.Get("num4"),
		feed.InputParams.Get("num5")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}
	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" Lotto649 feed from OLG
func (feed *DailyGrandNumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> DailyGrandNumbersEverWon: LoadFeed")
	}

	// instantiate a new feed structure
	newFeedData := DailyGrandNumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_DAILYGRAND)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.DailyGrandResults = newFeedData.DailyGrandResults
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.DailyGrandResults.DailyGrandElement.Searched = feed.DailyGrandResults.DailyGrandElement.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByPrizeMatch(feed.DailyGrandResults.DailyGrandElement.Draws))
	}

	return nil

}
