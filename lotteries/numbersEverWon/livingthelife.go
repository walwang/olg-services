package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type LivingLifeNumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	LivingLifeResults LivingLifeResults `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the Living the Lifr feed
type LivingLifeResults struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	LivingLifeElement *LivingLife `xml:"livingthelife" json:"livingthelife,omitempty"`
}

// This corresponds to the unique "livingthelife" xml element in the LivingLife feed
type LivingLife struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"livingthelife" json:"-"`

	Draws []LivingLifeDraw `xml:"draw" json:"draws"`
}

// This corresponds to one of the many "draw" xml elements in the Living the Life section of
// "Have your numbers ever won" feeds
type LivingLifeDraw struct {
	XMLName xml.Name `xml:"draw" json:"-"`

	// Date of draw: e.g. "2015-03-14"
	Date string `xml:"date" json:"date"`

	// Shortened day code: e.g. "Sat"
	Day string `xml:"day" json:"day"`

	// The event name
	Event string `xml:"event" json:"event,omitempty"`

	// The draw type
	DrawType string `xml:"drawType" json:"drawType,omitempty"`

	// Prize match description
	PrizeMatch string `xml:"prizeMatch" json:"prizeMatch,omitempty"`

	WinNums []LivingLifeCategory `xml:"winNums>category" json:"winNums,omitempty"`
}

/* BEGIN: Living the Life Draw Sorting Section */

// ByLivingLifePrizeMatch implements sort.Interface for []Draw based on
// the PrizeMatch field specifically for Living the Life lottery.
type ByLivingLifePrizeMatch []LivingLifeDraw

func (a ByLivingLifePrizeMatch) Len() int           { return len(a) }
func (a ByLivingLifePrizeMatch) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByLivingLifePrizeMatch) Less(i, j int) bool { return a[i].PrizeMatch < a[j].PrizeMatch }

/* END: Living the Life Draw Sorting Section */

// Corresponds to each "category" item inside winNums container element in the
// Living the Life section of "Have your numbers ever won" feeds
type LivingLifeCategory struct {
	XMLName xml.Name `xml:"category" json:"-"`

	Prize string `xml:"prize" json:"prize,omitempty"`

	// Regular numbers drawn with that occastion
	// and the match flag
	Regular []Number `xml:"regular>number" json:"regular,omitempty"`
}

/* Methods */

func (feed *LivingLifeNumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LivingLifeNumbersEverWon) IsValid() bool {
	return feed.LivingLifeResults.LivingLifeElement != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *LivingLifeNumbersEverWon) ValidateRequestParams() (int, string) {

	// Living the Life requires 7 numbers, each from 0 to 9 inclusive

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num4", feed.InputParams.Get("num4"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num5", feed.InputParams.Get("num5"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num6", feed.InputParams.Get("num6"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num7", feed.InputParams.Get("num7"), 0, 9); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	// ensure the parameters do not contain duplicates
	if validationErr = ValidateDuplication(
		feed.InputParams.Get("num1"),
		feed.InputParams.Get("num2"),
		feed.InputParams.Get("num3"),
		feed.InputParams.Get("num4"),
		feed.InputParams.Get("num5"),
		feed.InputParams.Get("num6"),
		feed.InputParams.Get("num7")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" LivingLife feed from OLG
func (feed *LivingLifeNumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LivingLifeNumbersEverWon: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := LivingLifeNumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_LIVING_THE_LIFE)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LivingLifeResults = newFeedData.LivingLifeResults
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.LivingLifeResults.LivingLifeElement.Searched = feed.LivingLifeResults.LivingLifeElement.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByLivingLifePrizeMatch(feed.LivingLifeResults.LivingLifeElement.Draws))
	}

	return nil

}
