package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type DailyKenoNumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	DailyKenoResults DailyKenoResults `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the DailyKeno feed
type DailyKenoResults struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	DailyKenoElement *DailyKeno `xml:"dailykeno" json:"dailykeno,omitempty"`
}

// This corresponds to the unique "dailykeno" xml element in the DailyKeno feed
type DailyKeno struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"dailykeno" json:"-"`

	Draws []Draw `xml:"draw" json:"draws"`
}

/* Methods */

func (feed *DailyKenoNumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *DailyKenoNumbersEverWon) IsValid() bool {
	return feed.DailyKenoResults.DailyKenoElement != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *DailyKenoNumbersEverWon) ValidateRequestParams() (int, string) {

	// Daily Keno requires 2-10 numbers, each from 1 to 70 inclusive
	var validationErr string = ""

	num1 := feed.InputParams.Get("num1")
	if validationErr = ValidateNonEmptyNumber("num1", num1, 1, 70); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	num2 := feed.InputParams.Get("num2")
	if validationErr = ValidateNonEmptyNumber("num2", num2, 1, 70); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	num3 := feed.InputParams.Get("num3")
	if num3 != "" {
		if validationErr = ValidateNonEmptyNumber("num3", num3, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num4 := feed.InputParams.Get("num4")
	if num4 != "" {
		if validationErr = ValidateNonEmptyNumber("num4", num4, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num5 := feed.InputParams.Get("num5")
	if num5 != "" {
		if validationErr = ValidateNonEmptyNumber("num5", num5, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num6 := feed.InputParams.Get("num6")
	if num6 != "" {
		if validationErr = ValidateNonEmptyNumber("num6", num6, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num7 := feed.InputParams.Get("num7")
	if num7 != "" {
		if validationErr = ValidateNonEmptyNumber("num7", num7, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num8 := feed.InputParams.Get("num8")
	if num8 != "" {
		if validationErr = ValidateNonEmptyNumber("num8", num8, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num9 := feed.InputParams.Get("num9")
	if num9 != "" {
		if validationErr = ValidateNonEmptyNumber("num9", num9, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	num10 := feed.InputParams.Get("num10")
	if num10 != "" {
		if validationErr = ValidateNonEmptyNumber("num10", num10, 1, 70); validationErr != "" {
			return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
		}
	}

	// ensure the parameters do not contain duplicates
	if num3 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num4 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num5 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4,
			num5); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num6 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4,
			num5,
			num6); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num7 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4,
			num5,
			num6,
			num7); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num8 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4,
			num5,
			num6,
			num7,
			num8); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num9 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4,
			num5,
			num6,
			num7,
			num8,
			num9); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else if num10 != "" {
		if validationErr = ValidateDuplication(
			num1,
			num2,
			num3,
			num4,
			num5,
			num6,
			num7,
			num8,
			num9,
			num10); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	} else {
		if validationErr = ValidateDuplication(
			num1,
			num2); validationErr != "" {
			return constants.FEED_STATUS_CODE_DUPLICATE_PARAMETER, validationErr
		}
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" DailyKeno feed from OLG
func (feed *DailyKenoNumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> DailyKenoNumbersEverWon: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := DailyKenoNumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_DAILY_KENO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.DailyKenoResults = newFeedData.DailyKenoResults
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.DailyKenoResults.DailyKenoElement.Searched = feed.DailyKenoResults.DailyKenoElement.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByPrizeMatch(feed.DailyKenoResults.DailyKenoElement.Draws))
	}

	return nil

}
