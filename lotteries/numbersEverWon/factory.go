package numbersEverWon

import (
	"olg-services/constants"
	"olg-services/interfaces"

	"olg-services/lotteries/games"
	"olg-services/publicis/utils/log"
	"strings"
)

// Factory function for the "Have your numbers ever won ?" series of feeds
// based on the GameType structure in lotteries/games package
func NewNumbersEverWon(gameType games.GameType, jobName string, sendErrorEmails, sendErrorSms bool) (interfaces.NewXmlFeed, error) {

	var feed interfaces.NewXmlFeed

	if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTO649 {
		feed = &Lotto649NumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_DAILY_KENO {
		feed = &DailyKenoNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_DAILYGRAND {
		feed = &DailyGrandNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LIVING_THE_LIFE {
		feed = &LivingLifeNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTARIO {
		feed = &LottarioNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_LOTTOMAX {
		feed = &LottoMaxNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_MEGADICE {
		feed = &MegaDiceNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_NHLLOTTO {
		feed = &NHLLottoNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_ONTARIO49 {
		feed = &Ontario49NumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_POKERLOTTO {
		feed = &PokerLottoNumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_FOUR {
		feed = &Pick4NumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_THREE {
		feed = &Pick3NumbersEverWon{}
	} else if strings.ToLower(gameType.Code) == constants.GAME_CODE_PICK_TWO {
		feed = &Pick2NumbersEverWon{}
	} else {
		return nil, log.NewErrorLocal("numbersEverWon.NewNumbersEverWon", "the specified gameType.Code ("+gameType.Code+") does not correspond to a valid game type")
	}

	// uncomment below as structures are created
	/*
		else if strings.ToLower(gameType.Code) == constants.GAME_CODE_WHEEL_OF_FORTUNE {
			feed = &WheelOfFortuneNumbersEverWon{}
		} else if strings.ToLower(gameType.Code) == "timeout" {
			// the timeout feed just waits 35 seconds
			feed = &TimeoutNumbersEverWon{}
		}
	*/
	// attempt to set the job name if the conversion to CronJob interface succeeds
	//	cronJobStructure, ok := feed.(interfaces.CronJob)
	//	if ok {
	//		cronJobStructure.SetJobName(jobName)
	//		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
	//		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	//	}

	return feed, nil

}
