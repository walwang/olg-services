package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
)

/* Types */

type LottarioNumbersEverWon struct {

	// embed the base new feeds
	base.BaseNewFeed

	LottarioResults LottarioResults `xml:"checkNums" json:"data"`
}

// This corresponds to the unique "checkNums" xml element in the Lottario feed
type LottarioResults struct {
	XMLName xml.Name `xml:"checkNums" json:"-"`

	LottarioElement *Lottario `xml:"lottario" json:"lottario,omitempty"`
}

// This corresponds to the unique "lottario" xml element in the Lottario feed
type Lottario struct {

	// embed the SearchNums structure

	// Comma-separated numbers that user selected for checking
	// e.g. 09,19,38,40,45,47
	SearchedNumbers SearchedNumbers `xml:"searchNums" json:"-"`
	Searched        []string        `xml:"-" json:"searchNums,omitempty"`

	XMLName xml.Name `xml:"lottario" json:"-"`

	Draws []LottarioDraw `xml:"draw" json:"draws"`
}

// The LottarioDraw embeds the common Draw struct, but adds support for the "earlyBird" info
type LottarioDraw struct {

	// embed the Draw structure
	Draw

	// add the early bird support

	// Numbers drawn with that particular "early bird" occastion
	// and the match flag
	EarlyBird []Number `xml:"earlyBird>number" json:"earlyBird,omitempty"`
}

/* BEGIN: Lottario Draw Sorting Section */

// ByLottarioPrizeMatch implements sort.Interface for []Draw based on
// the PrizeMatch field specifically for Lottario lottery.
type ByLottarioPrizeMatch []LottarioDraw

func (a ByLottarioPrizeMatch) Len() int           { return len(a) }
func (a ByLottarioPrizeMatch) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByLottarioPrizeMatch) Less(i, j int) bool { return a[i].PrizeMatch < a[j].PrizeMatch }

/* END: Lottario Draw Sorting Section */

/* Methods */

func (feed *LottarioNumbersEverWon) GetFeedUrl() string {

	return config.GlobalConfig.LotteryFeedURLs.NumbersEverWon
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *LottarioNumbersEverWon) IsValid() bool {
	return feed.LottarioResults.LottarioElement != nil
}

// Before loading the feed, validate the input parameters for this particular
// game type. This method is automatically called by the LoadFeed process
func (feed *LottarioNumbersEverWon) ValidateRequestParams() (int, string) {

	// Lottario requires 7 numbers, each from 1 to 45 inclusive

	var validationErr string = ""

	if validationErr = ValidateNonEmptyNumber("num1", feed.InputParams.Get("num1"), 1, 45); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num2", feed.InputParams.Get("num2"), 1, 45); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num3", feed.InputParams.Get("num3"), 1, 45); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num4", feed.InputParams.Get("num4"), 1, 45); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num5", feed.InputParams.Get("num5"), 1, 45); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = ValidateNonEmptyNumber("num6", feed.InputParams.Get("num6"), 1, 45); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	//	if validationErr = ValidateNonEmptyNumber("num7", feed.InputParams.Get("num7"), 1, 45); validationErr != "" {
	//		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	//	}

	// ensure the parameters do not contain duplicates
	if validationErr = ValidateDuplication(
		feed.InputParams.Get("num1"),
		feed.InputParams.Get("num2"),
		feed.InputParams.Get("num3"),
		feed.InputParams.Get("num4"),
		feed.InputParams.Get("num5"),
		feed.InputParams.Get("num6"),
	//feed.InputParams.Get("num7")
	); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

// Loads the "Have your numbers ever won" Lottario feed from OLG
func (feed *LottarioNumbersEverWon) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> LottarioNumbersEverWon: LoadFeed")
	}

	// instantiate a new feed structure
	newFeedData := LottarioNumbersEverWon{}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareNumbersEverWonFeed(&feed.BaseNewFeed, constants.GAME_CODE_LOTTARIO)

	err := base.LoadXMLWithFeedOptions(feedOptions, &newFeedData, feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.LottarioResults = newFeedData.LottarioResults
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformations
	feed.LottarioResults.LottarioElement.Searched = feed.LottarioResults.LottarioElement.SearchedNumbers.SplitSearchedNumbers()

	// if the request contains sortByPrize=y then sort the draws by the PrizeMatch field
	if feed.GetRequestParam("sortByPrize") == "Y" || feed.GetRequestParam("sortByPrize") == "y" {
		SortStandardDraws(ByLottarioPrizeMatch(feed.LottarioResults.LottarioElement.Draws))
	}

	return nil

}
