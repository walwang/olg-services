package numbersEverWon

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/lotteries/base"
	"olg-services/publicis/utils/log"
	"olg-services/publicis/utils/validation"
	"sort"
	"strings"
)

type SearchedNumbers string

// This corresponds to one of the many "draw" xml elements in the past winning numbers feeds
type Draw struct {
	XMLName xml.Name `xml:"draw" json:"-"`

	// Date of draw: e.g. "2015-03-14"
	Date string `xml:"date" json:"date"`

	// Shortened day code: e.g. "Sat"
	Day string `xml:"day" json:"day"`

	// Time of day : e.g. "EVENING"
	Time string `xml:"time" json:"time,omitempty"`

	// Regular numbers drawn with that occastion
	// and the match flag
	Regular []Number `xml:"regular>number" json:"regular,omitempty"`

	// The "bonus" element
	Bonus *Bonus `xml:"bonus" json:"bonus,omitempty"`

	// Prize match description
	PrizeMatch string `xml:"prizeMatch" json:"prizeMatch,omitempty"`

	// dailygrand
	DrawType string `xml:"drawType" json:"drawType,omitempty"`
}

/* BEGIN: Draw Sorting Section */

// ByPrizeMatch implements sort.Interface for []Draw based on
// the PrizeMatch field.
type ByPrizeMatch []Draw

func (a ByPrizeMatch) Len() int           { return len(a) }
func (a ByPrizeMatch) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByPrizeMatch) Less(i, j int) bool { return a[i].PrizeMatch < a[j].PrizeMatch }

func SortStandardDraws(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

/* END: Draw Sorting Section */

type Bonus struct {
	XMLName xml.Name `xml:"bonus" json:"-"`

	Value string `xml:",chardata" json:"value,omitempty"`
	Match string `xml:"match,attr" json:"match,omitempty"`
}

type Number struct {
	XMLName xml.Name `xml:"number" json:"-"`

	Value string `xml:",chardata" json:"value,omitempty"`
	Match string `xml:"match,attr" json:"match,omitempty"`
}

type Prizes []Prize

type Prize struct {
	XMLName xml.Name `xml:"prize" json:"-"`

	Match          string `xml:"match" json:"match,omitempty"`
	WinningTickets string `xml:"winningTickets" json:"winningTickets"`
	Amount         string `xml:"amount" json:"amount"`
}

// Splits comma-separated series into a slice of numbers
func (s *SearchedNumbers) SplitSearchedNumbers() []string {

	var targetList []string = make([]string, 0)

	if s == nil {
		log.Error("lotteries/numbersEverWon package:", log.NewErrorLocal("SearchedNumbers.SplitSearchedNumbers: returning to avoid panic", "the instance m is nil - this should not happen"))
		return targetList
	}

	if strings.Contains(string(*s), ",") {
		targetList = strings.Split(string(*s), ",")
		return targetList
	}

	// if comma not present, append the sole element
	targetList = append(targetList, string(*s))
	return targetList
}

/* ************************************************** */
/* BEGIN: Have Your Numbers Ever Won Common Functions */
/* ************************************************** */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
// NumbersEverWon feeds have a common set of input parameters, and some
// variability depending on the game code.
func PrepareNumbersEverWonFeed(feed *base.BaseNewFeed, gameCode string) (string, base.FeedOptions) {

	var requestParams string = ""
	var feedOptions base.FeedOptions
	var testEnv, _ = config.GetEntryValueDirectlyFromConfigFile("testEnvironment")

	if gameCode == constants.GAME_CODE_NHLLOTTO {

		if feed.InputParams != nil {
			requestParams = feed.GetQueryParams(
				feed.KVPair("game", gameCode),
				feed.KVPair("feedType", "checkNumbers"),
				feed.KVPairOmitEmpty("num1", feed.InputParams.Get("num1")),
				feed.KVPairOmitEmpty("num2", feed.InputParams.Get("num2")),
				feed.KVPairOmitEmpty("num3", feed.InputParams.Get("num3")),
				feed.KVPairOmitEmpty("num4", feed.InputParams.Get("num4")),
				feed.KVPairOmitEmpty("num5", feed.InputParams.Get("num5")),
				feed.KVPairOmitEmpty("nhlTeam", feed.InputParams.Get("nhlTeam")), // nhl team parameter
			)
		}

	} else {
		// prepare the request parameters
		if feed.InputParams != nil {
			requestParams = feed.GetQueryParams(
				feed.KVPair("env", testEnv),
				feed.KVPair("game", gameCode),
				feed.KVPair("feedType", "checkNumbers"),
				feed.KVPairOmitEmpty("num1", feed.InputParams.Get("num1")),
				feed.KVPairOmitEmpty("num2", feed.InputParams.Get("num2")),
				feed.KVPairOmitEmpty("num3", feed.InputParams.Get("num3")),
				feed.KVPairOmitEmpty("num4", feed.InputParams.Get("num4")),
				feed.KVPairOmitEmpty("num5", feed.InputParams.Get("num5")),
				feed.KVPairOmitEmpty("num6", feed.InputParams.Get("num6")),
				feed.KVPairOmitEmpty("num7", feed.InputParams.Get("num7")),
				feed.KVPairOmitEmpty("num8", feed.InputParams.Get("num8")),
				feed.KVPairOmitEmpty("num9", feed.InputParams.Get("num9")),
				feed.KVPairOmitEmpty("num10", feed.InputParams.Get("num10")),
				feed.KVPairOmitEmpty("grandNum", feed.InputParams.Get("grandNum")), // dailygrand parameter
			)
		}

	}

	feedOptions = base.FeedOptions{
		IsHttps:            false,
		InsecureSkipVerify: true,
	}

	// END: Test Mode Parameters

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

/* Validation functions */

// Insures the parameter is non-empty and adheres to a certain range.
// If the parameter has an invalid value, it returns an error message, otherwise it
// returns an empty string
func ValidateNonEmptyNumber(paramName string, paramValue string, minVal int, maxVal int) string {

	if paramValue == "" {
		return paramName + " is empty"
	}

	positiveVal, isPositiveInteger := validation.IsZeroOrPositiveInteger(paramValue)
	if isPositiveInteger && positiveVal >= minVal && positiveVal <= maxVal {
		return ""
	}
	return paramName + " not a valid number for this game type"
}

// If there are any duplicates in the string list, return the error message.
// Otherwise, return empty string
func ValidateDuplication(paramValues ...string) string {

	if validation.HasDuplicates(paramValues...) {
		return "parameter value duplication encountered"
	}

	return ""
}

/* ************************************************** */
/* END: Have Your Numbers Ever Won Common Functions   */
/* ************************************************** */
