package interfaces

import (
	"net/http"
	"time"
)

// This interface defines the behavior of a module that imports feeds using the
// new xml format from olg
type NewXmlFeed interface {
	LoadFeed() error
	GetFeedUrl() string

	IsValid() bool

	SetIsCached(isCached bool)

	GetStatusCode() int
	SetStatusCode(int)

	GetStatusMessage() string
	SetStatusMessage(string)

	GetErrorMessage() string
	SetErrorMessage(string)

	SetRequestParam(key string, value string)
	GetRequestParam(key string) string
	SetRequestParams(*http.Request)
	ValidateRequestParams() (int, string)
}

// This interface describes behaviour specific to the Past Winning Numbers feed
type IPastWinningNumbers interface {
	NewXmlFeed

	GetOnlyLastDraw() bool
	SetOnlyLastDraw(bool)

	GetGameCode() string
	TrimDateRange(startDate time.Time, endDate time.Time)
	AppendDraws(IPastWinningNumbers)

	TrimDrawsStartingWithIndex(index int)
}

// This interface describes behaviour specific to the Past Winning Numbers feed
type IWinningFrequency interface {
	NewXmlFeed

	SortBy(string)
}

// This interface describes behaviour specific to the Win Tracker feeds
type IWinTracker interface {
	NewXmlFeed

	SortBy(string)
}
