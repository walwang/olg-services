package interfaces

// The ICacheable interface allows the implementing structure to use the
// provided cache store
type ICacheable interface {
	SetCacheStore(interface{})
	GetCacheStore() interface{}
}

// This interface describes methods to cache data and subsequently retrieve it
// from cache, if already in there.
// Any type implementing this interface is assumed to be cacheable.
type IFeedCache interface {
	StoreCachedData(rawData string)
	GetCachedData() string
}
