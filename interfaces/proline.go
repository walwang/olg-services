package interfaces

import (
	"time"
)

// This interface describes behaviour specific to the Proline events feed
type IProlineEventsResults interface {
	NewXmlFeed

	SetSportId(string)
	GetSportId() string

	// any struct that is the concrete implementation of this interface and
	// represents the results feed should return true, otherwise false
	IsResultsFeed() bool

	AppendEvents(IProlineEventsResults)
	TrimDateRange(startDate time.Time, endDate time.Time)

	SortBy(string)
}

type IProlinePointSpread interface {
	NewXmlFeed

	SetSportId(string)
	GetSportId() string

	IsResultsFeed() bool

	AppendEvents(IProlinePointSpread)
	TrimDateRange(startDate time.Time, endDate time.Time)

	SortBy(string)
}

type IProlineProPicks interface {
	NewXmlFeed

	SetSportId(string)
	GetSportId() string

	IsResultsFeed() bool

	AppendEvents(IProlinePointSpread)
	TrimDateRange(startDate time.Time, endDate time.Time)

	SortBy(string)
}
