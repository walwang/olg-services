package constants

const (
	EMAIL_STATUS_CODE_OK       = 0
	EMAIL_STATUS_MESSAGE_OK    = "success"
	EMAIL_STATUS_MESSAGE_ERROR = "error"

	EMAIL_UNMARSHALING_INPUT_TO_EMAIL_OBJECT_ERROR = "Error by unmarshaling input string to email object"

	EMAIL_STATUS_CODE_SYSTEM_ERROR                      = -1000
	EMAIL_UNMARSHALING_INPUT_TO_EMAIL_OBJECT_ERROR_CODE = -1001
	OLG_READ_DATA_FROM_CACHE_ERROR_CODE                 = -1002
	OLG_READ_TEMPLATE_FROM_FILE_ERROR_CODE              = -1003
	OLG_EMAIL_SEND_GENERIC_ERROR_CODE                   = -1004

	OLG_READ_DATA_FROM_CACHE_ERROR    = "Error by retrieving from cache"
	OLG_READ_TEMPLATE_FROM_FILE_ERROR = "Error by reading file template"
	OLG_EMAIL_SEND_GENERIC_ERROR      = "Error by sending email"
)
