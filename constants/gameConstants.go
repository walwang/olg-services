package constants

const (
	GAME_CODE_LOTTO649         = "lotto649"
	GAME_CODE_DAILYGRAND       = "dailygrand"
	GAME_CODE_LOTTOMAX         = "lottomax"
	GAME_CODE_NHLLOTTO         = "nhllotto"
	GAME_CODE_POKERLOTTO       = "pokerlotto"
	GAME_CODE_WHEEL_OF_FORTUNE = "wheeloffortune"
	GAME_CODE_PICK_FOUR        = "pick4"
	GAME_CODE_PICK_THREE       = "pick3"
	GAME_CODE_PICK_TWO         = "pick2"
	GAME_CODE_DAILY_KENO       = "dailykeno"
	GAME_CODE_LIVING_THE_LIFE  = "livingthelife"
	GAME_CODE_MEGADICE         = "megadice"
	GAME_CODE_LOTTARIO         = "lottario"
	GAME_CODE_ONTARIO49        = "ontario49"
)
