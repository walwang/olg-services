package constants

// cronjob related constants
const (
	CRON_JOB_STATUS_RUNNING int = 0
	CRON_JOB_STATUS_STOPPED int = 1
	CRON_JOB_STATUS_FAULTY  int = 2
)
