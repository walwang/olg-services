package emailer

import (
	"html"
	"olg-services/publicis/utils/emailer/mandrill"
)

const MANDRILL_HOST string = "smtp.mandrillapp.com"
const MANDRILL_PORT string = "587"
const MANDRILL_USERNAME string = "silviunotathome@gmail.com"
const MANDRILL_API_KEY string = "3114k8X6FOYIO_CZfPsQ0g"

func SendMandrillEmail(emailSubjectLine string, emailMessage string, embeddedError error, destEmails ...string) error {

	mandrill.Key = MANDRILL_API_KEY

	// test the Mandrill service API key with Ping
	if err := mandrill.Ping(); err != nil {
		return err
	}

	htmlContent := "<html><body><p>" + html.EscapeString(emailMessage) + "</p>"
	if embeddedError != nil {
		htmlContent = htmlContent + "<p>" + html.EscapeString(embeddedError.Error()) + "</p>"
	}
	htmlContent = htmlContent + "</body></html>"

	plainTextContent := emailMessage
	if embeddedError != nil {
		plainTextContent = plainTextContent + "\r\n********* ERROR Below ***********\r\n" + embeddedError.Error()
	}

	// everything is OK if err is nil
	msg := mandrill.NewMessage()

	for i := range destEmails {
		msg.AddRecipient(destEmails[i], "")
	}

	msg.HTML = htmlContent
	msg.Text = plainTextContent // optional
	msg.Subject = emailSubjectLine
	msg.FromEmail = MANDRILL_USERNAME
	msg.FromName = "Publicis OLG Dev Team Alert"

	// if we need the response, use a "res" variable instead of the underscore
	if _, err := msg.Send(false); err != nil {
		return err
	}

	return nil
}
