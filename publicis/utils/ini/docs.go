/*Package ini provides functions for parsing INI configuration files.
INI files are parsed by go-ini line-by-line. Each line may be one of the following:

	A section definition: [section-name]
	A property: key = value
	A comment: #blahblah or ;blahblah
	Blank. The line will be ignored.

Properties defined before any section headers are placed in the default section, which has the empty string as it's key.
*/
package ini
