package nexmo

import (
	"errors"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

var nexmoApiKey string = ""
var nexmoApiSecret string = ""

func Init(apiKey, apiSecret string) {
	nexmoApiKey = apiKey
	nexmoApiSecret = apiSecret
}

func SendTextMessage(fromNumber string, toNumber string, textToSend string) error {

	if nexmoApiKey == "" || nexmoApiSecret == "" {
		return newCustomErrorLocal("Nexmo Initialization Error", "the api key and secret are not properly set")
	}

	nexmoClient, err := NewClientFromAPI(nexmoApiKey, nexmoApiSecret)
	if err != nil {
		return err
	}

	// Test if it works by retrieving your account balance
	if _, err := nexmoClient.Account.GetBalance(); err != nil {
		return newCustomError("Failed to obtain balance", err)
	}

	// Send an SMS
	// See https://docs.nexmo.com/index.php/sms-api/send-message for details.
	message := &SMSMessage{
		From:            fromNumber,
		To:              toNumber,
		Type:            Text,
		Text:            textToSend,
		ClientReference: "nexmo-olg-notifier " + strconv.FormatInt(time.Now().Unix(), 10),
		Class:           Standard,
	}

	msgResp, err := nexmoClient.SMS.Send(message)
	if err != nil {
		return newCustomError("Error sending the text message", err)
	} else {
		log.DebugWithInterface("Message successfully sent, the reply is: ", msgResp)
	}
	return nil

}

// Wraps an already existing error with a localized prefix
func newCustomError(errorPrefix string, originalError error) error {
	return errors.New(errorPrefix + ": " + originalError.Error())
}

// Wraps local issues in an error format, without needing an already existing error
func newCustomErrorLocal(errorPrefix string, localError string) error {
	return errors.New(errorPrefix + ": " + localError)
}
