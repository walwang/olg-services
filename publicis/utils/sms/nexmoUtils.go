package sms

import (
	"errors"
	"olg-services/publicis/utils/log"
	"olg-services/publicis/utils/sms/nexmo"
	"time"
)

const API_KEY string = "e63e748b"
const API_SECRET string = "fdd0cdfe"

func SendNexmoSms(fromNumber string, toNumbers []string, textToSend string) error {

	var cummulativeErrors string = ""

	// make sure we init nexmo with the proper api key and secret
	nexmo.Init(API_KEY, API_SECRET)

	// send the message to all recipients
	for i := range toNumbers {

		log.Debug("Attempting to sms: " + toNumbers[i])
		smsErr := nexmo.SendTextMessage(fromNumber, toNumbers[i], textToSend)
		if smsErr != nil {
			cummulativeErrors = cummulativeErrors + smsErr.Error() + "\n\r"
		}

		// wait 1000 miliseconds to avoid the throughput rate exceeded
		time.Sleep(1000 * time.Millisecond)
	}

	if cummulativeErrors != "" {
		return errors.New("Error sending nexmo text messages: \n\r" + cummulativeErrors)
	}

	return nil

}
