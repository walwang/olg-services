package validation

// Returns true if the list of string values contains at least one duplicate value.
// Otherwise it returns false
func HasDuplicates(values ...string) bool {

	m := map[string]bool{}

	// walk the slice and for each value we've not seen so far
	// create a map entry
	for _, v := range values {

		if _, seen := m[v]; !seen {
			m[v] = true
		} else {
			return true
		}
	}

	return false

}
