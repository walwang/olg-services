package validation

import (
	"strconv"
)

// Returns true if value is an integer, false otherwise
func IsInteger(val string) bool {

	if _, err := strconv.Atoi(val); err != nil {
		return false
	}

	return true
}

// Returns the integer value and true if value is an integer greater than zero,
// zero value and false otherwise
func IsPositiveInteger(val string) (int, bool) {

	if positiveInt, err := strconv.Atoi(val); err == nil {

		if positiveInt > 0 {
			return positiveInt, true
		}
		return 0, false
	}

	return 0, false
}

// Returns the integer value and true if value is an integer greater than or equal to zero,
// or zero value and false otherwise
func IsZeroOrPositiveInteger(val string) (int, bool) {

	if positiveInt, err := strconv.Atoi(val); err == nil {

		if positiveInt >= 0 {
			return positiveInt, true
		}
		return 0, false
	}

	return 0, false
}
