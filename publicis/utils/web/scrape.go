package web

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Utility function that uses http GET to grab the contents from the specified url
// location and save it locally, as specified in the destinationFile.
// This method uses a default 30 seconds timeout.
func ScrapeToFile(url string, destinationFile string) {

	timeout := time.Duration(30 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	resp, err := client.Get(url)
	if err != nil {

		d1 := []byte("ScrapeToFile(): http.Get Error: " + err.Error())
		writeErr := ioutil.WriteFile(destinationFile, d1, 0644)
		if writeErr != nil {
			fmt.Println("ScrapeToFile(): ioutil.WriteFile 1 error: ", writeErr)
		}
		return

	}

	if resp.Body != nil {
		defer resp.Body.Close()
	}

	contentBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {

		d1 := []byte("ScrapeToFile(): ioutil.ReadAll Error: " + err.Error())
		writeErr := ioutil.WriteFile(destinationFile, d1, 0644)
		if writeErr != nil {
			fmt.Println("ScrapeToFile(): ioutil.WriteFile 2 error: ", writeErr)
		}
		return
	}

	writeErr := ioutil.WriteFile(destinationFile, contentBytes, 0644)
	if writeErr != nil {
		fmt.Println("ScrapeToFile(): ioutil.WriteFile 3 error: ", writeErr)
	}
	return

}
