package config

import (
	"olg-services/publicis/utils/ini"
	"strconv"
	"strings"
)

// The AppServer struct represents a configured application server currently running the
// OLG feeds services
type AppServer struct {

	// The symbolic name of the server (e.g. "AppProd1")
	Name string

	// The internal, accessible hostname or Ip Address of the server and optional port
	Address string

	// If true, it is a production server
	IsProd bool
}

type AppServerCollection []AppServer

// init the collection from the configuration file
func (l *AppServerCollection) LoadFromIniFile(iniFile ini.File, section string) error {

	// iterate and look for up to 20 servers
	for i := 1; i < 21; i++ {

		serverKeyName := "appServer." + strconv.Itoa(i) + ".Name"
		serverKeyAddress := "appServer." + strconv.Itoa(i) + ".Address"
		serverKeyIsProd := "appServer." + strconv.Itoa(i) + ".IsProd"

		var okServerName bool
		serverName, okServerName := iniFile.Get(section, serverKeyName)

		var okServerAddress bool
		serverAddress, okServerAddress := iniFile.Get(section, serverKeyAddress)

		var okServerIsProd bool
		serverIsProd, okServerIsProd := iniFile.Get(section, serverKeyIsProd)

		if okServerName && okServerAddress && okServerIsProd {
			if serverName != "" && serverAddress != "" && serverIsProd != "" {

				serverIsProdBool := (strings.ToLower(serverIsProd) == "y" || strings.ToLower(serverIsProd) == "true" || strings.ToLower(serverIsProd) == "t" || strings.ToLower(serverIsProd) == "yes")

				appServer := NewAppServer(serverName, serverAddress, serverIsProdBool)

				lTmp := append([]AppServer(*l), appServer)

				*l = AppServerCollection(lTmp)
			}
		}

	}

	return nil
}

func NewAppServer(serverName string, address string, isProd bool) AppServer {

	appServer := AppServer{
		Name:    serverName,
		Address: address,
		IsProd:  isProd,
	}

	return appServer
}
