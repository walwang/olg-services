// The config package handles the general system configuration entries, globally
// accessible from all packages referencing this
package config

import (
	"errors"
	"net/mail"
	"olg-services/publicis/utils/ini"
	"runtime/debug"
	"strconv"
	"strings"
)

// Environment contains settings that are used to configure the application on the
// current server.
type Environment struct {

	// True if dummy mode (lots of debug info, and fake feeds).
	// Do not turn this on on production
	IsDummyMode bool

	// True if this is production environment
	IsProd bool

	// The logLevel can be one of the following numbers:
	// disabled=0, error=1, warn=2, info=3, debug=4
	LogLevel int

	// GCPercent is the Go garbage collection allocation setting
	// the default is 100. Setting it to 200, 300 or 400 will mean
	// more memory bloat, but less GC runs, potentially allowing for better performance.
	// Do not change this if you are not sure.
	GCPercent int

	// DefaultFeedTimeout is the default timeout value (in seconds) when connecting to the OLG feeds
	DefaultFeedTimeout int

	// DocsUrlBase, if not empty, takes precedence over the actual server hostname, in case
	// we want to make the docs public, via a proxy
	DocsUrlBase string
}

func (l *Environment) LoadFromIniFile(iniFile ini.File, section string) (err error) {

	var ok bool
	var isDummyMode, isProd string
	var currentLogLevel, gcPercent, defaultFeedTimeout string

	isDummyMode, ok = iniFile.Get(section, "dummyMode")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the dummyMode key is invalid, missing or empty")
	}

	l.IsDummyMode, err = strconv.ParseBool(isDummyMode)
	if err != nil {
		return errors.New("config.LoadConfigFromFile ERROR: the dummyMode value is invalid, must be true or false: " + err.Error())
	}

	isProd, ok = iniFile.Get(section, "isProd")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the isProd key is invalid, missing or empty")
	}
	l.IsProd, err = strconv.ParseBool(isProd)
	if err != nil {
		return errors.New("config.LoadConfigFromFile ERROR: the isProd value is invalid, must be true or false: " + err.Error())
	}

	currentLogLevel, ok = iniFile.Get(section, "logLevel")
	if ok == false {
		// if there is no currentLogLevel, assume disabled
		l.LogLevel = 0
	} else {
		l.LogLevel, err = strconv.Atoi(currentLogLevel)
		if err != nil {
			return errors.New("config.LoadConfigFromFile ERROR: the logLevel value is invalid, must be an integer if present: " + err.Error())
		}
	}

	gcPercent, ok = iniFile.Get(section, "gcPercent")
	if ok == true {
		// only set the GC ratio if a value exists in the settings
		l.GCPercent, err = strconv.Atoi(gcPercent)
		if err != nil {
			return errors.New("config.LoadConfigFromFile ERROR: the gcPercent value is invalid, must be an integer if present: " + err.Error())
		}
		debug.SetGCPercent(l.GCPercent)
	}

	defaultFeedTimeout, ok = iniFile.Get(section, "defaultFeedTimeout")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the defaultFeedTimeout key is invalid, missing or empty")
	}
	l.DefaultFeedTimeout, err = strconv.Atoi(defaultFeedTimeout)
	if err != nil {
		return errors.New("config.LoadConfigFromFile ERROR: the defaultFeedTimeout value is invalid, must be an integer larger than zero if present: " + err.Error())
	}
	if l.DefaultFeedTimeout < 1 {
		return errors.New("config.LoadConfigFromFile ERROR: the defaultFeedTimeout value must be an integer larger than zero")
	}

	l.DocsUrlBase, _ = iniFile.Get(section, "docsUrlBase")

	return nil

}

// The Notifier struct holds configuration settings for things such as recipients that should be emailed
// or be sent a text message if the feeds go down on OLG's side.
type Notifier struct {

	// a slice of strings containing the email addresses that should be sent the notification if
	// one of the OLG feeds goes down at source (on OLG's side)
	OLGFeedsDownEmailRecipients []string

	// a slice of strings containing the phone numbers that should be sent a text message if
	// one of the OLG feeds goes down at source (on OLG's side)
	OLGFeedsDownPhoneTextRecipients []string
}

func (n *Notifier) LoadFromIniFile(iniFile ini.File, section string) error {

	var ok bool
	var vOLGFeedsDownEmailRecipients = ""
	var vOLGFeedsDownPhoneTextRecipients = ""

	// email recipients
	vOLGFeedsDownEmailRecipients, ok = iniFile.Get(section, "notify.errors.feeds.emails")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the notify.errors.feeds.emails key is missing")
	} else {
		if vOLGFeedsDownEmailRecipients != "" {
			// eliminate all spaces from the comma separated emails
			vOLGFeedsDownEmailRecipients = strings.Replace(vOLGFeedsDownEmailRecipients, " ", "", -1)

			// split the string and load it into the slice
			n.OLGFeedsDownEmailRecipients = append(n.OLGFeedsDownEmailRecipients, strings.Split(vOLGFeedsDownEmailRecipients, ",")...)

			// verify that each entry is a valid email
			for _, email := range n.OLGFeedsDownEmailRecipients {
				_, err := mail.ParseAddress(email)
				if err != nil {
					return errors.New("config.LoadConfigFromFile ERROR: the notify.errors.feeds.emails contains at least an invalid email: " + email + " -> " + err.Error())
				}
			}
		}
	}

	// phone text message recipients
	vOLGFeedsDownPhoneTextRecipients, ok = iniFile.Get(section, "notify.errors.feeds.sms")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the notify.errors.feeds.sms key is missing")
	} else {
		if vOLGFeedsDownPhoneTextRecipients != "" {
			// eliminate all dashes, pluses, brackets and spaces from the comma separated phone numbers
			vOLGFeedsDownPhoneTextRecipients = strings.Replace(vOLGFeedsDownPhoneTextRecipients, " ", "", -1)
			vOLGFeedsDownPhoneTextRecipients = strings.Replace(vOLGFeedsDownPhoneTextRecipients, "-", "", -1)
			vOLGFeedsDownPhoneTextRecipients = strings.Replace(vOLGFeedsDownPhoneTextRecipients, "(", "", -1)
			vOLGFeedsDownPhoneTextRecipients = strings.Replace(vOLGFeedsDownPhoneTextRecipients, ")", "", -1)
			vOLGFeedsDownPhoneTextRecipients = strings.Replace(vOLGFeedsDownPhoneTextRecipients, "+", "", -1)

			// split the string and load it into the slice
			n.OLGFeedsDownPhoneTextRecipients = append(n.OLGFeedsDownPhoneTextRecipients, strings.Split(vOLGFeedsDownPhoneTextRecipients, ",")...)

			// verify that each entry is a valid phone number
			for _, phone := range n.OLGFeedsDownPhoneTextRecipients {

				if _, err := strconv.ParseInt(phone, 10, 64); err != nil {

					return errors.New("config.LoadConfigFromFile ERROR: the notify.errors.feeds.sms contains at least an invalid number:" + err.Error())
				}
			}
		}
	}

	return nil
}

type LotteryFeedURLs struct {
	WinningNumbers         string
	PastWinningNumbers     string
	NumbersEverWon         string
	InstantUnclaimedPrizes string
	WinningFrequency       string

	WinTrackerNHLLotto       string
	WinTrackerWheelOfFortune string
	WinTrackerMegaDice       string
	WinTrackerPokerLotto     string
}

func (l *LotteryFeedURLs) LoadFromIniFile(iniFile ini.File, section string) error {

	var ok bool
	l.WinningNumbers, ok = iniFile.Get(section, "feedUrlWinningNumbers")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlWinningNumbers key is invalid, missing or empty")
	}

	l.PastWinningNumbers, ok = iniFile.Get(section, "feedUrlPastWinningNumbers")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlPastWinningNumbers key is invalid, missing or empty")
	}

	l.NumbersEverWon, ok = iniFile.Get(section, "feedUrlNumbersEverWon")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlNumbersEverWon key is invalid, missing or empty")
	}

	l.InstantUnclaimedPrizes, ok = iniFile.Get(section, "feedUrlInstantUnclaimedPrizes")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlInstantUnclaimedPrizes key is invalid, missing or empty")
	}

	l.WinningFrequency, ok = iniFile.Get(section, "feedUrlWinningFrequency")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlWinningFrequency key is invalid, missing or empty")
	}

	l.WinTrackerNHLLotto, ok = iniFile.Get(section, "feedUrlWinTrackerNHLLotto")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlWinTrackerNHLLotto key is invalid, missing or empty")
	}

	l.WinTrackerWheelOfFortune, ok = iniFile.Get(section, "feedUrlWinTrackerWheelOfFortune")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlWinTrackerWheelOfFortune key is invalid, missing or empty")
	}

	l.WinTrackerMegaDice, ok = iniFile.Get(section, "feedUrlWinTrackerMegaDice")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlWinTrackerMegaDice key is invalid, missing or empty")
	}

	l.WinTrackerPokerLotto, ok = iniFile.Get(section, "feedUrlWinTrackerPokerLotto")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlWinTrackerPokerLotto key is invalid, missing or empty")
	}

	return nil

}

type ProlineFeedURLs struct {
	Events string
}

func (l *ProlineFeedURLs) LoadFromIniFile(iniFile ini.File, section string) error {

	var ok bool
	l.Events, ok = iniFile.Get(section, "feedUrlProlineEvents")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the feedUrlProlineEvents key is invalid, missing or empty")
	}

	return nil
}

//SDI settings
type SportsDirectSettings struct {
	EventsFeedPattern  string
	EventsFeedApiKey   string
	Url                string
	ApiKey             string
	NewerThanInDays    string
	EventTimeThreshold string
	CronInterval       string
}

func (l *SportsDirectSettings) LoadFromIniFile(iniFile ini.File, section string) error {

	var ok bool
	l.EventsFeedPattern, ok = iniFile.Get(section, "sdi.EventsFeedPattern")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.EventsFeedPattern key is invalid, missing or empty")
	}

	l.EventsFeedApiKey, ok = iniFile.Get(section, "sdi.EventsFeedApiKey")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.EventsFeedApiKey key is invalid, missing or empty")
	}

	l.Url, ok = iniFile.Get(section, "sdi.Url")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.Url key is invalid, missing or empty")
	}

	l.ApiKey, ok = iniFile.Get(section, "sdi.ApiKey")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.ApiKey key is invalid, missing or empty")
	}

	l.NewerThanInDays, ok = iniFile.Get(section, "sdi.NewerThanInDays")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.NewerThanInDays key is invalid, missing or empty")
	}

	l.EventTimeThreshold, ok = iniFile.Get(section, "sdi.EventTimeThreshold")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.EventTimeThreshold key is invalid, missing or empty")
	}

	l.CronInterval, ok = iniFile.Get(section, "sdi.CronInterval")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the sdi.CronInterval key is invalid, missing or empty")
	}

	return nil
}

type Configuration struct {

	// is only set to true if the LoadConfigFromFile function was successfully executed
	IsLoadedFromFile bool

	// the path of the config file
	ConfigFile string

	Environment Environment
	Notifier    Notifier

	LotteryFeedURLs LotteryFeedURLs
	ProlineFeedURLs ProlineFeedURLs

	// Settings pertaining to SDI (SportsDirectInc)
	SportsDirect SportsDirectSettings

	// A collection of application servers (that run the olg services feed apps)
	AppServers AppServerCollection
}

var GlobalConfig Configuration

// Loads a value directly from the ini file initially used to load the global config. This must not be used in production
// Use this only in dummy mode. Otherwise, load all entries from the config file in Go structures beforehand
func GetEntryValueDirectlyFromConfigFile(key string) (string, error) {

	// load the ini file
	iniFile, err := ini.LoadFile(GlobalConfig.ConfigFile)
	if err != nil {
		return "", err
	}

	// get the designated enviroment settings for the current server
	envSection, ok := iniFile.Get("", "env")
	if ok == false {
		return "", errors.New("config.GetEntryValue ERROR: the env key is invalid, missing or empty. Please make sure you have an env value pointing to a section")
	}

	var directValue string
	directValue, _ = iniFile.Get(envSection, key)

	return directValue, nil
}

// Loads the config entries from the specified file. Must be run before anything else in the main() method
// of the parent application.
func LoadConfigFromFile(filename string) error {

	// load the ini file
	iniFile, err := ini.LoadFile(filename)
	if err != nil {
		return err
	}

	GlobalConfig.ConfigFile = filename

	// get the designated enviroment settings for the current server
	envSection, ok := iniFile.Get("", "env")
	if ok == false {
		return errors.New("config.LoadConfigFromFile ERROR: the env key is invalid, missing or empty. Please make sure you have an env value pointing to a section")
	}

	// instruct each config module to load the settings from the ini file, based on the chosen section

	if err = GlobalConfig.Environment.LoadFromIniFile(iniFile, envSection); err != nil {
		return err
	}

	if err = GlobalConfig.Notifier.LoadFromIniFile(iniFile, envSection); err != nil {
		return err
	}

	if err = GlobalConfig.LotteryFeedURLs.LoadFromIniFile(iniFile, envSection); err != nil {
		return err
	}

	if err = GlobalConfig.ProlineFeedURLs.LoadFromIniFile(iniFile, envSection); err != nil {
		return err
	}

	if err = GlobalConfig.SportsDirect.LoadFromIniFile(iniFile, envSection); err != nil {
		return err
	}

	if err = GlobalConfig.AppServers.LoadFromIniFile(iniFile, envSection); err != nil {
		return err
	}

	GlobalConfig.IsLoadedFromFile = true
	return nil
}
