package cronJob

type CronJob interface {
	GetJobName() string
	SetJobName(string)

	GetSendEmailMessages() bool
	SetSendEmailMessages(bool)

	GetSendSmsMessages() bool
	SetSendSmsMessages(bool)

	ExecuteCronJob()
	IsJobStillRunning() bool
	SetJobRunningFlag(stillRunning bool)
	LogDebug(debugMessage string)
	LogError(errorMessage string, err error)
}

type CronJobStatus interface {
	GetJobName() string
	GetJobStatus() int
	GetJobStatusDescription() string
	GetLastCorrectlyProcessedDate() string
	GetLastProcessedDate() string
	GetLatestError() string
}
