package cronJob

import (
	"olg-services/notifier"
	"olg-services/publicis/utils/log"
	"sync/atomic"
)

type BaseCronJobStruct struct {

	// This flag should be ignored by xml operations. If 1, it means the previous operation has not
	// yet completed.
	operationAtomicFlag int64

	SendEmailMessages bool `json:"-"`
	SendSmsMessages   bool `json:"-"`

	JobName string `json:"-"`

	// should hold the JOB_STATUS_ constant values
	JobStatus int `json:"-"`
}

// global constants
const (
	JOB_STATUS_RUNNING int = 0
	JOB_STATUS_STOPPED int = 1
	JOB_STATUS_FAULTY  int = 2
)

// global variables

var destinationEmails []string = []string{"silviunotathome@gmail.com", "silviu.capota-mera@publicis.ca"}

// Implements the olg-services/cronJob/CronJob interface

// The base cron job does not do anything
func (cronJobStruct *BaseCronJobStruct) ExecuteCronJob() {

	// just return

	return
}

func (cronJobStruct *BaseCronJobStruct) LogDebug(debugMessage string) {

	log.Debug(debugMessage)
}

func (cronJobStruct *BaseCronJobStruct) LogError(errorMessage string, err error) {

	log.Error(errorMessage, err)

	cronJobStruct.LogEmailOrSmsError(errorMessage, err)

}

func (cronJobStruct *BaseCronJobStruct) LogEmailOrSmsInfo(infoMessage string) {

	cronJobStruct.LogEmailOrSms(notifier.NOTIFICATION_LEVEL_INFO, infoMessage, nil)

}

func (cronJobStruct *BaseCronJobStruct) LogEmailOrSmsError(errorMessage string, err error) {

	cronJobStruct.LogEmailOrSms(notifier.NOTIFICATION_LEVEL_ERROR, errorMessage, err)

}

func (cronJobStruct *BaseCronJobStruct) LogEmailOrSms(level int, errorOrInfoMessage string, err error) {

	// this flag can be 1 for email only, 2 for sms only , or 3 for both
	// if left as 0, it will not send anything
	var emailAndSmsDestinationFlag int = 0

	if cronJobStruct.SendEmailMessages {
		emailAndSmsDestinationFlag = emailAndSmsDestinationFlag + 1
	}

	if cronJobStruct.SendSmsMessages {
		emailAndSmsDestinationFlag = emailAndSmsDestinationFlag + 2
	}

	if emailAndSmsDestinationFlag == 0 {
		return
	}

	newNotification := notifier.Notification{}

	newNotification.Type = emailAndSmsDestinationFlag
	newNotification.Level = level
	newNotification.Subject = "OLG Services: " + cronJobStruct.GetJobName() + " " + notifier.GetLevelDescription(level)
	newNotification.Message = errorOrInfoMessage
	newNotification.ErrorMessage = err
	newNotification.SmsMessage = errorOrInfoMessage

	if notifier.Push(newNotification) == false {
		log.Error("Could not send email or sms for the"+cronJobStruct.GetJobName()+" job. The notification service might not be started.", nil)
	}

}

func (cronJobStruct *BaseCronJobStruct) IsJobStillRunning() bool {

	currentState := atomic.LoadInt64(&cronJobStruct.operationAtomicFlag)

	return currentState > 0
}

func (cronJobStruct *BaseCronJobStruct) SetJobRunningFlag(stillRunning bool) {

	if stillRunning == true {
		atomic.StoreInt64(&cronJobStruct.operationAtomicFlag, 1)
		return
	}

	atomic.StoreInt64(&cronJobStruct.operationAtomicFlag, 0)
}

func (cronJobStruct *BaseCronJobStruct) GetJobName() string {

	if cronJobStruct.JobName == "" {
		return "BaseCronJob"
	}

	return cronJobStruct.JobName
}

func (cronJobStruct *BaseCronJobStruct) SetJobName(jobName string) {

	cronJobStruct.JobName = jobName
}

func (cronJobStruct *BaseCronJobStruct) GetSendEmailMessages() bool {
	return cronJobStruct.SendEmailMessages
}

func (cronJobStruct *BaseCronJobStruct) SetSendEmailMessages(sendEmails bool) {
	cronJobStruct.SendEmailMessages = sendEmails
}

func (cronJobStruct *BaseCronJobStruct) GetSendSmsMessages() bool {
	return cronJobStruct.SendSmsMessages
}

func (cronJobStruct *BaseCronJobStruct) SetSendSmsMessages(sendSms bool) {
	cronJobStruct.SendSmsMessages = sendSms
}

func (cronJobStruct *BaseCronJobStruct) SetJobStatus(status int) {
	cronJobStruct.JobStatus = status
}

func (cronJobStruct *BaseCronJobStruct) GetJobStatus() int {
	return cronJobStruct.JobStatus
}

func (cronJobStruct *BaseCronJobStruct) GetJobStatusDescription() string {

	if cronJobStruct.JobStatus == JOB_STATUS_FAULTY {
		return "Faulty"
	}
	if cronJobStruct.JobStatus == JOB_STATUS_STOPPED {
		return "Stopped"
	}
	if cronJobStruct.JobStatus == JOB_STATUS_RUNNING {
		return "Running"
	}

	return "Unknown"
}
