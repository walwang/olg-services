package main

import (
	"flag"
	"fmt"
	_ "net/http/pprof"
	"olg-services/config"
	//	"olg-services/lotteries"
	//	"olg-services/notifier"
	//	"olg-services/proline/email"
	//	"olg-services/proline/sdi"
	"olg-services/server"
	//	"olg-services/server/metrics"
	"olg-services/publicis/utils/log"
	"os"
	"os/signal"
	"runtime/pprof"
	"syscall"
	"time"
	//	"github.com/paulbellamy/ratecounter"
	//	"github.com/robfig/cron"
)

var (
	// http listening address for the feeds web server
	feedServerAddress = flag.String("address", ":8080", "feeds http services address")
	// by default, the app starts in feed server mode
	isServer = flag.Bool("feedsServer", true, "activate the feeds web server - default is true")

	// if the app is run with the --cronJobs flag, it will activate the cron jobs tasks
	isCronJobService = flag.Bool("cronJobs", false, "activate the cron job service - default is false")

	// isWebConsoleServer = flag.Bool("webConsole", false, "activate the webconsole web server - default is false")

	isNotificationService = flag.Bool("notificationService", false, "activate the notifications service - default is false")

	// if enabled, write cpu profile to cpu.pprof in the same folder as the app
	enableCPUProfile = flag.Bool("cpuProfile", false, "if true, dump the cpu profile info to cpu.pprof")

	// if enabled, allow pprof tool to connect to port 6060
	enableHttpProfile = flag.Bool("httpProfile", false, "if true, dump the cpu profile info to cpu.pprof")

	// http listening address for the feeds web server
	metricsServerAddress = flag.String("metricsAddressAndPort", "localhost:8764", "metrics server http address and port (e.g. localhost:8764)")
)

func main() {

	flag.Parse()

	// Go signal notification works by sending `os.Signal`
	// values on a channel. We'll create a channel to
	// receive these notifications (we'll also make one to
	// notify us when the program can exit).
	sigs := make(chan os.Signal, 1)
	controlChan := make(chan bool, 1)

	// `signal.Notify` registers the given channel to
	// receive notifications of the specified signals (for example CTRL-C)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// This goroutine executes a blocking receive for
	// signals. When it gets one it'll print it out
	// and then notify the program that it can finish.
	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		controlChan <- true
	}()

	// get the configuration from the config file
	fmt.Printf("Loading config file...")
	confErr := config.LoadConfigFromFile("settings.conf")
	if confErr != nil {
		fmt.Println("FATAL ERROR: ", confErr.Error())
		return
	}
	fmt.Println("DONE")

	// set the log level to the value from the config file
	log.LoggerSetLevel(config.GlobalConfig.Environment.LogLevel)

	// enable and write CPU profile dump to the specified file if flag is true
	if *enableCPUProfile {
		f, err := os.Create("./cpu.pprof")
		if err != nil {
			log.FatalErr("CPU Profiling filename error", err)
		}

		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	// enable the profiler http services if flag is true
	if *enableHttpProfile {
		fmt.Printf("Initializing http profiler on port 6060...")
		server.StartWebProfiler("localhost:6060")
		time.Sleep(20 * time.Millisecond)
		fmt.Println("DONE")
	}

	webConsoleEnabled := false
	// // if the command line flag is true or we're in dummy mode, init the status web console as well
	// if *isWebConsoleServer || config.GlobalConfig.Environment.IsDummyMode {
	// 	webConsoleEnabled = true
	// }

	// start the main feeds server
	if *isServer {
		fmt.Printf("Initializing feeds server on %s...", *feedServerAddress)
		server.Start(*feedServerAddress, webConsoleEnabled)
		time.Sleep(50 * time.Millisecond)
		fmt.Println("DONE")
	}

	// start the metrics reporting subsystems
	// init metrics services for the webconsole subsystems

	//	fmt.Printf("Initializing metrics service at: [%s]...", *metricsServerAddress)
	//	metrics.InitMetricsService(*metricsServerAddress)
	//	fmt.Println("DONE")
	//	time.Sleep(20 * time.Millisecond)

	// start the notification service
	if *isNotificationService {
		fmt.Printf("Initializing notification service...")
		//		notifier.Start()
		fmt.Println("DONE")
	}

	// initialize the SDI package
	//sdi.Initialize()

	// initialize the Email package
	//email.Initialize()

	// if the command line flag is true, init the cron job scheduled processes
	if *isCronJobService {
		fmt.Printf("Initializing scheduling and feeds health monitor service...")
		//initCron()
		fmt.Println("DONE")
	}

	// wait one second to insure all servers are fully instantiated
	time.Sleep(300 * time.Millisecond)

	//	var (
	//		counter       *ratecounter.RateCounter
	//		hitspersecond = expvar.NewInt("req_per_second")
	//	)

	//	counter = ratecounter.NewRateCounter(1 * time.Second)
	//	counter.Incr(1)
	//	hitspersecond.Set(counter.Rate())

	fmt.Println("Main awaiting...")
	<-controlChan

}

func initCron() {

	// initialize the singleton structures so their cron-ready methods can be
	// attached to the cron module

	//	err := lotteries.InitCronTestStructures()
	//	if err != nil {
	//		log.FatalErr("initCron - lotteries.InitCronTestStructures()", err)
	//	}

	//	c := cron.New()

	//	c.AddFunc("@every 20s", lotteries.CurrentWinningNumbers.ExecuteCronJob)
	//	c.AddFunc("@every 30s", lotteries.Lotto649PastWinningNumbers.ExecuteCronJob)

	//	c.Start()
}
