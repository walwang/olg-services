package email

import (
	cache "github.com/pmylund/go-cache"
	"io/ioutil"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/publicis/utils/log"
	"time"
)

//olg keys
var OlgAtomCache *cache.Cache

const OLG_CACHE_KEY_TICKET_TEMPLATE = "OLG_CACHE_KEY_TICKET_TEMPLATE"
const OLG_CACHE_KEY_BET_TEMPLATE = "OLG_CACHE_KEY_BET_TEMPLATE"
const OLG_CACHE_EMAIL_FROM = "OLG_CACHE_EMAIL_FROM"
const OLG_CACHE_EMAIL_SUBJECT = "OLG_CACHE_EMAIL_SUBJECT"
const OLG_CACHE_EMAIL_SERVER = "OLG_CACHE_EMAIL_SERVER"
const OLG_CACHE_EMAIL_PORT = "OLG_CACHE_EMAIL_PORT"
const OLG_CACHE_EMAIL_USER = "OLG_CACHE_EMAIL_USER"
const OLG_CACHE_EMAIL_PASSWORD = "OLG_CACHE_EMAIL_PASSWORD"

//init caching engine
func initCaching() {
	//olg email templates never expires
	OlgAtomCache = cache.New(0, 10000*time.Millisecond)
}

//cache the ticket and bets templates
func CacheOlgEmailParams() {

	//init caching
	initCaching()

	//add ticket template (bets container) to cache
	//var filename = "olg-services/templates/ticket.tpl"
	var filename = "./templates/ticket.tpl"
	ticket, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error("ticket.tpl", log.NewErrorLocal(string(constants.OLG_READ_TEMPLATE_FROM_FILE_ERROR_CODE), constants.OLG_READ_TEMPLATE_FROM_FILE_ERROR))
	}
	AddOlgEmailParamToCache(OLG_CACHE_KEY_TICKET_TEMPLATE, string(ticket))

	//add bet template to cache
	//var filename2 = "olg-services/templates/bet.tpl"
	var filename2 = "./templates/bet.tpl"
	bet, err := ioutil.ReadFile(filename2)
	if err != nil {
		log.Error("bet.tpl", log.NewErrorLocal(string(constants.OLG_READ_TEMPLATE_FROM_FILE_ERROR_CODE), constants.OLG_READ_TEMPLATE_FROM_FILE_ERROR))
	}
	AddOlgEmailParamToCache(OLG_CACHE_KEY_BET_TEMPLATE, string(bet))

	//add email-from  to cache
	var from, _ = config.GetEntryValueDirectlyFromConfigFile("smtp.from")
	AddOlgEmailParamToCache(OLG_CACHE_EMAIL_FROM, from)

	//add email-subject to cache
	var subject, _ = config.GetEntryValueDirectlyFromConfigFile("smtp.subject")
	AddOlgEmailParamToCache(OLG_CACHE_EMAIL_SUBJECT, subject)

	//add email-server to cache
	var server, _ = config.GetEntryValueDirectlyFromConfigFile("smtp.server")
	AddOlgEmailParamToCache(OLG_CACHE_EMAIL_SERVER, server)

	//read email-port
	var port, _ = config.GetEntryValueDirectlyFromConfigFile("smtp.port")
	AddOlgEmailParamToCache(OLG_CACHE_EMAIL_PORT, port)

	//add email-user to cache
	var user, _ = config.GetEntryValueDirectlyFromConfigFile("smtp.user")
	AddOlgEmailParamToCache(OLG_CACHE_EMAIL_USER, user)

	//add email-password to cache
	var password, _ = config.GetEntryValueDirectlyFromConfigFile("smtp.password")
	AddOlgEmailParamToCache(OLG_CACHE_EMAIL_PASSWORD, password)

}

//get olg email param from cache
func GetOlgParamFromCache(cache_key string) (param string, found bool) {
	var a interface{}
	a, found = OlgAtomCache.Get(cache_key)
	if a == nil {
		log.Error(cache_key, log.NewErrorLocal(string(constants.OLG_READ_DATA_FROM_CACHE_ERROR_CODE), constants.OLG_READ_DATA_FROM_CACHE_ERROR))
		return param, false
	}
	param = a.(string)
	return param, found
}

//add olg email param to cache
func AddOlgEmailParamToCache(cache_key string, param string) {
	if param == "" {
		initCaching()
	}
	OlgAtomCache.Set(cache_key, param, 0)

}
