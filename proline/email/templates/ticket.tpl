<html lang="en">
<head>
	<!-- DISABLE BROWSER CACHE -->
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- MOBILE META-TAGS -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<!-- SEO OPTIMIZATION -->
	<meta name="description" content="Search Engine Placement">
	<meta name="keywords" content="Search Engine Placement">
	<meta name="robots" content="index, follow">
	<!-- ICONS -->
	<link rel="shortcut icon" href="https://ds9ku5f1ib7dv.cloudfront.net/img/icons/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="https://ds9ku5f1ib7dv.cloudfront.net/img/icons/apple-touch-icon-57x57.png?v=2.23">
	<link rel="apple-touch-icon" href="https://ds9ku5f1ib7dv.cloudfront.net/img/icons/apple-touch-icon-72x72.png?v=2.23" sizes="72x72">
	<link rel="apple-touch-icon" href="https://ds9ku5f1ib7dv.cloudfront.net/img/icons/apple-touch-icon-114x114.png?v=2.23" sizes="114x114">
	<link rel="apple-touch-icon" href="https://ds9ku5f1ib7dv.cloudfront.net/img/icons/apple-touch-icon-144x144.png?v=2.23" sizes="144x144">
	<script async="" src="//www.google-analytics.com/analytics.js?v=2.23"></script><script type="text/javascript">
		var CLOUDFRONT = 'https://ds9ku5f1ib7dv.cloudfront.net/';
		if (top != self) { top.location=location; }
		var KIOSK_REFRESH_TIME = 60000;
		var KIOSK_IDLE_TIME =  15;
	</script>
	<script type="text/javascript" src="https://ds9ku5f1ib7dv.cloudfront.net/js/device.min.js?v=2.23"></script> <!-- Browser detection and redirection -->
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/minified.css?v=2.23">
	<!-- CSS MEDIA-QUERIES -->
	<link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/tablet.min.css?v=2.23" media="screen and (min-width: 1024px) and (max-width: 1280px)">
	<link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/mobile.min.css?v=2.23" media="screen and (min-width: 320px) and (max-width: 1023px)">
	<link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/retina.min.css?v=2.23" media="only screen and (-webkit-min-device-pixel-ratio:2), only screen and (min-device-pixel-ratio:2)">
    <link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/print.min.css?v=2.23" media="print">
	<!-- BROWSER AND/OR DEVICE BASED CSS -->
	<link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/chrome.min.css?v=2.23">
	<!-- DESKTOP USERS -->
	<link rel="stylesheet" type="text/css" href="https://ds9ku5f1ib7dv.cloudfront.net/css/desktop.min.css?v=2.23">
	<script>if (top != self) top.location=location</script>
	<!-- Universal Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js?v=2.23','ga');
		ga('create', 'UA-3327255-1', 'auto');
		ga('send','pageview','/desktop' + location.pathname + location.search  + location.hash);
	</script>
	<title>POOLS</title>
</head>
<body class="en" id="home">
	<div id="main">
		<header id="header" style="height: 98px;"/>
		<!--Ticket-->
        <div class="ticket">
			<!-- Wrapper -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tbody>
				<tr>
					<td width="100%" valign="top" bgcolor="#ffffff" style="padding: 4px;">
						<!--Outlined table-->
						<table width="400" style="border:1px solid #d7d7d7" cellpadding="0" cellspacing="0" align="center">
						<tbody>
							<tr>
								<td style="padding-left:20px; padding-right:20px">
									<table width="100%" style="margin: auto;">
									<tbody>
										<tr>
											<td style="padding-top:10px; padding-bottom:10px" colspan="3" align="center">
												<a href="https://www.proline.ca"><img src="https://image.exct.net/lib/fe5815707261007a721c/i/1/43c1ad08-8.jpg" alt="Pools" border="0" align="center" width="93" style="padding-bottom: 0px;"></a>
											</td>
										</tr>
										<tr>
											<td style="padding-top:10px; padding-bottom:10px" align="left" width="33%">
												<span style="text-decoration: none; color: #003e61; font-size: 10px">{{date}}</span>
											</td>
											<td style="padding-top:10px; padding-bottom:10px" align="center" width="34%">
												<span style="text-decoration: none; font-size: 10px; color: #000000;">CD: {{datetime}}</span><br>
												<span style="text-decoration: none; font-size: 10px; color: #000000;">PP: {{period}}</span>
											</td>
				
											<td style="padding-bottom: 10px; padding-top: 10px;" align="right" width="33%">
												<img src="https://image.exct.net/lib/fe5815707261007a721c/i/1/ae3eb0ad-e.jpg" alt="" border="0" align="center" width="21" style="padding-bottom: 0px;">
												<span style="text-decoration: none; font-size: 10px; color: #003e61; padding-left: 5px;">Card<span style="color: #31996c;">{{num}}</span></span>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
							{{bets}}
							<tr>
								<td style="padding-left: 20px; padding-right: 20px; padding-top: 20px;">
									<table cellpadding="0" cellspacing="0">
										<tbody><tr>
											<td style="padding-right:5px; padding-bottom:20px">
												<span style="text-decoration: none; color: #000000;">WAGER </span>
												<span style="text-decoration: none; font-size: 18px; color: #000000;">{{wager}}</span>
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</tbody>
						</table>
						<!--End of Outlined table-->
					</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>           
	<div id="fadeInOverlay"></div>
</body>
</html>