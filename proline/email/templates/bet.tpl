<tr>
	<td>
		<table width="100%" cellpadding="0" cellspacing="0" align="center" style="background-color: rgb(225, 225, 225);">
			<tbody>
				<tr>
					<td style="padding: 4px 20px 4px 20px" class="edgePadding">
						<table class="table1" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td style="padding: 5px 5px 5px 5px" align="left" width="33%"><span style="text-decoration: none; font-size: 10px; color: #000000;">{{num}}</span></td>
									<td style="padding: 5px 5px 5px 5px" align="center" width="34%"><span style="text-decoration: none; font-size: 10px; color: #000000;"></span></td>
									<td style="padding: 5px 5px 5px 5px" align="right" width="33%"><span style="text-decoration: none; font-size: 10px; color: #000000;">{{sport}}</span></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding-left: 20px; padding-right: 20px" width="100%">
						<table cellpadding="0" cellspacing="0" width="100%" style="background-color: rgb(31, 97, 68);">
							<tbody>
								<tr>
									<td>
										<table cellpadding="0" cellspacing="0" width="100%">
											<tbody><tr>
												<td style="padding: 5px 5px 5px 5px" width="48%" align="left"><span style="text-decoration: none; font-size: 10px; color: #ffffff; font-weight: bold;">{{hometeam}}</span></td>
												<td style="padding: 5px 5px 5px 5px" align="center" width="4%"><span style="text-decoration: none; font-size: 10px; color: #ffffff;">@</span></td>
												<td style="padding: 5px 5px 5px 5px" align="right" width="48%"><span style="text-decoration: none; font-size: 10px; color: #ffffff; font-weight: bold;">{{awayteam}}</span></td>
											</tr>
										</tbody></table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding-left: 20px; padding-right: 20px">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td style="padding: 5px 5px 5px 5px" width="33%" align="left"><span style="text-decoration: none; font-size: 10px; color: #003e61;">{{datetime}}</span></td>
									<td style="padding: 5px 5px 5px 5px" align="center" width="34%"><span style="text-decoration: none; font-size: 10px; color: #003e61;"></span></td>
									<td style="padding: 5px 5px 5px 5px" align="right" width="33%"><span style="text-decoration: none; font-size: 10px; color: #003e61;">{{winner}}</span></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>