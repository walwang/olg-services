package email

import ()

type EmailBets struct {
	Request Request
}

type Request struct {
	Status string `json:"status"`
	Data   Data   `json:"data"`
}

type Data struct {
	Recipients []string `json:"recipients"`
	Cards      []Card   `json:"cards"`
}

type Card struct {
	Num        string `json:"num"`
	Name       string `json:"name"`
	Date       string `json:"date"`
	CloseDate  string `json:"datetime"`
	PlayPeriod string `json:"period"`
	Wager      string `json:"wager"`
	Bets       []Bet  `json:"bets"`
}

type Bet struct {
	Num      string `json:"num"`
	Sport    string `json:"sport"`
	HomeTeam string `json:"hometeam"`
	AwayTeam string `json:"awayteam"`
	DateTime string `json:"datetime"`
	Winner   string `json:"winner"`
}

type XMLSendEmailOutput struct {
	Code   int    `json:"code"`
	Status string `json:"status"`
	Error  string `json:"message,omitempty"`
}
