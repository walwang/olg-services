package email

import (
	"fmt"
	"olg-services/config"
	"os"
	"olg-services/publicis/utils/log"
	"strings"
)

func Initialize() {

	//create folders on fly (if do not exist)
	var err = CreateOlgEmailServicesFolders()
	if err != nil {
		log.Error("Error creating olg-services sub-folders", err)
		return
	}

	//cache olg email params
	CacheOlgEmailParams()
}

//create folders on fly (if do not exist)
func CreateOlgEmailServicesFolders() error {
	//create folders on fly (if do not exist)
	//1. logs
	var path, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.LogsFolder")
	//var path = "./olg-services/logs"
	err := os.MkdirAll(path, 0711)
	if err != nil {
		log.Error("Error creating directory 'logs'", err)
		return err
	}
	//2. template
	path, _ = config.GetEntryValueDirectlyFromConfigFile("email.TemplatesFolder")
	//var path = "./olg-services/templates"
	err = os.MkdirAll(path, 0711)
	if err != nil {
		log.Error("Error creating directory 'templates'", err)
		return err
	}
	//3. docs
	path, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.DocsFolder")
	//var path = "./olg-services/docs"
	err = os.MkdirAll(path, 0711)
	if err != nil {
		log.Error("Error creating directory 'docs'", err)
		return err
	}

	return nil
}

// Writes the log to a larger, email-dedicated file in the /logs folder with the
// service executable. This is good when we don't want to clog the main log with SDI
// massive debug info
func DebugFormatToEmailLogfile(message string, variantObjects ...interface{}) {
	// use the email-yyyy-mm-dd.log format
	logFile := log.Now().Format("email-2006-01-02.log")
	formattedMessage := fmt.Sprintf(message, variantObjects...)
	var logsFolder, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.LogsFolder")
	var values = []string{logsFolder, logFile}
	logFile = strings.Join(values, "/")
	log.DebugToFile(logFile, formattedMessage)
}
