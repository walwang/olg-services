package sdi

import (
	"encoding/xml"
	"io"
)

//Atom feed
type XMLAtomFeed struct {
	XMLName xml.Name           `xml:"feed"`
	Title   string             `xml:"title"`
	Id      string             `xml:"id"`
	Entries []XMLAtomFeedEntry `xml:"entry"`
}

//Atom feed entry
type XMLAtomFeedEntry struct {
	XMLName xml.Name `xml:"entry"`
	Title   string   `xml:"title"`
	Id      string   `xml:"id"`
	Updated string   `xml:"updated"`
}

//structures level 8
type XMLLocation struct {
	//XMLName  xml.Name `xml:"location"`
	City     string `xml:"city"`
	State    string `xml:"state"`
	Country  string `xml:"country"`
	Timezone string `xml:"timezone"`
}

type XMLVenueSeasonDetails struct {
	//XMLName  xml.Name `xml:"season-details"`
	Capacity string `xml:"capacity"`
}

//structures level 7
type XMLVenue struct {
	//XMLName  xml.Name              `xml:"venue"`
	Id       string                `xml:"id"`
	Name     string                `xml:"name"`
	Location XMLLocation           `xml:"location"`
	Details  XMLVenueSeasonDetails `xml:"season-details"`
}

type XMLCompetitionTeam struct {
	//XMLName xml.Name `xml:"team"`
	Id   string `xml:"id"`
	Name string `xml:"name"`
}

//structures level 6
type XMLBetting struct {
	//XMLName            xml.Name `xml:"betting"`
	HomeRotationNumber string `xml:"home-rotation-number"`
	AwayRotationNumber string `xml:"away-rotation-number"`
}

type XMLCompetitionDetails struct {
	//XMLName     xml.Name `xml:"details"`
	Type        string   `xml:"competition-type"`
	Day         string   `xml:"day"`
	NeutralSite string   `xml:"neutral-site"`
	Venue       XMLVenue `xml:"venue"`
}

type XMLCompetitionHomeTeamContent struct {
	//XMLName xml.Name           `xml:"home-team-content"`
	Team XMLCompetitionTeam `xml:"team"`
}

type XMLCompetitionAwayTeamContent struct {
	//XMLName xml.Name           `xml:"away-team-content"`
	Team XMLCompetitionTeam `xml:"team"`
}

type XMLCompetitionMeta struct {
	//XMLName  xml.Name `xml:"meta"`
	Property string `xml:"property"`
}

type XMLCompetitionResultScope struct {
	//XMLName xml.Name `xml:"result-scope"`
	Status string `xml:"competition-status"`
}

//structures level 5
type XMLSeasonDetails struct {
	//XMLName   xml.Name `xml:"details"`
	StartDate string `xml:"start-date"`
	EndDate   string `xml:"end-date"`
}

type XMLCompetition struct {
	//XMLName         xml.Name                      `xml:"competition"`
	Meta            XMLCompetitionMeta            `xml:"meta"`
	ResultScope     XMLCompetitionResultScope     `xml:"result-scope"`
	HomeTeamContent XMLCompetitionHomeTeamContent `xml:"home-team-content"`
	AwayTeamContent XMLCompetitionAwayTeamContent `xml:"away-team-content"`
	//Details               XMLCompetitionDetails         `xml:"details"`
	//Betting               XMLBetting                    `xml:"betting"`
	//PreviousCompetitionId string                        `xml:"previous-competition-id"`
	Id        string `xml:"id"`
	StartDate string `xml:"start-date"`
	//StartDateValue        time.Time
}

//structures level 4
type XMLSeason struct {
	//XMLName xml.Name         `xml:"season"`
	Id      string           `xml:"id"`
	Name    string           `xml:"name"`
	Details XMLSeasonDetails `xml:"details"`
}

//structures level 3
type XMLLeage struct {
	//XMLName xml.Name `xml:"league"`
	Id   string `xml:"id"`
	Name string `xml:"name"`
}

type XMLSeasonContent struct {
	//XMLName      xml.Name         `xml:"season-content"`
	Season       XMLSeason        `xml:"season"`
	Competitions []XMLCompetition `xml:"competition"`
}

//structures level 2
type XMLSport struct {
	//XMLName xml.Name `xml:"sport"`
	Id   string `xml:"id"`
	Name string `xml:"name"`
}

type XMLLeageContent struct {
	//XMLName       xml.Name         `xml:"league-content"`
	Leage         XMLLeage         `xml:"league"`
	SeasonContent XMLSeasonContent `xml:"season-content"`
}

//structure level 1
type XMLTeamSportContent struct {
	//XMLName      xml.Name        `xml:"team-sport-content"`
	Sport        XMLSport        `xml:"sport"`
	LeageContent XMLLeageContent `xml:"league-content"`
}

//structure level 0 (top)
type XMLSportContent struct {
	//XMLName          xml.Name            `xml:"content"`
	TeamSportContent XMLTeamSportContent `xml:"team-sport-content"`
}

//---------------------------------------------------
//SDI DOME OBJECT
//---------------------------------------------------
type SDITeamSport struct {
	//XMLName xml.Name   `xml:"team-sport"`
	Sports []SDISport `xml:"sport"`
}

type SDISport struct {
	//XMLName xml.Name   `xml:"sport"`
	Id     string     `xml:"id"`
	Name   string     `xml:"name"`
	Leages []SDILeage `xml:"league"`
}

type SDILeage struct {
	//XMLName      xml.Name         `xml:"league"`
	Id           string           `xml:"id"`
	Name         string           `xml:"name"`
	Season       XMLSeason        `xml:"season"`
	Competitions []XMLCompetition `xml:"competition"`
}

//---------------------------------------------------
//output jSON structure
//---------------------------------------------------
type XMLCompetitionOutput struct {
	Found             bool   `json:"found"`
	Sport             string `json:"sport"`
	Leage             string `json:"leage"`
	HomeTeam          string `json:"home-team"`
	AwayTeam          string `json:"away-team"`
	StartDate         string `json:"start-date"`
	Id                string `json:"data-match_id"`
	DataSportId       string `json:"data-sport_id"`
	DataCompetitionId string `json:"data-competition_id"`
	DataEditionId     string `json:"data-edition_id"`
}

type XMLFindSdiEventOutput struct {
	Code   int                  `json:"code"`
	Status string               `json:"status"`
	Error  string               `json:"message,omitempty"`
	Data   XMLCompetitionOutput `json:"data"`
}

//-------------------------------------------------------------
//sport types
//-------------------------------------------------------------
type XMLSportLeages struct {
	SportLeage []string `xml:"sport-leage"`
}

//-------------------------------------------------------------
//sports/leages/teams olg-sdi mapping structures
//-------------------------------------------------------------
type XMLOlgSidMap struct {
	SportLeages XMLSportLeages `xml:"sport-leages"`
	Sports      XMLOlgSidVal   `xml:"sports"`
	Leages      XMLOlgSidVal   `xml:"leages"`
	Teams       XMLOlgSidVal   `xml:"teams"`
}

type XMLOlgSidVal struct {
	Maps []string `xml:"map"`
}

//-------------------------------------------------------------
//function to read xml and transform to the proper structure
//-------------------------------------------------------------
func ReadSportContent(reader io.Reader) (XMLSportContent, error) {
	//fmt.Println("ReadStraps (1)")
	var xmlSportContent XMLSportContent
	if err := xml.NewDecoder(reader).Decode(&xmlSportContent); err != nil {
		return xmlSportContent, err
	}
	return xmlSportContent, nil
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
