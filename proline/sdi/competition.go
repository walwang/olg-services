package sdi

import (
	"encoding/xml"
	"time"
)

// The SdiCompetition corresponds to a competition xml element inside the SDI feed
type SdiCompetition struct {
	XMLName xml.Name `xml:"competition" json:"-"`

	// corresponds to the id xml element inside the competition element
	Id string `xml:"id" json:"id"`

	// the string value of the start-date element inside the competition element
	StartDate string

	// the actual Go value after the StartDate field was parsed inside a Go time.Time type
	StartDateValue time.Time
}
