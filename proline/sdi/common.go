package sdi

import (
	"encoding/xml"
)

const EVENT_DATE_PARSE_FORMAT = "2006-01-02"
const EVENT_DATE_WITH_TIME_PARSE_FORMAT = "2006-01-02T15:04:05"

// the format of the start-date xml element inside each competition item
// example: 2015-11-05T19:00:00-05:00
const SDI_START_DATE_TIME_PARSE_FORMAT = "2006-01-02T15:04:05"

// The SdiSport struct corresponds to the sport xml element inside team-sport-content
// of the Sdi feed
type SdiSport struct {
	XMLName xml.Name `xml:"sport" json:"-"`
	Id      string   `xml:"id" json:"id"`
	Name    string   `xml:"name" json:"name"`
}

// The SdiLeague struct corresponds to the league xml element inside
// team-sport-content/league-content section of the Sdi feed
type SdiLeague struct {
	XMLName xml.Name `xml:"league" json:"-"`
	Id      string   `xml:"id" json:"id"`

	// This is a slice of name elements inside the league element. It is intricate in the sense
	// that it is multipurposes. Hence, it needs to be loaded first into a structure then, based
	// on the various attributes, we can load specific values into the Name and the Code fields
	Names []SdiLeagueNameEntry `xml:"name" json:"-"`

	Name string `xml:"-" json:"name"`
	Code string `xml:"-" json:"code"`
}

// The SdiLeagueNameEntry struct corresponds to each name xml element inside league definition
// element of the Sdi feed
type SdiLeagueNameEntry struct {
	XMLName xml.Name `xml:"name" json:"-"`

	// The actual value of the name element
	Value string `xml:",chardata" json:"-"`

	// The value of the nick attribute
	Nick string `xml:"nick,attr" json:"-"`
}

// The SdiSeason struct corresponds to the season xml element inside season-content
// of the Sdi feed
type SdiSeason struct {
	XMLName   xml.Name `xml:"season" json:"-"`
	Id        string   `xml:"id" json:"id"`
	Name      string   `xml:"name" json:"name"`
	StartDate string   `xml:"details>start-date" json:"name"`
	EndDate   string   `xml:"details>end-date" json:"name"`
}
