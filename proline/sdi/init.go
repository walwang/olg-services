package sdi

import (
	"fmt"
	"olg-services/config"
	"olg-services/publicis/utils/log"
	"os"
	"strings"

	"github.com/robfig/cron"
)

func Initialize() {

	//new cron
	c := cron.New()

	//create folders on fly (if do not exist)
	var err = CreateOlgServicesFolders()
	if err != nil {
		log.Error("Error creating olg-services sub-folders", err)
		return
	}

	//get sdi feeds and cache them
	SdiDataFeedEvent()

	//set cron interval in seconds  (reset data from sdi interval)
	var cronInterval, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.CronInterval")
	var vals = []string{"@every ", cronInterval}
	cronInterval = strings.Join(vals, "")
	c.AddFunc(cronInterval, SdiDataFeedEvent)

	c.Start()
}

// Writes the log to a larger, sdi-dedicated file in the /logs folder with the
// service executable. This is good when we don't want to clog the main log with SDI
// massive debug info
func DebugFormatToSDILogfile(message string, variantObjects ...interface{}) {
	// use the sdi-yyyy-mm-dd.log format
	logFile := log.Now().Format("sdi-2006-01-02.log")
	formattedMessage := fmt.Sprintf(message, variantObjects...)
	var logsFolder, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.LogsFolder")
	var values = []string{logsFolder, logFile}
	logFile = strings.Join(values, "/")
	log.DebugToFile(logFile, formattedMessage)
}

//create folders on fly (if do not exist)
func CreateOlgServicesFolders() error {
	//create folders on fly (if do not exist)
	//1. logs
	var path, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.LogsFolder")
	//var path = "./olg-services/logs"
	err := os.MkdirAll(path, 0711)
	if err != nil {
		log.Error("Error creating directory 'logs'", err)
		return err
	}
	//2. docs
	path, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.DocsFolder")
	//var path = "./olg-services/docs"
	err = os.MkdirAll(path, 0711)
	if err != nil {
		log.Error("Error creating directory 'docs'", err)
		return err
	}
	//3. xml
	path, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.XmlFolder")
	//var path = "./olg-services/docs"
	err = os.MkdirAll(path, 0711)
	if err != nil {
		log.Error("Error creating directory 'xml'", err)
		return err
	}
	return nil
}
