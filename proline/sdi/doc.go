/*The sdi package handles the SportsDirect match stats feed and also provides functionality
to map the events in the SDI feed to the events in the OLG Proline family of feeds.
The feeds url are in the following format:
 http://xml.sportsdirectinc.com/sport/v2/soccer/CL/schedule/schedule_CL_7_days.xml?apiKey=741E333E-7546-45A1-8A03-1FA91BFFF499

 soccer" is the sport designator
 "CL" is the league designator
 "7_days" is the time interval for the data inside the feed. */
package sdi
