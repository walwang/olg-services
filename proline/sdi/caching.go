package sdi

import (
	cache "github.com/pmylund/go-cache"
	"olg-services/config"
	"olg-services/publicis/utils/log"
	"strconv"
	"time"
)

// Cache store for the sdi atom feed;
// this has to be refreshed fairly rarely, every 60 seconds or so

// sdi keys
var SdiAtomCache *cache.Cache

const SDI_CACHE_KEY_ATOM = "SDI_CACHE_KEY_ATOM"
const SDI_CACHE_KEY_SDI_DATA = "SDI_CACHE_KEY_SDI_DATA"
const SDI_CACHE_KEY_SPORTS_MAP = "SDI_CACHE_KEY_SPORTS_MAP"
const SDI_CACHE_KEY_LEAGES_MAP = "SDI_CACHE_KEY_LEAGES_MAP"
const SDI_CACHE_KEY_TEAMS_MAP = "SDI_CACHE_KEY_TEAMS_MAP"
const SDI_CACHE_KEY_EVENT_TIME_THRESHOLD = "SDI_CACHE_KEY_EVENT_TIME_THRESHOLD"

const SDI_READ_DATA_FROM_CACHE_ERROR_CODE = -4000
const SDI_READ_DATA_FROM_CACHE_ERROR = "Error by retrieving from cache"

func initCaching() {
	//sdi sports/leages/competitions feed never expires
	SdiAtomCache = cache.New(0, 10000*time.Millisecond)
}

//add sdi sports object to cache
func AddToCache(atomFeed XMLAtomFeed) {
	if SdiAtomCache == nil {
		initCaching()
	}
	SdiAtomCache.Set(SDI_CACHE_KEY_ATOM, atomFeed, 0)
}

//get sdi sports object from cache
func GetFromCache() (atomFeed XMLAtomFeed, found bool) {
	var a interface{}
	a, found = SdiAtomCache.Get(SDI_CACHE_KEY_ATOM)
	if a == nil {
		log.Error(SDI_CACHE_KEY_ATOM, log.NewErrorLocal(string(SDI_READ_DATA_FROM_CACHE_ERROR_CODE), SDI_READ_DATA_FROM_CACHE_ERROR))
		return atomFeed, false
	}
	atomFeed = a.(XMLAtomFeed)
	return atomFeed, found
}

//add sdi sports object to cache
func AddToCacheSdiData(feed []SDISport) {
	if feed == nil {
		initCaching()
	}
	SdiAtomCache.Set(SDI_CACHE_KEY_SDI_DATA, feed, 0)
}

//get sdi sports object from cache
func GetFromCacheSdiData() ([]SDISport, bool) {
	var a interface{}
	var found bool
	if SdiAtomCache == nil {
		return nil, false
	}
	a, found = SdiAtomCache.Get(SDI_CACHE_KEY_SDI_DATA)
	if a == nil {
		log.Error(SDI_CACHE_KEY_SDI_DATA, log.NewErrorLocal(string(SDI_READ_DATA_FROM_CACHE_ERROR_CODE), SDI_READ_DATA_FROM_CACHE_ERROR))
		return nil, false
	}
	//feed = a.([]SDISport)
	return a.([]SDISport), found
}

//get sdi sports/leages/teams mapping from cache
func GetFromCacheMapping(cache_key string) (mapping map[string]string, found bool) {
	var a interface{}
	a, found = SdiAtomCache.Get(cache_key)
	if a == nil {
		log.Error(cache_key, log.NewErrorLocal(string(SDI_READ_DATA_FROM_CACHE_ERROR_CODE), SDI_READ_DATA_FROM_CACHE_ERROR))
		return mapping, false
	}
	mapping = a.(map[string]string)
	return mapping, found
}

//add sdi sports/leages/teams mapping to cache
func AddToCacheMapping(cache_key string, mapping map[string]string) {
	if mapping == nil {
		initCaching()
	}
	SdiAtomCache.Set(cache_key, mapping, 0)
}

//add event threshold to cache
func AddToCacheEventThreshold(configKey string) {
	var eventTimeThreshold, _ = config.GetEntryValueDirectlyFromConfigFile(configKey)
	var iEventTimeThreshold, _ = strconv.Atoi(eventTimeThreshold)
	SdiAtomCache.Set(SDI_CACHE_KEY_EVENT_TIME_THRESHOLD, iEventTimeThreshold, 0)
}

//get sevent threshold from cache
func GetFromCacheEventThreshold() (iEventTimeThreshold int, found bool) {
	var a interface{}
	a, found = SdiAtomCache.Get(SDI_CACHE_KEY_EVENT_TIME_THRESHOLD)
	if a == nil {
		log.Error("GetFromCacheEventThreshold", log.NewErrorLocal(string(SDI_READ_DATA_FROM_CACHE_ERROR_CODE), SDI_READ_DATA_FROM_CACHE_ERROR))
		return 1, false
	}
	iEventTimeThreshold = a.(int)
	return iEventTimeThreshold, found
}
