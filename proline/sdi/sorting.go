package sdi

import (
	"sort"
	"strings"
)

// ByCompetitionEventDate implements sort.Interface for []SdiCompetition based on
// the StartDate field.
type ByCompetitionStartDate []SdiCompetition

func (a ByCompetitionStartDate) Len() int           { return len(a) }
func (a ByCompetitionStartDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByCompetitionStartDate) Less(i, j int) bool { return a[i].StartDate < a[j].StartDate }

// BEGIN: Generic convenience functions

func SortSdiCompetition(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

// Sort the ProlineEvents based on the sortByValue parameter. That one can be "startDate".
func SortSdiCompetitions(competitions []SdiCompetition, sortByValue string) {

	if sortByValue == "" {
		return
	}

	if strings.ToLower(sortByValue) == "startDate" {
		SortSdiCompetition(ByCompetitionStartDate(competitions))
	}

}
