package sdi

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"olg-services/config"
	"os"
	"path/filepath"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"
)

//get time in milliseconds
func GetTimeString() int64 {
	now := time.Now()
	// correct way to convert time to millisecond - with UnixNano()
	unixNano := now.UnixNano()
	umillisec := unixNano / 1000000
	//fmt.Printf("\n umillisec =  %d ", umillisec)
	return umillisec
}

//get sdi document with all sport/leage/.. records
func GetSdiSportEventsDocument(w http.ResponseWriter, xmlAtomFeed XMLAtomFeed) []SDISport {
	var domeSports []SDISport
	var timeout time.Duration = time.Duration(60 * time.Second)
	var client = http.Client{
		Timeout: timeout,
	}
	var k = 0
	for _, value := range xmlAtomFeed.Entries {
		//check if this url contains filtered sports (e.g. "soccer/ITA/","basketball/NCAAB/" )
		if IsSportOnTheList(value.Id) == true {
			k = k + 1
			// to read from xml from remote host
			resp, err := client.Get(value.Id)
			if err != nil {
				log.Error("GetSdiSportEventsDocument: LoadXML Error at http.Get(2)", err)
				continue
			}
			// if no errors we can run defer response body close
			defer func() {
				if resp != nil && resp.Body != nil {
					resp.Body.Close()
				}
			}()
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Error("GetSdiSportEventsDocument: LoadXML Error at ioutil.ReadAll(2)", err)
				continue
			}
			reader := bytes.NewReader(body)
			//add this sport node to the dome
			var xmlSportContent XMLSportContent
			if err := xml.NewDecoder(reader).Decode(&xmlSportContent); err != nil {
				DebugFormatToSDILogfile("GetSdiSportEventsDocument: TIME:\t  %s\n", time.Now())
				DebugFormatToSDILogfile("GetSdiSportEventsDocument: ATOM FEED:\t  %s\n", value.Id)
				DebugFormatToSDILogfile("GetSdiSportEventsDocument: FEED_STATUS_CODE_FEED_XML_DECODE_ERROR = -1010:\t  %s\n", err)
				continue
			}
			var valSport = xmlSportContent.TeamSportContent.Sport
			var valLeage = xmlSportContent.TeamSportContent.LeageContent.Leage
			var valSeasonContent = xmlSportContent.TeamSportContent.LeageContent.SeasonContent
			var sdiLeage = SDILeage{ /*valLeage.XMLName,*/ valLeage.Id, valLeage.Name, valSeasonContent.Season, valSeasonContent.Competitions}
			var leages []SDILeage
			leages = ExtendLeage(leages, sdiLeage)
			var sport = SDISport{ /*valSport.XMLName,*/ valSport.Id, valSport.Name, leages}
			for i, value2 := range xmlSportContent.TeamSportContent.LeageContent.SeasonContent.Competitions {
				fmt.Fprintf(w, "%d start date:\t %s home team:\t %s \t\taway team:\t %s \n", i+1, value2.StartDate, value2.HomeTeamContent.Team.Name, value2.AwayTeamContent.Team.Name)
			}
			domeSports = AddCreateSportLeageCompetition(sport, domeSports)
		}
	}

	//add event threshold to cache
	AddToCacheEventThreshold("sdi.EventTimeThreshold")

	return domeSports
}

//get sdi document with all sport/leage/.. records
func GetSdiSportEventsDocument2(xmlAtomFeed XMLAtomFeed) []SDISport {
	var domeSports []SDISport
	var timeout time.Duration = time.Duration(60 * time.Second)
	var client = http.Client{
		Timeout: timeout,
	}
	var k = 0
	log.Info("GetSdiSportEventsDocument2: START")
	for _, value := range xmlAtomFeed.Entries {
		//check if this url contains filtered sports (e.g. "soccer/ITA/","basketball/NCAAB/" )
		if IsSportOnTheList(value.Id) == true {
			DebugFormatToSDILogfile("GetSdiSportEventsDocument2: ATOM FEED:\t  %s\n", value.Id)
			k = k + 1
			// to read from xml from remote host
			resp, err := client.Get(value.Id)
			if err != nil {
				log.Error("GetSdiSportEventsDocument2: ERROR(LoadXML at http.Get)", err)
				continue
			}
			// if no errors we can run defer response body close
			defer func() {
				if resp != nil && resp.Body != nil {
					resp.Body.Close()
				}
			}()
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Error("GetSdiSportEventsDocument2: ERROR(LoadXML at ioutil.ReadAll)", err)
				continue
			}
			reader := bytes.NewReader(body)
			//add this sport node to the dome
			var xmlSportContent XMLSportContent
			if err := xml.NewDecoder(reader).Decode(&xmlSportContent); err != nil {
				log.Error("GetSdiSportEventsDocument2: \tNO DATA: "+value.Id, err)
				continue
			}
			var valSport = xmlSportContent.TeamSportContent.Sport
			var valLeage = xmlSportContent.TeamSportContent.LeageContent.Leage
			var valSeasonContent = xmlSportContent.TeamSportContent.LeageContent.SeasonContent
			DebugFormatToSDILogfile("Leage: \t  %s\n", valLeage.Name)
			var sdiLeage = SDILeage{ /*valLeage.XMLName,*/ valLeage.Id, valLeage.Name, valSeasonContent.Season, valSeasonContent.Competitions}
			var leages []SDILeage
			leages = ExtendLeage(leages, sdiLeage)
			var sport = SDISport{ /*valSport.XMLName,*/ valSport.Id, valSport.Name, leages}
			for i, value2 := range xmlSportContent.TeamSportContent.LeageContent.SeasonContent.Competitions {
				DebugFormatToSDILogfile("%d start date:\t %s home team:\t %s \t\t away team:\t %s ", i+1, value2.StartDate, value2.HomeTeamContent.Team.Name, value2.AwayTeamContent.Team.Name)
			}
			domeSports = AddCreateSportLeageCompetition(sport, domeSports)
		}
	}

	//add event threshold to cache
	AddToCacheEventThreshold("sdi.EventTimeThreshold")

	return domeSports
}

//get sdi atom document with the list of coming sport events urls
func GetSdiAtomXml(url string) XMLAtomFeed {
	var xmlAtomFeed XMLAtomFeed
	//we keep all SDI info in this dome object
	var timeout time.Duration = time.Duration(60 * time.Second)
	var client = http.Client{
		Timeout: timeout,
	}
	// to read from xml from remote host (sdi atom fee)
	resp, err := client.Get(url)
	if err != nil {
		log.Error("PeterSdiAtomFeed: LoadXML Error at http.Get", err)
	}
	// if no errors we can run defer response body close
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error("LoadXML Error at ioutil.ReadAll", err)
	}
	reader := bytes.NewReader(body)
	if err := xml.NewDecoder(reader).Decode(&xmlAtomFeed); err != nil {
		fmt.Printf("s%  \n", err)
	}
	return xmlAtomFeed
}

//get url from config (plus current date)
func GetSdiAtomUrlFromConfig() string {
	var url = ""
	var sdiUrl, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.Url")
	var apiKey, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.ApiKey")
	var newerThanInDays, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.NewerThanInDays")
	DebugFormatToSDILogfile("sdiUrl = \t  %s\n", sdiUrl)
	DebugFormatToSDILogfile("apiKey = \t  %s\n", apiKey)
	var newerThan = ""
	values := []string{}
	//get current date and add config-based number of days (may be subtracted if this number is negative)
	timeNow := time.Now()
	var iNewerThanInDays, _ = strconv.Atoi(newerThanInDays)
	timeNow = timeNow.AddDate(0, 0, iNewerThanInDays)
	var year = strconv.Itoa(timeNow.Year())
	var month = strconv.Itoa(int(timeNow.Month()))
	month = FormatDateParts(month)
	var day = strconv.Itoa(timeNow.Day())
	day = FormatDateParts(day)
	values = []string{year, month, day}
	date1 := strings.Join(values, "-")
	var hour = strconv.Itoa(timeNow.Hour())
	hour = FormatDateParts(hour)
	var minute = strconv.Itoa(timeNow.Minute())
	minute = FormatDateParts(minute)
	var second = strconv.Itoa(timeNow.Second())
	second = FormatDateParts(second)
	values = []string{hour, minute, second}
	date2 := strings.Join(values, ":")
	values = []string{date1, "T", date2, ".0"}
	newerThan = strings.Join(values, "")
	values = []string{sdiUrl, newerThan, apiKey}
	url = strings.Join(values, "")
	return url
}

//add zero in front of one digit month -> second parts of date
func FormatDateParts(input string) string {
	var output = input
	if len(input) == 1 {
		var values = []string{"0", input}
		output = strings.Join(values, "")
	}
	return output
}

//find SDI match against OLG data: sport, leage, home team, away team, start date
func FindSdiEvent(olgSport string, olgLeage string, olgHomeTeam string, olgAwayTeam string, olgDateTime string, sdiSports []SDISport) (string, string, string, string, string, XMLCompetition) {
	//reformat OLG date/time input: fill in "-" instead of "." and "T" instead of " " (space)
	olgDateTime = strings.Replace(olgDateTime, " ", "T", 1) + "+00:00"
	//olg-sdi event time difference tolerance threshold (in minutes)
	var iEventTimeThreshold, _ = GetFromCacheEventThreshold()
	//reformat OLG teams' names (depend on sport/leage)
	var FindSdiEvent XMLCompetition
	//olg-sdi sports mapping (from config) <= cache
	sports, found := GetFromCacheMapping(SDI_CACHE_KEY_SPORTS_MAP)
	if found == false {
		//fmt.Printf("SDI Sports mapping struct was not found\n")
		//DebugFormatToSDILogfile("FindSdiEvent: ERROR TIME:\t  %s\n", time.Now())
		//DebugFormatToSDILogfile("FindSdiEvent: ERROR:\t %s ERROR ID: %d\n", "SDI Sports mapping struct was not found", constants.FEED_STATUS_CODE_FEED_SPORTS_MAPPING_NOT_FOUND)
		return "", "", "", "", "", FindSdiEvent
	}
	//olg-sdi leages mapping (from config) <= cache
	leages, found := GetFromCacheMapping(SDI_CACHE_KEY_LEAGES_MAP)
	if found == false {
		//DebugFormatToSDILogfile("FindSdiEvent: ERROR TIME:\t  %s\n", time.Now())
		//DebugFormatToSDILogfile("FindSdiEvent: ERROR:\t %s ERROR ID: %d\n", "SDI Leages mapping struct was not found", constants.FEED_STATUS_CODE_FEED_LEAGES_MAPPING_NOT_FOUND)
		return "", "", "", "", "", FindSdiEvent
	}
	//olg-sdi teams mapping (from config) <= cache
	teams, found := GetFromCacheMapping(SDI_CACHE_KEY_TEAMS_MAP)
	if found == false {
		//DebugFormatToSDILogfile("FindSdiEvent: ERROR TIME:\t  %s\n", time.Now())
		//DebugFormatToSDILogfile("FindSdiEvent: ERROR:\t %s ERROR ID: %d\n", "SDI Teams mapping struct was not found", constants.FEED_STATUS_CODE_FEED_TEAMS_MAPPING_NOT_FOUND)
		return "", "", "", "", "", FindSdiEvent
	}
	//map OLG data to SDI data
	var olgSdiSport = sports[olgSport]
	var olgSdiLeage = leages[olgLeage]
	var olgSidHomeTeam = teams[olgHomeTeam]
	if len(olgSidHomeTeam) > 0 {
		olgHomeTeam = olgSidHomeTeam
	}
	var olgSidAwayTeam = teams[olgAwayTeam]
	if len(olgSidAwayTeam) > 0 {
		olgAwayTeam = olgSidAwayTeam
	}
	//
	if olgSport == "CFB" || olgSport == "FTB" {
		olgHomeTeam = strings.Replace(olgHomeTeam, " ST.", " STATE", 6)
		olgAwayTeam = strings.Replace(olgAwayTeam, " ST.", " STATE", 6)
	}
	//	//set home team to upper case
	//	var upperOlgHomeTeam = strings.ToUpper(olgHomeTeam)
	//	var upperOlgAwayTeam = strings.ToUpper(olgAwayTeam)
	//loop all sports/leages/competitions
	for _, value := range sdiSports {
		if olgSdiSport == value.Name {
			for _, value2 := range value.Leages {
				if strings.Contains(olgSdiLeage, value2.Name) == true {
					for _, value3 := range value2.Competitions {
						//if upperOlgHomeTeam == strings.ToUpper(value3.HomeTeamContent.Team.Name) && upperOlgAwayTeam == strings.ToUpper(value3.AwayTeamContent.Team.Name) {
						if olgHomeTeam == value3.HomeTeamContent.Team.Name && olgAwayTeam == value3.AwayTeamContent.Team.Name {
							//get time objects for OLG and SDI start dates
							t1, _ := time.Parse(time.RFC3339, olgDateTime)
							t2, _ := time.Parse(time.RFC3339, value3.StartDate)
							if t1.Year() == t2.Year() && t1.Month() == t2.Month() && t1.Day() == t2.Day() {
								//compare OLG and SDI start dates (30 minutes threshold to mark as different)
								var minDiff = int(60*t1.Hour() + t1.Minute() - 60*t2.Hour() - t2.Minute())
								if minDiff < 0 {
									minDiff = -minDiff
								}
								if minDiff < iEventTimeThreshold {
									return value.Name, value.Id, value2.Name, value2.Id, value2.Season.Id, value3
								}
							}
						}
					}
					continue
				}
			}
			break
		}
	}
	return "", "", "", "", "", FindSdiEvent
}

//read different olg-sdi mappings from config
func ReadMapFromConfig(configMap string) map[string]string {
	var vals = []string{configMap, "COUNT"}
	var mapCountConfigKey = strings.Join(vals, "")
	var OLG_SDI_MAP_COUNT, _ = config.GetEntryValueDirectlyFromConfigFile(mapCountConfigKey)
	//first argument to append must be slice; have untyped string
	var sports = make(map[string]string)
	var cnt, _ = strconv.Atoi(OLG_SDI_MAP_COUNT)
	for i := 0; i < cnt; i++ {
		//fmt.Printf("i: %d \n", i)
		var sI = strconv.Itoa(i + 1)
		//fmt.Printf("sI: %s \n", sI)
		vals = []string{configMap, sI}
		var configKey = strings.Join(vals, "")
		//fmt.Printf("configKey: %s \n", configKey)
		var OLG_SDI_MAP, _ = config.GetEntryValueDirectlyFromConfigFile(configKey)
		//fmt.Printf("OLG_SDI_MAP: %s \n", OLG_SDI_MAP)
		var keyValue []string = strings.Split(OLG_SDI_MAP, ":")
		var sKey = keyValue[0]
		var sValue = keyValue[1]
		sports[sKey] = sValue
	}
	return sports
}

//read different olg-sdi mappings from config
func ReadMapFromConfig2(entries []string) map[string]string {
	var outputs = make(map[string]string)
	for _, entry := range entries {
		//fmt.Printf(value)
		var keyValue []string = strings.Split(entry, ":")
		var sKey = keyValue[0]
		var sValue = keyValue[1]
		outputs[sKey] = sValue
	}
	return outputs
}

//add or create new "sport/leage/competiotion" node if not duplicated
func AddCreateSportLeageCompetition(sport SDISport, domeSports []SDISport) []SDISport {
	var domeSport SDISport
	var domeLeage SDILeage
	var domeCompetition XMLCompetition
	var leage SDILeage
	var iSport int
	//1. check if this sport already exists
	for i, value := range domeSports {
		if len(sport.Id) > 0 && sport.Id == value.Id {
			//fmt.Printf("AddCreateSportLeageCompetition: sport.Name (EXISTS)\t : %s  \n", sport.Name)
			domeSport = value
			iSport = i
			break
		}
	}
	//1.1 if the new one create it and return (input has only one sport)
	if len(domeSport.Id) == 0 {
		//fmt.Printf("AddCreateSportLeageCompetition: extend sport %s  \n", sport.Name)
		//fmt.Printf("AddCreateSportLeageCompetition: domeSports length (1) %d  \n", len(domeSports))
		domeSports = ExtendSport(domeSports, sport)
		domeSport = domeSports[len(domeSports)-1]
		//fmt.Printf("AddCreateSportLeageCompetition: domeSports length (2) %d  \n", len(domeSports))
		return domeSports
	}
	//2. check if this leage already exists
	for _, value := range sport.Leages {
		if len(value.Name) > 0 {
			domeLeage = GetLeageInSport(domeSports[iSport], value)
			if len(domeLeage.Id) > 0 {
				//fmt.Printf("AddCreateSportLeageCompetition: leage.Name (EXISTS)\t : %s \n", domeLeage.Name)
				leage = value
				break
			}
		}
	}
	//2.1 if the new one create it and return (input has only one leage)
	if len(domeLeage.Id) == 0 {
		leage = sport.Leages[0]
		domeSport.Leages = ExtendLeage(domeSports[iSport].Leages, leage)
		domeLeage = domeSport.Leages[len(domeSport.Leages)-1]
		if len(domeLeage.Name) > 0 {
			domeSports[iSport].Leages = domeSport.Leages
		}
		return domeSports
	}
	//3. check if this competiotion already exists
	for _, value := range leage.Competitions {
		domeCompetition = GetCompetitionInLeage(domeLeage, value)
		if len(domeCompetition.Id) == 0 {
			//2.1 if the new one create it and go ahead for another competition
			//(input has multiple competitions)
			domeLeage.Competitions = ExtendCompetition(domeLeage.Competitions, domeCompetition)
		}
	}
	return domeSports
}

//get competition in leage (it should be already there)
func GetCompetitionInLeage(leage SDILeage, competition XMLCompetition) XMLCompetition {
	var GetCompetitionInLeage XMLCompetition
	for _, value := range leage.Competitions {
		//e.g. id=/sport/football/competition:51501
		if competition.Id == value.Id {
			GetCompetitionInLeage = value
			break
		}
	}
	return GetCompetitionInLeage
}

//get leage in sport (it should be already there)
func GetLeageInSport(sport SDISport, leage SDILeage) SDILeage {
	var GetLeageInSport SDILeage
	for _, value := range sport.Leages {
		if leage.Name == value.Name {
			GetLeageInSport = value
			break
		}
	}
	return GetLeageInSport
}

//check if the input URL from the atom list is in configured list of sports
func IsSportOnTheList(sdiUrl string) bool {
	var isSportOnTheList bool = false
	//read sport types from config
	var xmlOlgSidMap XMLOlgSidMap
	ReadMappingFromXmlConfig(&xmlOlgSidMap)
	var sportTypes []string = xmlOlgSidMap.SportLeages.SportLeage
	for _, value := range sportTypes {
		if strings.Contains(sdiUrl, value) {
			isSportOnTheList = true
			//fmt.Printf(" sport:\t  %s\n", value)
			break
		}
	}
	return isSportOnTheList
}

//add competition element to the list of competitions
func ExtendCompetition(slice []XMLCompetition, element XMLCompetition) []XMLCompetition {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]XMLCompetition, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

//add leage element to the list of leages
func ExtendLeage(slice []SDILeage, element SDILeage) []SDILeage {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]SDILeage, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

//add sport element to the list of sports
func ExtendSport(slice []SDISport, element SDISport) []SDISport {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]SDISport, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

//feed cache with sdi data
func SdiDataFeedEvent() {
	//sdi atom feed url
	//var url string = "http://xml.sportsdirectinc.com/Atom?newerThan=2015-11-12T14:31:41.1384-3:00&apiKey=741E333E-7546-45A1-8A03-1FA91BFFF499"
	var url = GetSdiAtomUrlFromConfig()
	DebugFormatToSDILogfile("url = " + url)
	var xmlAtomFeed XMLAtomFeed = GetSdiAtomXml(url)
	//now we've got our full sports object from SDI
	//time to put it into cache:
	AddToCache(xmlAtomFeed)
	//read and convert Olg-Sdi data maping (sports/leages/teams) to our structure
	var xmlOlgSidMap XMLOlgSidMap
	ReadMappingFromXmlConfig(&xmlOlgSidMap)
	//olg-sdi sports mapping (from config) => cache
	//var sports map[string]string = ReadMapFromConfig("SPORT_OLG_SDI_MAP_")
	var sports map[string]string = ReadMapFromConfig2(xmlOlgSidMap.Sports.Maps)
	//AddToCacheSportsMapping(sports)
	AddToCacheMapping(SDI_CACHE_KEY_SPORTS_MAP, sports)
	//olg-sdi leages mapping (from config) => cache
	//var leages map[string]string = ReadMapFromConfig("LEAGE_OLG_SDI_MAP_")
	var leages map[string]string = ReadMapFromConfig2(xmlOlgSidMap.Leages.Maps)
	//AddToCacheLeagesMapping(leages)
	AddToCacheMapping(SDI_CACHE_KEY_LEAGES_MAP, leages)
	//olg-sdi teams mapping (from config) => cache
	//var teams map[string]string = ReadMapFromConfig("TEAM_OLG_SDI_MAP_")
	var teams map[string]string = ReadMapFromConfig2(xmlOlgSidMap.Teams.Maps)
	//AddToCacheTeamsMapping(teams)
	AddToCacheMapping(SDI_CACHE_KEY_TEAMS_MAP, teams)
	//we keep all SDI info in this dome object
	var domeSports []SDISport = GetSdiSportEventsDocument2(xmlAtomFeed)
	//set upper-case to SDI teams
	for i1, value := range domeSports {
		for i2, value2 := range value.Leages {
			for i, value3 := range value2.Competitions {
				domeSports[i1].Leages[i2].Competitions[i].HomeTeamContent.Team.Name = strings.ToUpper(value3.HomeTeamContent.Team.Name)
				domeSports[i1].Leages[i2].Competitions[i].AwayTeamContent.Team.Name = strings.ToUpper(value3.AwayTeamContent.Team.Name)
			}
		}
	}
	//PY 2016-01-11
	//	for _, value := range domeSports {
	//		for _, value2 := range value.Leages {
	//			for i, value3 := range value2.Competitions {
	//				i = i + 1
	//				if i < 10 {
	//					fmt.Printf("Name (1) = %s \n", value3.HomeTeamContent.Team.Name)
	//					fmt.Printf("Name (2) = %s \n", value3.AwayTeamContent.Team.Name)
	//				}
	//			}
	//		}
	//	}

	//put SDI object into cache
	AddToCacheSdiData(domeSports)

	var cnt = 0
	for _, value := range domeSports {
		cnt = cnt + len(value.Leages)
		DebugFormatToSDILogfile("leages in : %s %d", value.Name, len(value.Leages))
	}
	DebugFormatToSDILogfile("leages TOTAL  %d \n", cnt)
	DebugFormatToSDILogfile("TIME: %s \n", time.Now())
	//print leages
	for i3, value3 := range domeSports {
		DebugFormatToSDILogfile("sport:\t %d %s ", i3+1, value3.Name)
		for i4, value4 := range value3.Leages {
			DebugFormatToSDILogfile("leage:\t\t %d  %s ", i4+1, value4.Name)
		}
	}

}

//function to read xml and transform to the proper structure
func ReadOlgSdiMapping(reader io.Reader) (XMLOlgSidMap, error) {
	var xmlOlgSidMap XMLOlgSidMap
	if err := xml.NewDecoder(reader).Decode(&xmlOlgSidMap); err != nil {
		return xmlOlgSidMap, err
	}
	return xmlOlgSidMap, nil
}

//get sports/leages/teams olg-sdi mapping from xml mapping configfile
func ReadMappingFromXmlConfig(xmlOlgSidMap *XMLOlgSidMap) {
	// filepath.Abs appends the file name to the default working directly
	var olgSdiMappingFilePath, _ = config.GetEntryValueDirectlyFromConfigFile("sdi.OlgSdiMappingFilePath")
	//fmt.Printf("path = %s" + olgSdiMappingFilePath)
	olgFilePath, err := filepath.Abs(olgSdiMappingFilePath)
	//olgFilePath, err := filepath.Abs("olg-services/xml/OlgSdiMapping.xml")
	if err != nil {
		log.Error("ReadMappingFromXmlConfig: getting local path for OlgSdiMapping.xml failed", err)
		os.Exit(1)
	}
	// Open the straps.xml file
	file, err := os.Open(olgFilePath)
	if err != nil {
		log.Error("ReadMappingFromXmlConfig: opening OlgSdiMapping.xml failed", err)
		os.Exit(1)
	}
	defer file.Close()
	// read file
	*xmlOlgSidMap, err = ReadOlgSdiMapping(file)
	if err != nil {
		log.Error("ReadMappingFromXmlConfig: reading from OlgSdiMapping.xml failed", err)
		os.Exit(1)
	}
}
