package propicks

import (
	"encoding/xml"
	cache "github.com/pmylund/go-cache"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/proline/base"
	"olg-services/proline/sports"
	"olg-services/publicis/utils/log"
	"sort"
	"strings"
	"time"
)

const EVENT_DATE_PARSE_FORMAT = "2006-01-02"
const EVENT_DATE_WITH_TIME_PARSE_FORMAT = "2006-01-02T15:04:05"
const PROLINE_EVENT_DATE_TIME_PARSE_FORMAT = "2006-01-02 15:04:05.9"
const PARAM_TYPE_PROLINE_Cards = "ProPicks"

/* BEGIN: EventsBase */

// Base parent structure for "Proline Cards" type of feeds.
// It also helps with cacheability routines, by implementing interfaces.IFeedCache
type EventBase struct {
	base.BaseProlineFeed

	CacheStore *cache.Cache `json:"-"`

	Cards []Card `xml:"card" json:"data"`
}

// Common for both Card Events and Results
func (feed *EventBase) GetFeedUrl() string {
	return config.GlobalConfig.ProlineFeedURLs.Events
}

// Loads either the Proline Pointspread Cards or Results feed from OLG.
// If isResults is true, it loads the Results
func (feed *EventBase) LoadEventsResultsFeed(isResults bool) error {

	logFlag := "Proline ProPicks Events"
	if isResults {
		logFlag = "Proline ProPicks Results"
	}

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> "+logFlag+": LoadProPicksFeed")
	}

	var newFeedData interfaces.NewXmlFeed

	// instantiate a new structure for either events or results
	if isResults {
		newFeedData = &ProlineProPicksResultsFeed{}
	} else {
		newFeedData = &ProlineProPicksEventsFeed{}
	}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareEventsFeed(&feed.BaseProlineFeed, feed.GetSportId())

	// Because the proline feeds from OLG are old format, and do not come with a status code element,
	// we need to initialize the status code to OK then decode the feed, otherwise an empty value
	// will trigger an ascii to integer conversion error in the code.
	// The system will override the OK with any error in case it happens.
	// In essence, we must not allow StatusCode to me empty string as it needs to be an integer.
	newFeedData.SetStatusCode(constants.FEED_STATUS_CODE_OK)

	err := base.LoadXMLWithFeedOptions(feedOptions, newFeedData.(interfaces.NewXmlFeed), feed.GetFeedUrl()+requestParams)

	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.Cards = nil
	if isResults {
		// convert the newly loaded feed to *ProlineProPicksResultsFeed
		feed.Cards = make([]Card, len(newFeedData.(*ProlineProPicksResultsFeed).Cards))
		copy(feed.Cards, newFeedData.(*ProlineProPicksResultsFeed).Cards)
	} else {
		// convert the newly loaded feed to *ProlineProPicksEventsFeed
		feed.Cards = make([]Card, len(newFeedData.(*ProlineProPicksEventsFeed).Cards))
		copy(feed.Cards, newFeedData.(*ProlineProPicksEventsFeed).Cards)
	}

	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Cards {

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the event date and store it in the EventDateValue field
		if feed.IsCachedVersion {
			feed.Cards[i].PlayStartDateValue, err = time.Parse(PROLINE_EVENT_DATE_TIME_PARSE_FORMAT, feed.Cards[i].PlayStartDate)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage(logFlag + " feed - iterating through Cards - event date parsing error:" + err.Error())
				log.Error(logFlag+" feed - iterating through Cards - event date parsing error", err)
				return err
			}
		}
	}

	return nil
}

// Part of the interfaces.IProlineProPicks interface
// Eliminates any draws that are not inside the specified date range
func (feed *EventBase) TrimDateRange(startDate time.Time, endDate time.Time) {

	var cards []Card

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Cards {

		if InTimeSpan(startDate, endDate, feed.Cards[i].PlayStartDateValue) {
			cards = append(cards, feed.Cards[i])
		}
	}

	feed.Cards = cards

}

// Part of the interfaces.IProlineProPicks interface
// Merges the Cards inside the feedToAppend parameter to the parent feed
func (feed *EventBase) AppendEvents(feedToAppend interfaces.IProlinePointSpread) {

	log.Debug("AppendEvents - entered method")

	additionalFeed, ok := feedToAppend.(*ProlineProPicksEventsFeed)
	if ok {

		log.Debug("AppendEvents - found ProlineProPicksEventsFeed")
		feed.Cards = append(feed.Cards, additionalFeed.Cards...)
	}

}

// Sorts the values by name, if name exists for this game type.
// Implements the IProlineProPicks interface.
func (feed *EventBase) SortBy(sortByValue string) {

	// if the request contains a sortBy parameter then sort according to the value
	SortProPickCards(feed.Cards, feed.GetRequestParam("sortBy"))
}

// Common validation base method for "Proline Cards" type of feeds.
// Generally, the past winning number feeds all require just the
// start date and end date parameters to be valid. For special cases, this
// method will need to be overridden in the particular struct of that feed.
func (feed *EventBase) ValidateRequestParams() (int, string) {

	var validationErr string = ""

	if validationErr = base.ValidateNonEmptyDateTimeParameter("startDate", feed.InputParams.Get("startDate")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = base.ValidateNonEmptyDateTimeParameter("endDate", feed.InputParams.Get("endDate")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

/* Implement interfaces.IProlineEventsResults */

func (feed *EventBase) SetSportId(sportId string) {

	feed.SportId = sportId
}

// Implements interfaces.ICacheable. Returns the pointer to cache store created for this particular type of feed
func (feed *EventBase) GetSportId() string {

	return feed.SportId

}

/* Implement interfaces.ICacheable */

// Sets the pointer to cache store created for this particular type of feed.
// Implements interfaces.ICacheable.
func (feed *EventBase) SetCacheStore(store interface{}) {

	if store != nil {

		cacheStore, found := store.(*cache.Cache)
		if found {
			feed.CacheStore = cacheStore
		}
	}

}

// Returns the pointer to cache store created for this particular type of feed
// Implements interfaces.ICacheable.
func (feed *EventBase) GetCacheStore() interface{} {

	return feed.CacheStore

}

// The GetCachedFeedWithDateRange method takes an IProlineEventsResults interface entity, and returns a trimmed down
// version of it with only the values between the startDate and endDate included.
func GetCachedFeedWithDateRange(prolineEventsFeed interfaces.IProlineProPicks, sportType sports.SportType, startDate string, endDate string) (interfaces.IProlinePointSpread, error) {

	expectedDateTimeFormat := EVENT_DATE_WITH_TIME_PARSE_FORMAT

	startDateParsed, err := time.Parse(expectedDateTimeFormat, startDate)
	if err != nil {
		return nil, err
	}
	endDateParsed, err := time.Parse(expectedDateTimeFormat, endDate)
	if err != nil {
		return nil, err
	}

	// we need to make sure we clone the feed struct, because otherwise the cached struct itself
	// will be chopped up when doing the TrimDateRange operation
	prolineEventsFeedClone, err := NewProPicksEventsResultsFeedWithCache(nil, sportType, "proline propick cards - "+sportType.Code, false, false,
		prolineEventsFeed, prolineEventsFeed.IsResultsFeed())
	if err != nil {
		return nil, err
	}

	prolineEventsFeedClone.TrimDateRange(startDateParsed, endDateParsed)

	return prolineEventsFeedClone, nil

}

/* END: EventBase */

// This corresponds to one of the many "card" xml elements in the number frequency feeds
type Card struct {
	XMLName xml.Name `xml:"card" json:"-"`

	// The value of the "resultAvailable" xml element
	ResultAvailable string `xml:"resultAvailable" json:"resultAvailable"`

	// The value of the "scoringEn" xml element
	ScoringEn string `xml:"scoringEn" json:"scoringEn"`

	// The value of the "scoringFr" xml element
	scoringFr string `xml:"scoringFr" json:"scoringFr"`

	// The value of the "messageEn" xml element
	MessageEn string `xml:"messageEn" json:"messageEn"`

	// The value of the "messageFr" xml element
	MessageFr string `xml:"messageFr" json:"messageFr"`

	// The value of the "listNumber" xml element
	ListNumber string `xml:"listNumber" json:"listNumber"`

	// The value of the "cardId" xml element
	CardId string `xml:"cardId" json:"cardId"`

	// The value of the "prodGroupTypeCode" xml element
	ProdGroupTypeCode string `xml:"prodGroupTypeCode" json:"prodGroupTypeCode"`

	// The value of the "cardType" xml element
	CardType string `xml:"cardType" json:"cardType"`

	// The value of the "cardOpenDate" xml element
	CardOpenDate string `xml:"cardOpenDate" json:"cardOpenDate"`

	// The value of the "cardClosedDate" xml element
	CardClosedDate string `xml:"cardClosedDate" json:"cardClosedDate"`

	// The value of the "playStartDate" xml element
	PlayStartDate string `xml:"playStartDate" json:"playStartDate"`

	// the time.Date value will be calculated after ingesting the master xml feed
	PlayStartDateValue time.Time `xml:"-" json:"-"`

	// The value of the "playEndDate" xml element
	PlayEndDate string `xml:"playEndDate" json:"playEndDate"`

	// The value of the "sportId" xml element
	SportId string `xml:"sportId" json:"sportId"`

	// The value of the "cardDescription" xml element
	CardDescription string `xml:"cardDescription" json:"cardDescription"`

	// The value of the "cardDescriptionFr" xml element
	CardDescriptionFr string `xml:"cardDescriptionFr" json:"cardDescriptionFr"`

	// The value of the "suspend" xml element
	Suspend string `xml:"suspend" json:"suspend"`

	// The value of the "closed" xml element
	Closed string `xml:"closed" json:"closed"`

	// The value of the "active" xml element
	Active string `xml:"active" json:"active"`

	// The value of the "outcomeDesc" xml element
	OutcomeDesc string `xml:"outcomeDesc" json:"outcomeDesc"`

	// The value of the "outcomeDescFr" xml element
	OutcomeDescFr string `xml:"outcomeDescFr" json:"outcomeDescFr"`

	// The value of the "hashcode" xml element
	Hashcode string `xml:"hashcode" json:"hashcode"`

	// The value of the "payout" xml element
	Payout string `xml:"payout" json:"payout"`

	// The value of the "sport" xml element
	Sport string `xml:"sport" json:"sport"`
}

/* BEGIN: Proline ProPick Sorting Section */

// ByProlineProPicksOpenDate implements sort.Interface for []Card based on
// the CardOpenDate field.
type ByProlineProPicksOpenDate []Card

func (a ByProlineProPicksOpenDate) Len() int           { return len(a) }
func (a ByProlineProPicksOpenDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByProlineProPicksOpenDate) Less(i, j int) bool { return a[i].CardOpenDate < a[j].CardOpenDate }

// ByProlineProPicksPlayStartDate implements sort.Interface for []Card based on
// the PlayStartDate field.
type ByProlineProPicksPlayStartDate []Card

func (a ByProlineProPicksPlayStartDate) Len() int      { return len(a) }
func (a ByProlineProPicksPlayStartDate) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByProlineProPicksPlayStartDate) Less(i, j int) bool {
	return a[i].PlayStartDate < a[j].PlayEndDate
}

// ByProlineProPicksSportId implements sort.Interface for []Card based on
// the SportId (code)
type ByProlineProPicksSportId []Card

func (a ByProlineProPicksSportId) Len() int           { return len(a) }
func (a ByProlineProPicksSportId) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByProlineProPicksSportId) Less(i, j int) bool { return a[i].Sport < a[j].Sport }

func SortProlineEvent(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

// Sort the card based on the sortByValue parameter. That one can be "freq", "name", or "num".
func SortProPickCards(card []Card, sortByValue string) {

	if sortByValue == "" {
		return
	}

	if strings.ToLower(sortByValue) == "opendate" {
		SortProlineEvent(ByProlineProPicksOpenDate(card))
	} else if strings.ToLower(sortByValue) == "startdate" {
		SortProlineEvent(ByProlineProPicksPlayStartDate(card))
	} else if strings.ToLower(sortByValue) == "sportid" {
		SortProlineEvent(ByProlineProPicksSportId(card))
	}

}

/* END: ProPick Sorting Section */

/* ******************************************* */
/* BEGIN: Cards Common Functions */
/* ******************************************* */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
// Event feeds have a common set of input parameters, such as the
// start and end date
func PrepareEventsFeed(feed *base.BaseProlineFeed, sportId string) (string, base.FeedOptions) {

	var requestParams string = ""

	// prepare the request parameters
	if feed.InputParams != nil {
		requestParams = feed.GetQueryParams(
			feed.KVPair("sportId", sportId),
			feed.KVPair("type", PARAM_TYPE_PROLINE_Cards),
			feed.KVPair("startDate", feed.InputParams.Get("startDate")),
			feed.KVPair("endDate", feed.InputParams.Get("endDate")),
		)
	}

	var feedOptions base.FeedOptions

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* ******************************************* */
/* END: Frequency Common Functions   */
/* ******************************************* */
