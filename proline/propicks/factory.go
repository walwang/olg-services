package propicks

import (
	cache "github.com/pmylund/go-cache"
	"olg-services/interfaces"
	"olg-services/proline/sports"
	"strings"
)

func NewProPicksEventsResultsFeed(sportType sports.SportType, jobName string, sendErrorEmails, sendErrorSms bool, isResults bool) (interfaces.IProlineProPicks, error) {
	return NewProPicksEventsResultsFeedWithCache(nil, sportType, jobName, sendErrorEmails, sendErrorSms, nil, isResults)
}

func NewProPicksEventsResultsFeedWithCache(cacheStore *cache.Cache, sportType sports.SportType, jobName string,
	sendErrorEmails, sendErrorSms bool,
	feedToCloneFrom interfaces.IProlineProPicks, isResults bool) (interfaces.IProlineProPicks, error) {

	var feed interfaces.IProlineProPicks

	if strings.ToLower(sportType.Code) == "timeout" {
		feed = &ProlineProPicksCardsTimeoutFeed{}
	} else {

		if isResults {

			// Proline ProPicks Results instance

			structFeed := &ProlineProPicksResultsFeed{}

			structFeed.SetSportId(sportType.OlgSportId)

			if feedToCloneFrom != nil {

				cloneSource, ok := feedToCloneFrom.(*ProlineProPicksResultsFeed)
				if ok {
					structFeed.BaseProlineFeed = cloneSource.BaseProlineFeed
					structFeed.Cards = make([]Card, len(cloneSource.Cards))
					copy(structFeed.Cards, cloneSource.Cards)
				}

			}

			feed = structFeed

		} else {

			// Proline ProPicks Events instance

			structFeed := &ProlineProPicksEventsFeed{}

			structFeed.SetSportId(sportType.OlgSportId)

			if feedToCloneFrom != nil {

				cloneSource, ok := feedToCloneFrom.(*ProlineProPicksEventsFeed)
				if ok {
					structFeed.BaseProlineFeed = cloneSource.BaseProlineFeed
					structFeed.Cards = make([]Card, len(cloneSource.Cards))
					copy(structFeed.Cards, cloneSource.Cards)
				}

			}

			feed = structFeed
		}

	}

	// if the conversion to ICacheable interface succeeds, set the cache store
	cacheableStruct, ok := feed.(interfaces.ICacheable)
	if ok {
		cacheableStruct.SetCacheStore(cacheStore)
	}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	}

	return feed, nil

}
