package propicks

import (
	"encoding/xml"
)

/* Types */

type ProlineProPicksEventsFeed struct {
	XMLName xml.Name `xml:"proPickCards" json:"-"`

	// embed the Proline Events base struct
	EventBase
}

func (feed *ProlineProPicksEventsFeed) IsResultsFeed() bool {
	return false
}

// Loads the Proline ProPicks Events feed from OLG
func (feed *ProlineProPicksEventsFeed) LoadFeed() error {
	return feed.LoadEventsResultsFeed(false)
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlineProPicksEventsFeed) IsValid() bool {
	return feed.Cards != nil
}

type ProlineProPicksResultsFeed struct {
	XMLName xml.Name `xml:"proPickCardsResults" json:"-"`

	// embed the Proline Events base struct, this is shared between
	// Events and Results
	EventBase
}

func (feed *ProlineProPicksResultsFeed) IsResultsFeed() bool {
	return true
}

// Loads the Proline Pointspread Results feed from OLG
func (feed *ProlineProPicksResultsFeed) LoadFeed() error {
	return feed.LoadEventsResultsFeed(true)
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlineProPicksResultsFeed) IsValid() bool {
	return feed.Cards != nil
}
