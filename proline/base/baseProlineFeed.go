package base

import (
	"net/http"
	"net/url"
	"olg-services/constants"
	"olg-services/cronJob"
	"olg-services/publicis/utils/log"
	"strconv"
	"strings"
	"time"
)

type BaseProlineFeed struct {
	cronJob.BaseCronJobStruct

	// The type of the proline feed
	FeedType ProlineFeedType `json:"-"`

	// describes how the feed is ingested (https used, certificates, etc)
	FeedOptions FeedOptions `json:"-"`

	LastOkDate        time.Time `json:"-"`
	LastDateProcessed time.Time `json:"-"`
	LastErrorMessage  string    `json:"-"`

	/* The StatusCode, StatusMessage and the ErrorMessage values
	are passed as standardized JSON output, regardless of the xml having them */

	StatusCode    string `json:"code"`
	StatusMessage string `json:"status"`
	ErrorMessage  string `json:"message,omitempty"`

	// this should be set to true when the master (whole) feed is loaded into cache,
	// in order to trigger date sorting, trimming and other feed loading functionality
	IsCachedVersion bool `json:"-"`

	InputParams *url.Values `json:"-"`

	AdditionalParams *url.Values `json:"-"`

	// For certain proline feeds a sport id is passed to filter the results.
	SportId string `json:"-"`
}

// Overriding the default LogError for any Proline jobs. If the status is faulty already,
// do not send the emails as it is assumed that the first one had already been sent.
func (cronJobStruct *BaseProlineFeed) LogRecurringError(errorMessage string, err error) {

	log.Error(errorMessage, err)

	if cronJobStruct.GetJobStatus() == cronJob.JOB_STATUS_FAULTY {
		return
	}

	cronJobStruct.SetJobStatus(cronJob.JOB_STATUS_FAULTY)
	cronJobStruct.LogEmailOrSmsError(errorMessage, err)

}

func (cronJobStruct *BaseProlineFeed) LogRecoveryNoticeAndSetJobStatusToRunning(recoveryMessage string) {

	cronJobStruct.SetJobStatus(cronJob.JOB_STATUS_RUNNING)
	cronJobStruct.LogEmailOrSmsInfo(recoveryMessage)

}

/* Implement the CronJobStatus interface */

func (feed *BaseProlineFeed) GetLastCorrectlyProcessedDate() string {

	return feed.LastOkDate.Format(time.RFC3339)
}

func (feed *BaseProlineFeed) SetLastCorrectlyProcessedDate(t time.Time) {

	feed.LastOkDate = t
}

func (feed *BaseProlineFeed) GetLastDateProcessed() string {
	return feed.LastDateProcessed.Format(time.RFC3339)
}

func (feed *BaseProlineFeed) SetLastDateProcessed(t time.Time) {
	feed.LastDateProcessed = t
}

func (feed *BaseProlineFeed) GetLastErrorMessage() string {
	return feed.LastErrorMessage
}

func (feed *BaseProlineFeed) SetLastErrorMessage(errMessage string) {
	feed.LastErrorMessage = errMessage
}

func (feed *BaseProlineFeed) GetFeedCode() string {
	return feed.FeedType.Code
}

/* Implement the NewXmlFeed interface */

func (f *BaseProlineFeed) SetIsCached(isCached bool) {

	f.IsCachedVersion = isCached

}

func (f *BaseProlineFeed) SetStatusCode(statusCode int) {

	f.StatusCode = strconv.Itoa(statusCode)

}

// Returns the current status code returned from the feed. If the code is not
// a valid integer, return FEED_STATUS_CODE_INVALID_STATUS_CODE_RECEIVED
func (f *BaseProlineFeed) GetStatusCode() int {

	statusCode, err := strconv.Atoi(f.StatusCode)
	if err != nil {
		log.Error("BaseProlineFeed.GetStatusCode() - strconv.Atoi", err)
		return constants.FEED_STATUS_CODE_INVALID_STATUS_CODE_RECEIVED
	}

	return statusCode

}

func (f *BaseProlineFeed) GetStatusMessage() string {

	return f.StatusMessage
}

func (f *BaseProlineFeed) SetStatusMessage(statusMessage string) {

	f.StatusMessage = statusMessage

}

func (f *BaseProlineFeed) GetErrorMessage() string {

	return f.ErrorMessage
}

func (f *BaseProlineFeed) SetErrorMessage(errorMessage string) {

	f.ErrorMessage = errorMessage

}

/* Implement the interfaces.NewXmlFeed interface */

func (f *BaseProlineFeed) GetFeedUrl() string {

	return "base_url_please_replace"
}

// Part of the interfaces.NewXmlFeed interface
// Returns true by default, descendant structs must implement properly
func (feed *BaseProlineFeed) IsValid() bool {
	return true
}

// Part of the interfaces.NewXmlFeed interface
// Returns nil by default, descendant structs must implement properly
func (feed *BaseProlineFeed) LoadFeed() error {
	return nil
}

// Base implementation of ValidateRequestParams method
// that just returns constants.FEED_STATUS_CODE_OK and constants.FEED_STATUS_MESSAGE_OK.
// Custom feed structures will need to override this method.
func (feed *BaseProlineFeed) ValidateRequestParams() (int, string) {

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

func (feed *BaseProlineFeed) SetRequestParams(request *http.Request) {

	feed.InputParams = &request.Form

}

// Allows custom parameters to be passed to the feed
func (feed *BaseProlineFeed) SetRequestParam(key string, value string) {

	if feed.AdditionalParams == nil {
		feed.AdditionalParams = &url.Values{}
	}

	feed.AdditionalParams.Set(key, value)

}

// Search the request parameters first, then the additional parameters
// (the ones introduced programatically during the feed processing).
// If nothing is found, an empty string is returned.
func (feed *BaseProlineFeed) GetRequestParam(key string) string {

	var value string = ""

	if feed.InputParams != nil {

		value = feed.InputParams.Get(key)

		if value == "" && feed.AdditionalParams != nil {
			value = feed.AdditionalParams.Get(key)
		}

	}

	return value
}

/* Validation functions */

// Insures the date param is not empty adheres to a yyyy-mm-dd format.
// Returns empty if all good, or an appropriate error message string otherwise.
func ValidateNonEmptyDateParameter(paramName string, paramValue string) string {

	if paramValue == "" {
		return paramName + " is empty"
	}

	expectedDateTimeFormat := "2006-01-02"
	if _, err := time.Parse(expectedDateTimeFormat, paramValue); err != nil {
		return paramName + " not a valid yyyy-mm-dd date parameter"
	}

	return ""

}

// Insures the datetime param is not empty adheres to a yyyy-mm-ddThh:mm:ss format.
// Returns empty if all good, or an appropriate error message string otherwise.
func ValidateNonEmptyDateTimeParameter(paramName string, paramValue string) string {

	if paramValue == "" {
		return paramName + " is empty"
	}

	expectedDateTimeFormat := "2006-01-02T15:04:05"
	if _, err := time.Parse(expectedDateTimeFormat, paramValue); err != nil {
		return paramName + " not a valid yyyy-mm-ddThh:mm:ss date parameter"
	}

	return ""

}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* XML Feed functions */

/* Request URL building functions */

// Constructs an Http GET query string, including the initial question mark character.
// Expects a variadic list of keyValuePair strings in the "k=v" format
func (f *BaseProlineFeed) GetQueryParams(keyValuePairs ...string) string {

	var entireString string = "?"

	for i := range keyValuePairs {
		entireString = entireString + keyValuePairs[i] + "&"
	}

	if len(entireString) > 1 {
		// get rid of the final &
		entireString = strings.TrimRight(entireString, "&")
	}

	return entireString

}

// Constructs an k=v string out of the key and value, and returns it e.g. param1=abc
func (f *BaseProlineFeed) KVPair(key, value string) string {
	return key + "=" + value
}

// Constructs an k=v string out of the key and value, and returns it e.g. param1=abc
// If value is empty, returns an overall empty string.
func (f *BaseProlineFeed) KVPairOmitEmpty(key, value string) string {

	if value == "" {
		return ""
	}
	return key + "=" + value
}
