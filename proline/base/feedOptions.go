package base

// Options for how the http client behaves when connecting to the OLG Proline feeds
type FeedOptions struct {

	// Specifies that https will be used
	IsHttps bool `json:"-"`

	// Client timeout in seconds
	ClientTimeout int `json:"-"`

	// Certificate file location (including the filename)
	CertificateFilePath string `json:"-"`

	// Skip verification of insecure https connections
	InsecureSkipVerify bool `json:"-"`
}
