package base

// ProlineFeedType indicates the type of proline feed that is being ingested
type ProlineFeedType struct {

	// A numerical identifier
	FeedId int

	// The name of the feed in human readable format
	Name string

	// The shortened, unique code of the feed
	Code string
}

// Global map of feed types
var ProlineFeedTypes map[string]ProlineFeedType = map[string]ProlineFeedType{
	"prolineEvents":      ProlineFeedType{FeedId: 1, Name: "Proline Events", Code: "prolineEvents"},
	"prolineResults":     ProlineFeedType{FeedId: 2, Name: "Proline Results", Code: "prolineResults"},
	"pointspreadEvents":  ProlineFeedType{FeedId: 3, Name: "PointSpread Events", Code: "pointspreadEvents"},
	"pointspreadResults": ProlineFeedType{FeedId: 4, Name: "PointSpread Results", Code: "pointspreadResults"},
	"propicksEvents":     ProlineFeedType{FeedId: 5, Name: "ProPicks Events", Code: "propicksEvents"},
	"propicksResults":    ProlineFeedType{FeedId: 6, Name: "ProPicks Results", Code: "propicksResults"},
}
