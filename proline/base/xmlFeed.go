package base

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/publicis/utils/charset"
	_ "olg-services/publicis/utils/charset/data"
	"olg-services/publicis/utils/log"
	"time"
)

// Allows parameters to be passed to the feed via HTTP GET or POST.
// Each feed struct can override this as needed
func LoadRequestParams(feedToValidate interfaces.NewXmlFeed, request *http.Request) error {

	if request == nil {
		return nil
	}

	if err := request.ParseForm(); err != nil {
		return log.NewError("LoadRequestParams - req.ParseForm()", err)
	}

	feedToValidate.SetRequestParams(request)

	// if there are parameters passed in the form, attempt to call the associated feed
	// ValidateRequestParams method.
	if &request.Form != nil {

		code, message := feedToValidate.ValidateRequestParams()
		if code != constants.FEED_STATUS_CODE_OK {
			return log.NewErrorLocal("Invalid parameter", message)
		}

	}

	return nil
}

// Loads the xml feed from the specified url and loads it in the specified feedModule
// The additional feedOptions param specifies the http or https client transport and net options
func LoadXMLWithFeedOptions(feedOptions FeedOptions, feedModule interfaces.NewXmlFeed, url string) error {

	var feedClientTimeoutSeconds int = config.GlobalConfig.Environment.DefaultFeedTimeout
	var timeout time.Duration

	// attempt a default of 15 seconds to timeout from calling feeds
	// or use the timeout inside the feed options if greater than zero
	if feedOptions.ClientTimeout > 0 {
		timeout = time.Duration(time.Duration(feedOptions.ClientTimeout) * time.Second)
	} else {
		timeout = time.Duration(time.Duration(feedClientTimeoutSeconds) * time.Second)
	}

	var client http.Client

	// if http is used, a custom transport is needed
	if feedOptions.IsHttps {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: feedOptions.InsecureSkipVerify},
		}

		client = http.Client{
			Timeout:   timeout,
			Transport: tr,
		}

	} else {

		client = http.Client{
			Timeout: timeout,
		}

	}

	return loadXML(client, feedModule, url)
}

// Loads the xml feed from the specified url and loads it in the specified feedModule
func LoadXML(feedModule interfaces.NewXmlFeed, url string) error {

	var feedClientTimeoutSeconds int = config.GlobalConfig.Environment.DefaultFeedTimeout

	// the timeout is in the settings.conf in the defaultFeedTimeout key
	timeout := time.Duration(time.Duration(feedClientTimeoutSeconds) * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	return loadXML(client, feedModule, url)

}

// Loads the xml feed from the specified url and loads it in the specified feedModule
func loadXML(client http.Client, feedModule interfaces.NewXmlFeed, url string) error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "proline -> BaseProlineFeed: LoadXML for url: "+url)
	}

	timeoutCheck := time.Now()
	resp, err := client.Get(url)
	if err != nil {

		feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_HTTP_ERROR)

		elapsedTimeoutCheck := time.Since(timeoutCheck)
		if elapsedTimeoutCheck.Seconds() >= client.Timeout.Seconds() {
			feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_HTTP_CLIENT_TIMEOUT_ERROR)
		}

		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
		feedModule.SetErrorMessage(err.Error())

		return log.NewError("LoadXML Error at http.Get", err)
	}

	defer resp.Body.Close()

	// indicate we should use ioutilReadAll and not use a streamed
	// approach when instantiating the xml decoder
	// the tests indicate that for small payload the ioutil.ReadAll is more than
	// twice as fast as passing the body to the xml decoder as a Reader interface
	httpBodyReadAllApproach := true

	var decoder *xml.Decoder

	if httpBodyReadAllApproach {

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {

			feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_HTTP_ERROR)
			feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
			feedModule.SetErrorMessage(err.Error())

			return log.NewError("LoadXML Error at ioutil.ReadAll", err)
		}

		reader := bytes.NewReader(body)
		decoder = xml.NewDecoder(reader)

	} else {
		decoder = xml.NewDecoder(resp.Body)
	}

	decoder.CharsetReader = charset.NewReader
	err = decoder.Decode(feedModule)
	if err != nil {

		feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_XML_DECODE_ERROR)
		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
		feedModule.SetErrorMessage(err.Error())

		return log.NewError("LoadXML Error at decoder.Decode", err)
	}

	// because the feeds do not have a schema it's very possible that
	// after decoding, a different, invalid xml or html was ingested
	// without any errors, and the data structure is simply empty
	// we need to check if we are good
	if feedModule.IsValid() == false {
		feedModule.SetStatusCode(constants.FEED_STATUS_CODE_FEED_XML_DECODE_ERROR)
		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
		feedModule.SetErrorMessage("Invalid xml or html was ingested - please check the feed url")
		return log.NewErrorLocal("LoadXML Error post decoder.Decode", "Invalid xml or html was ingested from "+url)
	}

	// normally after the feed has been decoded, the status code arriving should be 0 (OK)
	if feedModule.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feedModule.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
		feedModule.SetErrorMessage("")
	}

	return nil

}
