package sports

// The SportType structure represents the OLG sport codes inside the Proline feeds
type SportType struct {

	// The sport id (or code) as defined by OLG the Proline feeds
	OlgSportId string

	// The human friendly name
	Name string

	// The OLG Code
	Code string
}

var SportTypes map[string]SportType = map[string]SportType{
	"all": SportType{OlgSportId: "ALL", Name: "All Sports", Code: "ALL"},
	"bbl": SportType{OlgSportId: "BBL", Name: "Major League Baseball", Code: "BBL"},
	"car": SportType{OlgSportId: "CAR", Name: "Racing Pools", Code: "CAR"},
	"cbk": SportType{OlgSportId: "CBK", Name: "College Basketball", Code: "CBK"},
	"cfb": SportType{OlgSportId: "CFB", Name: "College Football", Code: "CFB"},
	"ftb": SportType{OlgSportId: "FTB", Name: "American Football", Code: "FTB"},
	"hky": SportType{OlgSportId: "HKY", Name: "National Hockey League", Code: "HKY"},
	"scr": SportType{OlgSportId: "SCR", Name: "Soccer", Code: "SCR"},
}
