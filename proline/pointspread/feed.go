package pointspread

import (
	"encoding/xml"
)

/* Types */

type ProlinePointSpreadEventsFeed struct {
	XMLName xml.Name `xml:"pointSpreadEvents" json:"-"`

	// embed the Proline Events base struct
	EventBase
}

func (feed *ProlinePointSpreadEventsFeed) IsResultsFeed() bool {
	return false
}

// Loads the Proline Pointspread Events feed from OLG
func (feed *ProlinePointSpreadEventsFeed) LoadFeed() error {
	return feed.LoadEventsResultsFeed(false)
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlinePointSpreadEventsFeed) IsValid() bool {
	return feed.Events != nil
}

type ProlinePointSpreadResultsFeed struct {
	XMLName xml.Name `xml:"pointSpreadResults" json:"-"`

	// embed the Proline Events base struct, this is shared between
	// Events and Results
	EventBase
}

func (feed *ProlinePointSpreadResultsFeed) IsResultsFeed() bool {
	return true
}

// Loads the Proline Pointspread Results feed from OLG
func (feed *ProlinePointSpreadResultsFeed) LoadFeed() error {
	return feed.LoadEventsResultsFeed(true)
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlinePointSpreadResultsFeed) IsValid() bool {
	return feed.Events != nil
}
