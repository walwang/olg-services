package pointspread

import (
	cache "github.com/pmylund/go-cache"
	"olg-services/interfaces"
	"olg-services/proline/sports"
	"strings"
)

func NewProlinePointSpreadEventsResultsFeed(sportType sports.SportType, jobName string, sendErrorEmails, sendErrorSms bool, isResults bool) (interfaces.IProlinePointSpread, error) {
	return NewProlinePointSpreadEventsResultsFeedWithCache(nil, sportType, jobName, sendErrorEmails, sendErrorSms, nil, isResults)
}

func NewProlinePointSpreadEventsResultsFeedWithCache(cacheStore *cache.Cache, sportType sports.SportType, jobName string,
	sendErrorEmails, sendErrorSms bool,
	feedToCloneFrom interfaces.IProlinePointSpread, isResults bool) (interfaces.IProlinePointSpread, error) {

	var feed interfaces.IProlinePointSpread

	if strings.ToLower(sportType.Code) == "timeout" {
		feed = &ProlinePointSpreadEventsTimeoutFeed{}
	} else {

		if isResults {

			// Proline Pointspread Results instance

			structFeed := &ProlinePointSpreadResultsFeed{}

			structFeed.SetSportId(sportType.OlgSportId)

			if feedToCloneFrom != nil {

				cloneSource, ok := feedToCloneFrom.(*ProlinePointSpreadResultsFeed)
				if ok {
					structFeed.BaseProlineFeed = cloneSource.BaseProlineFeed
					structFeed.Events = make([]ProlinePointSpreadEvent, len(cloneSource.Events))
					copy(structFeed.Events, cloneSource.Events)
				}

			}

			feed = structFeed

		} else {

			// Proline Pointspread Events instance

			structFeed := &ProlinePointSpreadEventsFeed{}

			structFeed.SetSportId(sportType.OlgSportId)

			if feedToCloneFrom != nil {

				cloneSource, ok := feedToCloneFrom.(*ProlinePointSpreadEventsFeed)
				if ok {
					structFeed.BaseProlineFeed = cloneSource.BaseProlineFeed
					structFeed.Events = make([]ProlinePointSpreadEvent, len(cloneSource.Events))
					copy(structFeed.Events, cloneSource.Events)
				}

			}

			feed = structFeed
		}

	}

	// if the conversion to ICacheable interface succeeds, set the cache store
	cacheableStruct, ok := feed.(interfaces.ICacheable)
	if ok {
		cacheableStruct.SetCacheStore(cacheStore)
	}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	}

	return feed, nil

}
