package events

import (
	"encoding/xml"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/proline/base"
	"olg-services/publicis/utils/log"
	"time"
)

/* Types */

// The ProlineEventsTimeoutFeed struct is used to simulate a server timeout situation
type ProlineEventsTimeoutFeed struct {

	// embed the base new feeds
	EventBase

	TimeoutHtml TimeoutHtml `xml:"html" json:"data"`
}

func (feed *ProlineEventsTimeoutFeed) IsResultsFeed() bool {
	return false
}

// This corresponds to the timeout "html" element in the timeout feed
type TimeoutHtml struct {
	XMLName xml.Name `xml:"html" json:"-"`
	Body    string   `xml:"body" json:"body,omitempty"`
}

/* Methods */

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlineEventsTimeoutFeed) IsValid() bool {
	return feed.TimeoutHtml.Body != ""
}

// Part of the interfaces.NewXmlFeed interface
func (feed *ProlineEventsTimeoutFeed) GetFeedUrl() string {

	// Hit the timeout server with a 35 seconds setting
	return "http://www.publicistechlabs.ca/timeout/seconds/35"
}

// Loads the past winning numbers Timeout feed from the test server
func (feed *ProlineEventsTimeoutFeed) LoadFeed() error {

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> ProlineEventsTimeoutFeed: LoadFeed")
	}

	// instantiate a new structure for the
	newFeedData := ProlineEventsTimeoutFeed{}

	err := base.LoadXML(&newFeedData, feed.GetFeedUrl())
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.TimeoutHtml = newFeedData.TimeoutHtml
	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	return nil

}

// Part of the interfaces.IProlineEventsResults interface
// Eliminates any draws that are not inside the specified date range
func (feed *ProlineEventsTimeoutFeed) TrimDateRange(startDate time.Time, endDate time.Time) {

	// do nothing since it's a mock struct to simulate timeouts

}

// Part of the interfaces.IProlineEventsResults interface
// Merges the draws inside the feedToAppend parameter to the parent feed
func (feed *ProlineEventsTimeoutFeed) AppendDraws(feedToAppend interfaces.IPastWinningNumbers) {

	// do nothing since it's a mock struct to simulate timeouts

}

// Part of the interfaces.IProlineEventsResults interface
// Merges the events inside the feedToAppend parameter to the parent feed
func (feed *ProlineEventsTimeoutFeed) AppendEvents(feedToAppend interfaces.IProlineEventsResults) {

	// do nothing since it's a mock struct to simulate timeouts

}

// Sorts the values by name, if name exists for this game type.
// Implements the IProlineEventsResults interface.
func (feed *ProlineEventsTimeoutFeed) SortBy(sortByValue string) {

	// do nothing since it's a mock struct to simulate timeouts
}
