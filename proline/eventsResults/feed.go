package events

import (
	"encoding/xml"
)

/* Types */

type ProlineEventsFeed struct {
	XMLName xml.Name `xml:"prolineEvents" json:"-"`

	// embed the Proline Events base struct
	EventBase
}

func (feed *ProlineEventsFeed) IsResultsFeed() bool {
	return false
}

// Loads the Proline Events feed from OLG
func (feed *ProlineEventsFeed) LoadFeed() error {
	return feed.LoadEventsResultsFeed(false)
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlineEventsFeed) IsValid() bool {
	return feed.Events != nil
}

type ProlineResultsFeed struct {
	XMLName xml.Name `xml:"prolineResults" json:"-"`

	// embed the Proline Events base struct, this is shared between
	// Events and Results
	EventBase
}

func (feed *ProlineResultsFeed) IsResultsFeed() bool {
	return true
}

// Loads the Proline Results feed from OLG
func (feed *ProlineResultsFeed) LoadFeed() error {
	return feed.LoadEventsResultsFeed(true)
}

// Part of the interfaces.NewXmlFeed interface
// Attempts to detect if the feed has been loaded properly
func (feed *ProlineResultsFeed) IsValid() bool {
	return feed.Events != nil
}
