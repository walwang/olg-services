package events

import (
	"encoding/xml"
	cache "github.com/pmylund/go-cache"
	"olg-services/config"
	"olg-services/constants"
	"olg-services/interfaces"
	"olg-services/proline/base"
	"olg-services/proline/sports"
	"olg-services/publicis/utils/log"
	"sort"
	"strings"
	"time"
)

const EVENT_DATE_PARSE_FORMAT = "2006-01-02"

const EVENT_DATE_WITH_TIME_PARSE_FORMAT = "2006-01-02T15:04:05"

// the format of the eventDate xml element inside each prolineEvent iteam
const PROLINE_EVENT_DATE_TIME_PARSE_FORMAT = "2006-01-02 15:04:05.9"

const PARAM_TYPE_PROLINE_EVENTS = "ProLine"

/* BEGIN: EventsBase */

// Base parent structure for "Proline Events" type of feeds.
// It also helps with cacheability routines, by implementing interfaces.IFeedCache
type EventBase struct {

	// embed the base new feeds
	base.BaseProlineFeed

	CacheStore *cache.Cache `json:"-"`

	// we are going to reuse the events ProlineEvent item
	// because the input (xml) and output(json) structure is identical
	Events []ProlineEvent `xml:"prolineEvent" json:"data"`
}

// Common for both Events and Results
func (feed *EventBase) GetFeedUrl() string {
	return config.GlobalConfig.ProlineFeedURLs.Events
}

// Loads either the Proline Events or Results feed from OLG.
// If isResults is true, it loads the Results
func (feed *EventBase) LoadEventsResultsFeed(isResults bool) error {

	logFlag := "Proline Events"
	if isResults {
		logFlag = "Proline Results"
	}

	if config.GlobalConfig.Environment.IsDummyMode {
		defer log.TimeTrack(log.Now(), "lotteries -> "+logFlag+": LoadEventsResultsFeed")
	}

	var newFeedData interfaces.NewXmlFeed

	// instantiate a new structure for either results or events
	if isResults {
		newFeedData = &ProlineResultsFeed{}
	} else {
		newFeedData = &ProlineEventsFeed{}
	}

	// prepare the request parameters and the feed options depending on the environment
	requestParams, feedOptions := PrepareEventsFeed(&feed.BaseProlineFeed, feed.GetSportId())

	// Because the proline feeds from OLG are old format, and do not come with a status code element,
	// we need to initialize the status code to OK then decode the feed, otherwise an empty value
	// will trigger an ascii to integer conversion error in the code.
	// The system will override the OK with any error in case it happens.
	// In essence, we must not allow StatusCode to me empty string as it needs to be an integer.
	newFeedData.SetStatusCode(constants.FEED_STATUS_CODE_OK)

	err := base.LoadXMLWithFeedOptions(feedOptions, newFeedData.(interfaces.NewXmlFeed), feed.GetFeedUrl()+requestParams)
	if err != nil {

		// in case of an http or network or xml ingestion problem, we need to
		// get the status code and message from the new feed, if not nil
		feed.SetStatusCode(newFeedData.GetStatusCode())
		feed.SetStatusMessage(newFeedData.GetStatusMessage())
		feed.SetErrorMessage(newFeedData.GetErrorMessage())

		return err
	}

	// the new feed data loaded ok - now replace the existing feed with the current feed
	feed.Events = nil
	if isResults {
		// convert the newly loaded feed to *ProlineResultsFeed
		feed.Events = make([]ProlineEvent, len(newFeedData.(*ProlineResultsFeed).Events))
		copy(feed.Events, newFeedData.(*ProlineResultsFeed).Events)
	} else {
		// convert the newly loaded feed to *ProlineEventsFeed
		feed.Events = make([]ProlineEvent, len(newFeedData.(*ProlineEventsFeed).Events))
		copy(feed.Events, newFeedData.(*ProlineEventsFeed).Events)
	}

	feed.SetStatusCode(newFeedData.GetStatusCode())

	if feed.GetStatusCode() == constants.FEED_STATUS_CODE_OK {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_OK)
	} else {
		feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
	}

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Events {

		// for the cached version (the one stored in the cache that contains the whole one year range)
		// we need to parse the event date and store it in the EventDateValue field
		if feed.IsCachedVersion {
			feed.Events[i].EventDateValue, err = time.Parse(PROLINE_EVENT_DATE_TIME_PARSE_FORMAT, feed.Events[i].EventDate)
			if err != nil {
				feed.SetStatusCode(constants.FEED_STATUS_CODE_FEED_INVALID_CONTENT)
				feed.SetStatusMessage(constants.FEED_STATUS_MESSAGE_ERROR)
				feed.SetErrorMessage(logFlag + " feed - iterating through events - event date parsing error:" + err.Error())
				log.Error(logFlag+" feed - iterating through events - event date parsing error", err)
				return err
			}
		}
	}

	return nil
}

// Part of the interfaces.IProlineEventsResults interface
// Eliminates any draws that are not inside the specified date range
func (feed *EventBase) TrimDateRange(startDate time.Time, endDate time.Time) {

	var newEvents []ProlineEvent

	// apply various JSON-output-oriented transformation in each of the draws
	for i := range feed.Events {

		if InTimeSpan(startDate, endDate, feed.Events[i].EventDateValue) {
			newEvents = append(newEvents, feed.Events[i])
		}
	}

	feed.Events = newEvents

}

// Part of the interfaces.IProlineEventsResults interface
// Merges the events inside the feedToAppend parameter to the parent feed
func (feed *EventBase) AppendEvents(feedToAppend interfaces.IProlineEventsResults) {

	log.Debug("AppendEvents - entered method")

	additionalFeed, ok := feedToAppend.(*ProlineEventsFeed)
	if ok {

		log.Debug("AppendEvents - found ProlineEventsFeed")
		feed.Events = append(feed.Events, additionalFeed.Events...)
	}

}

// Sorts the values by name, if name exists for this game type.
// Implements the IProlineEventsResults interface.
func (feed *EventBase) SortBy(sortByValue string) {

	// if the request contains a sortBy parameter then sort according to the value
	SortProlineEvents(feed.Events, feed.GetRequestParam("sortBy"))
}

// Common validation base method for "Proline Events" type of feeds.
// Generally, the past winning number feeds all require just the
// start date and end date parameters to be valid. For special cases, this
// method will need to be overridden in the particular struct of that feed.
func (feed *EventBase) ValidateRequestParams() (int, string) {

	var validationErr string = ""

	if validationErr = base.ValidateNonEmptyDateTimeParameter("startDate", feed.InputParams.Get("startDate")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	if validationErr = base.ValidateNonEmptyDateTimeParameter("endDate", feed.InputParams.Get("endDate")); validationErr != "" {
		return constants.FEED_STATUS_CODE_INVALID_PARAMETER, validationErr
	}

	return constants.FEED_STATUS_CODE_OK, constants.FEED_STATUS_MESSAGE_OK
}

/* Implement interfaces.IProlineEventsResults */

func (feed *EventBase) SetSportId(sportId string) {

	feed.SportId = sportId
}

// Implements interfaces.ICacheable. Returns the pointer to cache store created for this particular type of feed
func (feed *EventBase) GetSportId() string {

	return feed.SportId

}

/* Implement interfaces.ICacheable */

// Sets the pointer to cache store created for this particular type of feed.
// Implements interfaces.ICacheable.
func (feed *EventBase) SetCacheStore(store interface{}) {

	if store != nil {

		cacheStore, found := store.(*cache.Cache)
		if found {
			feed.CacheStore = cacheStore
		}
	}

}

// Returns the pointer to cache store created for this particular type of feed
// Implements interfaces.ICacheable.
func (feed *EventBase) GetCacheStore() interface{} {

	return feed.CacheStore

}

// The GetCachedFeedWithDateRange method takes an IProlineEventsResults interface entity, and returns a trimmed down
// version of it with only the values between the startDate and endDate included.
func GetCachedFeedWithDateRange(prolineEventsFeed interfaces.IProlineEventsResults, sportType sports.SportType, startDate string, endDate string) (interfaces.IProlineEventsResults, error) {

	expectedDateTimeFormat := EVENT_DATE_WITH_TIME_PARSE_FORMAT

	startDateParsed, err := time.Parse(expectedDateTimeFormat, startDate)
	if err != nil {
		return nil, err
	}
	endDateParsed, err := time.Parse(expectedDateTimeFormat, endDate)
	if err != nil {
		return nil, err
	}

	// we need to make sure we clone the feed struct, because otherwise the cached struct itself
	// will be chopped up when doing the TrimDateRange operation
	prolineEventsFeedClone, err := NewProlineEventsResultsFeedWithCache(nil, sportType, "proline events - "+sportType.Code, false, false,
		prolineEventsFeed, prolineEventsFeed.IsResultsFeed())
	if err != nil {
		return nil, err
	}

	prolineEventsFeedClone.TrimDateRange(startDateParsed, endDateParsed)

	return prolineEventsFeedClone, nil

}

/* END: EventBase */

// This corresponds to one of the many "prolineEvent" xml elements in the number frequency feeds
type ProlineEvent struct {
	XMLName xml.Name `xml:"prolineEvent" json:"-"`

	// The value of the listNumber xml element.
	ListNumber string `xml:"listNumber" json:"listNumber,omProlineEventpty"`

	// The value of the eventNumber xml element.
	EventNumber string `xml:"eventNumber" json:"eventNumber,omProlineEventpty"`

	// The value of the "eventDate" xml element.
	EventDate string `xml:"eventDate" json:"eventDate"`

	// the time.Date value will be calculated after ingesting the master xml feed
	EventDateValue time.Time `xml:"-" json:"-"`

	// The value of the "availableDate" xml element.
	AvailableDate string `xml:"availableDate" json:"availableDate"`

	// The event's sport code (e.g. HKY for Hockey)
	SportId string `xml:"sportId" json:"sportId,omProlineEventpty"`

	// The home team's code for the event.
	HomeTeamId string `xml:"homeTeamId" json:"homeTeamId,omProlineEventpty"`

	// The visitor's team code for the event.
	VisitorTeamId string `xml:"visitorTeamId" json:"visitorTeamId,omProlineEventpty"`

	// The value of the vPlusOdds xml element
	VPlusOdds string `xml:"vPlusOdds" json:"vPlusOdds"`

	// The value of the vOdds xml element. The odds of the visiting team winning.
	VOdds string `xml:"vOdds" json:"vOdds"`

	// The value of the tOdds xml element. The odds of a tie result.
	TOdds string `xml:"tOdds" json:"tOdds"`

	// The value of the hOdds xml element. The odds of the home team winning.
	HOdds string `xml:"hOdds" json:"hOdds"`

	// The value of the hPlusOdds xml element
	HPlusOdds string `xml:"hPlusOdds" json:"hPlusOdds"`

	// The value of the overOdds xml element.
	OverOdds string `xml:"overOdds" json:"overOdds"`

	// The value of the underOdds xml element.
	UnderOdds string `xml:"underOdds" json:"underOdds"`

	// The value of the vScore xml element. Visiting team score.
	VScore string `xml:"vScore" json:"vScore"`

	// The value of the hScore xml element. Home team score.
	HScore string `xml:"hScore" json:"hScore"`

	// The value of the prolineResult xml element.
	ProlineResult string `xml:"prolineResult" json:"prolineResult"`

	// The value of the overUnderResult xml element.
	OverUnderResult string `xml:"overUnderResult" json:"overUnderResult"`

	// The value of the "notes" xml element.
	Notes string `xml:"notes" json:"notes"`

	// The value of the "tieDisplay" xml element.
	TieDisplay string `xml:"tieDisplay" json:"tieDisplay"`

	// The value of the "linkedId" xml element.
	LinkedId string `xml:"linkedId" json:"linkedId"`

	// The value of the "cutoffDate" xml element.
	// Date and time after which betting for this event is no longer allowed.
	CutoffDate string `xml:"cutoffDate" json:"cutoffDate"`

	// The value of the "suspend" xml element.
	Suspend string `xml:"suspend" json:"suspend"`

	// The value of the "closed" xml element.
	Closed string `xml:"closed" json:"closed"`

	// The value of the "prodGroupTypeCode" xml element
	ProdGroupTypeCode string `xml:"prodGroupTypeCode" json:"prodGroupTypeCode"`

	// The value of the "homeTeamDesc" xml element
	HomeTeamDesc string `xml:"homeTeamDesc" json:"homeTeamDesc"`

	// The value of the "visitorTeamDesc" xml element
	VisitorTeamDesc string `xml:"visitorTeamDesc" json:"visitorTeamDesc"`

	// The value of the "eventOutcomeCode" xml element
	EventOutcomeCode string `xml:"eventOutcomeCode" json:"eventOutcomeCode"`

	// The value of the "eventOutcomeDesc" xml element
	EventOutcomeDesc string `xml:"eventOutcomeDesc" json:"eventOutcomeDesc"`

	// The value of the "eventOutcomeDescFr" xml element
	EventOutcomeDescFr string `xml:"eventOutcomeDescFr" json:"eventOutcomeDescFr"`

	// The value of the "prolineOutcomeDescCode" xml element
	ProlineOutcomeDescCode string `xml:"prolineOutcomeDescCode" json:"prolineOutcomeDescCode"`

	// The value of the "prolineOutcomeDesc" xml element
	ProlineOutcomeDesc string `xml:"prolineOutcomeDesc" json:"prolineOutcomeDesc"`

	// The value of the "prolineOutcomeDescFr" xml element
	ProlineOutcomeDescFr string `xml:"prolineOutcomeDescFr" json:"prolineOutcomeDescFr"`

	// The value of the "overUnderOutcomeDescCode" xml element
	OverUnderOutcomeDescCode string `xml:"overUnderOutcomeDescCode" json:"overUnderOutcomeDescCode"`

	// The value of the "overUnderOutcomeDesc" xml element
	OverUnderOutcomeDesc string `xml:"overUnderOutcomeDesc" json:"overUnderOutcomeDesc"`

	// The value of the "overUnderOutcomeDescFr" xml element
	OverUnderOutcomeDescFr string `xml:"overUnderOutcomeDescFr" json:"overUnderOutcomeDescFr"`

	// The value of the "prolineSuspended" xml element
	ProlineSuspended string `xml:"prolineSuspended" json:"prolineSuspended"`

	// The value of the "prolineClosed" xml element
	ProlineClosed string `xml:"prolineClosed" json:"prolineClosed"`

	// The value of the "overUnderSuspended" xml element
	OverUnderSuspended string `xml:"overUnderSuspended" json:"overUnderSuspended"`

	// The value of the "overUnderClosed" xml element
	OverUnderClosed string `xml:"overUnderClosed" json:"overUnderClosed"`
}

/* BEGIN: ProlineEvent Sorting Section */

// ByProlineEventDate implements sort.Interface for []ProlineEvent based on
// the EventDate field.
type ByProlineEventDate []ProlineEvent

func (a ByProlineEventDate) Len() int           { return len(a) }
func (a ByProlineEventDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByProlineEventDate) Less(i, j int) bool { return a[i].EventDate < a[j].EventDate }

// ByProlineEventCutoffDate implements sort.Interface for []ProlineEvent based on
// the CutoffDate field.
type ByProlineEventCutoffDate []ProlineEvent

func (a ByProlineEventCutoffDate) Len() int           { return len(a) }
func (a ByProlineEventCutoffDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByProlineEventCutoffDate) Less(i, j int) bool { return a[i].CutoffDate < a[j].CutoffDate }

// ByProlineEventAvailableDate implements sort.Interface for []ProlineEvent based on
// the AvailableDate field.
type ByProlineEventAvailableDate []ProlineEvent

func (a ByProlineEventAvailableDate) Len() int      { return len(a) }
func (a ByProlineEventAvailableDate) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByProlineEventAvailableDate) Less(i, j int) bool {
	return a[i].AvailableDate < a[j].AvailableDate
}

// ByProlineEventSportId implements sort.Interface for []ProlineEvent based on
// the sport id (code)
type ByProlineEventSportId []ProlineEvent

func (a ByProlineEventSportId) Len() int           { return len(a) }
func (a ByProlineEventSportId) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByProlineEventSportId) Less(i, j int) bool { return a[i].SportId < a[j].SportId }

func SortProlineEvent(sortInterface sort.Interface) {
	sort.Sort(sortInterface)
}

// Sort the ProlineEvents based on the sortByValue parameter. That one can be "freq", "name", or "num".
func SortProlineEvents(ProlineEvents []ProlineEvent, sortByValue string) {

	if sortByValue == "" {
		return
	}

	if strings.ToLower(sortByValue) == "eventdate" {
		SortProlineEvent(ByProlineEventDate(ProlineEvents))
	} else if strings.ToLower(sortByValue) == "cutoffdate" {
		SortProlineEvent(ByProlineEventCutoffDate(ProlineEvents))
	} else if strings.ToLower(sortByValue) == "availabledate" {
		SortProlineEvent(ByProlineEventAvailableDate(ProlineEvents))
	} else if strings.ToLower(sortByValue) == "sportid" {
		SortProlineEvent(ByProlineEventSportId(ProlineEvents))
	}

}

/* END: ProlineEvent Sorting Section */

/* ******************************************* */
/* BEGIN: Events Common Functions */
/* ******************************************* */

// Prepares the feed, constructing the query string and creating the
// FeedOptions struct (based on either testing or production environments).
// Event feeds have a common set of input parameters, such as the
// start and end date
func PrepareEventsFeed(feed *base.BaseProlineFeed, sportId string) (string, base.FeedOptions) {

	var requestParams string = ""

	// prepare the request parameters
	if feed.InputParams != nil {
		requestParams = feed.GetQueryParams(
			feed.KVPair("sportId", sportId),
			feed.KVPair("type", PARAM_TYPE_PROLINE_EVENTS),
			feed.KVPair("startDate", feed.InputParams.Get("startDate")),
			feed.KVPair("endDate", feed.InputParams.Get("endDate")),
		)
	}

	var feedOptions base.FeedOptions

	// if there are additional custom parameters that were passed programatically
	// make sure to append those too as well
	if feed.AdditionalParams != nil {
		if len(requestParams) > 0 {
			requestParams = requestParams + "&"
		} else {
			requestParams = "?"
		}
		requestParams = requestParams + feed.AdditionalParams.Encode()
	}

	return requestParams, feedOptions
}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

/* ******************************************* */
/* END: Frequency Common Functions   */
/* ******************************************* */
