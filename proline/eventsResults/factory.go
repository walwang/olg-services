package events

import (
	cache "github.com/pmylund/go-cache"
	"olg-services/interfaces"
	"olg-services/proline/sports"
	"strings"
)

func NewProlineEventsResultsFeed(sportType sports.SportType, jobName string, sendErrorEmails, sendErrorSms bool, isResults bool) (interfaces.IProlineEventsResults, error) {
	return NewProlineEventsResultsFeedWithCache(nil, sportType, jobName, sendErrorEmails, sendErrorSms, nil, isResults)
}

func NewProlineEventsResultsFeedWithCache(cacheStore *cache.Cache, sportType sports.SportType, jobName string,
	sendErrorEmails, sendErrorSms bool,
	feedToCloneFrom interfaces.IProlineEventsResults, isResults bool) (interfaces.IProlineEventsResults, error) {

	var feed interfaces.IProlineEventsResults

	if strings.ToLower(sportType.Code) == "timeout" {
		// the timeout feed just waits 35 seconds
		feed = &ProlineEventsTimeoutFeed{}
	} else {

		if isResults {

			// Proline Results instance

			structFeed := &ProlineResultsFeed{}

			structFeed.SetSportId(sportType.OlgSportId)

			if feedToCloneFrom != nil {

				cloneSource, ok := feedToCloneFrom.(*ProlineResultsFeed)
				if ok {
					structFeed.BaseProlineFeed = cloneSource.BaseProlineFeed
					structFeed.Events = make([]ProlineEvent, len(cloneSource.Events))
					copy(structFeed.Events, cloneSource.Events)
				}

			}

			feed = structFeed

		} else {

			// Proline Events instance

			structFeed := &ProlineEventsFeed{}

			structFeed.SetSportId(sportType.OlgSportId)

			if feedToCloneFrom != nil {

				cloneSource, ok := feedToCloneFrom.(*ProlineEventsFeed)
				if ok {
					structFeed.BaseProlineFeed = cloneSource.BaseProlineFeed
					structFeed.Events = make([]ProlineEvent, len(cloneSource.Events))
					copy(structFeed.Events, cloneSource.Events)
				}

			}

			feed = structFeed
		}

	}

	// if the conversion to ICacheable interface succeeds, set the cache store
	cacheableStruct, ok := feed.(interfaces.ICacheable)
	if ok {
		cacheableStruct.SetCacheStore(cacheStore)
	}

	// attempt to set the job name if the conversion to CronJob interface succeeds
	cronJobStructure, ok := feed.(interfaces.CronJob)
	if ok {
		cronJobStructure.SetJobName(jobName)
		cronJobStructure.SetSendEmailMessages(sendErrorEmails)
		cronJobStructure.SetSendSmsMessages(sendErrorSms)
	}

	return feed, nil

}
