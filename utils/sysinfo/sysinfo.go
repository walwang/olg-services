// package sysinfo

// import (
// 	"errors"
// 	"os"
// 	"strings"

// 	"github.com/shirou/gopsutil/cpu"
// 	"github.com/shirou/gopsutil/mem"
// 	"github.com/shirou/gopsutil/process"
// )

// type ServerStatus struct {
// 	Error       string `json:"Error,omitempty"`
// 	TotalMemory uint64
// 	UsedMemory  uint64
// 	FreeMemory  uint64
// 	CPUCores    int
// }

// type ProcessStatus struct {
// 	Error            string `json:"Error,omitempty"`
// 	ProcessId        int
// 	Name             string
// 	RSSMem           uint64
// 	VSSMem           uint64
// 	MemoryPercentage float32
// }

// func GetLocalServerStatus() (status ServerStatus, err error) {

// 	err = nil
// 	status = ServerStatus{}

// 	// Gather Memory info
// 	v, err := mem.VirtualMemory()
// 	if err != nil {
// 		if !strings.Contains(err.Error(), "not implemented yet") {
// 			newErr := errors.New("mem.VirtualMemory(): " + err.Error())
// 			status.Error = newErr.Error()
// 			return status, newErr
// 		}

// 	}

// 	// on windows, some versions of psutil return a nil object
// 	if v != nil {

// 		status.UsedMemory = v.Used
// 		status.FreeMemory = v.Free
// 		status.TotalMemory = v.Total

// 	} else {
// 		status.Error = "On this platform, server memory stats are not available"
// 	}

// 	// Gather CPU info
// 	status.CPUCores, err = cpu.CPUCounts(true)
// 	if err != nil {
// 		return
// 	}

// 	return
// }

// // Returns process status info for the current process
// func GetCurrentProcessStatus() (processStatus ProcessStatus, err error) {

// 	err = nil
// 	processStatus = ProcessStatus{}

// 	myPid := os.Getpid()

// 	currentProcess, err := process.NewProcess(int32(myPid))
// 	if err != nil {
// 		processStatus.Error = err.Error()
// 		return processStatus, err
// 	}

// 	processStatus.ProcessId = myPid

// 	processStatus.Name, err = currentProcess.Name()
// 	if err != nil {
// 		if !strings.Contains(err.Error(), "not implemented yet") {
// 			newErr := errors.New("process.Name(): " + err.Error())
// 			processStatus.Error = newErr.Error()
// 			return processStatus, newErr
// 		} else {
// 			processStatus.Name = "-- name retrieval functionality not implemented --"
// 		}

// 	}

// 	// process memory stats
// 	memInfo, err := currentProcess.MemoryInfo()
// 	if err != nil {
// 		if !strings.Contains(err.Error(), "not implemented yet") {
// 			newErr := errors.New("process.MemoryInfo(): " + err.Error())
// 			processStatus.Error = newErr.Error()
// 			return processStatus, newErr
// 		}

// 	}

// 	// on windows, some versions of psutil return a nil object
// 	if memInfo == nil {
// 		processStatus.Error = "On this platform, process memory stats are not available"
// 		return processStatus, nil
// 	}

// 	processStatus.RSSMem = memInfo.RSS
// 	processStatus.VSSMem = memInfo.VMS

// 	processStatus.MemoryPercentage, err = currentProcess.MemoryPercent()
// 	if err != nil {
// 		if !strings.Contains(err.Error(), "not implemented yet") {
// 			newErr := errors.New("process.MemoryPercent(): " + err.Error())
// 			processStatus.Error = newErr.Error()
// 			return processStatus, newErr
// 		}
// 	}

// 	return processStatus, nil

// }

// // Returns info for an external process
// func GetProcessStatus(processName string) (process ProcessStatus, err error) {

// 	// For example, for a process named crmpoc, we would run
// 	// pidof crmpoc | xargs ps -o rss,vsz | grep -v RSS
// 	// The last grep command strips the header line and leaves only the values line.
// 	// The output should be a space separated tuple of values in kb, e.g:
// 	// 5743 234657

// 	return

// }
