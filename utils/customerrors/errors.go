package customerrors

import "errors"

// Wraps an already existing error with a localized prefix
func NewCustomError(errorPrefix string, originalError error) error {
	return errors.New(errorPrefix + ": " + originalError.Error())
}

// Wraps local issues in an error format, without needing an already existing error
func NewCustomErrorLocal(errorPrefix string, localError string) error {
	return errors.New(errorPrefix + ": " + localError)
}