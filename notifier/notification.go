package notifier

import (
	"fmt"
	"olg-services/config"
	"olg-services/publicis/utils/emailer"
	"olg-services/publicis/utils/sms"
)

// custom types and structures

type Notification struct {

	// the notification type can be one of the NOTIFICATION_TYPE_ constants
	Type int

	// the notification level can be one of the NOTIFICATION_LEVEL_ constants
	Level int

	Subject      string
	Message      string
	ErrorMessage error

	// if not empty, this will be used for sms communications
	SmsMessage string
}

// Global constants

const (
	NOTIFICATION_TYPE_CONSOLE_ONLY  int = 0
	NOTIFICATION_TYPE_EMAIL         int = 1
	NOTIFICATION_TYPE_SMS           int = 2
	NOTIFICATION_TYPE_EMAIL_AND_SMS int = 3

	NOTIFICATION_LEVEL_DEBUG int = 0
	NOTIFICATION_LEVEL_INFO  int = 1
	NOTIFICATION_LEVEL_ERROR int = 2
)

func (n *Notification) Execute() (err error) {

	err = nil

	var sendEmail bool = false
	var sendSms bool = false

	if n.Type == NOTIFICATION_TYPE_EMAIL || n.Type == NOTIFICATION_TYPE_EMAIL_AND_SMS {
		sendEmail = true
	}

	if n.Type == NOTIFICATION_TYPE_SMS || n.Type == NOTIFICATION_TYPE_EMAIL_AND_SMS {
		sendSms = true
	}

	if currentNotificationConsole != nil {

		n.SendToConsole()

	}

	if sendEmail {

		// get the emails that will receive the notifications
		destinationEmails := GetEmailsByNotificationLevel(n.Level)

		// send the email
		err = emailer.SendMandrillEmail(n.Subject, n.Message, n.ErrorMessage, destinationEmails...)
		if err != nil {
			fmt.Println(" *** Could not email the info notification: ", err.Error())
		}
	}

	if sendSms {

		// prepare the message
		smsMessage := n.SmsMessage

		if smsMessage == "" {
			smsMessage = n.Message
		}

		if n.ErrorMessage != nil {
			smsMessage = smsMessage + ":" + n.ErrorMessage.Error()
		}

		// get the sms numbers that will receive the notification
		destinationSmsNumbers := GetSmsNumbersByNotificationLevel(n.Level)

		// send the notification
		err = sms.SendNexmoSms(GetFromNumber(), destinationSmsNumbers, smsMessage)
		if err != nil {
			fmt.Println(" *** Could not sms the info notification: ", err.Error())
		}

	}

	return err
}

func (n *Notification) SendToConsole() {

	// update the webconsole with the ok status
	//winningNumbersStruct.lastErrorMessage = ""
	//webconsole.Jobs.SetJobStatus(winningNumbersStruct.GetJobName(), winningNumbersStruct)

}

func GetFromNumber() string {

	var fromNumber string = "12044002436"

	return fromNumber
}

func GetEmailsByNotificationLevel(level int) []string {

	var commaSeparatedEmailsFromConfig = config.GlobalConfig.Notifier.OLGFeedsDownEmailRecipients

	var destinationEmailsDebug []string = commaSeparatedEmailsFromConfig
	var destinationEmailsInfo []string = commaSeparatedEmailsFromConfig
	var destinationEmailsError []string = commaSeparatedEmailsFromConfig

	if level == NOTIFICATION_LEVEL_DEBUG {
		return destinationEmailsDebug
	} else if level == NOTIFICATION_LEVEL_INFO {
		return destinationEmailsInfo
	} else if level == NOTIFICATION_LEVEL_ERROR {
		return destinationEmailsError
	}
	return destinationEmailsDebug

}

func GetSmsNumbersByNotificationLevel(level int) []string {

	var commaSeparatedPhoneNumbersFromConfig = config.GlobalConfig.Notifier.OLGFeedsDownPhoneTextRecipients

	var destinationSmsDebug []string = commaSeparatedPhoneNumbersFromConfig
	var destinationSmsInfo []string = commaSeparatedPhoneNumbersFromConfig
	var destinationSmsError []string = commaSeparatedPhoneNumbersFromConfig

	if level == NOTIFICATION_LEVEL_DEBUG {
		return destinationSmsDebug
	} else if level == NOTIFICATION_LEVEL_INFO {
		return destinationSmsInfo
	} else if level == NOTIFICATION_LEVEL_ERROR {
		return destinationSmsError
	}
	return destinationSmsDebug

}
