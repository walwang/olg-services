package notifier

import (
	"fmt"
	"sync"
)

// custom types and structures

type notificationServiceStatus struct {
	sync.Mutex
	Running bool
}

// Global variables

var notificationChannel chan Notification = make(chan Notification, 50)
var stopServiceChannel chan struct{}

var serviceStatus notificationServiceStatus = notificationServiceStatus{}

// notification console
var currentNotificationConsole NotificationConsole = nil

func IsRunning() bool {

	var isRunning bool = false

	serviceStatus.Lock()
	isRunning = serviceStatus.Running
	serviceStatus.Unlock()

	return isRunning

}

// Starts the notifier service
func Start() {

	// service already running
	if IsRunning() {
		return
	}

	serviceStatus.Lock()
	if serviceStatus.Running == false {
		serviceStatus.Running = true
		stopServiceChannel = make(chan struct{})
		go run()
	}
	serviceStatus.Unlock()
}

// Stops the notifier service. Returns
func Stop() {

	// service already stopped
	if IsRunning() == false {
		return
	}

	// signal the service to stop
	serviceStatus.Lock()
	if serviceStatus.Running == true {
		serviceStatus.Running = false

		// use close on the dedicated stopServiceChannel to broadcast the service stop
		// this will trigger the select statement inside the run() go routine to
		// choose the case <-stopServiceChannel variant
		close(stopServiceChannel)
	}
	serviceStatus.Unlock()

}

// Pushes a new notification to the queue. Returns true if successfully pushed and service is running,
// and false if service is not running. The notification is not guaranteed to be sent, as the
// service may be stopped in the meanwhile.
func Push(newNotification Notification) bool {

	// service stopped
	if IsRunning() == false {
		return false
	}

	// we cannot be simply blocking here, we need to use a select
	select {
	case notificationChannel <- newNotification:
		return true
	default:
		return false
	}

	return true
}

// Registers any entity that implements the NotificationConsole interface
func RegisterConsole(console NotificationConsole) {
	currentNotificationConsole = console
}

// This is the main notifier service loop routine, that should be run as a go routine
func run() {

	var quitting bool

	// in Go,  an infinite for loop is not detrimental if combined with
	// channel reading operations (the select keyword)
	for {
		select {
		case incoming := <-notificationChannel:
			if quitting {

				// this is the last notification to be executed
				fmt.Println("Last notification to be executed")

				incoming.Execute()

				// get out of the infinite for loop
				break
			}

			// if not quitting, business as usual
			incoming.Execute()

		case <-stopServiceChannel:
			fmt.Println("notification engine quit signal received")
			quitting = true
		}
	}

}

func GetLevelDescription(level int) string {

	if level == NOTIFICATION_LEVEL_DEBUG {
		return "Debug"
	}
	if level == NOTIFICATION_LEVEL_INFO {
		return "Info"
	}
	if level == NOTIFICATION_LEVEL_ERROR {
		return "Error"
	}

	return "Unknown level"
}
