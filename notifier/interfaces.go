package notifier

type NotificationConsole interface {
	SetJobStatus(jobName string, jobStatus string, jobLastProcessed string, jobLastOkProcessed string, jobCurrentError string)
}
