/* The notifier package handles sending emails and sms messages in case of errors or resolutions.
The service is designed to be resilient and avoid panicking under any circumstances. */
package notifier
